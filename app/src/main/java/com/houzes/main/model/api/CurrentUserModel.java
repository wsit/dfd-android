package com.houzes.main.model.api;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;

import java.util.ArrayList;
import java.util.List;

public class CurrentUserModel {

    @SerializedName(("id"))
    private int id;
    @SerializedName(("first_name"))
    private String firstName;
    @SerializedName(("last_name"))
    private String lastName;
    @SerializedName(("email"))
    private String email;
    @SerializedName(("photo"))
    private String photo;
    @SerializedName("photo_thumb")
    private String photoThumb;
    @SerializedName(("is_active"))
    private boolean isActive;
    @SerializedName(("last_login"))
    private Object lastLogin;
    @SerializedName(("created_at"))
    private String createdAt;
    @SerializedName(("invited_by"))
    private Integer invitedBy;
    @SerializedName(("is_admin"))
    private boolean isAdmin;
    @SerializedName(("password"))
    private String password;
    @SerializedName(("updatedAt"))
    private String updated_at;
    @SerializedName(("phone_number"))
    private String phoneNumber;
    @SerializedName(("upgrade_info"))
    private UpgradeInfoModel upgradeInfo = new UpgradeInfoModel();

    public int getId() {
        return id;
    }

    public String getIdAsString() {
        return id + "";
    }

    public String getFullName() {
        try {
            List<String> flName = new ArrayList<>();
            if (firstName != null && firstName.trim().length() > 0) {
                flName.add(firstName);
            }
            if (lastName != null && lastName.trim().length() > 0) {
                flName.add(lastName);
            }
            return TextUtils.join(" ", flName);
        } catch (Exception ex) {
            return "";
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto() {
        return photo;
    }

    public boolean isActive() {
        return isActive;
    }

    public Object getLastLogin() {
        return lastLogin;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Integer getInvitedBy() {
        return invitedBy;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getPassword() {
        return password;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setUpgradeInfo(UpgradeInfoModel upgradeInfo) {
        this.upgradeInfo = upgradeInfo;
    }

    public UpgradeInfoModel getUpgradeInfo() {
        return upgradeInfo;
    }

    public String getPhotoThumb() {
        return photoThumb;
    }

    public PlanType getPlanType() {
        if (upgradeInfo.getPlan().getPlanName().toLowerCase().equals("solo")) {
            return PlanType.SOLO;
        } else if (upgradeInfo.getPlan().getPlanName().toLowerCase().equals("team")) {
            return PlanType.TEAM;
        } else {
            return PlanType.FREE;
        }
    }

    public boolean isTeamAdmin() {
        try {
            if (getPlanType().equals(PlanType.TEAM)) {
                if (getInvitedBy() == null) {
                    return true;
                } else if (getInvitedBy() <= 0) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }
}
