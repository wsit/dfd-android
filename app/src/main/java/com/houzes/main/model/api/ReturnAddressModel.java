package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class ReturnAddressModel {
    @SerializedName("id")
    private int id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("website")
    private String website;
    @SerializedName("phone_no")
    private String phoneNo;
    @SerializedName("address_street")
    private String addressStreet;
    @SerializedName("address_city")
    private String addressCity;
    @SerializedName("address_state")
    private String addressState;
    @SerializedName("address_zip")
    private String addressZip;
    @SerializedName("user")
    private int user;
    @SerializedName("logo")
    private String logo;
    @SerializedName("cover_photo")
    private String coverPhoto;
    @SerializedName("agent_license")
    private String agentLicense;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getCompanyName() {
        return companyName != null ? companyName : "";
    }

    public String getWebsite() {
        return website != null ? website : "";
    }

    public String getPhoneNo() {
        return phoneNo != null ? phoneNo : "";
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public int getUser() {
        return user;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getAgentLicense() {
        return agentLicense != null ? agentLicense : "";
    }

    public void setAgentLicense(String agentLicense) {
        this.agentLicense = agentLicense;
    }
}
