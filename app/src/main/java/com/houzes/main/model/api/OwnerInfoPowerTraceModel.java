package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class OwnerInfoPowerTraceModel {
    @SerializedName("payment")
    public Boolean payment;
    @SerializedName("upgrade_info")
    public UpgradeInfoModel upgradeInfo;
}
