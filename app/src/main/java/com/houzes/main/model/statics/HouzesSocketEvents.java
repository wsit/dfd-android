package com.houzes.main.model.statics;

public class HouzesSocketEvents {
    public static final String SOCKET_CONNECTED = "connected";
    public static final String LOCATION_UPDATE = "location::update";
    public static final String LOCATION_UPDATE_SUCCESS = "location::update.success";
    public static final String LOCATION_UPDATE_ERROR = "location::update.error";
    public static final String LOCATION_SHARE = "location::share";
    public static final String LOCATION_RECEIVE = "location::receive";
    public static final String LOCATION_SHARE_ERROR = "location::share.error";
}
