package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class NeighborInfoModel {
    private int id;
    @SerializedName("ownership_info")
    private OwnerInfoModel ownershipInfo;
    @SerializedName("power_trace")
    private PowerTraceModel powerTrace;
    @SerializedName("neighbor_address")
    private String neighborAddress;
    private String status;
    @SerializedName("owner_status")
    private String ownerStatus;
    @SerializedName("power_trace_status")
    private String powerTraceStatus;
    private double latitude;
    private double longitude;

    public int getId() {
        return id;
    }

    public OwnerInfoModel getOwnershipInfo() {
        return ownershipInfo;
    }

    public PowerTraceModel getPowerTrace() {
        return powerTrace;
    }

    public String getNeighborAddress() {
        return neighborAddress;
    }

    public String getStatus() {
        return status;
    }

    public String getOwnerStatus() {
        return ownerStatus;
    }

    public String getPowerTraceStatus() {
        return powerTraceStatus;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
