package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class MailerWizardTempleteModel {
    @SerializedName("item_id")
    private int id;
    private String code;
    private String caption;
    private String description;
    private String url;
    @SerializedName("thumbUrl")
    private String thumbUrl;

    public int getId() {
        return id;
    }

    public String getCaption() {
        return caption;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }
}
