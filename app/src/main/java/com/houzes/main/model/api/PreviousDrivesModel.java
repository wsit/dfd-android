package com.houzes.main.model.api;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class PreviousDrivesModel {
    private String polylines;

    public List<LatLng> getLatLng() {
        try {
            return new Gson().fromJson(this.polylines, new TypeToken<List<LatLng>>() {
            }.getType());
        } catch (Exception ex) {
            return null;
        }
    }
}
