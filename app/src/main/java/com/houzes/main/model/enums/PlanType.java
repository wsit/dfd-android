package com.houzes.main.model.enums;

public enum PlanType {
    FREE, SOLO, TEAM
}