package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class MailWizardSubscriptionsModel {
    private int id;
    @SerializedName("type_name")
    private String typeName;
    @SerializedName("days_interval")
    private String daysInterval;

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDaysInterval() {
        return daysInterval;
    }

}
