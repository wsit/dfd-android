package com.houzes.main.model.api;

public class HistoryPolyLine {
    public double latitude;
    public double longitude;
    public int position;

    public HistoryPolyLine(double _latitude, double _longitude, int _position) {
        latitude = _latitude;
        longitude = _longitude;
        position = _position;
    }
}
