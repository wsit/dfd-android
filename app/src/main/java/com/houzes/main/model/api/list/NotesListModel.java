package com.houzes.main.model.api.list;

import com.houzes.main.model.api.NotesModel;

import java.util.List;

public class NotesListModel {
    public int count;
    public String next;
    public String previous;
    public List<NotesModel> results;
}
