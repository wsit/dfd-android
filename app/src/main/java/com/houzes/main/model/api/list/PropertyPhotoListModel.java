package com.houzes.main.model.api.list;

import com.houzes.main.model.api.PropertyPhotoModel;

public class PropertyPhotoListModel {
    public int count;
    public String next;
    public String previous;
    public PropertyPhotoModel[] results;
}
