package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class AssociatedPhoneModel {
    @SerializedName("_name")
    String name;
    @SerializedName("_telephone_number")
    String telephoneNumber;

    public String getName() {
        return name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }
}