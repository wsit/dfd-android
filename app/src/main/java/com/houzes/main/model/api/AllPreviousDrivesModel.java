package com.houzes.main.model.api;

public class AllPreviousDrivesModel {

    private PreviousDrivesModel[] histories;
    private PropertyLatLngModel[] properties=new PropertyLatLngModel[]{};

    public PreviousDrivesModel[] getHistories() {
        return histories;
    }


    public PropertyLatLngModel[] getProperties() {
        return properties;
    }
}
