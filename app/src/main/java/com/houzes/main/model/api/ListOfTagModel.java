package com.houzes.main.model.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.kaede.tagview.Tag;

public class ListOfTagModel {
    public int id;
    public String name;
    public int user;
    public String created_at;
    public String updated_at;
    private Map<String, String> color;

    public String getColorCode() {
        if (color != null)
            return color.get("color_code");
        return "#ff0022";
    }

    public static String getColorCode(ListOfTagModel[] listOfTagModels, int _id) {

        if (listOfTagModels != null && _id > 0) {
            for (ListOfTagModel listOfTagModel : listOfTagModels) {
                if (listOfTagModel.id == _id) {
                    return listOfTagModel.getColorCode();
                }
            }
        }
        return "#000000";
    }

    public static String getColorCode(ListOfTagModel[] listOfTagModels, ListOfTagModel _listOfTagModel) {

        if (listOfTagModels != null && _listOfTagModel != null && _listOfTagModel.id > 0) {
            for (ListOfTagModel listOfTagModel : listOfTagModels) {
                if (listOfTagModel.id == _listOfTagModel.id) {
                    return listOfTagModel.getColorCode();
                }
            }
        }
        return "#ff0022";
    }

    public String getColorName() {
        if (color != null)
            return color.get("color_name");
        return "red";
    }

    public static List<Tag> getTags(ListOfTagModel[] listOfTagModels) {
        List<Tag> tags = new ArrayList<>();
        for (ListOfTagModel t1 : listOfTagModels) {
            tags.add(new Tag(t1.name, t1.getColorCode()));
        }
        return tags;
    }


}
