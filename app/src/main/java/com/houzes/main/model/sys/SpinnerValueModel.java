package com.houzes.main.model.sys;

public class SpinnerValueModel implements Comparable<SpinnerValueModel> {
    private String name;
    private Object tag;

    public SpinnerValueModel(String name, Object tag) {
        this.name = name;
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    @Override
    public int compareTo(SpinnerValueModel spinnerValueModel) {
        return this.name.compareTo(spinnerValueModel.name);
    }
}
