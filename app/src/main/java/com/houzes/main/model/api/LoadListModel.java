package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoadListModel {
    @SerializedName("member_id")
    private int memberId;
    @SerializedName("list")
    private List<UserListModel> list;


    public int getMemberId() {
        return memberId;
    }

    public List<UserListModel> getList() {
        return list;
    }
}
