package com.houzes.main.model.enums;

public enum ClickValues {
    ADD_TO_LIST, HIDE_HOME_MARKER_INFO
}
