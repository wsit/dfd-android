package com.houzes.main.model.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.houzes.main.R;
import com.houzes.main.model.enums.PlanType;

public class SubscriptionPlanModel {
    private int id;
    @SerializedName("plan_name")
    private String planName;
    @SerializedName("plan_cost")
    private float planCost;
    @SerializedName("plan_coin")
    private float planCoin;
    private String description;

    public int getId() {
        return id;
    }

    public String getPlanName() {
        return planName;
    }

    public float getPlanCost() {
        return planCost;
    }

    public float getPlanCoin() {
        return planCoin;
    }

    public PlanType getPlanType() {
        if (planName.toLowerCase().equals("team")) {
            return PlanType.TEAM;
        } else if (planName.toLowerCase().equals("solo")) {
            return PlanType.SOLO;
        } else {
            return PlanType.FREE;
        }
    }

    public int getColorCode() {
        if (getPlanType().equals(PlanType.TEAM)) {
            return R.color.teamType;
        } else if (getPlanType().equals(PlanType.SOLO)) {
            return R.color.soloType;
        } else {
            return R.color.freeType;
        }
    }

    public int getCardImage() {
        if (getPlanType().equals(PlanType.TEAM)) {
            return R.drawable.bg_card_team;
        } else if (getPlanType().equals(PlanType.SOLO)) {
            return R.drawable.bg_card_solo;
        } else {
            return R.drawable.bg_card_free;
        }
    }

    public int getPlanDescription() {
        if (getPlanType().equals(PlanType.TEAM)) {
            return R.string.team_account_access;
        } else if (getPlanType().equals(PlanType.SOLO)) {
            return R.string.team_account_access;
        } else {
            return R.string.free_account_access;
        }
    }

    public String[] getPlanText(Context context) {
        try {
            String[] json = new Gson().fromJson(description, String[].class);
            if (json != null && json.length > 1) {
                return json;
            }
        } catch (Exception ex) {
        }
        return new String[]{"Our most serious, fastest-growing clients start here", context.getString(getPlanDescription())};
    }

}