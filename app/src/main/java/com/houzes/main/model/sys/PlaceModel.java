package com.houzes.main.model.sys;

import com.google.android.gms.maps.model.LatLng;

public class PlaceModel {
    public LatLng latLng;
    public String placeId;
    public String address;

    public PlaceModel(String _placeId,LatLng _latLng) {
        latLng = _latLng;
        placeId = _placeId;
    }

    public PlaceModel( String _placeId, String _address,LatLng _latLng) {
        latLng = _latLng;
        placeId = _placeId;
        address = _address;
    }
}
