package com.houzes.main.model.api;

public class SubscriptionResponseModel {
    public int id;
    public float coin;
    public CurrentUserModel user;
    public SubscriptionPlanModel plan;
}