package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class NotesModel {
    public int id;
    public String title;
    public String notes;
    @SerializedName("created_at")
    public Date createdAt;
    @SerializedName("updated_at")
    public Date updatedAt;
    public int user;
    public int property;
}
