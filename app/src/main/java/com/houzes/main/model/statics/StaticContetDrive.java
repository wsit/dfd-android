package com.houzes.main.model.statics;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.houzes.main.action.helper.NotificationsHelper;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.socket.HouzesSocketHandler;
import com.houzes.main.action.socket.socket.SocketDrivingMarker;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.api.PropertyModel;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaticContetDrive {

    public static final float CURRENT_ANGEL_STATIC = 0f;
    public static final double CURRENT_LAT_STATIC = 40.712420;
    public static final double CURRENT_LNG_STATIC = -74.004959;

    public static float CURRENT_ANGEL = CURRENT_ANGEL_STATIC;
    public static double CURRENT_LAT = CURRENT_LAT_STATIC;
    public static double CURRENT_LON = CURRENT_LNG_STATIC;
    public static int CURRENT_LAT_LON_INDEX = 0;
    public static int MY_HISTORY_ID = 0;
    public static boolean IS_PUBLIC_SHARED_ENABEL = false;
    public static boolean IS_PUBLIC_SHARED_ENABEL_RESUME = false;
    public static LatLng OLD_LAT_LNG = new LatLng(CURRENT_LAT_STATIC, CURRENT_LNG_STATIC);
    public static LatLng CURRENT_LAT_LNG = new LatLng(CURRENT_LAT_STATIC, CURRENT_LNG_STATIC);
    public static SocketDriveType D_TYPE = SocketDriveType.PUBLIC;
    public static Map<Integer, SocketDrivingMarker> CONNECTED_SOCKET_USER = new HashMap<>();
    public static Map<Integer, PropertyModel> RESULT_PROPERTY = new HashMap<>();
    public static Map<Integer, List<LatLng>> POLYLINE_MODEL_LIST = new HashMap<>();
    public static int SELECTED_CURRENT_DRIVE_LIST_ID;
    public static String SELECTED_CURRENT_DRIVE_LIST_NAME = "";
    public static double TOTAL_DRIVE = 0;

    public static int getTotalAddProperty(Context context) {
        if (RESULT_PROPERTY != null) {
            return RESULT_PROPERTY.size();
        }
        return 0;
    }

    public static String getTotalDuration(Context context, long value) {
        String ret = "00:00:00";
        if (value == 0) {
            SharedPreferenceUtil.setValue("TOTAL_DURATIONS", "0", context);
        } else if (value < 0) {
            String dd = SharedPreferenceUtil.getValue("TOTAL_DURATIONS", context);
            long d1 = Long.valueOf(dd != null ? dd : "0");
            if (d1 <= 0) {
                d1 = new Date().getTime();
                getTotalDuration(context, d1);
            }
            long diff = new Date().getTime() - d1;

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;

            ret = "0" + diffHours + ":" + (diffMinutes > 9 ? diffMinutes : ("0" + diffMinutes)) + ":" + (diffSeconds > 9 ? diffSeconds : ("0" + diffSeconds));
        } else {
            SharedPreferenceUtil.setValue("TOTAL_DURATIONS", value + "", context);
        }
        return ret;
    }

    public static void driveStartCalculation(Context context, boolean isStart) {
        try {
            POLYLINE_MODEL_LIST = new HashMap<>();
            RESULT_PROPERTY = new HashMap<>();
            SELECTED_CURRENT_DRIVE_LIST_ID = 0;
            SELECTED_CURRENT_DRIVE_LIST_NAME = "";
            SharedPreferenceUtil.setInt(SharedPreferenceUtil.DB_HISTORY_ID, 0, context);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL, "false", context);
            StaticContent.getTotalDuration(context, new Date().getTime());
            if (isStart) {
                StaticContent.IS_PUBLIC_SHARED_ENABEL = true;
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL, "true", context);
                SharedPreferenceUtil.setInt(SharedPreferenceUtil.DB_HISTORY_ID, StaticContent.MY_HISTORY_ID, context);
            }
            PolylineModel.deleteAll(PolylineModel.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void driveStopCalculation(Context context, boolean isStop) {
        try {
            new NotificationsHelper(context).driveStop();
            HouzesSocketHandler.stopDriving();
            if (isStop) {
                StaticContent.IS_PUBLIC_SHARED_ENABEL = false;
                StaticContent.IS_PUBLIC_SHARED_ENABEL_RESUME = false;
                POLYLINE_MODEL_LIST = new HashMap<>();
                RESULT_PROPERTY = new HashMap<>();
                SELECTED_CURRENT_DRIVE_LIST_ID = 0;
                SELECTED_CURRENT_DRIVE_LIST_NAME = "";
                MY_HISTORY_ID = 0;
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL, "false", context);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL_RESUME, "false", context);
                SharedPreferenceUtil.setInt(SharedPreferenceUtil.DB_HISTORY_ID, 0, context);
                PolylineModel.deleteAll(PolylineModel.class);
                SharedPreferenceUtil.setDriveType(context, SocketDriveType.NONE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            POLYLINE_MODEL_LIST = new HashMap<>();
            RESULT_PROPERTY = new HashMap<>();
        }
    }
}
