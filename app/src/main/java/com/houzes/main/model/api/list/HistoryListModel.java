package com.houzes.main.model.api.list;

import com.houzes.main.model.api.HistoryModel;

import java.util.List;

public class HistoryListModel {
    public int count;
    public String next;
    public String previous;
    public List<HistoryModel> results;
}
