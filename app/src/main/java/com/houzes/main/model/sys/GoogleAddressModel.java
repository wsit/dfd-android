package com.houzes.main.model.sys;

import android.location.Address;
import android.text.TextUtils;

public class GoogleAddressModel {

    private String addres;
    private String street;
    private String city;
    private String state;
    private String zip;
    private double latitude;
    private double longitude;

    public GoogleAddressModel() {
    }

    public GoogleAddressModel(Address address) {
        try {
            addres = address.getAddressLine(0);
        } catch (Exception ex) {
        } finally {
            //street = ((address.getSubThoroughfare() != null ? address.getSubThoroughfare() : "") + " " + (address.getThoroughfare() != null ? address.getThoroughfare() : "")).trim();
            street = address.getAddressLine(0).split(",")[0].trim();
            city = address.getLocality();
            state = address.getAdminArea();
            zip = address.getPostalCode();
            latitude = address.getLatitude();
            longitude = address.getLongitude();
        }
    }

    public String getAddress() {
        return addres != null ? addres : "";
    }

    public String getPerfectAddress() {
        String[] stra = new String[]{getStreet(), getCity(), getState(), getZip()};
        return TextUtils.join(" ", stra);
    }

    public String getStreet() {
        return street != null ? street : "";
    }

    public String getCity() {
        return city != null ? city : "";
    }

    public String getState() {
        return state != null ? state : "";
    }

    public String getZip() {
        return zip != null ? zip : "";
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
