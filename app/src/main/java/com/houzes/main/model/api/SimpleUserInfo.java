package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class SimpleUserInfo {
    @SerializedName(("id"))
    private int id;
    @SerializedName(("first_name"))
    private String firstName;
    @SerializedName(("last_name"))
    private String lastName;
    @SerializedName(("email"))
    private String email;
    @SerializedName("photo_thumb")
    private String photoThumb;
    @SerializedName(("phone_number"))
    private String phoneNumber;

    public int getId() {
        return id;
    }

    public String getFullName() {
        return (firstName != null ? firstName.concat(" ") : "").concat(lastName != null ? lastName : "");
    }


    public String getEmail() {
        return email;
    }

    public String getPhotoThumb() {
        return photoThumb;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
