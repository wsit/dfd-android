package com.houzes.main.model.enums;

import android.text.TextUtils;

import com.houzes.main.R.drawable;
import com.houzes.main.R.string;

import java.util.regex.Pattern;

public enum CardType {
    VISA("^4\\d*", drawable.bt_ic_visa, 13, 16, 3, string.bt_cvv, (String) null),
    MASTERCARD("^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[0-1]|2720)\\d*", drawable.bt_ic_mastercard, 16, 16, 3, string.bt_cvc, (String) null),
    DISCOVER("^(6011|65|64[4-9]|622)\\d*", drawable.bt_ic_discover, 16, 16, 3, string.bt_cid, (String) null),
    AMEX("^3[47]\\d*", drawable.bt_ic_amex, 15, 16, 4, string.bt_cid, (String) null),
    DINERS_CLUB("^(36|38|30[0-5])\\d*", drawable.bt_ic_diners_club, 14, 14, 3, string.bt_cvv, (String) null),
    JCB("^35\\d*", drawable.bt_ic_jcb, 15, 16, 3, string.bt_cvv, (String) null),
    MAESTRO("^(5018|5020|5038|5[6-9]|6020|6304|6703|6759|676[1-3])\\d*", drawable.bt_ic_maestro, 12, 19, 3, string.bt_cvc, "^6\\d*"),
    UNIONPAY("^62\\d*", drawable.bt_ic_unionpay, 15, 19, 3, string.bt_cvn, (String) null),
    HIPER("^637(095|568|599|609|612)\\d*", drawable.bt_ic_hiper, 16, 16, 3, string.bt_cvc, (String) null),
    HIPERCARD("^606282\\d*", drawable.bt_ic_hipercard, 15, 16, 3, string.bt_cvc, (String) null),
    UNKNOWN("\\d+", drawable.ic_cd_card, 12, 19, 3, string.bt_cvv, (String) null),
    EMPTY("^$", drawable.ic_cd_card, 12, 19, 3, string.bt_cvv, (String) null);

    private static final int[] AMEX_SPACE_INDICES = new int[]{4, 10};
    private static final int[] DEFAULT_SPACE_INDICES = new int[]{4, 8, 12};
    private final Pattern mPattern;
    private final Pattern mRelaxedPrefixPattern;
    private final int mFrontResource;
    private final int mMinCardLength;
    private final int mMaxCardLength;
    private final int mSecurityCodeLength;
    private final int mSecurityCodeName;

    private CardType(String regex, int frontResource, int minCardLength, int maxCardLength, int securityCodeLength, int securityCodeName, String relaxedPrefixPattern) {
        this.mPattern = Pattern.compile(regex);
        this.mRelaxedPrefixPattern = relaxedPrefixPattern == null ? null : Pattern.compile(relaxedPrefixPattern);
        this.mFrontResource = frontResource;
        this.mMinCardLength = minCardLength;
        this.mMaxCardLength = maxCardLength;
        this.mSecurityCodeLength = securityCodeLength;
        this.mSecurityCodeName = securityCodeName;
    }

    public static CardType forCardNumber(String cardNumber) {
        if (cardNumber.length() == 0) {
            return EMPTY;
        } else {
            CardType patternMatch = forCardNumberPattern(cardNumber);
            if (patternMatch != EMPTY && patternMatch != UNKNOWN) {
                return patternMatch;
            } else {
                CardType relaxedPrefixPatternMatch = forCardNumberRelaxedPrefixPattern(cardNumber);
                if (relaxedPrefixPatternMatch != EMPTY && relaxedPrefixPatternMatch != UNKNOWN) {
                    return relaxedPrefixPatternMatch;
                } else {
                    return !cardNumber.isEmpty() ? UNKNOWN : EMPTY;
                }
            }
        }
    }

    private static CardType forCardNumberPattern(String cardNumber) {
        CardType[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            CardType cardType = var1[var3];
            if (cardType.getPattern().matcher(cardNumber).matches()) {
                return cardType;
            }
        }

        return EMPTY;
    }

    private static CardType forCardNumberRelaxedPrefixPattern(String cardNumber) {
        CardType[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            CardType cardTypeRelaxed = var1[var3];
            if (cardTypeRelaxed.getRelaxedPrefixPattern() != null && cardTypeRelaxed.getRelaxedPrefixPattern().matcher(cardNumber).matches()) {
                return cardTypeRelaxed;
            }
        }

        return EMPTY;
    }

    public Pattern getPattern() {
        return this.mPattern;
    }

    public Pattern getRelaxedPrefixPattern() {
        return this.mRelaxedPrefixPattern;
    }

    public int getFrontResource() {
        return this.mFrontResource;
    }

    public int getSecurityCodeName() {
        return this.mSecurityCodeName;
    }

    public int getSecurityCodeLength() {
        return this.mSecurityCodeLength;
    }

    public int getMinCardLength() {
        return this.mMinCardLength;
    }

    public int getMaxCardLength() {
        return this.mMaxCardLength;
    }

    public static String formatCard(String cardNumber) {
        try {
            if (cardNumber == null) return null;
            char delimiter = '-';
            return cardNumber.replaceAll(".{4}(?!$)", "$0" + delimiter);
        } catch (Exception ex) {
            return cardNumber;
        }
    }

    public int[] getSpaceIndices() {
        return this == AMEX ? AMEX_SPACE_INDICES : DEFAULT_SPACE_INDICES;
    }

    public static boolean isLuhnValid(String cardNumber) {
        String reversed = (new StringBuffer(cardNumber)).reverse().toString();
        int len = reversed.length();
        int oddSum = 0;
        int evenSum = 0;

        for (int i = 0; i < len; ++i) {
            char c = reversed.charAt(i);
            if (!Character.isDigit(c)) {
                throw new IllegalArgumentException(String.format("Not a digit: '%s'", c));
            }

            int digit = Character.digit(c, 10);
            if (i % 2 == 0) {
                oddSum += digit;
            } else {
                evenSum += digit / 5 + 2 * digit % 10;
            }
        }

        return (oddSum + evenSum) % 10 == 0;
    }

    public boolean validate(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return false;
        } else if (!TextUtils.isDigitsOnly(cardNumber)) {
            return false;
        } else {
            int numberLength = cardNumber.length();
            if (numberLength >= this.mMinCardLength && numberLength <= this.mMaxCardLength) {
                return !this.mPattern.matcher(cardNumber).matches() && this.mRelaxedPrefixPattern != null && !this.mRelaxedPrefixPattern.matcher(cardNumber).matches() ? false : isLuhnValid(cardNumber);
            } else {
                return false;
            }
        }
    }
}
