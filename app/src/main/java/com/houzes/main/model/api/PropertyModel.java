package com.houzes.main.model.api;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PropertyModel {
    private int id;
    private String street;
    private String city;
    private String state;
    private String zip;
    @SerializedName("cad_acct")
    private String cadAcct;
    @SerializedName("gma_tag")
    private int gmaTag;
    @SerializedName("photo_count")
    private int photoCount;
    @SerializedName("note_count")
    private int noteCount;
    private double latitude;
    private double longitude;
    @SerializedName("owner_info")
    private List<OwnerInfoModel> ownerInfo;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_list")
    private int userList;
    @SerializedName("power_trace_request_id")
    private int powerTraceRequestId;
    @SerializedName("property_tags")
    private ListOfTagModel[] tags;
    private String property;
    private PropertyPhotoModel[] photos;
    private NotesModel[] notes;

    public int getId() {
        return id;
    }

    public String getIdAsStr() {
        return id + "";
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getPropertyAddress() {
        List<String> l = new ArrayList<>();
        l.add(street != null ? street : "");
        l.add(city != null ? city : "");
        l.add(state != null ? state : "");
        l.add(zip != null ? zip : "");
        return TextUtils.join(" ", l);
    }

    public String getCadAcct() {
        return cadAcct;
    }

    public void setCadAcct(String cadAcct) {
        this.cadAcct = cadAcct;
    }

    public int getGmaTag() {
        return gmaTag;
    }

    public void setGmaTag(int gmaTag) {
        this.gmaTag = gmaTag;
    }

    public List<OwnerInfoModel> getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(List<OwnerInfoModel> ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getUserList() {
        return userList;
    }

    public void setUserList(int userList) {
        this.userList = userList;
    }

    public ListOfTagModel[] getTags() {
        return tags;
    }

    public void setTags(ListOfTagModel[] tags) {
        this.tags = tags;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getPhotoCount() {
        return photoCount > 0 ? photoCount : photos != null ? photos.length : 0;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public int getNoteCount() {
        return noteCount > 0 ? noteCount : notes != null ? notes.length : 0;
    }

    public void setNoteCount(int noteCount) {
        this.noteCount = noteCount;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public int getPowerTraceRequestId() {
        return powerTraceRequestId;
    }

    public void setPowerTraceRequestId(int powerTraceRequestId) {
        this.powerTraceRequestId = powerTraceRequestId;
    }

    public String getJson() {
        try {
            return new Gson().toJson(this);
        } catch (Exception ex) {
            return null;
        }
    }
}
