package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class PaymentPlanModel {
    private int id;
    @SerializedName("plan_name")
    private String planName;
    @SerializedName("plan_cost")
    private float planCost;
    @SerializedName("plan_coin")
    private float planCoin;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public float getPlanCost() {
        return planCost;
    }

    public void setPlanCost(float planCost) {
        this.planCost = planCost;
    }

    public float getPlanCoin() {
        return planCoin;
    }

    public void setPlanCoin(float planCoin) {
        this.planCoin = planCoin;
    }
}
