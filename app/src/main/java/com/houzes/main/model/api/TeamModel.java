package com.houzes.main.model.api;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.houzes.main.model.statics.StaticContent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TeamModel implements Serializable {
    private long id;
    private String first_name;
    private String last_name;
    private String email;
    private String phone_number;
    private long invited_by;
    private String photo;
    private String photo_thumb;
    private String created_at;
    private String updated_at;
    private boolean is_active;
    private boolean is_admin;
    private boolean isUser = false;


    public long getId() {
        return id;
    }

    public String getIdAsString() {
        return id + "";
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public long getInvitedBy() {
        return invited_by;
    }

    public boolean isActive() {
        return is_active;
    }

    public boolean isAdmin() {
        return is_admin;
    }

    public boolean isIsUser() {
        return isUser;
    }

    public void setIsUser(boolean isUser) {
        this.isUser = isUser;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotoTheme() {
        return photo_thumb;
    }

    public static TeamModel[] getTeamModels(JsonObject jsonObject, String json) {
        try {
            if (json != null) {
                jsonObject = new Gson().fromJson(json, JsonObject.class);
            }
            if (jsonObject != null) {
                TeamModel[] teamModels = new Gson().fromJson(jsonObject.getAsJsonArray("users"), TeamModel[].class);
                return teamModels != null ? teamModels : new TeamModel[]{};
            }
            return new TeamModel[]{};
        } catch (Exception ex) {
            return new TeamModel[]{};
        }
    }

    public static TeamModel[] removeMe(TeamModel[] teamModels) {
        if (teamModels != null) {
            List<TeamModel> team = new ArrayList<>();
            for (TeamModel t : teamModels) {
                if (t.getId() != StaticContent.CURRENT_USER_MODEL.getId()) {
                    team.add(t);
                }
            }
            return team.toArray(new TeamModel[]{});
        } else {
            return new TeamModel[]{};
        }
    }
}
