package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PowerTraceModel {
    @SerializedName("person_id")
    private int personId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    //@SerializedName("associated_phone")
    private AssociatedPhoneModel[] associatedPhone;
    private String email;
    private String address;
    @SerializedName("verified_cellphones")
    private String verifiedCellphones;
    private String social;
    @SerializedName("trace_name")
    private String traceName;
    private String cellphones;
    private String voip;
    private String landline;
    @SerializedName("source_id")
    private int sourceId;
    private String status;
    @SerializedName("verified_landline")
    private String verifiedLandline;

    public int getPersonId() {
        return personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public AssociatedPhoneModel[] getAssociatedPhone() {
        return associatedPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getVerifiedCellphones() {
        return verifiedCellphones;
    }

    public String getSocial() {
        return social;
    }

    public String getTraceName() {
        return traceName;
    }

    public String getCellphones() {
        return cellphones;
    }

    public String getVoip() {
        return voip;
    }

    public String getLandline() {
        return landline;
    }

    public int getSourceId() {
        return sourceId;
    }

    public String getStatus() {
        return status;
    }

    public String getVerifiedLandline() {
        return verifiedLandline;
    }

    public String getTwitter() {
        if (social != null)
            for (String s : social.split(","))
                if (social.contains("twitter.com"))
                    return s;
        return null;
    }

    public String getLinkedIn() {
        if (social != null)
            for (String s : social.split(","))
                if (social.contains("linkedin.com"))
                    return s;
        return null;
    }

    public String getGoogle() {
        if (social != null)
            for (String s : social.split(","))
                if (social.contains("google.com"))
                    return s;
        return null;
    }

    public String getFacebook() {
        if (social != null)
            for (String s : social.split(","))
                if (social.contains("facebook.com"))
                    return s;
        return null;
    }


    public List<String> getAllPhoneNumber() {
        List<String> l = new ArrayList<>();
        if (verifiedCellphones != null) {
            for (String s : verifiedCellphones.split(",")) {
                s = phoneParse(s);
                if (s != null && !l.contains(s.trim())) {
                    l.add(s.trim());
                }
            }
        }
        if (cellphones != null) {
            for (String s : cellphones.split(",")) {
                s = phoneParse(s);
                if (s != null && !l.contains(s.trim())) {
                    l.add(s.trim());
                }
            }
        }
        if (verifiedLandline != null) {
            for (String s : verifiedLandline.split(",")) {
                s = phoneParse(s);
                if (s != null && !l.contains(s.trim())) {
                    l.add(s.trim());
                }
            }
        }
        if (landline != null) {
            for (String s : landline.split(",")) {
                s = phoneParse(s);
                if (s != null && !l.contains(s.trim())) {
                    l.add(s);
                }
            }
        }

        if (associatedPhone != null && associatedPhone.length > 0) {
            for (AssociatedPhoneModel associatedPhoneModel : associatedPhone) {
                String s = phoneParse(associatedPhoneModel.telephoneNumber);
                if (s != null && !l.contains(s.trim())) {
                    l.add(s.trim());
                }
            }
        }

        return l;
    }

    private String phoneParse(String str) {
        if (str != null && str.length() > 5) {
            return str;
        }
        return null;
    }
}

