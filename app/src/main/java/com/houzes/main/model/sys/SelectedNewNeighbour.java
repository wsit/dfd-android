package com.houzes.main.model.sys;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class SelectedNewNeighbour {
    @SerializedName("property_id")
    public long id;
    public double latitude;
    public double longitude;
    public String address;
    public String street;
    public String city;
    public String state;
    public String zip;

    public String getPerfectAddress() {
        try {
            return TextUtils.join(" ", new String[]{street, city, state, zip});
        } catch (Exception ex) {
            return "";
        }
    }
}
