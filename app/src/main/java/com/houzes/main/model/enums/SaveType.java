package com.houzes.main.model.enums;

public enum SaveType {
    SAVE, SHOW, EMPTY
}
