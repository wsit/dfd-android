package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class OwnerFormattedName {
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    @SerializedName("middle_name")
    public String middleName;
    @SerializedName("formatted_full_name")
    public String formattedFullName;
}
