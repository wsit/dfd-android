package com.houzes.main.model.api;

public class UserListModel implements Comparable<UserListModel> {
    private int id;
    private String name;
    private String leads_count;
    private String created_at;
    private String updated_at;
    private int user;
    private boolean is_default;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeads_count() {
        return leads_count;
    }

    public void setLeads_count(String leads_count) {
        this.leads_count = leads_count;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public boolean isDefault() {
        return is_default;
    }

    @Override
    public int compareTo(UserListModel o) {
        return this.name.compareTo(o.name);
    }
}
