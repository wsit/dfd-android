package com.houzes.main.model.sys;

import com.houzes.main.action.helper.JsonUtility;

public class RequestDataFrom {
    public String url;
    public String json;
    public Object data;

    public RequestDataFrom(String _url, String _json, Object _data) {
        this.url = _url;
        this.json = _json;
        this.data = _data;
    }

    @Override
    public String toString() {
        JsonUtility jsonUtility=new JsonUtility();
        jsonUtility.put("url",url);
        jsonUtility.put("json",json);
        jsonUtility.put("data",data);
        return jsonUtility.toString();
    }
}
