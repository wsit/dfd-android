package com.houzes.main.model.api;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class PropertyLatLngModel {
    private int id;
    private double latitude;
    private double longitude;
    @SerializedName("property_tags")
    private IdModel[] propertyTags;

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public IdModel[] getPropertyTags() {
        return propertyTags;
    }

    public String getJson() {
        try {
            return new Gson().toJson(this);
        } catch (Exception ex) {
            return null;
        }
    }
}
