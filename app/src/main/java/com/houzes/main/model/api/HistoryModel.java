package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class HistoryModel {
    private int id;
    private int user;
    @SerializedName("start_point_latitude")
    private Double startPointLatitude;
    @SerializedName("start_point_longitude")
    private Double startPointLongitude;
    @SerializedName("end_point_latitude")
    private Double endPointLatitude;
    @SerializedName("end_point_longitude")
    private Double endPointLongitude;
    private Double length;
    private String image;
    @SerializedName("property_count")
    private int propertyCount;
    private String polylines;
    @SerializedName("start_time")
    private String startTime;
    @SerializedName("end_time")
    private String endTime;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public Double getStartPointLatitude() {
        return startPointLatitude;
    }

    public void setStartPointLatitude(Double startPointLatitude) {
        this.startPointLatitude = startPointLatitude;
    }

    public Double getStartPointLongitude() {
        return startPointLongitude;
    }

    public void setStartPointLongitude(Double startPointLongitude) {
        this.startPointLongitude = startPointLongitude;
    }

    public Double getEndPointLatitude() {
        return endPointLatitude;
    }

    public void setEndPointLatitude(Double endPointLatitude) {
        this.endPointLatitude = endPointLatitude;
    }

    public Double getEndPointLongitude() {
        return endPointLongitude;
    }

    public void setEndPointLongitude(Double endPointLongitude) {
        this.endPointLongitude = endPointLongitude;
    }

    public Double getLength() {
        return length;
    }

    public String getLength2D() {
        try {
            return new DecimalFormat("#.##").format(length);
        } catch (Exception ex) {
            return length + "";
        }
    }

    public String getLength2DML() {
        try {
            return new DecimalFormat("#.##").format(length * 0.6214);
        } catch (Exception ex) {
            return length + "";
        }
    }


    public void setLength(Double length) {
        this.length = length;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPolylines() {
        return polylines;
    }

    public void setPolylines(String polylines) {
        this.polylines = polylines;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getStartTimeFormat() {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
            return dateFormat.format(format.parse(startTime));
        } catch (Exception ex) {
        }
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getEndTimeFormat() {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
            return dateFormat.format(format.parse(endTime));
        } catch (Exception ex) {
        }
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getDiffTime() {
        try {
            long diff = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(getEndTime()).getTime() - new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(getStartTime()).getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            return diffHours + ":" + diffMinutes;
        } catch (Exception ex) {
            return "00;00";
        }
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getPropertyCount() {
        return propertyCount;
    }

    public void setPropertyCount(int propertyCount) {
        this.propertyCount = propertyCount;
    }
}
