package com.houzes.main.model.statics;

import com.houzes.main.model.api.ListOfTagModel;

public class StaticProperty {
    public static ListOfTagModel[] LIST_OF_TAGS = new ListOfTagModel[]{};
    public static final String BROADCAST_INTENT_VALUE = "com.houzes.main.ADD_PROPERTY_INFO";
    public static final String BROADCAST_LOAD_LIST = "com.houzes.main.LOAD_LIST";
}
