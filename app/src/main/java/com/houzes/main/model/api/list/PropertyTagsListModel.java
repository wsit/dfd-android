package com.houzes.main.model.api.list;

import com.houzes.main.model.api.NotesModel;
import com.houzes.main.model.api.PropertyLatLngModel;

import java.util.List;

public class PropertyTagsListModel {
    public int count;
    public String next;
    public String previous;
    public PropertyLatLngModel[] results;
}
