package com.houzes.main.model;

public enum SocketDriveType {
    NONE, PRIVATE, PUBLIC
}