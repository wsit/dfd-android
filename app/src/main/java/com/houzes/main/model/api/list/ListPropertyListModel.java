package com.houzes.main.model.api.list;

import com.houzes.main.model.api.PropertyModel;

import java.util.List;

public class ListPropertyListModel {
    public int count;
    public String next;
    public String previous;
    public List<PropertyModel> results;
}
