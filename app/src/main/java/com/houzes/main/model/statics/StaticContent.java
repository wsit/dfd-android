package com.houzes.main.model.statics;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.StrictMode;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.CurrentUserModel;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.sys.GoogleAddressModel;

import org.json.JSONObject;

public class StaticContent extends StaticContetDrive {
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    public static int SIGN_IN_USER_ID = 0;
    public static int APP_CURRENT_VERSION = 20;
    public static boolean IS_NORMAL_MAP_VIEW = false;
    public static boolean IS_UPGRADE_MESSAGE_LOAD = true;
    public static CurrentUserModel CURRENT_USER_MODEL = null;

    public static final int POLYLINE_WIDTH = 8;
    public static final int LOCATION_TIME = 5000;
    public static final int LOCATION_DISTENCE = 10;
    public static GoogleAddressModel SELECTED_PROPERTY_DETAILES = new GoogleAddressModel();
    public static final String SOCKT_DOMAIN = AppConfigRemote.SOCKT_DOMAIN;
    public static final int POLYLINE_COLOR = Color.parseColor("#E91E63");

    public static final String TAG = "HOUZES-LOG";
    public static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    public static void setPermission(Activity activity) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
        for (int i = 0; i < PERMISSIONS.length; i++) {
            if (ContextCompat.checkSelfPermission(activity, PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
                break;
            }
        }
    }

    public static Integer getParentUserId() {
        try {
            if (CURRENT_USER_MODEL.getInvitedBy() != null && CURRENT_USER_MODEL.getInvitedBy() > 0) {
                return CURRENT_USER_MODEL.getInvitedBy();
            } else {
                return CURRENT_USER_MODEL.getId();
            }
        } catch (Exception ex) {
            return 0;
        }
    }

}
