package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class FirebaseOpenClass {
    @SerializedName("open")
    public String className = "SplashActivity";
    public Map<String, String> data = new HashMap<>();
}
