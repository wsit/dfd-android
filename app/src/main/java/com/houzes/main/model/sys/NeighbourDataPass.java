
package com.houzes.main.model.sys;

import android.os.Parcel;

import java.util.List;

public class NeighbourDataPass {

    private boolean ownerInfo = true;
    private boolean powerTrace = false;
    private int totalCoin;
    private List<SelectedNewNeighbour> selectedNewNeighbourList;

    public NeighbourDataPass(boolean ownerInfo, boolean powerTrace, int totalCoin, List<SelectedNewNeighbour> selectedNewNeighbourList) {
        this.ownerInfo = ownerInfo;
        this.powerTrace = powerTrace;
        this.totalCoin = totalCoin;
        this.selectedNewNeighbourList = selectedNewNeighbourList;
    }


    public boolean isOwnerInfo() {
        return ownerInfo;
    }

    public boolean isPowerTrace() {
        return powerTrace;
    }

    public int getTotalCoin() {
        return totalCoin;
    }

    public List<SelectedNewNeighbour> getSelectedNewNeighbourList() {
        return selectedNewNeighbourList;
    }

    protected NeighbourDataPass(Parcel in) {
        ownerInfo = in.readByte() != 0;
        powerTrace = in.readByte() != 0;
        totalCoin = in.readInt();
    }
}
