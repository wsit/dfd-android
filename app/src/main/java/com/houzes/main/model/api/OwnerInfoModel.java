package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class OwnerInfoModel {
    @SerializedName("full_name")
    public String ownerName;
    @SerializedName("full_address")
    public String ownerAddress;
    @SerializedName("formatted_name")
    public OwnerFormattedName formattedName;
    @SerializedName("company_name")
    public String companyName;
    //public String estate;
}
