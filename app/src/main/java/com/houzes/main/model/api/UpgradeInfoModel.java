package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;

public class UpgradeInfoModel {
    @SerializedName("is_team_invitable")
    private boolean isTeamInvitable = false;
    @SerializedName("amount")
    private double amount = 0.00;
    @SerializedName("power_trace")
    private double powerTrace = 0.00;
    @SerializedName("owner_info")
    private double ownerInfo = 0.00;
    @SerializedName("mailer_wizard")
    private double mailWizard = 0.00;
    @SerializedName("plan")
    private SubscriptionPlanModel plan;
    @SerializedName("discount")
    private Float discount;

    public boolean isTeamInvitable() {
        return isTeamInvitable;
    }

    public double getAmount() {
        return amount;
    }

    public double getPowerTrace() {
        return powerTrace;
    }

    public double getOwnerInfo() {
        return ownerInfo;
    }

    public double getMailWizard() {
        return mailWizard;
    }

    public SubscriptionPlanModel getPlan() {
        return plan;
    }

    public boolean isUpgrade() {
        try {
            if (plan.getPlanName().toLowerCase().equals("free")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public Float getDiscount() {
        return discount;
    }
}
