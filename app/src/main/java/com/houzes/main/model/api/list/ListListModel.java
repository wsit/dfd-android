package com.houzes.main.model.api.list;

import com.houzes.main.model.api.UserListModel;

import java.util.List;

public class ListListModel {
    public int count;
    public String next;
    public String previous;
    public List<UserListModel> results;
}
