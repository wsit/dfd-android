package com.houzes.main.model.api;

import com.google.gson.annotations.SerializedName;
import com.houzes.main.action.helper.MessageUtil;

public class CardInfoModel implements Comparable<CardInfoModel> {
    private int id;
    @SerializedName("card_name")
    private String cardName;
    @SerializedName("card_number")
    private String cardNumber;
    @SerializedName("card_code")
    private String cardCode;
    @SerializedName("exp_date")
    private String expDate;

    public Integer getId() {
        return id;
    }

    public String getCardName() {
        return cardName != null ? cardName : "";
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardNumberEnc() {
        return "XXXX-XXXX-XXXX-".concat(MessageUtil.getLastNChar(cardNumber, 4));
    }

    public String getCardCode() {
        return cardCode;
    }

    public String getExpDate() {
        return expDate;
    }

    @Override
    public int compareTo(CardInfoModel o) {
        return this.getId().compareTo(o.getId());
    }
}
