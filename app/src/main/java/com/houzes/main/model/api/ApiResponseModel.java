package com.houzes.main.model.api;

public class ApiResponseModel<T> {
    public boolean status = false;
    public String message = "Something went wrong! try again.";
    public T data;
}
