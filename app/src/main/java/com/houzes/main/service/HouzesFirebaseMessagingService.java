package com.houzes.main.service;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.helper.NotificationsHelper;
import com.houzes.main.model.api.FirebaseOpenClass;

import java.util.Map;

public class HouzesFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().get("id") != null && Integer.parseInt(remoteMessage.getData().get("id")) == 2) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.houzes.main"));
            PendingIntent pendingIntent = PendingIntent.getActivities(this, 0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);
            new NotificationsHelper(this).sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), pendingIntent, BitmapFactory.decodeResource(getResources(), R.drawable.ic_noti_0), true, Integer.parseInt(remoteMessage.getData().get("id")));
        } else {
            try {
                if (remoteMessage.getData().get("data") != null) {
                    FirebaseOpenClass openClass = new Gson().fromJson(remoteMessage.getData().get("data"), FirebaseOpenClass.class);
                    Intent i = new Intent(this, Class.forName("com.houzes.main.view.".concat(openClass.className)));
                    for (Map.Entry<String, String> mapItem : openClass.data.entrySet()) {
                        i.putExtra(mapItem.getKey(), mapItem.getValue());
                    }
                    PendingIntent pendingIntent = PendingIntent.getActivities(this, 0, new Intent[]{i}, PendingIntent.FLAG_UPDATE_CURRENT);
                    new NotificationsHelper(this).sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), pendingIntent, BitmapFactory.decodeResource(getResources(), R.drawable.ic_noti_0), true, Integer.parseInt(remoteMessage.getData().get("id")));
                } else {
                    new NotificationsHelper(this).sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null, BitmapFactory.decodeResource(getResources(), R.drawable.ic_noti_0), true);
                }
            } catch (Exception ex) {
                new NotificationsHelper(this).sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null, BitmapFactory.decodeResource(getResources(), R.drawable.ic_noti_0), true);
            }
        }
    }
}