package com.houzes.main.action.service.api.iapi;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.model.api.CurrentUserModel;

public interface ProfileIService {

    void onProfileSuccess(CurrentUserModel profileListModel);

    void onProfileFailed(String message);

    void onProfileUpdate(CurrentUserModel profileListModel, BottomSheetDialog dialog);

}
