package com.houzes.main.action.service.api.iapi;

import com.houzes.main.model.api.list.NotesListModel;

public interface NoteIService {

    void onNoteAdd();

    void onNoteList(NotesListModel notesListModel, boolean refresh);

    void onNoteUpdate();

    void onNoteDelete();

    void onNoteAddFailed(String message);

    void onNoteFailed(String message);
}
