package com.houzes.main.action.socket.socket;

import com.google.gson.annotations.SerializedName;

public class SocketConnectedUser {
    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("is_driving")
    private boolean isDriving;
    @SerializedName("angle")
    private float angle;

    public int getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isDriving() {
        return isDriving;
    }

    public float getAngle() {
        return angle;
    }
}
