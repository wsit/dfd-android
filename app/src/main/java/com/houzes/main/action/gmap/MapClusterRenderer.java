package com.houzes.main.action.gmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.houzes.main.R;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.statics.StaticContent;

public class MapClusterRenderer extends DefaultClusterRenderer<MarkerItem> {
    private Context context;

    public MapClusterRenderer(Context context, GoogleMap map, ClusterManager<MarkerItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
    }

    @Override
    protected void onClusterItemRendered(MarkerItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);
        try {
            if (marker.getSnippet() != null && marker.getSnippet().startsWith("{")) {
                PropertyLatLngModel propertyModel = new Gson().fromJson(marker.getSnippet(), PropertyLatLngModel.class);
                marker.setIcon(MapUtility.getBitmapDescriptorForPropertyMarker(context, propertyModel.getPropertyTags()));
            }
        } catch (Exception ex) {
        }
    }

    //@Override
    //protected void onBeforeClusterRendered(Cluster<MarkerItem> cluster, MarkerOptions markerOptions) {
    //   markerOptions.icon(BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(cluster.getSize())));
    //}

    public Bitmap drawTextToBitmap(int size) {
        try {
            String sizeText = (size + "");
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_cluster_marker);
            bitmap = MapUtility.tintImage(bitmap, MapUtility.getRandomColor());

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setColor(StaticContent.IS_NORMAL_MAP_VIEW ? Color.WHITE : Color.BLACK);
            paint.setTextSize(30);
            Rect boundsText = new Rect();
            paint.getTextBounds(sizeText, 0, 0, boundsText);
            int x = (bitmap.getWidth() - boundsText.width()) / 2;
            new Canvas(bitmap).drawText(sizeText, x, (x + 12), paint);
            return bitmap;
        } catch (Exception e) {
            return BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_cluster_marker);
        }
    }
}