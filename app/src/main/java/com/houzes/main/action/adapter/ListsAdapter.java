package com.houzes.main.action.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.houzes.main.R;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.action.service.api.iapi.ListIService;

import java.util.List;

public class ListsAdapter extends RecyclerView.Adapter<ListsAdapter.ViewHolder> {

    public List<UserListModel> userListModels;
    private Context context;
    private ListIService listIService;
    private ImageView current;


    public ListsAdapter(Context context, ListIService listIService, List<UserListModel> userListModels) {
        this.userListModels = userListModels;
        this.context = context;
        this.listIService = listIService;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_item_lists, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder baseViewHolder, int position) {
        baseViewHolder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userListModels == null ? 0 : userListModels.size();
    }

    public void add(UserListModel response) {
        try {
            userListModels.add(response);
            notifyItemInserted(userListModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(List<UserListModel> projectItems) {

        try {
            for (UserListModel response : projectItems) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private View liteItemView;

    public class ViewHolder extends AbstractViewHolder {
        public ImageView list_icon;
        public TextView list_text;
        public LinearLayout list_id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            list_icon = itemView.findViewById(R.id.list_icon);
            list_text = itemView.findViewById(R.id.list_text);
            list_id = itemView.findViewById(R.id.list_id);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final UserListModel userListModel = userListModels.get(position);
                list_text.setText(userListModel.getName());

                list_id.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listIService.onClickEvent(userListModel, current, list_icon);
                        current = list_icon;

                        if (liteItemView == null) {
                            list_id.setBackgroundResource(R.color.list_item_background_color);
                            liteItemView = list_id;
                        } else {
                            liteItemView.setBackgroundResource(R.drawable.bottom_border_gray);
                            list_id.setBackgroundResource(R.color.list_item_background_color);
                            liteItemView = list_id;
                        }
                    }
                });

                list_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listIService.onClickEvent(userListModel, current, list_icon);
                        current = list_icon;

                        if (liteItemView == null) {
                            list_id.setBackgroundResource(R.color.list_item_background_color);
                            liteItemView = list_id;
                        } else {
                            liteItemView.setBackgroundResource(R.drawable.bottom_border_gray);
                            list_id.setBackgroundResource(R.color.list_item_background_color);
                            liteItemView = list_id;
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void clear() {

        }
    }
}
