package com.houzes.main.action.gmap;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog.Builder;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import static android.content.Context.SENSOR_SERVICE;
import static com.houzes.main.model.statics.StaticContent.CURRENT_ANGEL;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LAT;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LAT_LNG;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LON;
import static com.houzes.main.model.statics.StaticContent.OLD_LAT_LNG;

public class GeoLocator implements SensorEventListener {
    public Double lattitude = null;
    public Double longitude;
    private float currentDegree = 0f;
    private final int REQUEST_LOCATION = 1;
    private Context context;
    private Activity activity;
    private LocationCallback locationCallback;

    public GeoLocator(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        SensorManager mSensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
    }

    public GeoLocator get(int milliSecond, int meter, final LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        LocationManager locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled("gps")) {
            this.showSettingsAlert();
            Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();
        } else if (locationManager.isProviderEnabled("gps")) {
            if (ActivityCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") != 0 && ActivityCompat.checkSelfPermission(this.context, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
                ActivityCompat.requestPermissions(this.activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
            } else {
                Location location = locationManager.getLastKnownLocation("network");
                Location location1 = locationManager.getLastKnownLocation("gps");
                Location location2 = locationManager.getLastKnownLocation("passive");
                double latti;
                double longi;
                if (location != null) {
                    latti = location.getLatitude();
                    longi = location.getLongitude();
                    lattitude = latti;
                    longitude = longi;
                } else if (location1 != null) {
                    latti = location1.getLatitude();
                    longi = location1.getLongitude();
                    lattitude = latti;
                    longitude = longi;
                } else if (location2 != null) {
                    latti = location2.getLatitude();
                    longi = location2.getLongitude();
                    lattitude = latti;
                    longitude = longi;
                } else {
                    Toast.makeText(context, "Unable to Trace your location", Toast.LENGTH_SHORT).show();
                }
            }
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, milliSecond, meter, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                location.setBearing(currentDegree);
                CURRENT_LAT = location.getLatitude();
                CURRENT_LON = location.getLongitude();
                CURRENT_ANGEL = location.getBearing();
                CURRENT_LAT_LNG = new LatLng(CURRENT_LAT, CURRENT_LON);
                if (OLD_LAT_LNG.latitude != CURRENT_LAT_LNG.latitude) {
                    OLD_LAT_LNG = CURRENT_LAT_LNG;
                    locationCallback.getLocation(location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, milliSecond, meter, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                location.setBearing(currentDegree);
                CURRENT_LAT = location.getLatitude();
                CURRENT_LON = location.getLongitude();
                CURRENT_ANGEL = location.getBearing();
                CURRENT_LAT_LNG = new LatLng(CURRENT_LAT, CURRENT_LON);
                if (OLD_LAT_LNG.latitude != CURRENT_LAT_LNG.latitude) {
                    OLD_LAT_LNG = CURRENT_LAT_LNG;
                    locationCallback.getLocation(location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, milliSecond, meter, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                location.setBearing(currentDegree);
                CURRENT_LAT = location.getLatitude();
                CURRENT_LON = location.getLongitude();
                CURRENT_ANGEL = location.getBearing();
                CURRENT_LAT_LNG = new LatLng(CURRENT_LAT, CURRENT_LON);
                if (OLD_LAT_LNG.latitude != CURRENT_LAT_LNG.latitude) {
                    OLD_LAT_LNG = CURRENT_LAT_LNG;
                    locationCallback.getLocation(location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
        return this;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float degree = Math.round(event.values[0]);
        if (currentDegree != degree) {
            currentDegree = degree;
            if (locationCallback != null) {
                locationCallback.getBearing(degree);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public interface LocationCallback {
        public void getLocation(Location location);

        public void getBearing(float bearing);
    }

    public void showSettingsAlert() {
        Builder alertDialog = new Builder(this.context);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                GeoLocator.this.context.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
