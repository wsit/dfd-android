package com.houzes.main.action.service.api.iapi;

public interface GlobalIService<T> {
    void onGetData(T data);

    void onError(String message);
}
