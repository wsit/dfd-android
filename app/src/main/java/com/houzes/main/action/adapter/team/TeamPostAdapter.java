package com.houzes.main.action.adapter.team;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.houzes.main.R;
import com.houzes.main.action.adapter.AbstractViewHolder;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.ProfileTeamMemberActivity;
import com.houzes.main.view.SwipeRevealLayout;

import java.util.List;

public class TeamPostAdapter extends RecyclerView.Adapter<AbstractViewHolder> {
    private IView iView;
    private Context context;
    public List<TeamModel> teamModels;

    public TeamPostAdapter(List<TeamModel> teamModels, Context context, IView iView) {
        this.teamModels = teamModels;
        this.context = context;
        this.iView = iView;

    }

    @NonNull
    @Override
    public AbstractViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_swipe_item_team, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder abstractViewHolder, int position) {
        abstractViewHolder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return teamModels == null ? 0 : teamModels.size();
    }

    private SwipeRevealLayout prePassion = null;

    public void restoreStates(SwipeRevealLayout swipeRevealLayout) {
        swipeRevealLayout.init(context, null);
    }

    public class ViewHolder extends AbstractViewHolder {

        public TextView teamMemberName;
        public ImageView teamMemberImage;
        public ImageView teamReInviteButton;
        public ImageView deleteTeamButton;
        public ImageView teamProfile;
        public ImageView pendingUserIcon;
        public CardView main_view;
        public SwipeRevealLayout swipeRevealLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            teamMemberName = itemView.findViewById(R.id.team_member_name);
            teamMemberImage = itemView.findViewById(R.id.team_member_image);
            teamReInviteButton = itemView.findViewById(R.id.team_re_invite);
            deleteTeamButton = itemView.findViewById(R.id.delete_team);
            teamProfile = itemView.findViewById(R.id.team_profile);
            pendingUserIcon = itemView.findViewById(R.id.pending_user);
            main_view = itemView.findViewById(R.id.main_view);
            swipeRevealLayout = itemView.findViewById(R.id.mainSwipeRevealLayout);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final TeamModel teamModel = teamModels.get(position);
                if (StaticContent.CURRENT_USER_MODEL.isAdmin()) {
                    if (teamModel.getId() == StaticContent.CURRENT_USER_MODEL.getId()) {
                        deleteTeamButton.setVisibility(View.GONE);
                    } else {
                        deleteTeamButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    deleteTeamButton.setVisibility(View.GONE);
                }

                if (teamModel.isIsUser()) {
                    if (teamModel.getId() == StaticContent.CURRENT_USER_MODEL.getId()) {
                        teamMemberName.setText("Me");
                    } else {
                        teamMemberName.setText(teamModel.getFirstName() + " " + teamModel.getLastName());
                    }
                    if (teamModel.isAdmin()) {
                        teamMemberName.setTextColor(Color.RED);
                    } else {
                        if (teamModel.getId() == StaticContent.CURRENT_USER_MODEL.getId()) {
                            teamMemberName.setTextColor(Color.parseColor("#24C778"));
                        }
                    }
                    teamReInviteButton.setVisibility(View.GONE);
                } else {
                    teamMemberName.setText(teamModel.getEmail());
                    teamReInviteButton.setVisibility(View.VISIBLE);
                    pendingUserIcon.setVisibility(View.VISIBLE);
                }
                if (teamModel.getPhoto() != null) {
                    Glide.with(context).load(teamModel.getPhotoTheme()).apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(70))).placeholder(R.drawable.loading_fream_round).error(R.drawable.profile_name).into(teamMemberImage);
                } else {
                    Glide.with(context).load(R.drawable.profile_name).into(teamMemberImage);
                }
                deleteTeamButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iView.deleteMemberRequest(teamModel.getId());
                    }
                });

                main_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ProfileTeamMemberActivity.class);
                        intent.putExtra(IntentExtraName.TEAM_MEMBER.name(), teamModel);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
                teamProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ProfileTeamMemberActivity.class);
                        intent.putExtra(IntentExtraName.TEAM_MEMBER.name(), teamModel);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
                teamReInviteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iView.addMemberRequest(teamModel.getEmail());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                swipeRevealLayout.setOnSwapListener(new SwipeRevealLayout.OnSwap() {
                    @Override
                    public void left() {
                        if (prePassion != null) {
                            restoreStates(prePassion);
                        }
                        prePassion = swipeRevealLayout;
                    }

                    @Override
                    public void right() {
                        prePassion = null;
                    }
                });
            }

        }

        @Override
        protected void clear() {

        }
    }

    public interface IView {
        void addMemberRequest(String email);

        void deleteMemberRequest(long memberId);
    }

}
