package com.houzes.main.action.service.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.HistoryModel;
import com.houzes.main.model.api.list.HistoryListModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.HistoryIService;
import com.houzes.main.action.service.views.iviews.CallbackObject;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;

public class HistotyApiService {
    private Context context;
    private HistoryIService<HistoryModel> historyIService;
    private ApiResponseModel<HistoryModel> historyModelApiResponseModel;

    public HistotyApiService(Context context, HistoryIService<HistoryModel> historyIService) {
        this.context = context;
        this.historyIService = historyIService;
        this.historyModelApiResponseModel = null;
    }

    public void addStartPoint(FormEncodingBuilder formEncodingBuilder, final Type type) {
        try {
            String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
            RequestBody formBody = formEncodingBuilder.build();
            Request request = new Request.Builder()
                    .url(new AppConfigRemote().getBASE_URL() + "/api/history/start-driving/")
                    .header("Authorization", authorization)
                    .post(formBody)
                    .build();

            new OkHttpClient().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    historyIService.onHistoryFailed(MessageUtil.getMessage(e));
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    historyModelApiResponseModel = (ApiResponseModel<HistoryModel>) new Gson().fromJson(response.body().string(), type);
                    historyIService.getHistory(historyModelApiResponseModel);
                }
            });
        } catch (Exception e) {
            historyIService.onHistoryFailed(MessageUtil.getMessage(e));
        }
    }

    public void addEndPoint(File file, String length, String polylines) {
        if (StaticContent.MY_HISTORY_ID > 0) {
            okhttp3.RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("end_point_latitude", StaticContent.CURRENT_LAT + "")
                    .addFormDataPart("end_point_longitude", StaticContent.CURRENT_LAT + "")
                    .addFormDataPart("image", file.getName(), okhttp3.RequestBody.create(MediaType.parse("image/png"), file))
                    .addFormDataPart("end_time", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'").format(new Date()))
                    .addFormDataPart("polylines", polylines)
                    .addFormDataPart("length", length)
                    .build();


            try {
                String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
                final okhttp3.Request request = new okhttp3.Request.Builder()
                        .url(new AppConfigRemote().getBASE_URL() + "/api/history/" + StaticContent.MY_HISTORY_ID + "/end-driving/")
                        .header("Authorization", authorization)
                        .post(requestBody)
                        .build();

                new okhttp3.OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        historyIService.onHistoryFailed(MessageUtil.getMessage(e));
                    }

                    @Override
                    public void onResponse(Call call, okhttp3.Response response) throws IOException {
                        if (response.isSuccessful()) {
                            historyModelApiResponseModel = new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<HistoryModel>>() {
                            }.getType());
                            historyIService.getHistory(historyModelApiResponseModel);
                        } else {
                            historyIService.onHistoryFailed(MessageUtil.getMessage(context, response.code()));
                        }
                    }

                });
            } catch (Exception e) {
                historyIService.onHistoryFailed(MessageUtil.getMessage(e));
            }
        } else {
            historyIService.onHistoryFailed("History is not found");
        }
    }

    public void getList(String url) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                historyIService.onHistoryFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        HistoryListModel historyModels = new Gson().fromJson(response.body().string(), HistoryListModel.class);
                        historyIService.getHistoryList(historyModels);
                    } catch (Exception e) {
                        historyIService.onHistoryFailed(MessageUtil.getMessage(e));
                    }
                } else {
                    historyIService.onHistoryFailed(MessageUtil.getMessage(context, response.code()));
                }
            }
        });
    }

    public static void getList(final Context _context, String _url, final CallbackIService<HistoryListModel> callbackIService) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(_context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(_url)
                .build();

        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException ex) {
                callbackIService.getError(ex, MessageUtil.getMessage(ex));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        HistoryListModel historyModels = new Gson().fromJson(response.body().string(), HistoryListModel.class);
                        callbackIService.getValue(historyModels);
                    } catch (Exception ex) {
                        callbackIService.getError(ex, MessageUtil.getMessage(ex));
                    }
                } else {
                    callbackIService.getError(new Exception("Response Code: " + response.code()), MessageUtil.getMessage(_context, response.code()));
                }
            }
        });
    }

    public static void addStartPoint(final Context context, FormEncodingBuilder formEncodingBuilder, final CallbackObject callbackObject) {
        try {
            String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
            RequestBody formBody = formEncodingBuilder.build();
            Request request = new Request.Builder()
                    .url(new AppConfigRemote().getBASE_URL() + "/api/history/start-driving/")
                    .header("Authorization", authorization)
                    .post(formBody)
                    .build();

            new OkHttpClient().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    callbackObject.get(null, e);
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        ApiResponseModel<HistoryModel> dd = (ApiResponseModel<HistoryModel>) new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<HistoryModel>>() {
                        }.getType());
                        if (dd != null && dd.status) {
                            callbackObject.get(dd.data, null);
                        } else {
                            callbackObject.get(null, new Exception(dd.message));
                        }
                    } else {
                        callbackObject.get(null, new Exception(MessageUtil.getMessage(context, response.code())));
                    }
                }
            });
        } catch (Exception e) {
            callbackObject.get(null, e);
            e.printStackTrace();
        }
    }

    public static void addEndPoint(final Context context, double length, String polylines, final CallbackObject callbackObject) {
        String url = new AppConfigRemote().getBASE_URL() + "/api/history/" + StaticContent.MY_HISTORY_ID + "/end-driving/";
        if (StaticContent.MY_HISTORY_ID > 0) {
            okhttp3.RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("end_point_latitude", StaticContent.CURRENT_LAT + "")
                    .addFormDataPart("end_point_longitude", StaticContent.CURRENT_LAT + "")
                    .addFormDataPart("end_time", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'").format(new Date()))
                    .addFormDataPart("polylines", polylines)
                    .addFormDataPart("length", length + "")
                    .build();
            try {
                String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
                final okhttp3.Request request = new okhttp3.Request.Builder().url(url).header("Authorization", authorization).post(requestBody).build();
                new okhttp3.OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        callbackObject.get(null, e);
                    }

                    @Override
                    public void onResponse(Call call, okhttp3.Response response) throws IOException {
                        if (response.isSuccessful()) {
                            ApiResponseModel<HistoryModel> dd = (ApiResponseModel<HistoryModel>) new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<HistoryModel>>() {
                            }.getType());
                            if (dd != null && dd.status) {
                                callbackObject.get(dd.data, null);
                            } else {
                                callbackObject.get(null, new Exception(dd.message));
                            }
                        } else {
                            callbackObject.get(null, new Exception(MessageUtil.getMessage(context, response.code())));
                        }
                    }
                });
            } catch (Exception e) {
                callbackObject.get(null, e);
            }
        } else {
            callbackObject.get(null, new Exception("History ID Is Not Found."));
        }
    }

    public static void addImage(final Context context, int id, File file, final CallbackObject callbackObject) {
        if (id > 0) {
            okhttp3.RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("image", file.getName(), okhttp3.RequestBody.create(MediaType.parse("image/png"), file)).build();
            try {
                String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
                okhttp3.Request request = new okhttp3.Request.Builder().url(AppConfigRemote.getBASE_URL() + "/api/history/" + id + "/upload-driving-image/").header("Authorization", authorization).post(requestBody).build();
                new okhttp3.OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        callbackObject.get(false, e);
                    }

                    @Override
                    public void onResponse(Call call, okhttp3.Response response) throws IOException {
                        if (response.isSuccessful()) {
                            callbackObject.get(true, null);
                        } else {
                            callbackObject.get(false, new Exception(MessageUtil.getMessage(context, response.code())));
                        }
                    }
                });
            } catch (Exception e) {
                callbackObject.get(false, e);
                e.printStackTrace();
            }
        } else {
            callbackObject.get(false, new Exception("History Not Found"));
        }
    }
}
