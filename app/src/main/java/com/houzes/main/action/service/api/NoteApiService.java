package com.houzes.main.action.service.api;

import android.content.Context;

import com.google.gson.Gson;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.list.NotesListModel;
import com.houzes.main.action.service.api.iapi.NoteIService;
import com.houzes.main.action.service.views.iviews.SingleDataIService;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NoteApiService {

    private NoteIService noteIService;
    private Context context;

    public NoteApiService(Context context, NoteIService _noteIService) {
        this.context = context;
        noteIService = _noteIService;
    }

    public void addNote(String title, String note, int property) {
        String requestUrl = AppConfigRemote.BASE_URL + "/api/note/";

        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);

        RequestBody requestBody = new FormBody.Builder()
                .add("title", title)
                .add("notes", note)
                .add("property", property + "")
                .build();

        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .post(requestBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                noteIService.onNoteAddFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        JSONObject responseObject = new JSONObject(responseBody);
                        noteIService.onNoteAdd();

                    } else {
                        noteIService.onNoteAddFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    noteIService.onNoteAddFailed(MessageUtil.getMessage(e));
                }
            }
        });

    }

    public void updateNote(String title, String note, int id, final SingleDataIService singleDataIService) {
        String requestUrl = AppConfigRemote.BASE_URL + "/api/note/" + id + "/";

        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);

        RequestBody requestBody = new FormBody.Builder()
                .add("title", title)
                .add("notes", note)
                .build();

        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .patch(requestBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                noteIService.onNoteFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        noteIService.onNoteUpdate();
                        singleDataIService.getValue(null);
                    } else {
                        noteIService.onNoteFailed(MessageUtil.getMessage(context, response.code()));
                    }
                } catch (Exception e) {
                    noteIService.onNoteFailed(MessageUtil.getMessage(e));
                }
            }
        });

    }

    public void getNote(String url, final boolean refresh) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                noteIService.onNoteFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        final NotesListModel notesListModel = new Gson().fromJson(responseBody, NotesListModel.class);
                        noteIService.onNoteList(notesListModel, refresh);

                    } else {
                        noteIService.onNoteFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    noteIService.onNoteFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void deleteNote(int id) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(AppConfigRemote.BASE_URL + "/api/note/" + id + "/")
                .delete()
                .build();


        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                noteIService.onNoteFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        noteIService.onNoteDelete();

                    } else {
                        noteIService.onNoteFailed(MessageUtil.getMessage(context, response.code()));
                    }
                } catch (Exception e) {
                    noteIService.onNoteFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }
}
