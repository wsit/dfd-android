package com.houzes.main.action.gmap;

import android.location.Location;

public interface GpsLocation {
    void gpsLocation(Location location);
}
