package com.houzes.main.action.service.views.iviews;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}