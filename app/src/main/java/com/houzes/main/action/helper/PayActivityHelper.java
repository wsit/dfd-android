package com.houzes.main.action.helper;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.Gson;
import com.houzes.main.view.PaymentStartActivity;
import com.houzes.main.R;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.sys.RequestDataFrom;

public class PayActivityHelper {
    private Activity activity;
    private String url;
    private String json;
    private double amount;

    public PayActivityHelper(Activity activity) {
        this.activity = activity;
    }

    public PayActivityHelper url(String url) {
        this.url = url;
        return this;
    }

    public PayActivityHelper json(String json) {
        this.json = json;
        return this;
    }

    public PayActivityHelper amount(double amount) {
        this.amount = amount;
        return this;
    }

    public void start() {
        Intent intent = new Intent(activity, PaymentStartActivity.class);
        intent.putExtra(IntentExtraName.REQUEST_FORM_DATA.name(), new Gson().toJson(new RequestDataFrom(url, json, amount)));
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
