package com.houzes.main.action.service.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyPhotoModel;
import com.houzes.main.model.api.list.PropertyPhotoListModel;
import com.houzes.main.action.service.api.iapi.PropertyPhotoIService;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PropertyImageApiService {
    private Context context;
    private PropertyPhotoIService propertyPhotoIService;
    private String authorization = "";

    public PropertyImageApiService(Context context, PropertyPhotoIService propertyPhotoIService) {
        this.context = context;
        this.propertyPhotoIService = propertyPhotoIService;
        this.authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
    }

    public void getList(String url) {
        try {
            Request httpRequest = new Request.Builder()
                    .header("Authorization", authorization)
                    .url(url)
                    .build();

            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    propertyPhotoIService.onFailed(MessageUtil.getMessage(e));
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        String responseBody = response.body().string();
                        if (response.isSuccessful()) {
                            PropertyPhotoListModel propertyPhotoListModel = new Gson().fromJson(responseBody, PropertyPhotoListModel.class);
                            propertyPhotoIService.onGetList(propertyPhotoListModel);
                        } else {
                            propertyPhotoIService.onFailed(MessageUtil.getMessage(context, response.code()));
                        }

                    } catch (Exception e) {
                        propertyPhotoIService.onFailed(MessageUtil.getMessage(e));
                    }
                }
            });
        } catch (Exception e) {
            propertyPhotoIService.onFailed(MessageUtil.getMessage(e));
        }
    }

    public Map<String, Object> uploadPhoto(int propertyId, int countUploadImage, String path) {
        Map<String, Object> data = new HashMap<>();
        try {
            File file = new File(path);
            MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
            multipartBody.addFormDataPart("photo1", file.getName(), RequestBody.create(MediaType.parse("image/png"), file));
            Request request = new Request.Builder()
                    .url(AppConfigRemote.BASE_URL + "/api/photo/property/" + propertyId + "/multiple-upload/")
                    .header("Authorization", authorization)
                    .post(multipartBody.build())
                    .build();
            Response response = new OkHttpClient().newCall(request).execute();
            if (response.isSuccessful()) {
                ApiResponseModel<PropertyPhotoModel[]> apiResponseModel = new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<PropertyPhotoModel[]>>() {
                }.getType());
                propertyPhotoIService.onUpload(apiResponseModel.data);
                data.put("status", true);
                data.put("message", apiResponseModel.message);
            } else {
                data.put("status", false);
                data.put("message", MessageUtil.getMessage(null, response.code()));
            }
            data.put("count", countUploadImage);
            return data;
        } catch (Exception ex) {
        }
        return null;
    }
}
