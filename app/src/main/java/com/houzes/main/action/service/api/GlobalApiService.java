package com.houzes.main.action.service.api;

import android.content.Context;

import com.google.gson.Gson;
import com.houzes.main.action.config.DfdClient;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.GlobalIService;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GlobalApiService<T> {
    private T t;
    private Type clazz;
    private Context context;

    public GlobalApiService(Context context, Type type) {
        this.context = context;
        this.clazz = type;
    }

    public void getList(String url, final GlobalIService globalIService) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        DfdClient.init().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                globalIService.onError(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        t = null;
                        t = (T) new Gson().fromJson(responseBody, clazz);
                        if (t != null) {
                            globalIService.onGetData(t);
                        } else {
                            globalIService.onError(MessageUtil.getMessage("null"));
                        }

                    } else {
                        globalIService.onError(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    globalIService.onError(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void get(String url, final GlobalIService globalIService) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        DfdClient.init().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                globalIService.onError(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        t = null;
                        if (clazz == String.class) {
                            t = (T) responseBody;
                        } else {
                            t = (T) new Gson().fromJson(responseBody, clazz);
                        }

                        if (t != null) {
                            globalIService.onGetData(t);
                        } else {
                            globalIService.onError(MessageUtil.getMessage("null"));
                        }

                    } else {
                        globalIService.onError(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    globalIService.onError(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void post(String url, String json, CallbackIService callbackIService) {
        post(url, json, callbackIService, true);
    }

    public void post(String url, String json, final CallbackIService callbackIService, boolean setToken) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request.Builder builder = new Request.Builder();
        if (setToken) {
            builder.header("Authorization", authorization);
        }
        Request httpRequest = builder.url(url).post(RequestBody.create(MediaType.parse("application/json"), json)).build();
        DfdClient.init().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callbackIService.getError(e, MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        if (clazz == String.class) {
                            t = (T) responseBody;
                        } else {
                            t = (T) new Gson().fromJson(responseBody, clazz);
                        }
                        callbackIService.getValue(t);

                    } else {
                        Exception ex = new Exception(responseBody);
                        StackTraceElement st1 = new StackTraceElement(responseBody, response.isSuccessful() + "", this.getClass().getName(), 52);
                        ex.setStackTrace(new StackTraceElement[]{st1});
                        callbackIService.getError(ex, MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    callbackIService.getError(e, MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void delete(final String _url, final CallbackIService _callbackIService) {
        Request.Builder builder = new Request.Builder().header("Authorization", ("Bearer " + SharedPreferenceUtil.getToken(context)));

        DfdClient.init().newCall(builder.url(_url).delete().build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                _callbackIService.getError(e, MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        t = (T) new Gson().fromJson(responseBody, clazz);
                        _callbackIService.getValue(t);
                    } else {
                        Exception ex = new Exception(responseBody);
                        StackTraceElement st1 = new StackTraceElement(responseBody, response.isSuccessful() + "", this.getClass().getName(), 52);
                        ex.setStackTrace(new StackTraceElement[]{st1});
                        _callbackIService.getError(ex, MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    _callbackIService.getError(e, MessageUtil.getMessage(e));
                }
            }
        });
    }

}
