package com.houzes.main.action.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.houzes.main.action.config.AppConfigRemote;

public class UriHelper {
    public static String next(String next) {
        if (next == null) {
            return null;
        }
        next = next.replaceFirst("http://", "https://");
        return next;
    }

    public static String getOPUrl(int propertyId) {
        return AppConfigRemote.BASE_URL.concat("/api/property/").concat(propertyId + "").concat("/payment/");
    }

    public static String getMailWizard(boolean isTrueForPropertyAndisFalseForNeighbor, int id) {
        return AppConfigRemote.BASE_URL.concat("/api/mail-wizard/").concat(isTrueForPropertyAndisFalseForNeighbor ? "property" : "neighbor").concat("/" + id + "/");
    }

    public static void openFacebook(Context context, String url) {
        try {
            PackageManager packageManager = context.getPackageManager();
            try {
                int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
                if (versionCode >= 3002850) {
                    url = "fb://facewebmodal/f?href=" + url;
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
            facebookIntent.setData(Uri.parse(url));
            context.startActivity(facebookIntent);
        } catch (Exception ex) {
        }
    }

    public static void openTwitter(Context context, String url) {
        try {
            Intent intent = null;
            PackageInfo info = context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            if (info.applicationInfo.enabled)
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            else
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception ex) {
        }
    }

    public static void openUrl(Context context, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception ex) {
        }
    }
}
