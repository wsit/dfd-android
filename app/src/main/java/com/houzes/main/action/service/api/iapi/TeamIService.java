package com.houzes.main.action.service.api.iapi;

import com.houzes.main.model.api.TeamModel;

import java.util.List;

public interface TeamIService {
    void onTeamDelete();

    void onTeamFailed(String message);

    void onTeamAdd(TeamModel teamModel);

    void onTeamAddFailed(String message);
    void onTeamMessage(String message);

    void onTeamGet(List<TeamModel> teamModels);
}
