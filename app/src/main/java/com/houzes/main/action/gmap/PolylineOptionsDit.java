package com.houzes.main.action.gmap;

import android.graphics.Color;

import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Arrays;
import java.util.List;

public class PolylineOptionsDit {
    private static final int PATTERN_GAP_LENGTH_PX = 10;
    private static final Gap GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    private static final Dot DOT = new Dot();
    private static final List<PatternItem> PATTERN_DOTTED = Arrays.asList(DOT, GAP);

    public static PolylineOptions getDottedPolylines(List<LatLng> points) {
        PolylineOptions polylineOptions = new PolylineOptions().color(Color.RED).pattern(PATTERN_DOTTED);
        for (LatLng point : points) {
            polylineOptions.add(point);
        }
        return polylineOptions;
    }
}
