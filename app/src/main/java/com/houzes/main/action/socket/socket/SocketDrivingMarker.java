package com.houzes.main.action.socket.socket;

import com.google.android.gms.maps.model.Marker;
import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.api.SimpleUserInfo;

import java.util.Date;

public class SocketDrivingMarker {
    public int id;
    public double lat;
    public double lon;
    public float angel;
    public Marker marker;
    public long latTime = new Date().getTime();
    public boolean isRotate = false;
    public SocketDriveType driveType = SocketDriveType.PUBLIC;
    public SimpleUserInfo userInfo;

    public SocketDrivingMarker sync(SocketShareLocation socketShareLocation) {
        id = socketShareLocation.getUserId();
        lat = socketShareLocation.getLatitude();
        lon = socketShareLocation.getLongitude();
        angel = socketShareLocation.getAngle();
        driveType = socketShareLocation.getDriveType();
        return this;
    }

    public void setUserInfo(SimpleUserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
