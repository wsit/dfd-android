package com.houzes.main.action.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;

public class SharedPreferenceUtil {

    private static final int MODE_PRIVATE = 0x0000;
    public static final String USER_ID = "id";
    public static final String USER_EMAIL = "email";
    public static final String USER_IMAGE = "photo";
    public static final String USER_PHONE = "phone_number";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String CREATE_AT = "created_at";
    public static final String ACCESS_TOKEN = "access_token";
    private static final String AUTH_USER_PREF = "AUTH_USER_PREF";
    public static final String URL_AUTHORIZATION = "urlAuthorization";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String DB_PUBLIC_SHARED_ENABEL = "DB_PUBLIC_SHARED_ENABEL";
    public static final String DB_PUBLIC_SHARED_ENABEL_RESUME = "DB_PUBLIC_SHARED_ENABEL_RESUME";
    public static final String USER_DRIVE_TYPE = "USER_DRIVE_TYPE";
    public static final String DB_HISTORY_ID = "DB_HISTORY_ID";


    public static String getToken(Context context) {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        String value = pref.getString(ACCESS_TOKEN, null);
        return value;
    }

    public static void setToken(Context context, String value) {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(ACCESS_TOKEN, value);
        editor.commit();
    }

    public static String getFirebaceCurrentTokenOKEN(Context context) {
        return getValue("FIREBACE_CURRENT_TOKEN", context);
    }

    public static void setFirebaceCurrentTokenOKEN(Context context, String value) {
        setValue("FIREBACE_CURRENT_TOKEN", value, context);
    }

    public static String getValue(String key, Context context) {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        String value = pref.getString(key, null);
        return value;
    }


    public static void setValue(String key, String value, Context context) {

        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setInt(String key, int value, Context context) {

        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(String key, Context context) {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        int value = pref.getInt(key, 0);
        return value;
    }

    public static void setBool(String key, boolean value, Context context) {

        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBool(String key, Context context) {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, MODE_PRIVATE);
        boolean value = pref.getBoolean(key, false);
        return value;
    }

    public static boolean isLoggedIn(Context context) {
        String access_token = SharedPreferenceUtil.getValue(SharedPreferenceUtil.ACCESS_TOKEN, context);
        if (access_token != null && !access_token.isEmpty()) {
            return true;
        }
        return false;
    }

    public static void logOut(Context context) {
        try {
            StaticContetDrive.driveStopCalculation(context, true);
            SharedPreferences settings = context.getSharedPreferences(SharedPreferenceUtil.AUTH_USER_PREF, Context.MODE_PRIVATE);
            settings.edit().clear().commit();
        } catch (Exception ex) {
        }
    }

    public static void setMapStyle(Context context, boolean normal) {
        StaticContent.IS_NORMAL_MAP_VIEW = normal;
        setBool("SATELLITE_NORMAL_VIEW", normal, context);
    }

    public static int getMapStyle(Context context) {
        boolean value = getBool("SATELLITE_NORMAL_VIEW", context);
        StaticContent.IS_NORMAL_MAP_VIEW = value;
        return value ? 1 : 2;
    }

    public static boolean PUBLIC_SHARE_ENABLE(Context context) {
        String val = SharedPreferenceUtil.getValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL, context);
        if (val != null && val.equals("true")) {
            return true;
        }
        return false;
    }

    public static boolean PUBLIC_SHARE_ENABLE_PAUSE(Context context) {
        String val = SharedPreferenceUtil.getValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL_RESUME, context);
        if (val != null && val.equals("true")) {
            return true;
        }
        return false;
    }

    public static void setDriveType(Context context, SocketDriveType driveType) {
        if (driveType.equals(SocketDriveType.PUBLIC)) {
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_DRIVE_TYPE, "PUBLIC", context);
        } else if (driveType.equals(SocketDriveType.PRIVATE)) {
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_DRIVE_TYPE, "PRIVATE", context);
        } else {
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_DRIVE_TYPE, null, context);
        }
    }

    public static SocketDriveType getDriveType(Context context) {
        String val = SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_DRIVE_TYPE, context);
        if (val != null && val.equals("PUBLIC")) {
            return SocketDriveType.PUBLIC;
        } else if (val != null && val.equals("PRIVATE")) {
            return SocketDriveType.PRIVATE;
        }
        return SocketDriveType.NONE;
    }
}
