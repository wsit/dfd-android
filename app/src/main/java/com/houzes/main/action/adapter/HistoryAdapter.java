package com.houzes.main.action.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.houzes.main.view.HistoryDetailsActivity;
import com.houzes.main.R;
import com.houzes.main.model.api.HistoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    public List<HistoryModel> historyModels;
    private Context context;

    public HistoryAdapter(List<HistoryModel> historyModels, Context context) {
        this.historyModels = historyModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rcview_item_history_info, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryAdapter.ViewHolder viewHolder, int position) {
        final HistoryModel historyModel = historyModels.get(position);
        viewHolder.textHistoryDate.setText(historyModel.getStartTimeFormat());
        viewHolder.textVisitedProperty.setText(historyModel.getPropertyCount() + " Properties");

        if (historyModel.getImage() != null) {
            viewHolder.mapImage.setBackgroundColor(Color.WHITE);
            Picasso.with(context).load(historyModel.getImage()).placeholder(R.drawable.loading_fream_round).error(R.drawable.ic_map_background).into(viewHolder.mapImage);
        } else {
            viewHolder.mapImage.setBackgroundResource(R.drawable.ic_map_background);
        }

        viewHolder.diffTime.setText(historyModel.getDiffTime());
        viewHolder.textTotalVisitedPath.setText(historyModel.getLength2DML());
        viewHolder.mapImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HistoryDetailsActivity.class);
                intent.putExtra("historyModel", new Gson().toJson(historyModel));
                ((Activity) context).startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    public void add(HistoryModel response) {
        try {
            historyModels.add(response);
            notifyItemInserted(historyModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(List<HistoryModel> projectItems) {
        try {
            for (HistoryModel response : projectItems) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (historyModels != null)
            return historyModels.size();
        return 0;
    }

    public class ViewHolder extends AbstractViewHolder {

        public ImageView mapImage;
        public TextView textHistoryDate;
        public TextView textVisitedProperty;
        public TextView textTotalVisitedPath;
        public TextView diffTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mapImage = (ImageView) itemView.findViewById(R.id.map_image);
            textHistoryDate = (TextView) itemView.findViewById(R.id.HistoryDate);
            textVisitedProperty = (TextView) itemView.findViewById(R.id.VisitedProperty);
            textTotalVisitedPath = (TextView) itemView.findViewById(R.id.TotalVisitedPath);
            diffTime = (TextView) itemView.findViewById(R.id.diff_time);
        }

        @Override
        protected void clear() {

        }
    }

}
