package com.houzes.main.action.service.views.iviews;

public interface PropertyFragmentIService {
    void goNext();
    void fragmentLoad();
    void loadedProperty(int count);
}
