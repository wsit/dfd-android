package com.houzes.main.action.service.api;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.ListOfTagModel;
import com.houzes.main.model.statics.StaticProperty;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.GlobalIService;

public class ApiService {

    public static final void loadTags(Context context, final CallbackIService<ListOfTagModel[]> callbackIService) {
        try {
            new GlobalApiService<ApiResponseModel<ListOfTagModel[]>>(context, new TypeToken<ListOfTagModel[]>() {
            }.getType()).getList(AppConfigRemote.ALL_TAG_URL, new GlobalIService<ListOfTagModel[]>() {

                @Override
                public void onGetData(final ListOfTagModel[] results) {
                    StaticProperty.LIST_OF_TAGS = results;
                    if (callbackIService != null) {
                        callbackIService.getValue(results);
                    }
                }

                @Override
                public void onError(String message) {
                    if (callbackIService != null) {
                        callbackIService.getError(new Exception(message), message);
                    }
                }
            });
        } catch (Exception ex) {
            if (callbackIService != null) {
                callbackIService.getError(ex, ex.getMessage());
            }
        }
    }
}
