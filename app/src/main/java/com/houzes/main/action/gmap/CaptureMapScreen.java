package com.houzes.main.action.gmap;

import android.graphics.Bitmap;
import android.os.Environment;

import com.google.android.gms.maps.GoogleMap;

import java.io.File;
import java.io.FileOutputStream;

public class CaptureMapScreen {
    public static void take(GoogleMap googleMap, final Image image) {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {
                    File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "dft");
                    boolean success = true;
                    if (!folder.exists()) {
                        success = folder.mkdirs();
                    }
                    if (success) {
                        String path = "/mnt/sdcard/dft/map-screen-" + System.currentTimeMillis() + ".png";
                        FileOutputStream out = new FileOutputStream(path);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                        image.get(bitmap, path, "Success");
                    } else {
                        image.get(null, null, "Try Again");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    image.get(null, null, e.getMessage());
                }
            }
        };

        googleMap.snapshot(callback);
    }

    public interface Image {
        void get(Bitmap bitmap, String path, String message);
    }
}
