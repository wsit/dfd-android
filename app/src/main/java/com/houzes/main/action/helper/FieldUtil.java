package com.houzes.main.action.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FieldUtil {
    public static boolean isEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static String usaPhoneNumber(String number) {
        number = number.replace("+11", "+1");
        number = number.replace("+1+1", "+1");
        String regex = "^((\\+1)|)([2-9]{1}[0-9]{2})([0-9]{3}[0-9]{4})$";
        Matcher matcher = Pattern.compile(regex).matcher(number);

        if (matcher.find()) {
            return number.replace("+1", "");
        }
        return null;
    }

    public static String formatUsaPhoneNumber(String number) {
        try {
            if(number==null){
                return "";
            }
            return number.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1)-$2-$3");
        } catch (Exception ex) {
            return number;
        }
    }

    public static String nameToNumber(String mon) {
        if (mon.equals("January")) {
            return "01";
        } else if (mon.equals("February")) {
            return "02";
        } else if (mon.equals("March")) {
            return "03";
        } else if (mon.equals("April")) {
            return "04";
        } else if (mon.equals("May")) {
            return "05";
        } else if (mon.equals("June")) {
            return "06";
        } else if (mon.equals("July")) {
            return "07";
        } else if (mon.equals("August")) {
            return "08";
        } else if (mon.equals("September")) {
            return "09";
        } else if (mon.equals("October")) {
            return "10";
        } else if (mon.equals("November")) {
            return "11";
        } else if (mon.equals("December")) {
            return "12";
        }
        return "01";
    }
}
