package com.houzes.main.action.socket;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.model.statics.StaticContent;

import org.json.JSONObject;


public class HouzesSocketHandler {
    private static Socket houzesSocket;

    public static void listen(String host, int userID, final HouzesSocketEventListener houzesSocketEventListener) throws Exception {
        houzesSocket = HouzesSocketFactory.getSocket(host, String.valueOf(userID));
        houzesSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                houzesSocketEventListener.onConnect();
            }
        }).on(HouzesSocketEvents.SOCKET_CONNECTED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                houzesSocketEventListener.onConnected((String) args[0]);
            }
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                houzesSocketEventListener.onError("Failed to connect.");
            }

        }).on(HouzesSocketEvents.LOCATION_RECEIVE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    SocketShareLocation socketShareLocation = new Gson().fromJson(((JSONObject) args[0]).toString(), SocketShareLocation.class);
                    if (socketShareLocation.getUserId() != StaticContent.CURRENT_USER_MODEL.getId()) {
                        houzesSocketEventListener.onLocationReceived(socketShareLocation);
                    }
                } catch (Exception ex) {
                    houzesSocketEventListener.onError(MessageUtil.getMessage(ex));
                }
            }

        }).on(HouzesSocketEvents.LOCATION_RECEIVE_ROOM, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    SocketShareLocation socketShareLocation = new Gson().fromJson(((JSONObject) args[0]).toString(), SocketShareLocation.class);
                    if (socketShareLocation.getUserId() != StaticContent.CURRENT_USER_MODEL.getId()) {
                        houzesSocketEventListener.onLocationReceived(socketShareLocation);
                    }
                } catch (Exception ex) {
                    houzesSocketEventListener.onError(MessageUtil.getMessage(ex));
                }
            }

        }).on(HouzesSocketEvents.DRIVING_STOPPED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    houzesSocketEventListener.onUserDisconnected(Integer.parseInt((String) args[0]));
                } catch (Exception ex) {
                }
            }

        }).on(HouzesSocketEvents.USER_DISCONNECTED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    houzesSocketEventListener.onUserDisconnected(Integer.parseInt((String) args[0]));
                } catch (Exception ex) {
                }
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                houzesSocketEventListener.onDisconnect();
            }

        });

        houzesSocket.connect();
    }

    public static void shareUserLocation(SocketShareLocation socketShareLocation) {
        houzesSocket.emit(HouzesSocketEvents.LOCATION_SHARE, socketShareLocation.toShareString());
    }

    public static void stopDriving() {
        houzesSocket.emit(HouzesSocketEvents.STOP_DRIVING);
    }

    public static void disconnectSocket() {
        houzesSocket.disconnect();
    }
}