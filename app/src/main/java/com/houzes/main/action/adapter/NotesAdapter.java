package com.houzes.main.action.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.houzes.main.R;
import com.houzes.main.action.service.api.NoteApiService;
import com.houzes.main.action.service.views.iviews.NoteEditClickIService;
import com.houzes.main.model.api.NotesModel;
import com.houzes.main.view.fragment.FrontPopup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    public List<NotesModel> notesModels;
    private Context context;
    private NoteEditClickIService noteEditClickIService;
    private NoteApiService noteApiService;

    public NotesAdapter(List<NotesModel> notesModels, Context context, NoteApiService noteApiService) {
        this.notesModels = notesModels;
        this.context = context;
        this.noteApiService = noteApiService;
        this.noteEditClickIService = (NoteEditClickIService) context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_swipe_item_notes, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder baseViewHolder, int position) {
        baseViewHolder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return notesModels == null ? 0 : notesModels.size();
    }

    public void add(NotesModel response) {
        try {
            notesModels.add(response);
            notifyItemInserted(notesModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(List<NotesModel> projectItems) {

        try {
            for (NotesModel response : projectItems) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class ViewHolder extends AbstractViewHolder {
        public TextView title;
        public TextView date;
        public TextView info;
        public ImageView delete_note;
        public ImageView edit_note;
        public ImageView view_note;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.date);
            info = itemView.findViewById(R.id.info);
            delete_note = itemView.findViewById(R.id.delete_note);
            edit_note = itemView.findViewById(R.id.edit_note);
            view_note = itemView.findViewById(R.id.view_note);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final NotesModel notesModel = notesModels.get(position);
                title.setText(notesModel.title != null ? notesModel.title : "");
                info.setText(notesModel.notes != null ? notesModel.notes : "");

                try {
                    DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
                    String strDate = dateFormat.format(notesModel.createdAt);
                    date.setText(strDate);
                } catch (Exception ex) {
                    date.setText("");
                }


                edit_note.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noteEditClickIService.getValue(notesModel, title, info);
                    }
                });

                view_note.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noteEditClickIService.getValue(notesModel, null, null);
                    }
                });
                info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noteEditClickIService.getValue(notesModel, null, null);
                    }
                });

                delete_note.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new FrontPopup(context).setTitle("Remove Note").setMessage("Are you sure you want to remove this note?").create().setCancel("No", null)
                                .setOkay("Yes", new FrontPopup.OkayClick() {
                                    @Override
                                    public void onClick() {
                                        try {
                                            notesModels.remove(position);
                                            notifyItemRemoved(getAdapterPosition());
                                            noteApiService.deleteNote(notesModel.id);
                                        } catch (Exception ex) {
                                        }
                                    }
                                }).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void clear() {

        }
    }
}
