package com.houzes.main.action.socket.socket;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class SocketUpdateLocation {
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("angle")
    private float angle;
    @SerializedName("is_driving")
    private boolean isDriving;


    public SocketUpdateLocation() {
    }

    public SocketUpdateLocation(double latitude, double longitude, float angle, boolean isDriving) {
        this.angle = angle;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isDriving = isDriving;
    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
