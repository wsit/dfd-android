package com.houzes.main.action.service.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.ScoutModel;
import com.houzes.main.action.service.api.iapi.ScoutIService;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ScoutApiService {

    private ScoutIService teamIServiceListener;
    private Context context;
    private AppConfigRemote appConfigRemote;

    public ScoutApiService(Context context, ScoutIService teamIServiceListener) {
        this.context = context;
        this.teamIServiceListener = teamIServiceListener;
        appConfigRemote = new AppConfigRemote();
    }

    public void getScoutList() {
        String requestUrl = appConfigRemote.getBASE_URL() + "/api/scout/";


        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                teamIServiceListener.onScoutFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    JSONArray jsonarray = new JSONArray(responseBody);
                    JSONObject responseObject = new JSONObject();
                    responseObject.put("scouts", jsonarray);
                    if (response.isSuccessful()) {
                        final List<ScoutModel> list = new Gson().fromJson(responseObject.get("scouts").toString(), new TypeReference<List<ScoutModel>>() {
                        }.getType());
                        teamIServiceListener.onScoutList(list);

                    } else {
                        teamIServiceListener.onScoutFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    teamIServiceListener.onScoutFailed(MessageUtil.getMessage(e));
                }


            }
        });

    }

    public void deleteScout(long id) {
        String url = appConfigRemote.getBASE_URL() + "/api/scout/" + id + "/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .delete()
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                teamIServiceListener.onScoutFailed(MessageUtil.getMessage(e));
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        teamIServiceListener.onScoutDelete();
                    } else {
                        teamIServiceListener.onScoutFailed(MessageUtil.getMessage(context, response.code()));
                    }
                } catch (Exception e) {
                    teamIServiceListener.onScoutFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void addScout(String first_name, String last_name, String email, String phone) {
        try {
            String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
            String requestUrl = appConfigRemote.getBASE_URL() + "/api/scout/";
            MediaType JSON = MediaType.parse("application/json");

            JSONObject postData = new JSONObject();
            postData.put("first_name", first_name);
            postData.put("last_name", last_name);
            postData.put("email", email);
            postData.put("phone_number", phone);
            postData.put("manager_id", SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_ID, context));


            RequestBody requestBody = RequestBody.create(JSON, postData.toString());
            Request httpRequest = new Request.Builder()
                    .header("Authorization", authorization)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .url(requestUrl)
                    .post(requestBody)
                    .build();
            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    teamIServiceListener.onScoutAddFailed(MessageUtil.getMessage(e));
                }

                @Override
                public void onResponse(Call call, Response response) {
                    try {
                        String responseBody = response.body().string();
                        if (response.isSuccessful()) {
                            ApiResponseModel<ScoutModel> apiResponseModel = new Gson().fromJson(responseBody, new TypeToken<ApiResponseModel<ScoutModel>>() {
                            }.getType());
                            ScoutModel scoutModel = apiResponseModel.data;
                            teamIServiceListener.onScoutAdd(scoutModel);
                        } else {
                            teamIServiceListener.onScoutAddFailed(MessageUtil.getMessage(context, response.code()));
                        }

                    } catch (Exception e) {
                        teamIServiceListener.onScoutAddFailed(MessageUtil.getMessage(e));
                    }
                }
            });
        } catch (Exception e) {
            teamIServiceListener.onScoutAddFailed(MessageUtil.getMessage(e));
        }
    }
}
