package com.houzes.main.action.adapter.team;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.houzes.main.R;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.view.ProfileScoutMemberActivity;
import com.houzes.main.action.adapter.AbstractViewHolder;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.model.api.ScoutModel;
import com.houzes.main.action.service.api.ScoutApiService;

import java.util.List;

public class ScoutPostAdapter extends RecyclerView.Adapter<AbstractViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;
    private ScoutApiService scoutApiService;

    public List<ScoutModel> scoutModels;
    private Context context;

    public ScoutPostAdapter(List<ScoutModel> scoutModels, Context context, ScoutApiService scoutApiService) {
        this.scoutModels = scoutModels;
        this.context = context;
        this.scoutApiService = scoutApiService;

    }

    @NonNull
    @Override
    public AbstractViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_swipe_item_scout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder abstractViewHolder, int position) {
        abstractViewHolder.onBind(position);
    }


    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == scoutModels.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return scoutModels == null ? 0 : scoutModels.size();
    }

    public void add(ScoutModel response) {
        try {
            scoutModels.add(response);
            notifyItemInserted(scoutModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(List<ScoutModel> projectItems) {

        try {
            for (ScoutModel response : projectItems) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    ScoutModel getItem(int position) {
        return scoutModels.get(position);
    }


    public class ViewHolder extends AbstractViewHolder {
        public TextView scout_name;
        public TextView scout_link;
        public ImageView copy_team;
        public ImageView delete_team;
        public ImageView scout_icon;
        public ImageView scout_profile;
        public CardView scout_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            scout_name = itemView.findViewById(R.id.scout_name);
            scout_link = itemView.findViewById(R.id.scout_link);
            copy_team = itemView.findViewById(R.id.copy_team);
            delete_team = itemView.findViewById(R.id.delete_team);
            scout_icon = itemView.findViewById(R.id.scout_icon);
            scout_profile = itemView.findViewById(R.id.scout_profile);
            scout_view = itemView.findViewById(R.id.scout_view);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final ScoutModel myList = scoutModels.get(position);

                scout_name.setText(myList.getFirst_name() + " " + myList.getLast_name());
                scout_link.setText(myList.getUrl());

                copy_team.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setText(myList.getUrl());
                        Toast.makeText(context, "Copy To Clipboard", Toast.LENGTH_SHORT).show();
                    }
                });

                delete_team.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new FrontPopup(context)
                                .setTitle("Remove Scout")
                                .setMessage("Are you sure you want to remove this scout?")
                                .create()
                                .setCancel("No", null)
                                .setOkay("Yes", new FrontPopup.OkayClick() {
                                    @Override
                                    public void onClick() {
                                        try {
                                            scoutModels.remove(position);
                                            notifyItemRemoved(getAdapterPosition());
                                            scoutApiService.deleteScout(myList.getId());
                                        } catch (Exception ex) {
                                        }
                                    }
                                }).show();

                    }
                });

                scout_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ProfileScoutMemberActivity.class);
                        intent.putExtra(IntentExtraName.SCOUT_MEMBER.name(), myList);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
                scout_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ProfileScoutMemberActivity.class);
                        intent.putExtra(IntentExtraName.SCOUT_MEMBER.name(), myList);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void clear() {

        }
    }
}
