package com.houzes.main.action.socket;

import com.houzes.main.model.statics.StaticContent;

class HouzesSocketEvents {
    static final String SOCKET_CONNECTED = "connected";
    static final String USER_DISCONNECTED = "user::disconnected";

    static final String LOCATION_SHARE = "location::share";
    static final String LOCATION_RECEIVE = "location::receive";
    static final String LOCATION_RECEIVE_ROOM = "location::receive" + StaticContent.getParentUserId();

    static final String STOP_DRIVING = "driver::leave";
    static final String DRIVING_STOPPED = "driver::left";
}