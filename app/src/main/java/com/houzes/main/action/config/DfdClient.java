package com.houzes.main.action.config;

import android.content.Context;

import com.houzes.main.action.helper.SharedPreferenceUtil;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class DfdClient extends OkHttpClient {
    public static OkHttpClient init() {
        return new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public static Request requestGet(Context context, String url) {
        Request httpRequest = new Request.Builder()
                .header("Authorization", "Bearer " + SharedPreferenceUtil.getToken(context))
                .url(url)
                .build();
        return httpRequest;
    }

    public static Request requestPost(Context context, RequestBody requestBody, String url) {
        Request httpRequest = new Request.Builder()
                .header("Authorization", "Bearer " + SharedPreferenceUtil.getToken(context))
                .url(url)
                .post(requestBody)
                .build();
        return httpRequest;
    }
}
