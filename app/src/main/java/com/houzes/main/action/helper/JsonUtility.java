package com.houzes.main.action.helper;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class JsonUtility {
    public Map<String, Object> map = new HashMap<>();

    public JsonUtility put(String name, Object value) {
        map.put(name, value);
        return this;
    }

    public JsonUtility putInteger(String name, Integer value) {
        map.put(name, value);
        return this;
    }

    public JsonUtility putInteger(String name, boolean value) {
        map.put(name, value ? 1 : 0);
        return this;
    }

    @Override
    public String toString() {
        return new Gson().toJson(map);
    }
    public String toJson() {
        return new Gson().toJson(map);
    }
}
