package com.houzes.main.action.service.views.iviews;

public interface HomeFragmentIService {
    void loaded();
    void startDriving();
    void stopDriving(String json);
}
