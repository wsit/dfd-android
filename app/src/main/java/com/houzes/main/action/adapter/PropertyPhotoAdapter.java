package com.houzes.main.action.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyPhotoModel;
import com.houzes.main.view.fragment.FrontPopup;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PropertyPhotoAdapter extends RecyclerView.Adapter<PropertyPhotoAdapter.ViewHolder> {

    public List<PropertyPhotoModel> propertyPhotoModels = new ArrayList<>();
    private Context context;
    private RelativeLayout photos;

    public PropertyPhotoAdapter(Context _context, PropertyPhotoModel[] _propertyPhotoModels) {
        this.propertyPhotoModels.addAll(Arrays.asList(_propertyPhotoModels));
        this.context = _context;
        this.photos = ((Activity) context).findViewById(R.id.photos);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_item_photos1, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder baseViewHolder, int position) {
        baseViewHolder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return propertyPhotoModels == null ? 0 : propertyPhotoModels.size();
    }

    public void add(PropertyPhotoModel response) {
        try {
            propertyPhotoModels.add(response);
            notifyItemInserted(propertyPhotoModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(PropertyPhotoModel[] _propertyPhotoModels) {

        try {
            for (PropertyPhotoModel response : _propertyPhotoModels) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class ViewHolder extends AbstractViewHolder {
        public ImageView select_icon;
        public ImageView main_photo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            select_icon = itemView.findViewById(R.id.select_icon);
            main_photo = itemView.findViewById(R.id.main_photo);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final PropertyPhotoModel model = propertyPhotoModels.get(position);
                select_icon.setVisibility(View.GONE);
                String path = model.getThumbPhotoUrl() != null && model.getThumbPhotoUrl().length() > 5 ? model.getThumbPhotoUrl() : model.getPhotoUrl();
                Picasso.with(context).load(path).placeholder(R.drawable.loading_fream_round).error(R.drawable.ic_error).into(main_photo);
                main_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog _dialog = new Dialog(context, R.style.activityFullScreen);
                        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        _dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        _dialog.setContentView(R.layout.app_popup_imageview);
                        _dialog.show();
                        Picasso.with(context).load(model.getPhotoUrl()).placeholder(R.drawable.loading_fream_round).error(R.drawable.ic_error).into((ImageView) _dialog.findViewById(R.id.popup_image_item));
                    }
                });

                main_photo.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        new FrontPopup(context)
                                .setTitle("Remove Photo")
                                .setMessage("Are you sure you want to remove this photo?")
                                .create()
                                .setCancel("No", null)
                                .setOkay("Yes", new FrontPopup.OkayClick() {
                                    @Override
                                    public void onClick() {
                                        try {
                                            Toast.makeText(context, "Deleting this photo.", Toast.LENGTH_SHORT).show();
                                            String url = AppConfigRemote.BASE_URL.concat("/api/photo/").concat(model.getId() + "/");
                                            new GlobalApiService<>(context, ApiResponseModel.class).delete(url, new CallbackIService<ApiResponseModel>() {
                                                @Override
                                                public void getValue(final ApiResponseModel apiResponseModel) {
                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (apiResponseModel != null) {
                                                                propertyPhotoModels.remove(position);
                                                                notifyItemRemoved(getAdapterPosition());
                                                                Toast.makeText(context, apiResponseModel.message, Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                Toast.makeText(context, context.getString(R.string.error_swwta), Toast.LENGTH_SHORT).show();

                                                            }
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void getError(Exception ex, String message) {
                                                    ActivityHelper.Toast((Activity) context, message);
                                                }
                                            });
                                        } catch (Exception ex) {
                                            ActivityHelper.Toast((Activity) context, context.getString(R.string.error_swwta));
                                            ex.printStackTrace();
                                        }
                                    }
                                }).show();
                        return false;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void clear() {

        }
    }
}
