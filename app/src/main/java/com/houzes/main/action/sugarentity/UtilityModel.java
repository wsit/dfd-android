package com.houzes.main.action.sugarentity;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

@Table(name = "utility_model")
public class UtilityModel extends SugarRecord {
    @Column(name = "utility_name")
    public String utilityName;
    @Column(name = "utility_value")
    public Object utilityValue;

    public UtilityModel() {

    }

    public UtilityModel(String utilityName, Object utilityValue) {
        this.utilityName = utilityName;
        this.utilityValue = utilityValue;
    }

    public String getUtilityName() {
        return utilityName;
    }

    public void setUtilityName(String utilityName) {
        this.utilityName = utilityName;
    }

    public Object getUtilityValue() {
        return utilityValue;
    }

    public void setUtilityValue(Object utilityValue) {
        this.utilityValue = utilityValue;
    }
}
