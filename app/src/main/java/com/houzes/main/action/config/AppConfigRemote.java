package com.houzes.main.action.config;

public class AppConfigRemote {
    public static final String CLIENT_ID = "b4eGPYQSTehw9G6sv36G8mXWXuW3qWd5z3EL4qpq";
    public static final String CLIENT_SECRET = "NzrpAlEBLOE8RN7bbbFSVAF8Q8lm3DqFgUlwLZ5gS7ribxsiLfBO5F659KjDkfgjHNAvvplv8hSsTpjQP5YJTFeMHsTubkGq0Eero6jqWIJgVu8tx29pfvTczKatDKny";
    public static final String GRANT_TYPE = "password";
    public static final String SOCKT_DOMAIN = "https://socket.houzes.com";
    public static final String BASE_URL = "https://api.houzes.com";
    //public static final String BASE_URL = "https://devapi.houzes.com";

    public static String getBASE_URL() {
        return BASE_URL;
    }

    public static final String ALL_TAG_URL = BASE_URL + "/api/tag/";
    public static final String ALL_TEAM_URL = BASE_URL + "/api/team/";
    public static final String ALL_TEAM_LIST = BASE_URL + "/api/list/load-list/";
    public static final String ALL_LIST_URL = BASE_URL + "/api/list/?limit=100";
    public static final String CURRENT_USER_INFO = BASE_URL + "/api/user/get-current-user-info/";
    public static final String PAYMENT_PLAN = BASE_URL + "/api/plan/";
    public static final String PAYMENT_CHARGE = BASE_URL + "/api/payment-gateway/charge-card/";
    public static final String SUBSCRIPTION_CHARGE = BASE_URL + "/api/upgrade-profile/";
    public static final String BULLING_CARD = BASE_URL + "/api/billing-card/";
}
