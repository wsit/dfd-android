package com.houzes.main.action.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.houzes.main.R;
import com.houzes.main.model.sys.SpinnerValueModel;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    private Activity context;
    private List<SpinnerValueModel> list;
    private float fontSize = -1;

    public SpinnerAdapter(Activity context, List<SpinnerValueModel> list) {
        this.list = list;
        this.context = context;
    }

    public SpinnerAdapter(Activity context, List<SpinnerValueModel> list, float size) {
        this.list = list;
        this.fontSize = size;
        this.context = context;
    }

    public SpinnerAdapter(Activity context, @NonNull String[] values) {
        this.context = context;
        this.list = new ArrayList<>();
        for (String s : values) {
            this.list.add(new SpinnerValueModel(s, s));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = ((LayoutInflater) context.getLayoutInflater()).inflate(R.layout.sys_design_spinner_textview, null);
        TextView tv = v.findViewById(R.id.textViewSpinnerItem);
        tv.setText(list.get(position).getName());
        tv.setTag(list.get(position).getTag());

        if (fontSize > 0) {
            tv.setTextSize(fontSize);
        }
        return v;
    }
}