package com.houzes.main.action.service.api;

import android.content.Context;

import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.service.views.iviews.CallbackObject;
import com.houzes.main.action.helper.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ContentAddApiService {
    public static void saveProperty(Context context, String property_address, double latitude, double longitude, final CallbackObject callbackObject) {
        String requestUrl = new AppConfigRemote().getBASE_URL() + "/api/list-properties/create/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        JSONObject postData = new JSONObject();
        try {
            postData.put("property_address", property_address);
            postData.put("latitude", latitude);
            postData.put("longitude", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), postData.toString());

        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .post(requestBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callbackObject.get(null, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        callbackObject.get(responseBody, null);

                    } else {
                        callbackObject.get(null, new Exception("Server Failed To Save The Property"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callbackObject.get(null, e);
                }
            }
        });
    }

    public static void saveHistoryDetailed(Context context, int historyId, int propertyId, final CallbackObject callbackObject) {
        try {
            String requestUrl = new AppConfigRemote().getBASE_URL() + "/api/history-detail/";
            String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
            JSONObject postData = new JSONObject();
            try {
                postData.put("history", historyId);
                postData.put("property", propertyId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), postData.toString());

            final Request httpRequest = new Request.Builder()
                    .header("Authorization", authorization)
                    .url(requestUrl)
                    .post(requestBody)
                    .build();

            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                    callbackObject.get(null, e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        String responseBody = response.body().string();
                        if (response.isSuccessful()) {
                            callbackObject.get(responseBody, null);

                        } else {
                            callbackObject.get(null, new Exception("Server Failed To Save The Property"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        callbackObject.get(null, e);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            callbackObject.get(null, ex);
        }
    }
}
