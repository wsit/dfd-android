package com.houzes.main.action.socket;

import com.houzes.main.action.socket.socket.SocketConnectedUser;
import com.houzes.main.action.socket.socket.SocketShareLocation;

public interface HouzesSocketEventListener {
    void onConnect();

    void onConnected(String email);

    void onError(String message);

    void onLocationUpdateSuccess(SocketConnectedUser socketConnectedUser);

    void onLocationUpdateError(String message);

    void onLocationReceived(SocketShareLocation socketShareLocation);

    void onLocationShareSuccess();

    void onLocationShareError(String message);

    void onStopDriving(int userId);

    void onUserDisconnected(int userId);

    void onDisconnect();
}
