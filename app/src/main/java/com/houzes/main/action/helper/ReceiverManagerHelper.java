package com.houzes.main.action.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.List;

public class ReceiverManagerHelper {

    private static List<BroadcastReceiver> receivers = new ArrayList<BroadcastReceiver>();
    private static ReceiverManagerHelper ref;
    private Context context;

    public ReceiverManagerHelper(Context context) {
        this.context = context;
    }

    public static synchronized ReceiverManagerHelper init(Context context) {
        if (ref == null) ref = new ReceiverManagerHelper(context);
        return ref;
    }

    public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter intentFilter) {
        receivers.add(receiver);
        Intent intent = context.registerReceiver(receiver, intentFilter);

        return intent;
    }

    public BroadcastReceiver isReceiverRegistered(BroadcastReceiver receiver) {
        for (BroadcastReceiver br : receivers) {
            if (br.getClass().getGenericSuperclass().toString().equals(receiver.getClass().getGenericSuperclass().toString())) {
                return br;
            }
        }
        return null;
    }

    public void unregisterReceiver(BroadcastReceiver receiver) {
        BroadcastReceiver br = isReceiverRegistered(receiver);
        if (br != null) {
            try {
                context.unregisterReceiver(br);
                receivers.remove(br);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}