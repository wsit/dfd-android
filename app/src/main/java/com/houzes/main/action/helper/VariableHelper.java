package com.houzes.main.action.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class VariableHelper {

    public static float getRotation(double value) {
        int val = (int) value;
        val = val / 10;
        val = val * 10;
        return val;
    }

    public static Bitmap getBitmapFromDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatResources.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public BitmapDescriptor getMarkerIcon(String hex) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(hex), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    public static double getLength(LatLng o, LatLng n) {
        try {
            double R = 6378.137;
            double dLat = n.latitude * Math.PI / 180 - o.latitude * Math.PI / 180;
            double dLon = n.longitude * Math.PI / 180 - n.longitude * Math.PI / 180;
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(n.latitude * Math.PI / 180) * Math.cos(o.latitude * Math.PI / 180) *
                            Math.sin(dLon / 2) * Math.sin(dLon / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double d = R * c;
            return d * 1000;
        } catch (Exception ex) {
            return -100;
        }
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double rlat1 = Math.PI * lat1 / 180.0f;
        double rlat2 = Math.PI * lat2 / 180.0f;
        double rlon1 = Math.PI * lon1 / 180.0f;
        double rlon2 = Math.PI * lon2 / 180.0f;

        double theta = lon1 - lon2;
        double rtheta = Math.PI * theta / 180.0f;

        double dist = Math.sin(rlat1) * Math.sin(rlat2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.cos(rtheta);
        dist = Math.acos(dist);
        dist = dist * 180.0f / Math.PI;
        dist = dist * 60.0f * 1.1515f;

        if (unit == "K") {
            dist = dist * 1.609344f;
        }
        if (unit == "M") {
            dist = dist * 1.609344 * 1000f;
        }
        if (unit == "N") {
            dist = dist * 0.8684f;
        }
        return dist;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        boolean isAvailable = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            isAvailable = (locationMode != Settings.Secure.LOCATION_MODE_OFF);
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            isAvailable = !TextUtils.isEmpty(locationProviders);
        }

        boolean coarsePermissionCheck = (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        boolean finePermissionCheck = (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        return isAvailable && (coarsePermissionCheck || finePermissionCheck);
    }

    public static int kmToZoom(double km) {
        return (int) ((4 / 5000) * km);
    }

    public static int metToZoom(double meter) {
        meter = meter * 6;
        List<Object[]> l = new ArrayList<>();
        l.add(new Object[]{20, 1128.497220, 0.0});
        l.add(new Object[]{19, 2256.994440, 1128.497220});
        l.add(new Object[]{18, 4513.988880, 2256.994440});
        l.add(new Object[]{17, 9027.977761, 4513.988880});
        l.add(new Object[]{16, 18055.955520, 9027.977761});
        l.add(new Object[]{15, 36111.911040, 18055.955520});
        l.add(new Object[]{14, 72223.822090, 36111.911040});
        l.add(new Object[]{13, 144447.644200, 72223.822090});
        l.add(new Object[]{12, 288895.288400, 144447.644200});
        l.add(new Object[]{11, 577790.576700, 288895.288400});
        l.add(new Object[]{10, 1155581.153000, 577790.576700});
        l.add(new Object[]{9, 2311162.307000, 1155581.153000});
        l.add(new Object[]{8, 4622324.614000, 2311162.307000});
        l.add(new Object[]{7, 9244649.227000, 4622324.614000});
        l.add(new Object[]{6, 18489298.450000, 9244649.227000});
        l.add(new Object[]{5, 36978596.910000, 18489298.450000});
        l.add(new Object[]{4, 73957193.820000, 36978596.910000});
        l.add(new Object[]{3, 147914387.600000, 73957193.820000});
        l.add(new Object[]{2, 295828775.300000, 147914387.600000});
        l.add(new Object[]{1, 591657550.500000, 295828775.300000});

        for (Object[] o : l) {
            int z = (int) o[0];
            double max = (double) o[1];
            double min = (double) o[2];
            if (meter <= max && meter >= min) {
                return z;
            }
        }
        return 15;
    }


    public static CameraUpdate getZoomForDistance(LatLng originalPosition, double distance) {
        LatLng rightBottom = SphericalUtil.computeOffset(originalPosition, distance, 135);
        LatLng leftTop = SphericalUtil.computeOffset(originalPosition, distance, -45);
        LatLngBounds sBounds = new LatLngBounds(new LatLng(rightBottom.latitude, leftTop.longitude), new LatLng(leftTop.latitude, rightBottom.longitude));
        return CameraUpdateFactory.newLatLngBounds(sBounds, 0);

    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static String double2D(double val) {
        String x = new DecimalFormat("#.##").format(val);
        return x.startsWith(".") ? ".".concat(x) : x;
    }
}
