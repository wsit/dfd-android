package com.houzes.main.action.helper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.houzes.main.view.SignInActivity;

import java.net.UnknownHostException;

public class MessageUtil {
    public static String getMessage(Context context, int code) {
        if (code == 401) {
            if (context != null) {
                SharedPreferenceUtil.logOut(context);
                Intent intent = new Intent(context, SignInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            return "Your logon has expired! Please log in again to continue.";
        } else if (code == 500) {
            return "There was an error understanding the request.";
        } else if (code == 502) {
            return "The server is encountering problems.";
        } else if (code == 404) {
            return "Invalid operation! This item was not found.";
        }

        return String.format("There was an error understanding the request [%d]", code);
    }

    public static String getMessage(@NonNull String _message) {
        try {
            String message = _message.toLowerCase();
            if (message.equals("null")) {
                return "Found null value for this operation.";
            } else if (message.contains("failed to connect to")) {
                return "Option not available when offline.";
            } else if (message.contains("timeout")) {
                return "Looks like the server is taking too long to respond, please try again sometime.";
            } else if (message.contains("com.google.gson")) {
                return "Data mapping issues.";
            } else if (message.contains("permission denial")) {
                return "Permission denial.";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "Something went wrong";
    }

    public static String getMessage(@NonNull Exception ex) {
        ex.printStackTrace();
        try {
            if (ex != null && ex.getClass() == UnknownHostException.class) {
                return "Option not available when offline.";
            }

            String message = ex.getMessage();
            return getMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
            ex.printStackTrace();
            return "Something went wrong! Try again.";
        }
    }

    public static String toNaming(@NonNull String name) {
        try {
            String[] strings = name.toLowerCase().split(" ");
            for (int x = 0; x < strings.length; x++) {
                strings[x] = strings[x].substring(0, 1).toUpperCase() + strings[x].substring(1);
            }
            return TextUtils.join(" ", strings);
        } catch (Exception ex) {
            return name;
        }
    }

    public static String getLastNChar(String myString, int lastNum) {
        if (myString != null) {
            if (myString.length() > lastNum) {
                return myString.substring(myString.length() - lastNum);
            } else {
                return myString;
            }
        }
        return "";
    }
}
