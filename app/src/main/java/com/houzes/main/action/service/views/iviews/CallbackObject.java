package com.houzes.main.action.service.views.iviews;

public interface CallbackObject<T> {
    void get(T obj,Exception ex) ;
}
