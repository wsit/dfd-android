package com.houzes.main.action.sugarentity;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Table(name = "polyline_model")
public class PolylineModel extends SugarRecord {
    @SerializedName("latitude")
    private String lat;
    @SerializedName("longitude")
    private String lon;
    private int position = 0;

    public PolylineModel() {
    }

    public PolylineModel(final double lat, final double lon, final int position) {
        this.lat = lat + "";
        this.lon = lon + "";
        this.position = position;
    }


    public double getLat() {
        return Double.valueOf(lat);
    }

    public double getLon() {
        return Double.valueOf(lon);
    }

    public int getPosition() {
        return position;
    }

    public static Map<Integer, List<LatLng>> getListOfList(List<PolylineModel> polylineModels) {
        Map<Integer, List<LatLng>> ret = new HashMap<>();
        for (PolylineModel polylineModel : polylineModels) {
            List<LatLng> list = ret.get(polylineModel.getPosition());
            if (list == null) {
                list = new ArrayList<>();
                list.add(new LatLng(polylineModel.getLat(), polylineModel.getLon()));
                ret.put(polylineModel.getPosition(), list);
            } else {
                list.add(new LatLng(polylineModel.getLat(), polylineModel.getLon()));
            }
        }
        return ret;
    }

    public static Map<Integer, List<LatLng>> getListOfList(PolylineModel[] polylineModels) {
        Map<Integer, List<LatLng>> ret = new HashMap<>();
        for (PolylineModel polylineModel : polylineModels) {
            List<LatLng> list = ret.get(polylineModel.getPosition());
            if (list == null) {
                list = new ArrayList<>();
                list.add(new LatLng(polylineModel.getLat(), polylineModel.getLon()));
                ret.put(polylineModel.getPosition(), list);
            } else {
                list.add(new LatLng(polylineModel.getLat(), polylineModel.getLon()));
            }
        }
        return ret;
    }
}