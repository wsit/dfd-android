package com.houzes.main.action.socket.socket;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.houzes.main.model.SocketDriveType;

public class SocketShareLocation {
    @SerializedName("user_id")
    private int userId;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("angle")
    private float angle;
    @SerializedName("drive_type")
    private String driveType = "";
    @SerializedName("parent")
    private int parent;

    public SocketShareLocation(int userId, double latitude, double longitude, float angle, int parent, SocketDriveType driveType) {
        this.userId = userId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.angle = angle;
        this.parent = parent;
        this.driveType = driveType.name();
    }

    public int getUserId() {
        return userId;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getAngle() {
        return angle;
    }

    public int getParent() {
        return parent;
    }

    public SocketDriveType getDriveType() {
        if (driveType.equals("PUBLIC")) {
            return SocketDriveType.PUBLIC;
        } else if (driveType.equals("PRIVATE")) {
            return SocketDriveType.PRIVATE;
        } else {
            return SocketDriveType.NONE;
        }
    }

    public String toShareString() {
        return new Gson().toJson(this);
    }
}
