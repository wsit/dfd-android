package com.houzes.main.action.service.views.iviews;

import com.houzes.main.model.api.PropertyModel;

public interface HomeListActivityService {
    void hideListActivityToolbar();

    void addSuccess(PropertyModel propertyModel);

    void addFailed(String message);
}
