package com.houzes.main.action.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.houzes.main.view.PropertyDetailsActivity;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.LoadPropertyImage;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.helper.MessageUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LoadHouzesListAdapter extends RecyclerView.Adapter<LoadHouzesListAdapter.ViewHolder> {
    public List<PropertyModel> propertyModels;
    private Context context;

    public LoadHouzesListAdapter(Context context, List<PropertyModel> propertyModels) {
        this.context = context;
        this.propertyModels = propertyModels;

    }


    @NonNull
    @Override
    public LoadHouzesListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        return new LoadHouzesListAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.rcview_swipe_item_property_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LoadHouzesListAdapter.ViewHolder holder, final int position) {
        holder.onBind(position);
        holder.property_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PropertyDetailsActivity.class);
                intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModels.get(position)));
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return propertyModels.size();
    }

    public void add(PropertyModel response) {
        try {
            propertyModels.add(response);
            notifyItemInserted(propertyModels.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addAll(List<PropertyModel> projectItems) {

        try {
            for (PropertyModel response : projectItems) {
                add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class ViewHolder extends AbstractViewHolder {
        public ImageView theme_image;
        public TextView power_trace;
        public TextView address_name;
        public TextView photo_count;
        public TextView note_count;
        public TextView propertyDate;
        public View delete_property;
        public LinearLayout property_info;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            delete_property = itemView.findViewById(R.id.delete_property);
            theme_image = itemView.findViewById(R.id.theme_image);
            power_trace = itemView.findViewById(R.id.power_trace);
            address_name = itemView.findViewById(R.id.address_name);
            photo_count = itemView.findViewById(R.id.photo_count);
            note_count = itemView.findViewById(R.id.note_count);
            propertyDate = itemView.findViewById(R.id.date);
            property_info = itemView.findViewById(R.id.property_info);
        }

        public void onBind(final int position) {
            super.onBind(position);
            try {
                final PropertyModel propertyModel = propertyModels.get(position);
                address_name.setText(propertyModel.getPropertyAddress());
                photo_count.setText(propertyModel.getPhotoCount() + " Photos");
                note_count.setText(propertyModel.getNoteCount() + " Notes");
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(propertyModel.getCreatedAt());
                propertyDate.setText(new SimpleDateFormat("dd MMMM, yyyy").format(date));

                if (propertyModel.getPhotoCount() > 0) {
                    LoadPropertyImage.originalImage((Activity) context, theme_image, propertyModel.getId());
                }

                if (propertyModel.getPowerTraceRequestId() > 0) {
                    power_trace.setVisibility(View.VISIBLE);
                }

                delete_property.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new FrontPopup(context)
                                .setTitle("Delete Property")
                                .setMessage("Are you sure you want to delete this property?")
                                .create()
                                .setCancel("No", null)
                                .setOkay("Yes", new FrontPopup.OkayClick() {
                                    @Override
                                    public void onClick() {
                                        try {
                                            Toast.makeText(context, "Deleting property.", Toast.LENGTH_SHORT).show();
                                            String url = AppConfigRemote.BASE_URL.concat("/api/property/").concat(propertyModel.getId() + "/");
                                            new GlobalApiService<>(context, ApiResponseModel.class).delete(url, new CallbackIService<ApiResponseModel>() {
                                                @Override
                                                public void getValue(final ApiResponseModel apiResponseModel) {
                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (apiResponseModel != null) {
                                                                propertyModels.remove(position);
                                                                notifyItemRemoved(getAdapterPosition());
                                                                Toast.makeText(context, apiResponseModel.message, Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                Toast.makeText(context, context.getString(R.string.error_swwta), Toast.LENGTH_SHORT).show();

                                                            }
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void getError(Exception ex, String message) {
                                                    ActivityHelper.Toast((Activity) context, message);
                                                }
                                            });
                                        } catch (Exception ex) {
                                            ActivityHelper.Toast((Activity) context, MessageUtil.getMessage(ex));
                                        }
                                    }
                                }).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void clear() {
        }
    }
}
