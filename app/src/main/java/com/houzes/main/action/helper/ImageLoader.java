package com.houzes.main.action.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImageLoader {
    private Context context;
    private String url;
    private int pre;
    private int error;

    public ImageLoader with(Context context) {
        this.context = context;
        return this;
    }

    public ImageLoader load(String url) {
        this.url = url;
        return this;
    }

    public ImageLoader pre(int pre) {
        this.pre = pre;
        return this;
    }

    public ImageLoader error(@DrawableRes int error) {
        this.error = error;
        return this;
    }

    public void into(final ImageView imageView) {
        ImageView tempImageView = new ImageView(context);
        Glide.with(context).asGif().load(pre).into(imageView);
        Glide.with(context).load(url).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                imageView.setImageDrawable(resource);
                return false;
            }
        }).into(tempImageView);
    }

}
