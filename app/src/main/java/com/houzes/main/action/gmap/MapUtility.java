package com.houzes.main.action.gmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.socket.socket.SocketDrivingMarker;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.api.HistoryModel;
import com.houzes.main.model.api.HistoryPolyLine;
import com.houzes.main.model.api.IdModel;
import com.houzes.main.model.api.ListOfTagModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MapUtility {
    public static final int[] colorListNormal = new int[]{Color.RED, Color.BLACK, Color.BLUE, Color.parseColor("#dc24ed"), Color.parseColor("#FF7F00")};
    public static final int[] colorListSatellite = new int[]{Color.RED, Color.WHITE, Color.GREEN, Color.parseColor("#dc24ed"), Color.parseColor("#8cd7ff")};

    @ColorInt
    public static int getRandomColor() {
        if (StaticContent.IS_NORMAL_MAP_VIEW) {
            return colorListNormal[new Random().nextInt(4)];
        } else {
            return colorListSatellite[new Random().nextInt(4)];
        }
    }

    public static float bearingBetweenLocations(LatLng oldLatLon, LatLng newLatLon) {
        try {
            double PI = 3.14159;
            double lat1 = oldLatLon.latitude * PI / 180;
            double long1 = oldLatLon.longitude * PI / 180;
            double lat2 = newLatLon.latitude * PI / 180;
            double long2 = newLatLon.longitude * PI / 180;

            double dLon = (long2 - long1);

            double y = Math.sin(dLon) * Math.cos(lat2);
            double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                    * Math.cos(lat2) * Math.cos(dLon);

            double brng = Math.atan2(y, x);

            brng = Math.toDegrees(brng);
            brng = (brng + 360) % 360;

            return (float) brng;
        } catch (Exception ex) {
        }
        return 0;
    }

    public static double distanceKM(double lat1, double lat2, double lon1, double lon2) {
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2);

        double c = 2 * Math.asin(Math.sqrt(a));
        double r = 6371;
        return (c * r);
    }

    public static BitmapDescriptor getIconByType(Context context, boolean isPrivate) {
        if (isPrivate) {
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(context, R.drawable.marker_car_icon_private));
            return bitmapDescriptor;
        } else {
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(context, R.drawable.marker_car_icon_public1));
            return bitmapDescriptor;
        }
    }

    public static MarkerOptions getMarkerOptions(Context context, SocketShareLocation socketShareLocation) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(getIconByType(context, socketShareLocation.getDriveType() == SocketDriveType.PRIVATE ? true : false));
        markerOptions.position(new LatLng(socketShareLocation.getLatitude(), socketShareLocation.getLongitude()));
        markerOptions.anchor(0.5f, 0.5f);
        markerOptions.rotation(socketShareLocation.getAngle());
        return markerOptions;
    }

    public static MarkerOptions getMarkerOptions(Context context, LatLng latLng, float angel, boolean isPrivate) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(getIconByType(context, isPrivate));
        markerOptions.position(new LatLng(latLng.latitude, latLng.longitude));
        markerOptions.anchor(0.5f, 0.5f);
        markerOptions.rotation(angel);
        return markerOptions;
    }

    public static MarkerOptions getMarkerOptions(Context context, SocketDrivingMarker socketUserConnected) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(getIconByType(context, socketUserConnected.driveType.equals(SocketDriveType.PRIVATE) ? true : false));
        markerOptions.position(socketUserConnected.marker.getPosition());
        markerOptions.anchor(0.5f, 0.5f);
        markerOptions.rotation(socketUserConnected.angel);
        return markerOptions;
    }

    public static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap tintImage(Bitmap bitmap, int color) {
        Paint paint = new Paint();
        paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        Bitmap bitmapResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapResult);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return bitmapResult;
    }

    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static BitmapDescriptor getBitmapDescriptorColorFilterByPng(Context context, @DrawableRes int id, String color) {
        try {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), id);
            icon = tintImage(icon, Color.parseColor(color));
            return BitmapDescriptorFactory.fromBitmap(icon);
        } catch (Exception ex) {
            return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), id));
        }
    }

    public static BitmapDescriptor getBitmapDescriptorColorFilterBySvg(Context context, @DrawableRes int id, String color) {
        try {
            Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, id);
            if (color != null) {
                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                DrawableCompat.setTint(wrappedDrawable, Color.parseColor(color));
            }
            return getMarkerIconFromDrawable(unwrappedDrawable);
        } catch (Exception ex) {
            return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), id));
        }
    }

    public static BitmapDescriptor getBitmapDescriptorForPropertyMarker(Context context, IdModel[] idModels) {
        try {
            if (idModels != null && StaticProperty.LIST_OF_TAGS != null && StaticProperty.LIST_OF_TAGS.length > 0 && idModels.length > 0) {
                if (idModels.length == 1) {
                    String markerColor = ListOfTagModel.getColorCode(StaticProperty.LIST_OF_TAGS, idModels[0].getId());
                    BitmapDescriptor getBitmapDescriptorColorFilter = getBitmapDescriptorColorFilterByPng(context, R.drawable.marker_user_selected_tags_one, markerColor);
                    if (getBitmapDescriptorColorFilter != null) {
                        return getBitmapDescriptorColorFilter;
                    }
                } else if (idModels.length > 1) {
                    return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_multi));
                } else {
                    return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_one));
                }
            }
        } catch (Exception ex) {
        }
        return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_one));
    }

    public static BitmapDescriptor getBitmapDescriptorForPropertyMarker(Context context, ListOfTagModel[] listOfTagModels) {
        try {
            if (listOfTagModels != null && StaticProperty.LIST_OF_TAGS != null && StaticProperty.LIST_OF_TAGS.length > 0 && listOfTagModels.length > 0) {
                if (listOfTagModels.length == 1) {
                    String markerColor = ListOfTagModel.getColorCode(StaticProperty.LIST_OF_TAGS, listOfTagModels[0]);
                    BitmapDescriptor getBitmapDescriptorColorFilter = getBitmapDescriptorColorFilterByPng(context, R.drawable.marker_user_selected_tags_one, markerColor);
                    if (getBitmapDescriptorColorFilter != null) {
                        return getBitmapDescriptorColorFilter;
                    }
                } else if (listOfTagModels.length > 1) {
                    return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_multi));
                } else {
                    return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_one));
                }
            }
        } catch (Exception ex) {
        }
        return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_user_selected_tags_one));
    }

    public static BitmapDescriptor getLoadListMarker(Context context, IdModel[] idModels) {
        try {
            if (idModels != null && StaticProperty.LIST_OF_TAGS != null && StaticProperty.LIST_OF_TAGS.length > 0 && idModels.length > 0) {
                if (idModels.length == 1) {
                    String markerColor = ListOfTagModel.getColorCode(StaticProperty.LIST_OF_TAGS, idModels[0].getId());
                    BitmapDescriptor getBitmapDescriptorColorFilter = getBitmapDescriptorColorFilterByPng(context, R.drawable.marker_ll_one, markerColor);
                    if (getBitmapDescriptorColorFilter != null) {
                        return getBitmapDescriptorColorFilter;
                    }
                } else if (idModels.length > 1) {
                    return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_ll_multi));
                }
            }
        } catch (Exception ex) {
        }
        return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker_ll_one));
    }


    public static double distanceMeter(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {
        float R = 6378.137f; // Radius of earth in KM
        double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;
        return d * 1000;
    }

    public static HistoryModel getPolylinesInfo(List<PolylineModel> polylineModels, boolean saveToServer) {
        HistoryModel historyModel = new HistoryModel();

        try {
            double length = 0;
            for (Map.Entry<Integer, List<LatLng>> m : PolylineModel.getListOfList(polylineModels).entrySet()) {
                LatLng pl = null;
                for (LatLng l : m.getValue()) {
                    if (pl != null) {
                        double lll = MapUtility.distanceKM(pl.latitude, l.latitude, pl.longitude, l.longitude);
                        length = length + lll;
                    }
                    pl = l;
                }
            }
            historyModel.setLength(length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (saveToServer) {
            List<HistoryPolyLine> historyPolyLines = new ArrayList<>();
            for (PolylineModel polylineModel : polylineModels) {
                historyPolyLines.add(new HistoryPolyLine(polylineModel.getLat(), polylineModel.getLon(), polylineModel.getPosition()));
            }
            historyModel.setPolylines(new Gson().toJson(historyPolyLines));
        }
        return historyModel;
    }

    public static void rotateMarker(final Marker marker, final float toRotation, final SocketDrivingMarker socketUserFromMap) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = socketUserFromMap.marker.getRotation();
        final long duration = 1000;
        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                float rot = t * toRotation + (1 - t) * startRotation;
                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }
}
