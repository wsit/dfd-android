package com.houzes.main.action.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;

public class ImageHelper {

    public static Drawable xmlStringToDrawable(Context context, String xmlString) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(xmlString));
            return Drawable.createFromXml(context.getResources(), parser);
        } catch (Exception ex) {
            return null;
        }
    }
}
