package com.houzes.main.action.service.api.iapi;

public interface CallbackIService<T> {
    void getValue(T t);

    void getError(Exception ex, String message);
}
