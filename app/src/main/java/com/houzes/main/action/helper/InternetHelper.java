package com.houzes.main.action.helper;

import android.content.Context;
import android.net.ConnectivityManager;

public class InternetHelper {
    public static boolean isConnection(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
