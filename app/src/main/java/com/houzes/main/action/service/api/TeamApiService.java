package com.houzes.main.action.service.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.iapi.TeamIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.TeamModel;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TeamApiService {

    private static final String TAG = "BuildingFlatAPIService";
    private TeamIService teamIService;
    private Context context;
    private AppConfigRemote appConfigRemote;

    public TeamApiService(Context context, TeamIService teamIService) {
        this.context = context;
        this.teamIService = teamIService;
        appConfigRemote = new AppConfigRemote();
    }

    public void getTeamList() {
        String url = appConfigRemote.getBASE_URL() + "/api/team/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                teamIService.onTeamFailed(MessageUtil.getMessage(e));
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        final List<TeamModel> list1 = new Gson().fromJson(new JSONObject(responseBody).get("users").toString(), new TypeReference<List<TeamModel>>() {
                        }.getType());
                        final List<TeamModel> list2 = new Gson().fromJson(new JSONObject(responseBody).get("unregistered_invitations").toString(), new TypeReference<List<TeamModel>>() {
                        }.getType());
                        for (TeamModel t : list1) {
                            t.setIsUser(true);
                        }
                        list1.addAll(list2);
                        teamIService.onTeamGet(list1);
                    } else {
                        teamIService.onTeamFailed((MessageUtil.getMessage(context, response.code())));
                    }

                } catch (Exception e) {
                    teamIService.onTeamFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void deleteTeam(long id, boolean isUser) {
        String url = appConfigRemote.getBASE_URL() + "/api/" + (isUser ? "user" : "team") + "/" + id + "/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .delete()
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                teamIService.onTeamFailed(MessageUtil.getMessage(e));
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        teamIService.onTeamDelete();
                    } else {
                        teamIService.onTeamFailed((MessageUtil.getMessage(context, response.code())));
                    }
                } catch (Exception e) {
                    teamIService.onTeamFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void addMember(String memberEmail) {
        try {
            String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
            String requestUrl = appConfigRemote.getBASE_URL() + "/api/team/";

            MediaType JSON = MediaType.parse("application/json");
            JSONObject postData = new JSONObject();
            postData.put("email", memberEmail);
            postData.put("user", SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_ID, context));

            RequestBody requestBody = RequestBody.create(JSON, postData.toString());
            Request httpRequest = new Request.Builder()
                    .header("Authorization", authorization)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .url(requestUrl)
                    .post(requestBody)
                    .build();


            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    teamIService.onTeamAddFailed(MessageUtil.getMessage(e));
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) {
                    try {
                        System.out.println("onResponse()");
                        String responseBody = response.body().string();
                        if (response.isSuccessful()) {
                            ApiResponseModel<TeamModel> apiResponseModel = new Gson().fromJson(responseBody, new TypeToken<ApiResponseModel<TeamModel>>() {
                            }.getType());
                            if (apiResponseModel.status) {
                                TeamModel teamModel = apiResponseModel.data;
                                teamIService.onTeamAdd(teamModel);
                            } else {
                                teamIService.onTeamMessage(apiResponseModel.message);
                            }
                        } else {
                            teamIService.onTeamAddFailed((MessageUtil.getMessage(context, response.code())));
                        }

                    } catch (Exception e) {
                        teamIService.onTeamAddFailed(MessageUtil.getMessage(e));
                    }
                }
            });
        } catch (Exception e) {
            teamIService.onTeamAddFailed(MessageUtil.getMessage(e));
        }
    }
}
