package com.houzes.main.action.service.api.iapi;

import com.houzes.main.model.api.PropertyPhotoModel;
import com.houzes.main.model.api.list.PropertyPhotoListModel;

public interface PropertyPhotoIService {
    void onFailed(String message);

    void onGetList(PropertyPhotoListModel propertyPhotoListModel);

    void onUpload(PropertyPhotoModel[] propertyPhotoModels);
}
