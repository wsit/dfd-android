package com.houzes.main.action.gmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterItem;
import com.houzes.main.model.api.PropertyLatLngModel;

public class MarkerItem implements ClusterItem {
    private LatLng mPosition;
    private String mTitle;
    private String mSnippet;

    public MarkerItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public MarkerItem(double lat, double lng, PropertyLatLngModel propertyLatLngModel) {
        mTitle = "";
        mPosition = new LatLng(lat, lng);
        mSnippet = new Gson().toJson(propertyLatLngModel);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }
}