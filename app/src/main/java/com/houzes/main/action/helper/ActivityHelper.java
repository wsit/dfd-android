package com.houzes.main.action.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RawRes;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.fragment.BottomPopup;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class ActivityHelper {
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Toolbar setToolbar(Activity activity, int toolbarId, String title) {
        Toolbar toolbar = activity.findViewById(toolbarId);
        toolbar.setTitle(title);
        toolbar.setSubtitleTextColor(activity.getResources().getColor(R.color.toolbar_text_color));
        toolbar.setTitleTextColor(activity.getResources().getColor(R.color.toolbar_text_color));
        toolbar.setBackgroundColor(activity.getResources().getColor(R.color.toolbar_background));
        return toolbar;
    }

    public static Toolbar setToolbar(Activity activity, int toolbarId, @StringRes int title) {
        Toolbar toolbar = activity.findViewById(toolbarId);
        toolbar.setTitle(title);
        toolbar.setSubtitleTextColor(activity.getResources().getColor(R.color.toolbar_text_color));
        toolbar.setTitleTextColor(activity.getResources().getColor(R.color.toolbar_text_color));
        toolbar.setBackgroundColor(activity.getResources().getColor(R.color.toolbar_background));
        return toolbar;
    }

    public static void Toast(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void openURI(Activity activity, String url) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        } catch (Exception ex) {
        }
    }

    public static void mapStyle(final Activity activity, final GoogleMap _googleMap) {
        final View root = activity.findViewById(R.id.map_view_style_layout);
        View map_view_style_button = activity.findViewById(R.id.map_view_style_button);
        View normal_button = activity.findViewById(R.id.normal_button);
        View satellite_button = activity.findViewById(R.id.satellite_button);
        _googleMap.setMapType(SharedPreferenceUtil.getMapStyle(activity));
        map_view_style_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                root.setVisibility(View.VISIBLE);
                root.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_in_right));
            }
        });
        normal_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceUtil.setMapStyle(activity, true);
                _googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                root.setVisibility(View.GONE);
                root.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_out_right));
            }
        });

        satellite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceUtil.setMapStyle(activity, false);
                _googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                root.setVisibility(View.GONE);
                root.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_out_right));
            }
        });
        _googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                root.setVisibility(View.GONE);
                root.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_out_right));
            }
        });
    }

    public static String readRawTextFile(Context ctx, @RawRes int resId) {
        InputStream inputStream = ctx.getResources().openRawResource(resId);
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while ((line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (Exception e) {
            return null;
        }
        return text.toString();
    }

    public static InputStream getInputStream(String str) {
        return (InputStream) new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));

    }

    public static double getOwnerInfoCoin(Context context) {
        if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo() != null) {
            return StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getOwnerInfo();
        }
        return 0.00;
    }

    public static double getPowerTraceCoin(Context context) {
        if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo() != null) {
            return StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPowerTrace();
        }
        return 0.00;
    }

    public static double getPowerTraceCoin() {
        if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo() != null) {
            return StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPowerTrace();
        }
        return 0.00;
    }

    public static double getOwnerInfoCoin() {
        if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo() != null) {
            return StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getOwnerInfo();
        }
        return 0.00;
    }

    public static int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public static MenuItem createMenuItem(MenuItem menuItem, boolean force) {
        MenuItem m = menuItem = new MenuItem() {
            @Override
            public int getItemId() {
                return 0;
            }

            @Override
            public int getGroupId() {
                return 0;
            }

            @Override
            public int getOrder() {
                return 0;
            }

            @Override
            public MenuItem setTitle(CharSequence charSequence) {
                return null;
            }

            @Override
            public MenuItem setTitle(int i) {
                return null;
            }

            @Override
            public CharSequence getTitle() {
                return null;
            }

            @Override
            public MenuItem setTitleCondensed(CharSequence charSequence) {
                return null;
            }

            @Override
            public CharSequence getTitleCondensed() {
                return null;
            }

            @Override
            public MenuItem setIcon(Drawable drawable) {
                return null;
            }

            @Override
            public MenuItem setIcon(int i) {
                return null;
            }

            @Override
            public Drawable getIcon() {
                return null;
            }

            @Override
            public MenuItem setIntent(Intent intent) {
                return null;
            }

            @Override
            public Intent getIntent() {
                return null;
            }

            @Override
            public MenuItem setShortcut(char c, char c1) {
                return null;
            }

            @Override
            public MenuItem setNumericShortcut(char c) {
                return null;
            }

            @Override
            public char getNumericShortcut() {
                return 0;
            }

            @Override
            public MenuItem setAlphabeticShortcut(char c) {
                return null;
            }

            @Override
            public char getAlphabeticShortcut() {
                return 0;
            }

            @Override
            public MenuItem setCheckable(boolean b) {
                return null;
            }

            @Override
            public boolean isCheckable() {
                return false;
            }

            @Override
            public MenuItem setChecked(boolean b) {
                return null;
            }

            @Override
            public boolean isChecked() {
                return false;
            }

            @Override
            public MenuItem setVisible(boolean b) {
                return null;
            }

            @Override
            public boolean isVisible() {
                return false;
            }

            @Override
            public MenuItem setEnabled(boolean b) {
                return null;
            }

            @Override
            public boolean isEnabled() {
                return false;
            }

            @Override
            public boolean hasSubMenu() {
                return false;
            }

            @Override
            public SubMenu getSubMenu() {
                return null;
            }

            @Override
            public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
                return null;
            }

            @Override
            public ContextMenu.ContextMenuInfo getMenuInfo() {
                return null;
            }

            @Override
            public void setShowAsAction(int i) {

            }

            @Override
            public MenuItem setShowAsActionFlags(int i) {
                return null;
            }

            @Override
            public MenuItem setActionView(View view) {
                return null;
            }

            @Override
            public MenuItem setActionView(int i) {
                return null;
            }

            @Override
            public View getActionView() {
                return null;
            }

            @Override
            public MenuItem setActionProvider(ActionProvider actionProvider) {
                return null;
            }

            @Override
            public ActionProvider getActionProvider() {
                return null;
            }

            @Override
            public boolean expandActionView() {
                return false;
            }

            @Override
            public boolean collapseActionView() {
                return false;
            }

            @Override
            public boolean isActionViewExpanded() {
                return false;
            }

            @Override
            public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
                return null;
            }
        };

        if (force) {
            menuItem = m;
        } else {
            if (menuItem == null) {
                menuItem = m;
            }
        }
        return menuItem;
    }

    public static void sendSmsOrPhone(final Activity activity, final String phoneNumber) {
        new BottomPopup().load(activity, R.layout.bottom_popup_call_message, new BottomPopup.GetDialog() {
            @Override
            public void get(BottomSheetDialog bottomSheetDialog) {
                ((TextView) bottomSheetDialog.findViewById(R.id.textViewPhoneNumber)).setText(phoneNumber);
                bottomSheetDialog.findViewById(R.id.phone_call_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                            activity.startActivity(intent);
                        } catch (Exception e) {
                        }
                    }
                });

                bottomSheetDialog.findViewById(R.id.phone_sms_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.parse("smsto:" + Uri.encode(phoneNumber)));
                            activity.startActivity(intent);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public static void sendEmailActivity(final Activity activity, final String userName, final String[] emails) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL, emails);
            i.putExtra(Intent.EXTRA_SUBJECT, "");
            i.putExtra(Intent.EXTRA_TEXT, "\n\n\n\n--------------------------\nBy\n" + userName);

            activity.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (Exception ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
