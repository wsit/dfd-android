package com.houzes.main.action.service.views.iviews;

import android.widget.TextView;

import com.houzes.main.model.api.NotesModel;

public interface NoteEditClickIService {

    void getValue(NotesModel object, TextView title, TextView info);
}
