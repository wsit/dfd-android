package com.houzes.main.action.service.api.iapi;

import android.widget.ImageView;

import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListListModel;

import java.util.Map;

public interface ListIService {
    void onListAdd(UserListModel userListModel);

    void onListAddAssignDone(Map map);

    void onListGet(ListListModel listModels, boolean refresh);

    void onListFailed(String message);

    void onClickEvent(UserListModel userListModel, ImageView privious, ImageView now);
}
