package com.houzes.main.action.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.UriHelper;
import com.houzes.main.model.api.NeighborInfoModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.MailerWizardTempActivity;

public class CurrentNeighboursAdapter extends BaseAdapter {
    private IView iView;
    private Context context;
    private NeighborInfoModel[] neighborInfoModels = new NeighborInfoModel[]{};

    public CurrentNeighboursAdapter(Context context, NeighborInfoModel[] neighborInfoModels, IView iView) {
        this.iView = iView;
        this.context = context;
        this.neighborInfoModels = neighborInfoModels;
    }

    @Override
    public int getCount() {
        return this.neighborInfoModels.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = ((Activity) context).getLayoutInflater().inflate(R.layout.rcview_my_neighbours, null);
        final NeighborInfoModel neighborInfoModel = neighborInfoModels[position];

        /**
         * Owner Name
         */
        if (neighborInfoModel.getOwnershipInfo() != null && neighborInfoModel.getOwnershipInfo().ownerName != null) {
            view.findViewById(R.id.layout_fullname).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_fullname)).setText(MessageUtil.toNaming(neighborInfoModel.getOwnershipInfo().ownerName));

        } else {
            view.findViewById(R.id.layout_fullname).setVisibility(View.GONE);
        }

        /**
         * Owner address
         */
        if (neighborInfoModel.getNeighborAddress() != null) {
            view.findViewById(R.id.layout_address).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_address)).setText(neighborInfoModel.getNeighborAddress());

        } else {
            view.findViewById(R.id.layout_address).setVisibility(View.GONE);
        }

        /**
         * email
         */
        if (neighborInfoModel.getPowerTrace() != null && neighborInfoModel.getPowerTrace().getEmail() != null) {
            view.findViewById(R.id.layout_email).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_email)).setText(TextUtils.join("\n", neighborInfoModel.getPowerTrace().getEmail().toLowerCase().split(",")).replace(" ", ""));
            view.findViewById(R.id.label_email).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityHelper.sendEmailActivity(
                            (Activity) context,
                            StaticContent.CURRENT_USER_MODEL.getFullName(),
                            neighborInfoModel.getPowerTrace().getEmail().toLowerCase().replace(" ", "").trim().split(",")
                    );
                }
            });
        } else {
            view.findViewById(R.id.layout_email).setVisibility(View.GONE);
        }

        /**
         * Twitter profile
         */
        if (neighborInfoModel.getPowerTrace() != null && neighborInfoModel.getPowerTrace().getTwitter() != null) {
            view.findViewById(R.id.layout_twitter).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_twitter)).setText(neighborInfoModel.getPowerTrace().getTwitter());
            view.findViewById(R.id.layout_twitter).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(context, neighborInfoModel.getPowerTrace().getTwitter());
                }
            });

        } else {
            view.findViewById(R.id.layout_twitter).setVisibility(View.GONE);
        }

        /**
         * Facebook profile
         */
        if (neighborInfoModel.getPowerTrace() != null && neighborInfoModel.getPowerTrace().getFacebook() != null) {
            view.findViewById(R.id.layout_facebook).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_facebook)).setText(neighborInfoModel.getPowerTrace().getFacebook());
            view.findViewById(R.id.layout_facebook).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openFacebook(context, neighborInfoModel.getPowerTrace().getFacebook());
                }
            });

        } else {
            view.findViewById(R.id.layout_facebook).setVisibility(View.GONE);
        }

        /**
         * Google profile
         */
        if (neighborInfoModel.getPowerTrace() != null && neighborInfoModel.getPowerTrace().getGoogle() != null) {
            view.findViewById(R.id.layout_google).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_google)).setText(neighborInfoModel.getPowerTrace().getGoogle());
            view.findViewById(R.id.layout_google).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(context, neighborInfoModel.getPowerTrace().getGoogle());
                }
            });

        } else {
            view.findViewById(R.id.layout_google).setVisibility(View.GONE);
        }


        /**
         * Lindkin profile
         */
        if (neighborInfoModel.getPowerTrace() != null && neighborInfoModel.getPowerTrace().getLinkedIn() != null) {
            view.findViewById(R.id.layout_lindkin).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.label_linkedin)).setText(neighborInfoModel.getPowerTrace().getLinkedIn());
            view.findViewById(R.id.layout_lindkin).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(context, neighborInfoModel.getPowerTrace().getLinkedIn());
                }
            });

        } else {
            view.findViewById(R.id.layout_lindkin).setVisibility(View.GONE);
        }

        /**
         * Phone number
         */
        LinearLayout layoutPhone = view.findViewById(R.id.layout_phone);
        LinearLayout allPhoneNumber = view.findViewById(R.id.all_phone_number);
        layoutPhone.setVisibility(View.GONE);
        if (neighborInfoModel.getPowerTrace() != null) {
            if (neighborInfoModel.getPowerTrace().getAllPhoneNumber().size() > 0) {
                layoutPhone.setVisibility(View.VISIBLE);
                allPhoneNumber.setVisibility(View.VISIBLE);
                allPhoneNumber.removeAllViews();
                for (final String ph : neighborInfoModel.getPowerTrace().getAllPhoneNumber()) {
                    TextView tv = (TextView) ((Activity) context).getLayoutInflater().inflate(R.layout.rcview_item_phone_numbers, null);
                    tv.setText(ph);
                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityHelper.sendSmsOrPhone(((Activity) context), (ph + ""));
                        }
                    });
                    allPhoneNumber.addView(tv);
                }
            }
        }

        view.findViewById(R.id.fetch_owner_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iView.OPM(neighborInfoModel.getId(), false, true, (neighborInfoModel.getOwnerStatus() != null ? "Re-fetch " : ""));
            }
        });
        view.findViewById(R.id.fetch_power_trace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iView.OPM(neighborInfoModel.getId(), true, false, (neighborInfoModel.getPowerTraceStatus() != null ? "Re-fetch " : ""));
            }
        });

        view.findViewById(R.id.fetch_mail_wizard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MailerWizardTempActivity.class).putExtra(IntentExtraName.URL.name(), UriHelper.getMailWizard(false, neighborInfoModel.getId())));
            }
        });

        if (neighborInfoModel.getOwnershipInfo() != null) {
            ((ImageView) view.findViewById(R.id.owner_info_icon)).setImageResource(R.drawable.pd_item_owner_info_refetch);
        }
        if (neighborInfoModel.getPowerTrace() != null) {
            ((ImageView) view.findViewById(R.id.power_trace_icon)).setImageResource(R.drawable.pd_item_power_trace_refetch);
        }

        try {
            ImageView loading_oi = view.findViewById(R.id.loading_oi);
            ImageView loading_pt = view.findViewById(R.id.loading_pt);
            if (neighborInfoModel.getOwnerStatus() != null && neighborInfoModel.getOwnerStatus().toLowerCase().contains("request")) {
                loading_oi.setVisibility(View.VISIBLE);
                Glide.with(context).asGif().load(R.raw.loading_oi_pt).into(loading_oi);
            } else {
                loading_oi.setVisibility(View.GONE);
                loading_oi.setImageResource(R.drawable.pd_item_owner_info);
            }

            if (neighborInfoModel.getPowerTraceStatus() != null && neighborInfoModel.getPowerTraceStatus().toLowerCase().contains("request")) {
                loading_pt.setVisibility(View.VISIBLE);
                Glide.with(context).asGif().load(R.raw.loading_oi_pt).into(loading_pt);
            } else {
                loading_pt.setVisibility(View.GONE);
                loading_pt.setImageResource(R.drawable.pd_item_power_trace);
            }
        } catch (Exception ex) {
        }
        return view;
    }

    public interface IView {
        void OPM(final int id, final boolean powerTrace, final boolean ownerInfo, final String re);
    }
}
