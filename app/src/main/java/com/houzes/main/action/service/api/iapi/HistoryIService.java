package com.houzes.main.action.service.api.iapi;

import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.list.HistoryListModel;

public interface HistoryIService<HistoryModel> {
    void getHistory(ApiResponseModel<HistoryModel> historyModelApiResponseModel);

    void getHistoryList(HistoryListModel historyListModel);

    void onHistoryFailed(String message);
}
