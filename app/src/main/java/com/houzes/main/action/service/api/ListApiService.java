package com.houzes.main.action.service.api;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.ListIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListListModel;
import com.houzes.main.model.statics.StaticContent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ListApiService {
    private static final String TAG = "LISTS";
    private ListIService listIService;
    private Context context;
    private AppConfigRemote appConfigRemote;

    public ListApiService(Context context, ListIService _listIService) {
        this.context = context;
        listIService = _listIService;
        appConfigRemote = new AppConfigRemote();
    }

    public void getList(String url, final boolean refresh) {
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(url)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listIService.onListFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        final ListListModel listListModel = new Gson().fromJson(responseBody, ListListModel.class);
                        listIService.onListGet(listListModel, refresh);

                    } else {
                        listIService.onListFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    listIService.onListFailed(MessageUtil.getMessage(e));
                }
            }
        });
    }

    public void addList(String name) {
        final String requestUrl = appConfigRemote.getBASE_URL() + "/api/list/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        RequestBody formBody = RequestBody
                .create(MediaType.parse("application/json"),
                        "{\"name\": \"" + name + "\"}"
                );


        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .post(formBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                listIService.onListFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        ApiResponseModel<UserListModel> apiResponseModel = new Gson().fromJson(responseBody, new TypeToken<ApiResponseModel<UserListModel>>() {
                        }.getType());
                        listIService.onListAdd(apiResponseModel.data);

                    } else {
                        listIService.onListFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    listIService.onListFailed(MessageUtil.getMessage(e));
                }
            }
        });

    }

    public void saveProperty(int user_list, @Nullable Integer _historyID) {
        String requestUrl = appConfigRemote.getBASE_URL() + "/api/property/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(context);
        JsonUtility postData = new JsonUtility()
                .put("street", StaticContent.SELECTED_PROPERTY_DETAILES.getStreet())
                .put("city", StaticContent.SELECTED_PROPERTY_DETAILES.getCity())
                .put("state", StaticContent.SELECTED_PROPERTY_DETAILES.getState())
                .put("zip", StaticContent.SELECTED_PROPERTY_DETAILES.getZip())
                .put("latitude", StaticContent.SELECTED_PROPERTY_DETAILES.getLatitude())
                .put("longitude", StaticContent.SELECTED_PROPERTY_DETAILES.getLongitude())
                .put("user_list", user_list);
        if (_historyID != null && _historyID > 0) {
            postData.put("history", _historyID);
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), postData.toJson());

        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .post(requestBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                listIService.onListFailed(MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        Map map = new Gson().fromJson(responseBody, Map.class);
                        listIService.onListAddAssignDone(map);

                    } else {
                        listIService.onListFailed(MessageUtil.getMessage(context, response.code()));
                    }

                } catch (Exception e) {
                    listIService.onListFailed(MessageUtil.getMessage(e));
                }
            }
        });

    }

    public static void saveProperty(final Context _context, int user_list, @Nullable Integer _historyID, final CallbackIService<PropertyModel> _callbackIService) {
        String requestUrl = AppConfigRemote.getBASE_URL() + "/api/property/";
        String authorization = "Bearer " + SharedPreferenceUtil.getToken(_context);
        JsonUtility postData = new JsonUtility()
                .put("street", StaticContent.SELECTED_PROPERTY_DETAILES.getStreet())
                .put("city", StaticContent.SELECTED_PROPERTY_DETAILES.getCity())
                .put("state", StaticContent.SELECTED_PROPERTY_DETAILES.getState())
                .put("zip", StaticContent.SELECTED_PROPERTY_DETAILES.getZip())
                .put("latitude", StaticContent.SELECTED_PROPERTY_DETAILES.getLatitude())
                .put("longitude", StaticContent.SELECTED_PROPERTY_DETAILES.getLongitude())
                .put("user_list", user_list);
        if (_historyID != null && _historyID > 0) {
            postData.put("history", _historyID);
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), postData.toString());

        final Request httpRequest = new Request.Builder()
                .header("Authorization", authorization)
                .url(requestUrl)
                .post(requestBody)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                _callbackIService.getError(e, MessageUtil.getMessage(e));
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    String responseBody = response.body().string();
                    if (response.isSuccessful()) {
                        ApiResponseModel<PropertyModel> data = new Gson().fromJson(responseBody, new TypeToken<ApiResponseModel<PropertyModel>>() {
                        }.getType());

                        if (data != null && data.status) {
                            _callbackIService.getValue(data.data);
                        } else {
                            _callbackIService.getError(new Exception(""), data.message);
                        }

                    } else {
                        _callbackIService.getError(new Exception(""), MessageUtil.getMessage(_context, response.code()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    _callbackIService.getError(e, MessageUtil.getMessage(e));
                }
            }
        });

    }
}
