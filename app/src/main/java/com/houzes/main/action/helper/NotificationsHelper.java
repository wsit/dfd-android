package com.houzes.main.action.helper;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.houzes.main.R;
import com.houzes.main.action.gmap.CaptureMapScreen;
import com.houzes.main.view.SplashActivity;

import java.util.Random;

public class NotificationsHelper extends ContextWrapper {

    private NotificationManager mManager;
    public static final int ID_FOR_START_DRIVER = 1;
    public static final int ID_FOR_UPDATE_APP = 2;
    public static final int ID = new Random().nextInt();
    public static final String ANDROID_CHANNEL_ID = "hnc" + new Random().nextInt();
    public static final String ANDROID_CHANNEL_NAME = "ac" + new Random().nextInt();
    private Context context;

    public NotificationsHelper(Context base) {
        super(base);
        this.context = base;
        createChannels();
    }

    private void createChannels() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel androidChannel = new NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            androidChannel.enableLights(true);
            androidChannel.enableVibration(true);
            androidChannel.setLightColor(Color.GREEN);
            androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(androidChannel);
        }
    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }

    public Notification.Builder getAndroidChannelNotification(String title, String body) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new Notification.Builder(getApplicationContext(), ANDROID_CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more)
                    .setAutoCancel(true);
        }
        return null;
    }

    public NUModel sendNotification(String title, String body, PendingIntent pendingIntent, Bitmap bitmap, boolean cancel) {
        return sendNotification(title, body, pendingIntent, bitmap, cancel, ID);
    }

    public NUModel sendNotification(String title, String body, PendingIntent pendingIntent, Bitmap bitmap, boolean cancel, int id) {
        String channelId = ANDROID_CHANNEL_ID;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_noti_1)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setColor(Color.parseColor("#24bc74"))
                        .setSound(defaultSoundUri)
                        .setAutoCancel(cancel)
                        .setContentIntent(pendingIntent);
        if (bitmap != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(id, notificationBuilder.build());

        return new NUModel(id, notificationManager, notificationBuilder);
    }

    public void drive(String title, String body) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, "houzes-drive")
                        .setSmallIcon(R.drawable.ic_noti_1)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setColor(Color.parseColor("#24bc74"))
                        .setSound(defaultSoundUri)
                        .setAutoCancel(false)
                        .setOngoing(true)
                        .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity.class), 0));
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("houzes-drive", "houzes-drive", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(ID_FOR_START_DRIVER, notificationBuilder.build());
    }

    public void driveStop() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
    }

    public class NUModel {
        public int id;
        public NotificationCompat.Builder builder;
        public NotificationManager notificationManager;

        public NUModel(int id, NotificationManager notificationManager, NotificationCompat.Builder builder) {
            id = id;
            builder = builder;
            notificationManager = notificationManager;
        }

        public void cancel() {
            notificationManager.cancel(id);
        }
    }
}
