package com.houzes.main.action.service.api.iapi;

import com.houzes.main.model.api.ScoutModel;

import java.util.List;

public interface ScoutIService {

    void onScoutList(List<ScoutModel> scoutListModel);

    void onScoutFailed(String message);

    void onScoutDelete();

    void onScoutAdd(ScoutModel scoutModel);

    void onScoutAddFailed(String message);

}
