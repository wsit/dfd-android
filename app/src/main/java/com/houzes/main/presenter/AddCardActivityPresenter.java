package com.houzes.main.presenter;

import android.app.Activity;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.common.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.CardInfoModel;
import com.houzes.main.model.api.UpgradeInfoModel;
import com.houzes.main.model.enums.CardType;
import com.houzes.main.model.statics.StaticContent;

import java.util.List;

public class AddCardActivityPresenter extends ActivityPresenter {
    private IView iview;

    public AddCardActivityPresenter(IView iview) {
        super((Activity) iview);
        this.iview = iview;
    }

    public void loadAllSavedCard(long delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                iview.showProgressDialog();
                new GlobalApiService<List<CardInfoModel>>(activity, new TypeToken<List<CardInfoModel>>() {
                }.getType()).get(AppConfigRemote.BULLING_CARD, new GlobalIService<List<CardInfoModel>>() {
                    @Override
                    public void onGetData(final List<CardInfoModel> cardInfoModels) {
                        ui(new UiThread() {
                            @Override
                            public void run() {
                                iview.loadAllSavedCard(cardInfoModels);
                                iview.hideProgressDialog();
                            }
                        });
                    }

                    @Override
                    public void onError(final String message) {
                        ui(new UiThread() {
                            @Override
                            public void run() {
                                iview.toastMessage(message);
                                iview.hideProgressDialog();
                            }
                        });
                    }
                });
            }
        }, delay);
    }

    public String applyPaymentForChargeJson(int plan, boolean isSave, @NonNull String amount, @NonNull String cardName, @NonNull String cardNumber, @NonNull String code, @NonNull String exDate) {
        JsonUtility jsonObject = new JsonUtility();
        jsonObject.put("plan", plan);
        jsonObject.put("is_save", isSave);
        jsonObject.put("amount", amount);
        jsonObject.put("card_name", cardName);
        jsonObject.put("card_number", cardNumber);
        jsonObject.put("card_code", code);
        jsonObject.put("expiration_date", exDate);
        return jsonObject.toString();
    }

    public String applyPaymentForSubscriptionJson(int plan, boolean isSave, @NonNull String cardName, @NonNull String cardNumber, @NonNull String code, @NonNull String exDate) {
        JsonUtility jsonObject = new JsonUtility();
        jsonObject.put("plan", plan);
        jsonObject.put("is_save", isSave);
        jsonObject.put("card_name", cardName);
        jsonObject.put("card_number", cardNumber);
        jsonObject.put("card_code", code);
        jsonObject.put("expiration_date", exDate);
        return jsonObject.toString();
    }

    public void applyPayment(String url, String json) {
        iview.showProgressDialog();
        new GlobalApiService<ApiResponseModel<UpgradeInfoModel>>(activity, new TypeToken<ApiResponseModel<UpgradeInfoModel>>() {
        }.getType()).post(url, json, new CallbackIService<ApiResponseModel<UpgradeInfoModel>>() {
            @Override
            public void getValue(final ApiResponseModel<UpgradeInfoModel> upgradeInfoModelApiResponseModel) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.hideProgressDialog();
                        if (upgradeInfoModelApiResponseModel.status) {
                            StaticContent.CURRENT_USER_MODEL.setUpgradeInfo(upgradeInfoModelApiResponseModel.data);
                            iview.applyPayment(upgradeInfoModelApiResponseModel.message);
                        } else {
                            iview.applyPaymentFailed(upgradeInfoModelApiResponseModel.message);
                        }
                    }
                });
            }

            @Override
            public void getError(Exception ex, final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.applyPaymentFailed(message);
                        iview.hideProgressDialog();
                    }
                });
            }
        });

    }

    public void inputEnterAmount(EditText v, boolean hasFocus) {
        try {
            if (hasFocus) {
                int val = Integer.parseInt(v.getText().toString());
                if (val < 10) {
                    v.setText("10");
                }
            }
        } catch (Exception ex) {
        }
    }

    public void inputEnterAmountChange(EditText v, TextView textViewSelectedCardFee) {
        try {
            if (!v.getText().toString().trim().equals("")) {
                int am = Integer.parseInt(v.getText().toString());
                if (am >= 10) {
                    textViewSelectedCardFee.setText("USD" + am);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    public void showHideAllCard(View showOrHideIcon, TextView showOrHideTitle, View myAllCardLayout, View addNewCardLayout) {
        if (showOrHideIcon.getTag() != null) {
            if (showOrHideTitle.getTag() == null) {
                myAllCardLayout.setVisibility(View.VISIBLE);
                myAllCardLayout.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.popup_show));

                addNewCardLayout.setVisibility(View.GONE);

                showOrHideTitle.setTag(1);
                showOrHideTitle.setText("Hide saved card");
                showOrHideIcon.setRotation(180);
            } else {
                myAllCardLayout.setVisibility(View.GONE);
                myAllCardLayout.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.popup_hide));

                addNewCardLayout.setVisibility(View.GONE);
                showOrHideTitle.setTag(null);
                showOrHideTitle.setText("Show saved card");
                showOrHideIcon.setRotation(0);
            }
        } else {
            iview.toastMessage("Save card unavailable");
        }
    }


    public void editInputOnInput(final EditText cardNumberNew, final EditText cardCvvNew, final ImageView cardImage) {
        cardNumberNew.setFilters(new InputFilter[]{new InputFilter.LengthFilter(CardType.UNKNOWN.getMaxCardLength())});
        cardCvvNew.setFilters(new InputFilter[]{new InputFilter.LengthFilter(CardType.UNKNOWN.getSecurityCodeLength())});

        cardNumberNew.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = cardNumberNew.getText().toString();
                CardType cardType = CardType.forCardNumber(str);
                cardImage.setImageResource(cardType.getFrontResource());
                cardNumberNew.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardType.getMaxCardLength())});
                cardCvvNew.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardType.getSecurityCodeLength()), new InputFilter.LengthFilter(cardType.getSecurityCodeLength())});
                cardCvvNew.setHint(cardType.getSecurityCodeName());

                if (str.length() == cardType.getMaxCardLength()) {
                    cardCvvNew.requestFocus();
                }
            }
        });
    }


    public interface IView {
        void loadAllSavedCard(final List<CardInfoModel> cardInfoModels);

        void toastMessage(String message);

        void applyPayment(String message);

        void applyPaymentFailed(String message);

        void showProgressDialog();

        void hideProgressDialog();


    }
}
