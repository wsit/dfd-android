package com.houzes.main.presenter;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.socket.HouzesSocketEventListener;
import com.houzes.main.action.socket.HouzesSocketHandler;
import com.houzes.main.action.socket.socket.SocketConnectedUser;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.model.api.AllPreviousDrivesModel;
import com.houzes.main.model.api.PreviousDrivesModel;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.GoogleAddressModel;

import java.util.List;
import java.util.Locale;

public class HomeActivityPresenter extends ActivityPresenter {
    private IView iView;
    private PendingIntent geofencePendingIntent;

    public HomeActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void loadSocket() {
        try {
            HouzesSocketHandler.listen(StaticContent.SOCKT_DOMAIN, StaticContent.CURRENT_USER_MODEL.getId(), new HouzesSocketEventListener() {
                @Override
                public void onConnect() {
                    Log.d(StaticContent.TAG, "Socket Connecting...");
                }

                @Override
                public void onConnected(String userEmail) {
                    Log.d(StaticContent.TAG, "Socket Connected By " + userEmail);
                }

                @Override
                public void onError(String message) {
                    Log.d(StaticContent.TAG, "Socket Getting Error [" + message + "]");
                }

                @Override
                public void onLocationUpdateSuccess(SocketConnectedUser socketUpdateLocation) {
                    iView.onLocationUpdateSuccess(true);
                    Log.d(StaticContent.TAG, "Socket Update Location");
                }

                @Override
                public void onLocationUpdateError(String message) {
                    iView.onLocationUpdateSuccess(false);
                    Log.d(StaticContent.TAG, "Socket Getting Update Error [" + message + "]");
                }

                @Override
                public void onLocationReceived(final SocketShareLocation socketShareLocation) {
                    Log.d(StaticContent.TAG, "Socket received " + socketShareLocation.getUserId());
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.onLocationReceived(socketShareLocation);
                        }
                    });
                }

                @Override
                public void onLocationShareSuccess() {
                    iView.onLocationShareSuccess(true);
                    Log.d(StaticContent.TAG, "Socket Share Location");
                }

                @Override
                public void onLocationShareError(String message) {
                    iView.onLocationShareSuccess(false);
                    Log.d(StaticContent.TAG, "Socket Getting Share Error [" + message + "]");
                }

                @Override
                public void onStopDriving(int userId) {
                    Log.d(StaticContent.TAG, "Socket Stop By " + userId);
                    iView.onStopDriving(userId);
                }

                @Override
                public void onUserDisconnected(int userId) {
                    Log.d(StaticContent.TAG, "Socket Disconnect By " + userId);
                    iView.onStopDriving(userId);
                }

                @Override
                public void onDisconnect() {
                    Log.d(StaticContent.TAG, "Socket Disconnecting...");
                }
            });
        } catch (Exception ex) {
            Log.d(StaticContent.TAG, "Socket Getting Error [" + ex.getMessage() + "]");
        }
    }

    public void getSelectedAddress(final double latitude, final double longitude) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            if (addresses.size() > 0) {
                                StaticContent.SELECTED_PROPERTY_DETAILES = new GoogleAddressModel(addresses.get(0));
                                iView.getSelectedAddress(StaticContent.SELECTED_PROPERTY_DETAILES.getPerfectAddress());
                            } else {
                                iView.toastMessage("Address Not Found");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                return null;
            }
        }.execute();
    }

    public void fetchAllPropertyByUrl(String url) {
        new GlobalApiService<PropertyLatLngModel[]>(activity, PropertyLatLngModel[].class).getList(url, new GlobalIService<PropertyLatLngModel[]>() {
            @Override
            public void onGetData(final PropertyLatLngModel[] data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            if (data != null) {
                                iView.getPropertyLatLng(data);
                            } else {
                                iView.toastMessage("Property location not loaded");
                            }
                        } catch (Exception ex) {
                            iView.toastMessage(MessageUtil.getMessage(ex));
                        }
                    }
                });

            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(message);
                    }
                });
            }
        });
    }

    public CircleOptions getCircleOptions() {
        return new CircleOptions().center(StaticContent.CURRENT_LAT_LNG).strokeColor(Color.argb(40, 40, 40, 40)).fillColor(Color.argb(100, 150, 150, 150)).radius(2000);

    }

    public void fetchAllPreviousDriveByUrl(String url) {
        new GlobalApiService<PreviousDrivesModel[]>(activity, AllPreviousDrivesModel.class).getList(url, new GlobalIService<AllPreviousDrivesModel>() {
            @Override
            public void onGetData(final AllPreviousDrivesModel data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            if (data != null) {
                                iView.getPreviousDriveLine(data.getHistories());
                                iView.getPropertyLatLng(data.getProperties());
                            } else {
                                iView.toastMessage("Drive history not loaded");
                            }
                        } catch (Exception ex) {
                            iView.toastMessage(MessageUtil.getMessage(ex));
                        }
                    }
                });

            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(message);
                    }
                });
            }
        });
    }

    public PendingIntent getGeofencePendingIntent() {
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent hitGeofenceIntent = new Intent(IntentExtraName.HIT_GEOFENCE.name());
        geofencePendingIntent = PendingIntent.getBroadcast(activity, 0, hitGeofenceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    public void createGeofence(LatLng latLng, float radius) {
        Geofence geofence = new Geofence.Builder()
                .setRequestId("HOUZES-GEOFENCE")
                .setCircularRegion(latLng.latitude, latLng.longitude, radius)
                .setExpirationDuration(60 * 60 * 1000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();

        iView.getGeofencingRequest(new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_EXIT)
                .addGeofence(geofence)
                .build());
    }

    public interface IView {
        void onLocationUpdateSuccess(boolean isSuccess);

        void onLocationReceived(SocketShareLocation socketShareLocation);

        void onLocationShareSuccess(boolean isSuccess);

        void onStopDriving(int userId);

        void toastMessage(String message);

        void driveStart();

        void driveStop(String json);

        void showProgressDialog(String message);

        void hideProgressDialog(boolean isUI);

        void getSelectedAddress(String address);

        void getFilterUrl(int type, String url);

        void getPropertyLatLng(PropertyLatLngModel[] propertyLatLngModels);

        void getPreviousDriveLine(PreviousDrivesModel[] previousDrivesModels);

        void getGeofencingRequest(GeofencingRequest geofencingRequest);
    }
}
