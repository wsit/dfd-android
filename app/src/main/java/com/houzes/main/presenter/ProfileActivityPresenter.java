package com.houzes.main.presenter;

import android.app.Activity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.config.DfdClient;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.CurrentUserModel;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivityPresenter extends ActivityPresenter {
    private IView iView;

    public ProfileActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void getUser() {
        Request httpRequest = new Request.Builder().header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity))).url(AppConfigRemote.CURRENT_USER_INFO).build();
        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.getError(MessageUtil.getMessage(e), null);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        try {
                            String responseBody = response.body().string();
                            if (response.isSuccessful()) {
                                iView.getUser(new Gson().fromJson(responseBody, CurrentUserModel.class), null);
                            } else {
                                iView.getError(MessageUtil.getMessage(activity, response.code()), null);
                            }
                        } catch (Exception e) {
                            iView.getError(MessageUtil.getMessage(e), null);
                        }
                    }
                });
            }
        });
    }

    public void update(Map<String, String> map, final BottomSheetDialog dialog) {
        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry<String, String> item : map.entrySet()) {
            builder.add(item.getKey(), item.getValue());
        }
        Request httpRequest = new Request.Builder()
                .url(String.format("%s/api/user/%s/", AppConfigRemote.BASE_URL, SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_ID, activity)))
                .header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity)))
                .patch(builder.build())
                .build();
        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.getError(MessageUtil.getMessage(e), dialog);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        try {
                            if (response.isSuccessful()) {
                                ApiResponseModel<CurrentUserModel> authApiServiceApiResponseModel = new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<CurrentUserModel>>() {
                                }.getType());
                                if (authApiServiceApiResponseModel.status) {
                                    iView.toastMessage("Update successfully");
                                    iView.getUser(authApiServiceApiResponseModel.data, dialog);
                                } else {
                                    iView.getError(authApiServiceApiResponseModel.message, dialog);
                                }
                            } else {
                                iView.getError(MessageUtil.getMessage(activity, response.code()), dialog);
                            }
                        } catch (Exception e) {
                            iView.getError(MessageUtil.getMessage(e), dialog);
                        }
                    }
                });
            }
        });
    }

    public void updatePhoto(File file) {
        iView.showProgressDialog();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("photo", file.getName(), RequestBody.create(MediaType.parse("image/png"), file))
                .build();

        Request httpRequest = new Request.Builder()
                .url(String.format("%s/api/user/%s/", AppConfigRemote.BASE_URL, SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_ID, activity)))
                .header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity)))
                .patch(requestBody)
                .build();
        DfdClient.init().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.getError(MessageUtil.getMessage(e), null);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        try {
                            if (response.isSuccessful()) {
                                ApiResponseModel<CurrentUserModel> authApiServiceApiResponseModel = new Gson().fromJson(response.body().string(), new TypeToken<ApiResponseModel<CurrentUserModel>>() {
                                }.getType());
                                if (authApiServiceApiResponseModel.status) {
                                    iView.toastMessage("Update successfully");
                                    iView.getUser(authApiServiceApiResponseModel.data, null);
                                } else {
                                    iView.getError(authApiServiceApiResponseModel.message, null);
                                }
                            } else {
                                iView.getError(MessageUtil.getMessage(activity, response.code()), null);
                            }
                        } catch (Exception e) {
                            iView.getError(MessageUtil.getMessage(e), null);
                        }
                    }
                });
            }
        });
    }

    public void showHidePassword(final EditText editText, final ImageView imageView) {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getTag() != null) {
                    editText.setTag(null);
                    imageView.setImageResource(R.drawable.ic_eye_color_java_off);
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    editText.setTag(1);
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    imageView.setImageResource(R.drawable.ic_eye_color_java);
                }
                editText.setSelection(editText.getText().length());
            }
        });
    }

    public void onChangeErrorHide(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editText.setError(null);
            }
        });
    }

    public interface IView {
        void getUser(@NonNull CurrentUserModel currentUserModel, BottomSheetDialog dialog);

        void getError(@NonNull String message, BottomSheetDialog dialog);

        void toastMessage(@NonNull String message);

        void showProgressDialog();

        void hideProgressDialog();
    }
}
