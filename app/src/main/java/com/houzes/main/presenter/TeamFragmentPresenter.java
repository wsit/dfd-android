package com.houzes.main.presenter;

import android.app.Activity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.statics.StaticContent;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TeamFragmentPresenter extends ActivityPresenter {
    private IView iView;

    public TeamFragmentPresenter(IView iView, Activity activity) {
        super(activity);
        this.iView = iView;
    }

    public void loadAllTeamMember() {
        iView.showShimmer();
        Request httpRequest = new Request.Builder().header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity))).url(AppConfigRemote.ALL_TEAM_URL).build();
        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideShimmer();
                        iView.toastMessage(MessageUtil.getMessage(e));
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideShimmer();
                        try {
                            String responseBody = response.body().string();
                            if (response.isSuccessful()) {
                                final List<TeamModel> list1 = new Gson().fromJson(new JSONObject(responseBody).get("users").toString(), new TypeReference<List<TeamModel>>() {
                                }.getType());
                                final List<TeamModel> list2 = new Gson().fromJson(new JSONObject(responseBody).get("unregistered_invitations").toString(), new TypeReference<List<TeamModel>>() {
                                }.getType());
                                for (TeamModel t : list1) {
                                    t.setIsUser(true);
                                }
                                list1.addAll(list2);
                                iView.loadAllTeamMember(list1);
                            } else {
                                iView.toastMessage((MessageUtil.getMessage(activity, response.code())));
                            }

                        } catch (Exception e) {
                            iView.toastMessage(MessageUtil.getMessage(e));
                        }
                    }
                });

            }
        });
    }

    public void deleteTeam(final long id, boolean isUser) {
        iView.showProgress();
        Request httpRequest = new Request.Builder()
                .header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity)))
                .url(AppConfigRemote.BASE_URL.concat("/api/").concat(isUser ? "user" : "team" + "/" + id + "/"))
                .delete()
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgress();
                        iView.toastMessage(MessageUtil.getMessage(e));
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgress();
                        if (response.isSuccessful()) {
                            iView.toastMessage("Member remove successfully");
                            iView.onTeamDelete();
                        } else {
                            iView.toastMessage((MessageUtil.getMessage(activity, response.code())));
                        }
                    }
                });
            }
        });
    }

    public void addMember(String memberEmail, boolean reInvitation) {
        try {
            iView.showProgress();
            MediaType JSON = MediaType.parse("application/json");
            JSONObject postData = new JSONObject();
            postData.put("email", memberEmail);
            postData.put("invitation", reInvitation);
            postData.put("user", StaticContent.CURRENT_USER_MODEL.getId());
            RequestBody requestBody = RequestBody.create(JSON, postData.toString());
            Request httpRequest = new Request.Builder()
                    .header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity)))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .url(AppConfigRemote.ALL_TEAM_URL)
                    .post(requestBody)
                    .build();


            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgress();
                            iView.toastMessage(MessageUtil.getMessage(e));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgress();
                            try {
                                String responseBody = response.body().string();
                                if (response.isSuccessful()) {
                                    ApiResponseModel<TeamModel> apiResponseModel = new Gson().fromJson(responseBody, new TypeToken<ApiResponseModel<TeamModel>>() {
                                    }.getType());
                                    if (apiResponseModel.status) {
                                        iView.addMemberSuccess();
                                        iView.toastMessage(apiResponseModel.message + "");
                                    } else {
                                        iView.toastMessage(apiResponseModel.message);
                                    }
                                } else {
                                    iView.toastMessage((MessageUtil.getMessage(activity, response.code())));
                                }

                            } catch (Exception e) {
                                iView.toastMessage(MessageUtil.getMessage(e));
                            }
                        }
                    });

                }
            });
        } catch (Exception e) {
            iView.toastMessage(MessageUtil.getMessage(e));
        }
    }

    public interface IView {
        void loadAllTeamMember(List<TeamModel> teamModels);

        void addMemberSuccess();

        void onTeamDelete();

        void showShimmer();

        void showProgress();

        void hideShimmer();

        void hideProgress();

        void toastMessage(String message);
    }
}
