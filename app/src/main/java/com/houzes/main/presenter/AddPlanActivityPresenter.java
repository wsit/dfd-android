package com.houzes.main.presenter;

import android.app.Activity;

import com.google.common.reflect.TypeToken;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.model.api.SubscriptionPlanModel;

import java.util.ArrayList;
import java.util.List;

public class AddPlanActivityPresenter extends ActivityPresenter {
    private IView iView;

    public AddPlanActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void getPlanList() {
        iView.showProgressDialog();
        new GlobalApiService<List<SubscriptionPlanModel>>(activity, new TypeToken<List<SubscriptionPlanModel>>() {
        }.getType()).get(AppConfigRemote.PAYMENT_PLAN, new GlobalIService<List<SubscriptionPlanModel>>() {
            @Override
            public void onGetData(final List<SubscriptionPlanModel> paymentPlanModels) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        if (paymentPlanModels != null) {
                            iView.getPlanList(paymentPlanModels);
                        } else {
                            iView.toastMessage(MessageUtil.getMessage("null"));
                            iView.getPlanList(new ArrayList<SubscriptionPlanModel>());
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.toastMessage(message);
                        iView.getPlanList(new ArrayList<SubscriptionPlanModel>());
                    }
                });
            }
        });
    }

    public interface IView {
        void getPlanList(List<SubscriptionPlanModel> paymentPlanModels);

        void toastMessage(String message);

        void showProgressDialog();

        void hideProgressDialog();
    }
}
