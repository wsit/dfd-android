package com.houzes.main.presenter;

import android.app.Activity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.config.DfdClient;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.action.helper.InternetHelper;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.CurrentUserModel;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthActivityPresenter extends ActivityPresenter {
    private IView iView;

    public AuthActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void postEmailForResetPassword(String email) {
        iView.showProgressDialog();
        new GlobalApiService<>(activity, ApiResponseModel.class).post(AppConfigRemote.BASE_URL.concat("/api/forget-password/"), new JsonUtility().put("email", email).toString(), new CallbackIService<ApiResponseModel>() {
            @Override
            public void getValue(final ApiResponseModel apiResponseModel) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        if (apiResponseModel != null) {
                            iView.postSuccess(apiResponseModel.message);
                        } else {
                            iView.postFailed(MessageUtil.getMessage("null"));
                        }
                    }
                });
            }

            @Override
            public void getError(Exception ex, final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.postFailed(message);
                    }
                });
            }
        }, false);
    }

    public void loginByEmail(String email, String password) {
        try {
            iView.showProgressDialog();
            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("username", email)
                    .add("password", password)
                    .add("grant_type", "password")
                    .add("client_id", AppConfigRemote.CLIENT_ID)
                    .add("client_secret", AppConfigRemote.CLIENT_SECRET)
                    .build();

            Request httpRequest = new Request.Builder().url(AppConfigRemote.BASE_URL.concat("/o/token/")).post(formBody).build();

            new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            iView.postFailed(MessageUtil.getMessage(e));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            try {
                                String responseBody = response.body().string();
                                if (response.isSuccessful()) {
                                    JSONObject responseObject = new JSONObject(responseBody);
                                    if (responseObject.getString(SharedPreferenceUtil.ACCESS_TOKEN) != null) {
                                        SharedPreferenceUtil.setToken(activity, responseObject.getString(SharedPreferenceUtil.ACCESS_TOKEN));
                                        iView.postSuccess("Login Success");
                                    }
                                } else {
                                    if (response.code() == 400) {
                                        iView.postFailed("Email Or Password Not Match");
                                    } else if (response.code() == 401) {
                                        iView.postFailed(activity.getString(R.string.error_swwta));
                                    } else {
                                        iView.postFailed(MessageUtil.getMessage(null, response.code()));
                                    }
                                }
                            } catch (Exception e) {
                                iView.postFailed(MessageUtil.getMessage(e));
                            }
                        }
                    });
                }
            });
        } catch (Exception ex) {
            iView.hideProgressDialog();
            iView.postFailed(MessageUtil.getMessage(ex));
        }
    }

    public void loginBySocial(String token, String backend) {
        try {
            iView.showProgressDialog();
            RequestBody formBody = new FormBody.Builder()
                    .add("grant_type", "convert_token")
                    .add("client_id", AppConfigRemote.CLIENT_ID)
                    .add("client_secret", AppConfigRemote.CLIENT_SECRET)
                    .add("backend", backend)
                    .add("token", token)
                    .build();

            Request httpRequest = new Request.Builder().url(new AppConfigRemote().getBASE_URL() + "/auth/convert-token").post(formBody).build();
            new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            iView.postFailed(MessageUtil.getMessage(e));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            try {
                                String responseBody = response.body().string();
                                if (response.isSuccessful()) {
                                    JSONObject responseObject = new JSONObject(responseBody);
                                    if (responseObject.getString(SharedPreferenceUtil.ACCESS_TOKEN) != null) {
                                        SharedPreferenceUtil.setToken(activity, responseObject.getString(SharedPreferenceUtil.ACCESS_TOKEN));
                                        iView.postSuccess("Login Success");
                                    }
                                } else {
                                    if (response.code() == 400) {
                                        iView.postFailed("Invalid Social Account");
                                    } else if (response.code() == 401) {
                                        iView.postFailed(activity.getString(R.string.error_swwta));
                                    } else {
                                        iView.postFailed(MessageUtil.getMessage(null, response.code()));
                                    }
                                }
                            } catch (Exception e) {
                                iView.postFailed(MessageUtil.getMessage(e));
                            }
                        }
                    });
                }
            });
        } catch (Exception ex) {
            iView.hideProgressDialog();
            iView.postFailed(MessageUtil.getMessage(ex));
        }
    }

    public void postReg(RequestBody requestBody) {
        try {
            iView.showProgressDialog();
            Request httpRequest = new Request.Builder().url(AppConfigRemote.BASE_URL.concat("/api/user/")).post(requestBody).build();
            new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            iView.postFailed(MessageUtil.getMessage(e));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            iView.hideProgressDialog();
                            try {
                                if (response.isSuccessful()) {
                                    ApiResponseModel apiResponseModel = new Gson().fromJson(response.body().string(), ApiResponseModel.class);
                                    iView.postSuccess(apiResponseModel.message);
                                } else {
                                    iView.postFailed(MessageUtil.getMessage(null, response.code()));
                                }
                            } catch (Exception e) {
                                iView.postFailed(MessageUtil.getMessage(e));
                            }
                        }
                    });
                }
            });
        } catch (Exception ex) {
            iView.hideProgressDialog();
            iView.postFailed(MessageUtil.getMessage(ex));
        }
    }

    public void getCurrentUserInfo() {
        getCurrentUserInfo(null);
    }

    public void getCurrentUserInfo(String firebaseToken) {
        DfdClient.init().newCall(DfdClient.requestGet(activity, AppConfigRemote.CURRENT_USER_INFO.concat(firebaseToken != null ? firebaseToken : ""))).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(MessageUtil.getMessage(e));
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                String json = response.body().string();
                                CurrentUserModel responseModel = new Gson().fromJson(json, CurrentUserModel.class);
                                if (responseModel != null && responseModel.getFirstName() != null) {
                                    iView.postSuccess(responseModel);
                                } else {
                                    iView.postFailed("Unauthorized");
                                }
                            } catch (Exception ex) {
                                iView.postFailed(MessageUtil.getMessage(ex));
                            }
                        } else {
                            iView.postFailed(MessageUtil.getMessage(null, response.code()));
                        }
                    }
                });
            }
        });
    }

    public void showHidePassword(EditText editText, ImageView imageView) {
        if (editText.getTag() != null) {
            editText.setTag(null);
            imageView.setImageResource(R.drawable.ic_eye_color_java_off);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            editText.setTag(1);
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            imageView.setImageResource(R.drawable.ic_eye_color_java);
        }
        editText.setSelection(editText.getText().length());
    }

    public void onInputChange(final EditText editText, final int minLength) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editText.getText().length() == 0) {
                    editText.setBackgroundResource(R.drawable.bg_sign_form_edittext);
                } else if (editText.getText().length() > minLength) {
                    editText.setBackgroundResource(R.drawable.bg_sign_form_edittext_right);
                } else {
                    editText.setBackgroundResource(R.drawable.bg_sign_form_edittext_wrong);
                }
            }
        });
    }

    public boolean isInternet() {
        boolean val = InternetHelper.isConnection(activity);
        if (!val) {
            iView.toastMessage(activity.getString(R.string.no_internet_1));
        }
        return val;
    }

    public void setMessage(final String message, boolean isUiThread) {
        if (isUiThread) {
            ui(new UiThread() {
                @Override
                public void run() {
                    iView.toastMessage(message);
                }
            });
        } else {
            iView.toastMessage(message);
        }
    }

    public void postFailed(String message) {
        iView.postFailed(message);
    }

    public boolean onFormValidation(EditText firstName, EditText lastName, EditText email, EditText phone, EditText password) {
        boolean valid = true;
        if (!InternetHelper.isConnection(activity)) {
            setMessage(activity.getString(R.string.no_internet_1), false);
            return false;
        }
        if (!isValidString(firstName.getText().toString(), 1)) {
            firstName.setError(activity.getString(R.string.find_fname));
            valid = false;
        }

        if (!isValidString(lastName.getText().toString(), 1)) {
            lastName.setError(activity.getString(R.string.find_lname));
            valid = false;
        }

        if (!FieldUtil.isEmail(email.getText().toString())) {
            email.setError("You'll use this when you log in and if you ever need to reset your password");
            valid = false;
        }

        if (FieldUtil.usaPhoneNumber(phone.getText().toString()) == null) {
            phone.setError(activity.getString(R.string.find_phone));
            valid = false;
        }

        if (!isValidString(password.getText().toString(), 5)) {
            password.setError("Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &).");
            valid = false;
        }
        return valid;
    }

    protected boolean isValidString(String text, int num) {
        String sequence = text;
        return sequence.length() > num;
    }

    public interface IView {
        void postSuccess(@NonNull Object message);

        void postFailed(@NonNull String message);

        void toastMessage(@NonNull String message);

        void showProgressDialog();

        void hideProgressDialog();
    }
}
