package com.houzes.main.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.config.DfdClient;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.service.views.InputFilterMinMax;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.MailWizardSubscriptionsModel;
import com.houzes.main.model.api.MailerWizardTempleteModel;
import com.houzes.main.model.api.OwnerInfoPowerTraceModel;
import com.houzes.main.model.api.ReturnAddressModel;
import com.houzes.main.model.statics.StaticContent;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MailerWizardTempActivityPresenter extends ActivityPresenter {
    private IView iview;

    public MailerWizardTempActivityPresenter(IView iview) {
        super((Activity) iview);
        this.iview = iview;
    }

    public void loadMailerWizardTemplate() {
        iview.shimmerStart();
        new GlobalApiService<ApiResponseModel<MailerWizardTempleteModel[]>>(activity, new TypeToken<ApiResponseModel<MailerWizardTempleteModel[]>>() {
        }.getType()).get(AppConfigRemote.BASE_URL.concat("/api/mail-wizard/all/"), new GlobalIService<ApiResponseModel<MailerWizardTempleteModel[]>>() {
            @Override
            public void onGetData(final ApiResponseModel<MailerWizardTempleteModel[]> data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.shimmerStop();
                        if (data.status) {
                            iview.loadMailerWizardTemplate(data.data);
                        } else {
                            iview.toastMessage(data.message);
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.shimmerStop();
                        iview.toastMessage(message);
                    }
                });
            }
        });
    }

    public void loadPreviousReturnAddress() {
        new GlobalApiService<ApiResponseModel<ReturnAddressModel>>(activity, new TypeToken<ApiResponseModel<ReturnAddressModel>>() {
        }.getType()).get(AppConfigRemote.BASE_URL.concat("/api/mail-wizard-user-info/get/"), new GlobalIService<ApiResponseModel<ReturnAddressModel>>() {
            @Override
            public void onGetData(final ApiResponseModel<ReturnAddressModel> data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        if (data.status) {
                            iview.getPreviousReturnAddress(data.data);
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
            }
        });
    }

    public void loadAllPackage() {
        new GlobalApiService<>(activity, MailWizardSubscriptionsModel[].class).get(AppConfigRemote.BASE_URL.concat("/api/mail-wizard-subscriptions/"), new GlobalIService<MailWizardSubscriptionsModel[]>() {
            @Override
            public void onGetData(final MailWizardSubscriptionsModel[] data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.loadAllPackage(data);
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iview.toastMessage(message);
                    }
                });
            }
        });
    }

    private double totalCost;

    public void inOrDecreaseMwNumber(final EditText mwNumber, final TextView showTotalCost, final double perCost) {
        mwNumber.setFilters(new InputFilter[]{new InputFilterMinMax(1, 200)});
        mwNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int x = Integer.parseInt(mwNumber.getText().toString());
                    totalCost = perCost * x;
                    showTotalCost.setText("Total Cost: $" + VariableHelper.double2D(totalCost));
                } catch (Exception ex) {
                }
            }
        });
    }

    public void postMailWizard(String url, int tempId, int subsId, int frequency, String firstName, String lastName, String email, String phone, String street, String city, String state, String zip, String companyName, String agentCode, String website, Object logoImage, Object coverImage) {
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing");
        progressDialog.show();
        try {
            MultipartBody.Builder multipartBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("tem_item_id", String.valueOf(tempId))
                    .addFormDataPart("subs_id", String.valueOf(subsId))
                    .addFormDataPart("mail_count", String.valueOf(frequency))

                    .addFormDataPart("first_name", firstName)
                    .addFormDataPart("last_name", lastName)
                    .addFormDataPart("email", email)
                    .addFormDataPart("phone_no", phone)
                    .addFormDataPart("address_street", street)
                    .addFormDataPart("address_city", city)
                    .addFormDataPart("address_state", state)
                    .addFormDataPart("address_zip", zip)
                    .addFormDataPart("company_name", companyName)
                    .addFormDataPart("website", website)
                    .addFormDataPart("offer_code", agentCode);

            if (logoImage != null) {
                multipartBody.addFormDataPart("logo", new File(String.valueOf(logoImage)).getName(), RequestBody.create(MediaType.parse("image/png"), new File(String.valueOf(logoImage))));
            }
            if (logoImage != null) {
                multipartBody.addFormDataPart("property_image", new File(String.valueOf(coverImage)).getName(), RequestBody.create(MediaType.parse("image/png"), new File(String.valueOf(coverImage))));
            }

            RequestBody requestBody = multipartBody.build();
            Request httpRequest = new Request.Builder()
                    .url(url)
                    .header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity)))
                    .post(requestBody)
                    .build();
            DfdClient.init().newCall(httpRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            progressDialog.cancel();
                            iview.toastMessage(MessageUtil.getMessage(e.getMessage()));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            progressDialog.cancel();
                            try {
                                if (response.isSuccessful()) {
                                    ApiResponseModel<OwnerInfoPowerTraceModel> apiResponseModel = new Gson().fromJson(response.body().string(), new com.google.gson.reflect.TypeToken<ApiResponseModel<OwnerInfoPowerTraceModel>>() {
                                    }.getType());
                                    if (apiResponseModel != null) {
                                        String message = apiResponseModel.message == null ? activity.getString(R.string.error_swwta) : (apiResponseModel.message.trim().startsWith("[")) ? TextUtils.join("\n", new Gson().fromJson(apiResponseModel.message, String[].class)) : apiResponseModel.message;
                                        try {
                                            if (apiResponseModel.data != null && apiResponseModel.data.upgradeInfo != null) {
                                                StaticContent.CURRENT_USER_MODEL.setUpgradeInfo(apiResponseModel.data.upgradeInfo);
                                            }
                                            if (apiResponseModel.data != null) {
                                                iview.paymentMessage1(message, apiResponseModel.data.payment, apiResponseModel.status);
                                            } else {
                                                iview.paymentMessage1(message, true, apiResponseModel.status);
                                            }
                                        } catch (Exception ex) {
                                            iview.paymentMessage1(MessageUtil.getMessage(ex), true, false);
                                        }
                                    } else {
                                        iview.paymentMessage1(activity.getString(R.string.error_swwta), true, false);
                                    }
                                } else {
                                    iview.toastMessage(MessageUtil.getMessage(activity, response.code()));
                                }
                            } catch (Exception e) {
                                iview.paymentMessage1(activity.getString(R.string.error_swwta), true, false);
                            }
                        }
                    });
                }
            });
        } catch (Exception ex) {
            progressDialog.cancel();
            iview.toastMessage(MessageUtil.getMessage(ex.getMessage()));
        }
    }

    public void aa() {

    }

    public interface IView {
        void loadMailerWizardTemplate(MailerWizardTempleteModel[] mailerWizardTempleteModels);

        void loadAllPackage(MailWizardSubscriptionsModel[] mailWizardSubscriptionsModels);

        void shimmerStart();

        void shimmerStop();

        void toastMessage(String message);

        void getPostMailWizard(String json);

        void getPreviousReturnAddress(ReturnAddressModel returnAddressModel);

        void paymentMessage1(String message, boolean isPayment, boolean isSuccess);
    }
}
