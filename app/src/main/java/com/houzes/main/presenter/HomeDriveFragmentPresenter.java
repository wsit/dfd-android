package com.houzes.main.presenter;

import android.app.Activity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.api.LoadListModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListListModel;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HomeDriveFragmentPresenter extends ActivityPresenter {
    private IView iView;

    public HomeDriveFragmentPresenter(IView iView, Activity activity) {
        super(activity);
        this.iView = iView;
    }

    public void loadAllList() {
        Request httpRequest = new Request.Builder().header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity))).url(AppConfigRemote.BASE_URL + "/api/list/load-list/").build();
        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(MessageUtil.getMessage(e));
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            String responseBody = response.body().string();
                            if (response.isSuccessful()) {
                                LoadListModel[] data = new Gson().fromJson(responseBody, LoadListModel[].class);
                                List<UserListModel> userListModels=new ArrayList<>();
                                for(LoadListModel loadListModel:data){
                                    userListModels.addAll(loadListModel.getList());
                                }
                                iView.allLoadList(userListModels);
                            } else {
                                iView.toastMessage((MessageUtil.getMessage(activity, response.code())));
                            }
                        } catch (Exception e) {
                            iView.toastMessage(MessageUtil.getMessage(e));
                        }
                    }
                });

            }
        });
    }

    public void loadAllTeamMember() {
        Request httpRequest = new Request.Builder().header("Authorization", "Bearer ".concat(SharedPreferenceUtil.getToken(activity))).url(AppConfigRemote.ALL_TEAM_URL).build();
        new OkHttpClient().newCall(httpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(MessageUtil.getMessage(e));
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            String responseBody = response.body().string();
                            if (response.isSuccessful()) {
                                final List<TeamModel> list1 = new Gson().fromJson(new JSONObject(responseBody).get("users").toString(), new TypeReference<List<TeamModel>>() {
                                }.getType());
                                iView.loadAllTeamMember(list1);
                            } else {
                                iView.toastMessage((MessageUtil.getMessage(activity, response.code())));
                            }

                        } catch (Exception e) {
                            iView.toastMessage(MessageUtil.getMessage(e));
                        }
                    }
                });
            }
        });
    }

    public interface IView {
        void loadAllTeamMember(List<TeamModel> teamModels);

        void allLoadList(List<UserListModel> userListModels);

        void toastMessage(String message);
    }
}
