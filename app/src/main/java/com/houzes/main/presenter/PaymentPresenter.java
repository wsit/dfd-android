package com.houzes.main.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.OwnerInfoPowerTraceModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.AddCardActivity;

public class PaymentPresenter extends ActivityPresenter {
    private IView iView;

    public PaymentPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void paymentRequestOrFetch(String url, String json) {
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing");
        progressDialog.show();
        try {
            new GlobalApiService<OwnerInfoPowerTraceModel>(activity, new TypeToken<ApiResponseModel<OwnerInfoPowerTraceModel>>() {
            }.getType()).post(url, json, new CallbackIService<ApiResponseModel<OwnerInfoPowerTraceModel>>() {
                @Override
                public void getValue(final ApiResponseModel<OwnerInfoPowerTraceModel> apiResponseModel) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            progressDialog.cancel();
                            if (apiResponseModel != null) {
                                String message = apiResponseModel.message == null ? activity.getString(R.string.error_swwta) : (apiResponseModel.message.trim().startsWith("[")) ? TextUtils.join("\n", new Gson().fromJson(apiResponseModel.message, String[].class)) : apiResponseModel.message;
                                try {
                                    if (apiResponseModel.data != null && apiResponseModel.data.upgradeInfo != null) {
                                        StaticContent.CURRENT_USER_MODEL.setUpgradeInfo(apiResponseModel.data.upgradeInfo);
                                    }
                                    if (apiResponseModel.data != null) {
                                        iView.paymentMessage(message, apiResponseModel.data.payment, apiResponseModel.status);
                                    } else {
                                        iView.paymentMessage(message, true, apiResponseModel.status);
                                    }
                                } catch (Exception ex) {
                                    iView.paymentMessage(MessageUtil.getMessage(ex), true, false);
                                }
                            } else {
                                iView.paymentMessage(activity.getString(R.string.error_swwta), true, false);
                            }
                        }
                    });
                }

                @Override
                public void getError(Exception ex, final String message) {
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            progressDialog.cancel();
                            iView.paymentMessage(message, true, false);
                        }
                    });
                }
            });
        } catch (Exception ex) {
            progressDialog.cancel();
            iView.paymentMessage(MessageUtil.getMessage(ex), true, false);
        }
    }

    public void setAction(boolean isPayment, boolean success) {
        if (!isPayment) {
            if (StaticContent.CURRENT_USER_MODEL.getInvitedBy() != null && StaticContent.CURRENT_USER_MODEL.getInvitedBy() > 0) {
                Toast.makeText(activity, activity.getString(R.string.contact_admin), Toast.LENGTH_SHORT).show();
            } else {
                activity.startActivity(new Intent(activity, AddCardActivity.class));
            }
        }
    }

    public interface IView {
        void paymentMessage(String message, boolean isPayment, boolean isSuccess);
    }
}
