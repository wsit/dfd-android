package com.houzes.main.presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListPropertyListModel;
import com.houzes.main.model.api.list.PropertyTagsListModel;
import com.houzes.main.model.sys.SpinnerValueModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PropertyPresenter extends ActivityPresenter {
    private IView iView;

    public PropertyPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void getTag() {
        List<SpinnerValueModel> temp = new ArrayList<>();
        temp.add(new SpinnerValueModel("Loading...", null));
        iView.getList(temp);
        new GlobalApiService<UserListModel[]>(activity, UserListModel[].class).get(String.format(AppConfigRemote.BASE_URL.concat("/api/tag/")), new GlobalIService<UserListModel[]>() {
            @Override
            public void onGetData(final UserListModel[] listModels) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        if (listModels != null) {
                            List<SpinnerValueModel> temp = new ArrayList<>();
                            temp.add(new SpinnerValueModel("Choose a Tag", null));
                            for (UserListModel l : listModels) {
                                temp.add(new SpinnerValueModel(l.getName(), l));
                            }
                            iView.getTag(temp);
                        } else {
                            List<SpinnerValueModel> temp = new ArrayList<>();
                            temp.add(new SpinnerValueModel("Choose a Tag", null));
                            iView.getTag(temp);
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        List<SpinnerValueModel> temp = new ArrayList<>();
                        temp.add(new SpinnerValueModel("Choose a Tag", null));
                        iView.getTag(temp);
                        iView.toastMessage(message);
                    }
                });
            }
        });
    }

    public void getList(String url) {
        List<SpinnerValueModel> temp = new ArrayList<>();
        temp.add(new SpinnerValueModel("Loading...", null));
        iView.getList(temp);
        new GlobalApiService<UserListModel[]>(activity, UserListModel[].class).get(url, new GlobalIService<UserListModel[]>() {
            @Override
            public void onGetData(final UserListModel[] listModels) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        //Arrays.sort(listModels);
                        if (listModels != null) {
                            List<SpinnerValueModel> temp = new ArrayList<>();
                            temp.add(new SpinnerValueModel("Choose a List", null));
                            for (UserListModel l : listModels) {
                                temp.add(new SpinnerValueModel(l.getName(), l));
                            }
                            iView.getList(temp);
                        } else {
                            List<SpinnerValueModel> temp = new ArrayList<>();
                            temp.add(new SpinnerValueModel("Choose a List", null));
                            iView.getList(temp);
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        List<SpinnerValueModel> temp = new ArrayList<>();
                        temp.add(new SpinnerValueModel("Choose a List", null));
                        iView.getList(temp);
                        iView.toastMessage(message);
                    }
                });
            }
        });
    }

    public void getProperty(String url) {
        getPropertyNext(url);
        iView.loadNewly();
    }

    public void getPropertyNext(String url) {
        new GlobalApiService(activity, ListPropertyListModel.class).getList(url, new GlobalIService<ListPropertyListModel>() {
            @Override
            public void onGetData(final ListPropertyListModel data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.getPropertyNext(data.next);
                        iView.getPropertyCount(data.count);
                        if (data.results != null) {
                            iView.getProperty(data.results);
                        } else {
                            iView.getProperty(new ArrayList<PropertyModel>());
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {

            }
        });
    }

    public void loadAllTeam() {
        new GlobalApiService<JsonObject>(activity, JsonObject.class).getList(AppConfigRemote.ALL_TEAM_URL, new GlobalIService<JsonObject>() {
            @Override
            public void onGetData(final JsonObject data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            TeamModel[] teamModels = new Gson().fromJson(data.getAsJsonArray("users"), TeamModel[].class);
                            if (teamModels != null) {
                                iView.getAllTeamMate(teamModels);
                            } else {
                                iView.toastMessage("Teammate not found");
                            }
                        } catch (Exception ex) {
                            iView.toastMessage(MessageUtil.getMessage(ex));
                        }
                    }
                });

            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(message);
                    }
                });
            }
        });

    }

    public void getPropertyLatLng(String url, String page) {
        new GlobalApiService<PropertyTagsListModel>(activity, PropertyTagsListModel.class).getList(url, new GlobalIService<PropertyTagsListModel>() {
            @Override
            public void onGetData(final PropertyTagsListModel data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            if (data != null) {
                                iView.getPropertyLatLng(data.results, data.next);
                            } else {
                                iView.toastMessage("Property location not loaded");
                            }
                        } catch (Exception ex) {
                            iView.toastMessage(MessageUtil.getMessage(ex));
                        }
                    }
                });

            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(message);
                    }
                });
            }
        });

    }

    public void getPropertyLatLng(String url) {
        new GlobalApiService<PropertyLatLngModel[]>(activity, PropertyLatLngModel[].class).getList(url, new GlobalIService<PropertyLatLngModel[]>() {
            @Override
            public void onGetData(final PropertyLatLngModel[] data) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        try {
                            if (data != null) {
                                iView.getPropertyLatLng(data, null);
                            } else {
                                iView.toastMessage("Property location not loaded");
                            }
                        } catch (Exception ex) {
                            iView.toastMessage(MessageUtil.getMessage(ex));
                        }
                    }
                });

            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.toastMessage(message);
                    }
                });
            }
        });

    }

    public interface IView {
        void getPropertyNext(String next);

        void getPropertyLatLng(PropertyLatLngModel[] propertyLatLngModels, String next);

        void getPropertyPrevious(String previous);

        void getPropertyCount(int count);

        void toastMessage(@NonNull String message);

        void getProperty(List<PropertyModel> propertyModels);

        void getList(List<SpinnerValueModel> listModels);

        void getTag(List<SpinnerValueModel> listModels);

        void getAllTeamMate(TeamModel[] teamModels);

        void loadNewly();
    }

}
