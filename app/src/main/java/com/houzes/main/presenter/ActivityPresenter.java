package com.houzes.main.presenter;

import android.app.Activity;

public class ActivityPresenter {
    public Activity activity;

    public ActivityPresenter(Activity activity) {
        this.activity = activity;
    }

    protected void ui(final UiThread uiThread) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                uiThread.run();
            }
        });
    }

    public interface UiThread {
        void run();
    }


}
