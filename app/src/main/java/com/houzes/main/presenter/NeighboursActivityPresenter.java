package com.houzes.main.presenter;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.NeighborInfoModel;
import com.houzes.main.model.sys.GoogleAddressModel;
import com.houzes.main.model.sys.SelectedNewNeighbour;

import java.util.List;

public class NeighboursActivityPresenter extends ActivityPresenter {
    private IView iView;

    public NeighboursActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void getCurrentList(int propertyID) {
        iView.showProgressDialog();
        new GlobalApiService<NeighborInfoModel[]>(activity, NeighborInfoModel[].class).get(String.format("%s/api/get-neighborhood/property/%d/", AppConfigRemote.BASE_URL, propertyID), new GlobalIService<NeighborInfoModel[]>() {
            @Override
            public void onGetData(final NeighborInfoModel[] neighborInfoModels) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        if (neighborInfoModels != null) {
                            iView.getCurrentList(neighborInfoModels);
                        } else {
                            iView.toastMessage(MessageUtil.getMessage("null"));
                            iView.getCurrentList(new NeighborInfoModel[]{});
                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ui(new UiThread() {
                    @Override
                    public void run() {
                        iView.hideProgressDialog();
                        iView.toastMessage(message);
                        iView.getCurrentList(new NeighborInfoModel[]{});
                    }
                });
            }
        });
    }

    public void getAddressFromSelectedNewNeighbour(final SelectedNewNeighbour selectedNewNeighbour) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    final List<Address> address = new Geocoder(activity).getFromLocation(selectedNewNeighbour.latitude, selectedNewNeighbour.longitude, 1);
                    ui(new UiThread() {
                        @Override
                        public void run() {
                            try {
                                if (address != null && address.size() > 0) {
                                    GoogleAddressModel googleAddressModel = new GoogleAddressModel(address.get(0));
                                    selectedNewNeighbour.address = googleAddressModel.getAddress();
                                    selectedNewNeighbour.street = googleAddressModel.getStreet();
                                    selectedNewNeighbour.city = googleAddressModel.getCity();
                                    selectedNewNeighbour.state = googleAddressModel.getState();
                                    selectedNewNeighbour.zip = googleAddressModel.getZip();
                                    iView.getSelectedNewNeighbour(selectedNewNeighbour);
                                } else {
                                    iView.toastMessage("Property address not found");
                                }
                            } catch (Exception ex) {
                                iView.toastMessage(MessageUtil.getMessage(ex));
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public interface IView {
        void getCurrentList(NeighborInfoModel[] neighborInfoModels);

        void toastMessage(String message);

        void showProgressDialog();

        void hideProgressDialog();

        void getSelectedNewNeighbour(SelectedNewNeighbour selectedNewNeighbour);
    }
}
