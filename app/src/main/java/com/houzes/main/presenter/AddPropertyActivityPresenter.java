package com.houzes.main.presenter;

import android.app.Activity;
import android.net.Uri;

import com.houzes.main.model.sys.GoogleAddressModel;

import static com.houzes.main.model.statics.StaticContetDrive.CURRENT_LAT_LNG;

public class AddPropertyActivityPresenter extends ActivityPresenter {
    private IView iView;

    public AddPropertyActivityPresenter(IView iView) {
        super((Activity) iView);
        this.iView = iView;
    }

    public void setDestinationUrl(double latitude, double longitude) {
        iView.getDestinationUrl(Uri.parse("http://maps.google.com/maps?saddr=" + CURRENT_LAT_LNG.latitude + "," + CURRENT_LAT_LNG.longitude + "&daddr=" + latitude + "," + longitude));
    }

    public interface IView {
        void getAddress(GoogleAddressModel addressModel);

        void getDestinationUrl(Uri url);

        void cameraPassion(double lat, double lng);
        void hideLayout();
        void showLayout();
    }
}
