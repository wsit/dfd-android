package com.houzes.main.view.fragment.home;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.adapter.SpinnerAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.gmap.MapUtility;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.NotificationsHelper;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.HistotyApiService;
import com.houzes.main.action.service.views.iviews.CallbackObject;
import com.houzes.main.action.socket.HouzesSocketHandler;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.api.HistoryModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.model.sys.SpinnerValueModel;
import com.houzes.main.presenter.HomeActivityPresenter;
import com.houzes.main.presenter.HomeDriveFragmentPresenter;
import com.houzes.main.view.partial.SpinnerPartial;
import com.squareup.okhttp.FormEncodingBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.houzes.main.model.statics.StaticContent.CURRENT_ANGEL;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LAT;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LON;

public class HomeDriveFragment extends Fragment implements HomeDriveFragmentPresenter.IView {

    private View root;
    private Context context;
    private float oldTouchValue;
    private HomeActivityPresenter.IView iView;

    private ViewFlipper driveInfoFlipper;
    private RelativeLayout section0, section1, section2;
    private View step1, step2, step3, step4;
    private TextView title_toast_bar;
    private HomeDriveFragmentPresenter $;
    private float get_pointer = 0;
    private RelativeLayout section0AllItem;
    private List<UserListModel> userListModelList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.$ = new HomeDriveFragmentPresenter(this, (Activity) container.getContext());
        this.root = inflater.inflate(R.layout.fragment_home_bottom, container, false);
        setView();
        return root;
    }

    public HomeDriveFragment load(HomeActivityPresenter.IView iView) {
        this.iView = iView;
        return this;
    }

    private void setView() {
        section0AllItem = root.findViewById(R.id.section_0_all_item);
        title_toast_bar = $.activity.findViewById(R.id.title_toast_bar);
        driveInfoFlipper = root.findViewById(R.id.drive_info_flipper);
        driveInfoFlipper.setInAnimation(context, R.anim.slide_in_right_drive_start);
        driveInfoFlipper.setOutAnimation(context, R.anim.slide_out_left_drive_start);
        driveInfoFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent touchevent) {
                switch (touchevent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        oldTouchValue = touchevent.getX();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        float currentX = touchevent.getX();
                        if (oldTouchValue < currentX) {
                            driveInfoFlipper.setInAnimation(context, android.R.anim.slide_in_left);
                            driveInfoFlipper.setOutAnimation(context, android.R.anim.slide_out_right);
                            driveInfoFlipper.showPrevious();
                        }
                        if (oldTouchValue > currentX) {
                            driveInfoFlipper.setInAnimation(context, R.anim.slide_in_right);
                            driveInfoFlipper.setOutAnimation(context, R.anim.slide_out_left);
                            driveInfoFlipper.showNext();
                        }
                        break;
                    }
                }
                return true;
            }
        });


        section0 = root.findViewById(R.id.section_0);
        section1 = root.findViewById(R.id.section_1);
        section2 = root.findViewById(R.id.section_2);

        step1 = root.findViewById(R.id.step_1);
        step2 = root.findViewById(R.id.step_2);
        step3 = root.findViewById(R.id.step_3);
        step4 = root.findViewById(R.id.step_4);

        section0.setVisibility(View.GONE);
        section2.setVisibility(View.VISIBLE);
        section1.setAnimation(AnimationUtils.loadAnimation(context, R.anim.popup_show));
        section2.setAnimation(AnimationUtils.loadAnimation(context, R.anim.popup_show));

        section0AllItem.measure(section0AllItem.getMeasuredHeight(), section0AllItem.getMeasuredWidth());
        final int section0AllItemHeight = section0AllItem.getMeasuredHeight();

        if (StaticContent.IS_PUBLIC_SHARED_ENABEL) {
            if (StaticContent.IS_PUBLIC_SHARED_ENABEL_RESUME) {
                setStep4(StaticContetDrive.D_TYPE);

            } else {
                setStep3(StaticContetDrive.D_TYPE);
            }
        } else {
            setStep1();
        }

        List<SpinnerValueModel> durations = new ArrayList<>();
        durations.add(new SpinnerValueModel("3 months", 3));
        durations.add(new SpinnerValueModel("6 months", 6));
        durations.add(new SpinnerValueModel("9 months", 9));
        durations.add(new SpinnerValueModel("12 months", 12));
        ((Spinner) root.findViewById(R.id.filter_item_type_durations_spinner)).setAdapter(new SpinnerAdapter($.activity, durations, 12));

        final ImageView img1 = root.findViewById(R.id.img1);
        final ImageView img2 = root.findViewById(R.id.img2);
        final ImageView img3 = root.findViewById(R.id.img3);
        ((TextView) root.findViewById(R.id.total_drive)).setText(StaticContent.TOTAL_DRIVE + "");
        driveInfoFlipper.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                img1.setImageResource(R.drawable.pointer_item_icon_vf_deactive);
                img2.setImageResource(R.drawable.pointer_item_icon_vf_deactive);
                img3.setImageResource(R.drawable.pointer_item_icon_vf_deactive);
                int child = driveInfoFlipper.getDisplayedChild();
                if (child == 0) {
                    ((TextView) root.findViewById(R.id.total_property)).setText(String.valueOf(StaticContent.getTotalAddProperty(context)));
                    img1.setImageResource(R.drawable.pointer_item_icon_vf_active);
                }
                if (child == 1) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            final HistoryModel temp1 = MapUtility.getPolylinesInfo(PolylineModel.listAll(PolylineModel.class), false);
                            $.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView) root.findViewById(R.id.total_drive)).setText(temp1.getLength2DML() + "");
                                    img2.setImageResource(R.drawable.pointer_item_icon_vf_active);
                                }
                            });
                            return null;
                        }
                    }.execute();
                }
                if (child == 2) {
                    ((TextView) root.findViewById(R.id.total_notes)).setText(StaticContent.getTotalDuration(context, -1) + "");
                    img3.setImageResource(R.drawable.pointer_item_icon_vf_active);
                }
            }
        });

        root.findViewById(R.id.hide_section_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                section1.setVisibility(View.GONE);
                driveInfoFlipper.stopFlipping();
                root.findViewById(R.id.i).setVisibility(View.VISIBLE);
                root.findViewById(R.id.ii).setVisibility(View.VISIBLE);
            }
        });

        root.findViewById(R.id.i).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                section0.setVisibility(View.GONE);
                section1.setVisibility(View.VISIBLE);
                driveInfoFlipper.startFlipping();
                section1.setAnimation(AnimationUtils.loadAnimation(context, R.anim.popup_show));
                root.findViewById(R.id.i).setVisibility(View.GONE);
                root.findViewById(R.id.ii).setVisibility(View.GONE);

            }
        });

        root.findViewById(R.id.ii).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                section0.setVisibility(View.GONE);
                section1.setVisibility(View.VISIBLE);
                driveInfoFlipper.startFlipping();
                root.findViewById(R.id.i).setVisibility(View.GONE);
                root.findViewById(R.id.ii).setVisibility(View.GONE);
            }
        });

        root.findViewById(R.id.section_0_close1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                section0.setVisibility(View.GONE);
                section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_down));
            }
        });

        root.findViewById(R.id.m).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (section0.getVisibility() == View.VISIBLE) {
                    get_pointer = 0;
                    section0.setVisibility(View.GONE);
                    section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_down));
                } else {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) section0AllItem.getLayoutParams();
                    params.height = section0AllItemHeight;
                    section0AllItem.setLayoutParams(params);
                    root.findViewById(R.id.hide_section_1).performClick();
                    section0.setVisibility(View.VISIBLE);
                    section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_up));
                }

                if (view.getTag() == null) {
                    $.loadAllTeamMember();
                }
            }
        });

        root.findViewById(R.id.mm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (section0.getVisibility() == View.VISIBLE) {
                    get_pointer = 0;
                    section0.setVisibility(View.GONE);
                    section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_down));
                } else {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) section0AllItem.getLayoutParams();
                    params.height = section0AllItemHeight;
                    section0AllItem.setLayoutParams(params);
                    root.findViewById(R.id.hide_section_1).performClick();
                    section0.setVisibility(View.VISIBLE);
                    section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_up));
                }
                if (view.getTag() == null) {
                    $.loadAllTeamMember();
                }
            }
        });

        root.findViewById(R.id.filter_item_load_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                root.findViewById(R.id.filter_item_load_list).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.ll_pd));
                root.findViewById(R.id.filter_item_previous_drive).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.freeType2));
                root.findViewById(R.id.filter_apply_button).setTag(null);
                root.findViewById(R.id.filter_item_type_ll_child).setVisibility(View.VISIBLE);
                root.findViewById(R.id.filter_item_type_pd_child).setVisibility(View.GONE);
            }
        });

        root.findViewById(R.id.filter_item_previous_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                root.findViewById(R.id.filter_item_load_list).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.freeType2));
                root.findViewById(R.id.filter_item_previous_drive).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.ll_pd));
                root.findViewById(R.id.filter_apply_button).setTag(true);
                root.findViewById(R.id.filter_item_type_ll_child).setVisibility(View.GONE);
                root.findViewById(R.id.filter_item_type_pd_child).setVisibility(View.VISIBLE);

            }
        });
        root.findViewById(R.id.filter_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iView.getFilterUrl(0, AppConfigRemote.BASE_URL);
                ((Spinner) root.findViewById(R.id.filter_team_spinner)).setSelection(0);
            }
        });

        root.findViewById(R.id.filter_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int uId = Integer.valueOf(((Spinner) root.findViewById(R.id.filter_team_spinner)).getSelectedView().getTag() + "");
                    if (uId > 0) {
                        if (iView != null) {
                            section0.setVisibility(View.GONE);
                            section0.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_down));
                            if (view.getTag() == null) {
                                int lId = Integer.valueOf(((Spinner) root.findViewById(R.id.filter_item_type_spinner)).getSelectedView().getTag() + "");
                                iView.getFilterUrl(1, String.format("%s/api/list/%d/load-list/properties/", AppConfigRemote.BASE_URL, lId));
                                Toast.makeText(context, "Please wait...", Toast.LENGTH_SHORT).show();
                            } else {
                                int dId = Integer.valueOf(((Spinner) root.findViewById(R.id.filter_item_type_durations_spinner)).getSelectedView().getTag() + "");
                                iView.getFilterUrl(2, String.format("%s/api/history/previous-drives/user/%d/?month=%d", AppConfigRemote.BASE_URL, uId, dId));
                                Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, "Please select valid list", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        root.findViewById(R.id.apple_home).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float yyy = 0;
                if (get_pointer == 0) {
                    get_pointer = event.getRawY();
                }
                yyy = section0AllItemHeight - (event.getRawY() - get_pointer);
                if (yyy > 10 && yyy < section0AllItemHeight) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) section0AllItem.getLayoutParams();
                    params.height = (int) yyy;
                    section0AllItem.setLayoutParams(params);
                } else if (yyy < 10) {
                    section0.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) section0AllItem.getLayoutParams();
                    params.height = section0AllItemHeight;
                    section0AllItem.setLayoutParams(params);
                }
                return true;
            }
        });

        SpinnerPartial.spinnerSelect((Spinner) root.findViewById(R.id.filter_team_spinner), new SpinnerPartial.SpinnerSelect() {
            @Override
            public void get(int position, String text, Object tag) {
                try {
                    int uId = Integer.valueOf(tag + "");
                    if (uId > 0) {
                        List<SpinnerValueModel> spinnerValueModels = new ArrayList<>();
                        for (UserListModel userListModel : userListModelList) {
                            if (userListModel.getUser() == uId) {
                                spinnerValueModels.add(new SpinnerValueModel(userListModel.getName(), userListModel.getId()));
                            }
                        }
                        ((Spinner) root.findViewById(R.id.filter_item_type_spinner)).setAdapter(new SpinnerAdapter($.activity, spinnerValueModels, 12));
                    }
                } catch (Exception ex) {
                    Log.e(StaticContent.TAG, ex.getMessage());
                }
            }
        });
    }

    private void setStep1() {
        step1.setVisibility(View.VISIBLE);
        step2.setVisibility(View.GONE);
        step3.setVisibility(View.GONE);
        step4.setVisibility(View.GONE);
        section1.setVisibility(View.GONE);
        driveInfoFlipper.stopFlipping();
        step1.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_right));

        root.findViewById(R.id.drive_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setStep2();
                StaticContent.getTotalDuration(context, 0);
                StaticContent.TOTAL_DRIVE = 0;
            }
        });

        StaticContent.IS_PUBLIC_SHARED_ENABEL = false;
        SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL, "false", context);
        SharedPreferenceUtil.setInt(SharedPreferenceUtil.DB_HISTORY_ID, 0, context);
    }

    private void setStep2() {
        step1.setVisibility(View.GONE);
        step2.setVisibility(View.VISIBLE);
        step3.setVisibility(View.GONE);
        step4.setVisibility(View.GONE);
        section1.setVisibility(View.GONE);
        step1.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_out_left));
        step2.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_right));


        root.findViewById(R.id.public_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                driveStart(SocketDriveType.PUBLIC);
            }
        });
        root.findViewById(R.id.private_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                driveStart(SocketDriveType.PRIVATE);
            }
        });
        Toast.makeText(context, "Please choose your driving visibility.", Toast.LENGTH_SHORT).show();
    }

    private void setStep3(SocketDriveType driveType) {
        SharedPreferenceUtil.setDriveType($.activity, driveType);
        title_toast_bar.setText("Drive Started Successfully");
        title_toast_bar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                title_toast_bar.setVisibility(View.GONE);
            }
        }, 3000);


        step1.setVisibility(View.GONE);
        step2.setVisibility(View.GONE);
        step3.setVisibility(View.VISIBLE);
        step4.setVisibility(View.GONE);
        section1.setVisibility(View.VISIBLE);
        driveInfoFlipper.startFlipping();
        step2.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_out_left));
        step3.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_right));

        root.findViewById(R.id.drive_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticContent.IS_PUBLIC_SHARED_ENABEL_RESUME = true;
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL_RESUME, "true", context);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL_RESUME, "true", context);
                setStep4(SocketDriveType.PUBLIC);
            }
        });
        new NotificationsHelper(context).drive("HouZes", "HouZes In Driving");
    }

    private void setStep4(final SocketDriveType driveType) {
        title_toast_bar.setText("PAUSE");
        title_toast_bar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                title_toast_bar.setVisibility(View.GONE);
            }
        }, 3000);

        step1.setVisibility(View.GONE);
        step2.setVisibility(View.GONE);
        step3.setVisibility(View.GONE);
        step4.setVisibility(View.VISIBLE);

        step3.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_out_left));
        step4.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_right));

        root.findViewById(R.id.resume_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //StaticContetDrive.CURRENT_LAT_LON_INDEX++;
                SharedPreferenceUtil.setInt("CURRENT_LAT_LON_INDEX", StaticContetDrive.CURRENT_LAT_LON_INDEX, context);
                StaticContent.IS_PUBLIC_SHARED_ENABEL_RESUME = false;
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.DB_PUBLIC_SHARED_ENABEL_RESUME, "false", context);
                setStep3(driveType);

            }
        });
        root.findViewById(R.id.finish_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                driveStop();
                title_toast_bar.setText("STOPPING...");
                title_toast_bar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        title_toast_bar.setVisibility(View.GONE);
                    }
                }, 7000);
            }
        });

        if (section1.getVisibility() == View.GONE) {
            root.findViewById(R.id.i).setVisibility(View.VISIBLE);
            root.findViewById(R.id.ii).setVisibility(View.VISIBLE);
        } else {
            root.findViewById(R.id.i).setVisibility(View.GONE);
            root.findViewById(R.id.ii).setVisibility(View.GONE);
        }

    }

    private void driveStart(final SocketDriveType driveType) {
        StaticContetDrive.driveStartCalculation(context, false);
        iView.showProgressDialog("Driving starting...");
        try {
            HouzesSocketHandler.shareUserLocation(new SocketShareLocation(StaticContent.CURRENT_USER_MODEL.getId(), CURRENT_LAT, CURRENT_LON, CURRENT_ANGEL, StaticContent.getParentUserId(), driveType));
            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder().add("start_point_latitude", CURRENT_LAT + "").add("start_point_longitude", CURRENT_LON + "").add("start_time", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSS'Z'").format(new Date()));
            HistotyApiService.addStartPoint(context, formEncodingBuilder, new CallbackObject() {
                @Override
                public void get(Object obj, Exception ex) {
                    iView.hideProgressDialog(false);
                    if (obj != null) {
                        if (ex == null) {
                            StaticContent.MY_HISTORY_ID = ((HistoryModel) obj).getId();
                            StaticContetDrive.driveStartCalculation(context, true);
                            $.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setStep3(driveType);
                                }
                            });
                            StaticContetDrive.CURRENT_LAT_LON_INDEX = 0;
                            SharedPreferenceUtil.setInt("CURRENT_LAT_LON_INDEX", 0, context);
                            ActivityHelper.Toast($.activity, "Drive Start Successfully");
                        } else {
                            ActivityHelper.Toast($.activity, "Drive Start Failed");
                        }
                    } else {
                        ActivityHelper.Toast($.activity, "Drive Start Failed");
                    }
                }
            });
        } catch (Exception ex) {
            iView.hideProgressDialog(true);
            ActivityHelper.Toast($.activity, "Drive Start Failed");
        }
    }

    private void driveStop() {
        StaticContetDrive.driveStopCalculation(context, false);
        iView.showProgressDialog("Driving stopping...");
        HistoryModel historyModelTemp = MapUtility.getPolylinesInfo(PolylineModel.listAll(PolylineModel.class), true);
        HistotyApiService.addEndPoint(context, historyModelTemp.getLength(), historyModelTemp.getPolylines(), new CallbackObject() {
            @Override
            public void get(final Object obj, final Exception ex) {
                iView.hideProgressDialog(false);
                if (obj != null) {
                    StaticContetDrive.driveStopCalculation(context, true);
                    $.activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setStep1();
                            StaticContetDrive.CURRENT_LAT_LON_INDEX = 0;
                            SharedPreferenceUtil.setInt("CURRENT_LAT_LON_INDEX", 0, context);
                            iView.toastMessage("Driving stopped.");
                            iView.driveStop(new Gson().toJson(obj));
                        }
                    });
                } else {
                    ActivityHelper.Toast($.activity, "Drive Stop Failed");
                    if (ex != null) {
                        ActivityHelper.Toast($.activity, ex.getMessage());
                    } else {
                        ActivityHelper.Toast($.activity, "Driving stopped failed by unknown error.");
                    }
                }
            }
        });
    }

    @Override
    public void loadAllTeamMember(List<TeamModel> teamModels) {
        $.loadAllList();
        root.findViewById(R.id.m).setTag(true);
        root.findViewById(R.id.mm).setTag(true);
        List<SpinnerValueModel> spinnerValueModels = new ArrayList<>();
        spinnerValueModels.add(new SpinnerValueModel("Myself", StaticContent.SIGN_IN_USER_ID));
        for (TeamModel teamModel : teamModels) {
            if (teamModel.getId() != StaticContent.SIGN_IN_USER_ID) {
                String name = (teamModel.getFirstName() + " " + teamModel.getLastName());
                spinnerValueModels.add(new SpinnerValueModel(name, teamModel.getId()));
            }
        }
        ((Spinner) root.findViewById(R.id.filter_team_spinner)).setAdapter(new SpinnerAdapter($.activity, spinnerValueModels, 12));
    }

    @Override
    public void allLoadList(final List<UserListModel> userListModels) {
        userListModelList = userListModels;
        List<SpinnerValueModel> spinnerValueModels = new ArrayList<>();
        for (UserListModel userListModel : userListModelList) {
            if (userListModel.getUser() == StaticContent.SIGN_IN_USER_ID) {
                spinnerValueModels.add(new SpinnerValueModel(userListModel.getName(), userListModel.getId()));
            }
        }
        ((Spinner) root.findViewById(R.id.filter_item_type_spinner)).setAdapter(new SpinnerAdapter($.activity, spinnerValueModels, 12));
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
    }
}
