package com.houzes.main.view;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;


public abstract class AppBaseBackActivity extends AppCompatActivity {

    protected Toolbar toolbar;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        toolbar = ActivityHelper.setToolbar(this, R.id.toolbar, "");
        toolbar.setNavigationIcon(R.drawable.toolbar_back_arrow);
        toolbar.getContentInsetLeft();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
    }
}