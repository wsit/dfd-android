package com.houzes.main.view.fragment.team;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.action.adapter.team.ScoutPostAdapter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;
import com.houzes.main.model.api.ScoutModel;
import com.houzes.main.action.service.api.ScoutApiService;
import com.houzes.main.action.service.api.iapi.ScoutIService;
import com.houzes.main.action.helper.FieldUtil;

import java.util.List;

public class ScoutFragment extends Fragment {
    ImageView add_member;
    SwipeRefreshLayout scout_swipe_refresh_recycler_view;
    RecyclerView scout_recycler_view;
    ScoutApiService scoutApiService;
    ProgressDialog progressDialog;

    private MyShimmerFrameLayout shimmerFrameLayout;
    private ScoutPostAdapter scoutPostAdapter;
    private View noDataFound;

    private Context context;
    private Activity activity;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.activity = (Activity) container.getContext();
        this.root = inflater.inflate(R.layout.fragment_scout, container, false);
        this.startView();
        return root;
    }

    private void startView() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing...");
        add_member = root.findViewById(R.id.floatingActionButton);
        scout_swipe_refresh_recycler_view = root.findViewById(R.id.scoutListSwipeRefresh);
        scout_recycler_view = root.findViewById(R.id.scout_recycler_view);
        scout_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        noDataFound = root.findViewById(R.id.no_data_found);
        shimmerFrameLayout = root.findViewById(R.id.shimmer_view);
        shimmerFrameLayout.startNow();

        loadApi();
        scout_swipe_refresh_recycler_view.setEnabled(false);
        scoutApiService.getScoutList();
    }

    private void loadApi() {
        scoutApiService = new ScoutApiService(context, new ScoutIService() {
            @Override
            public void onScoutList(final List<ScoutModel> list) {
                try {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            scoutPostAdapter = new ScoutPostAdapter(list, context, scoutApiService);
                            scout_recycler_view.setAdapter(scoutPostAdapter);
                            scout_swipe_refresh_recycler_view.setRefreshing(false);

                            if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                                shimmerFrameLayout.stopNow();
                            }

                            if (scoutPostAdapter != null && scoutPostAdapter.scoutModels != null && scoutPostAdapter.scoutModels.size() > 0) {
                                noDataFound.setVisibility(View.GONE);
                            } else {
                                noDataFound.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onScoutFailed(final String message) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        scout_swipe_refresh_recycler_view.setRefreshing(false);
                        if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                            shimmerFrameLayout.stopNow();
                        }
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onScoutDelete() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Your Scout Deleted.", Toast.LENGTH_SHORT).show();
                        shimmerFrameLayout.startNow();
                        scoutApiService.getScoutList();
                    }
                });
            }

            @Override
            public void onScoutAdd(final ScoutModel scoutModel) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        shimmerFrameLayout.startNow();
                        scoutApiService.getScoutList();
                        Toast.makeText(context, "New Scout Added.", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onScoutAddFailed(String message) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        Toast.makeText(context, "An Error Occurred While Adding A Scout.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        scout_swipe_refresh_recycler_view.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                scoutApiService.getScoutList();
                shimmerFrameLayout.startNow();
                scout_swipe_refresh_recycler_view.setRefreshing(false);
            }
        });

        add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomPopup().startWithEditText(context, R.layout.bottom_popup_add_scout, new BottomPopup.GetDialog() {
                    @Override
                    public void get(final BottomSheetDialog dialog) {
                        dialog.findViewById(R.id.add_scout_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String fName = ((EditText) dialog.findViewById(R.id.scout_first_name)).getText().toString();
                                        String lName = ((EditText) dialog.findViewById(R.id.scout_second_name)).getText().toString();
                                        String email = ((EditText) dialog.findViewById(R.id.scout_second_email)).getText().toString();
                                        String phone = ((EditText) dialog.findViewById(R.id.scout_second_phone)).getText().toString();
                                        if (!FieldUtil.isEmail(email)) {
                                            ((EditText) dialog.findViewById(R.id.scout_second_email)).setError("Invalid Email Address");
                                        } else if (FieldUtil.usaPhoneNumber(phone) == null) {
                                            ((EditText) dialog.findViewById(R.id.scout_second_phone)).setError("Invalid Phone Number");
                                        } else if (fName.length() < 2) {
                                            ((EditText) dialog.findViewById(R.id.scout_first_name)).setError("At Least 3 Char");
                                        } else {
                                            scoutApiService.addScout(fName, lName, email, phone);
                                            dialog.cancel();
                                            progressDialog.show();
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}
