package com.houzes.main.view.fragment.home;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.model.sys.GoogleAddressModel;
import com.houzes.main.view.HomeActivity;
import com.houzes.main.view.PropertyDetailsActivity;

import java.util.List;
import java.util.Locale;

import static com.houzes.main.model.statics.StaticContent.CURRENT_LAT_LNG;
import static com.houzes.main.model.statics.StaticContent.IS_PUBLIC_SHARED_ENABEL;

public class HomeMarkerAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private RelativeLayout pointerLayout;
    private IView iView;

    public HomeMarkerAdapter(Context context, RelativeLayout pointerLayout) {
        this.context = context;
        this.iView = (IView) context;
        this.pointerLayout = pointerLayout;
    }

    @Override
    public View getInfoWindow(Marker arg0) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        boolean loadPopup = true;
        try {
            PropertyModel json = (PropertyModel) marker.getTag();
            if (json != null) {
                context.startActivity(new Intent(context, PropertyDetailsActivity.class).putExtra(IntentExtraName.PROPERTY_DATA.name(), json.getJson()));
                loadPopup = false;
            }
        } catch (Exception ex) {

        } finally {
            if (loadPopup) {
                if (marker.getTag() == null) {
                    ((Activity) context).findViewById(R.id.driver_info_section).setVisibility(View.VISIBLE);
                    ((Activity) context).findViewById(R.id.driver_info_section).findViewById(R.id.driver_info_section).setAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_right));
                }
                return null;
            } else {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.map_marker_info_window, null);
                v.setVisibility(View.GONE);
                final TextView pointer_item_text = pointerLayout.findViewById(R.id.pointer_item_text);
                final ImageView pointer_item_cancel = pointerLayout.findViewById(R.id.pointer_item_cancel);
                ImageView pointer_item_love = pointerLayout.findViewById(R.id.pointer_item_love);
                ImageView pointer_item_list = pointerLayout.findViewById(R.id.pointer_item_list);
                ImageView pointer_item_note = pointerLayout.findViewById(R.id.pointer_item_note);

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {

                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                                    final List<Address> addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);

                                    if (addresses.size() > 0) {
                                        StaticContent.SELECTED_PROPERTY_DETAILES = new GoogleAddressModel(addresses.get(0));
                                        pointerLayout.setVisibility(View.VISIBLE);
                                        pointer_item_text.setText(StaticContent.SELECTED_PROPERTY_DETAILES.getPerfectAddress());

                                    } else {
                                        pointerLayout.setVisibility(View.GONE);
                                        Toast.makeText(context, "Address Not Found", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        return null;
                    }
                }.execute();

                pointer_item_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pointerLayout.setVisibility(View.GONE);
                        iView.hideHomeMarkerInfo();
                    }
                });

                pointer_item_love.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iView.propertyAddToList();
                    }
                });


                pointer_item_list.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_ID > 0) {
                            Intent intent = new Intent(context, PropertyDetailsActivity.class);
                            intent.putExtra(IntentExtraName.HOME_MAP_MARKER_LIST_ITEM_ADDRESS_INFO.name(), new Gson().toJson(StaticContent.SELECTED_PROPERTY_DETAILES));
                            context.startActivity(intent);
                        } else {
                            iView.propertyAddToList();
                        }
                    }
                });


                pointer_item_note.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("http://maps.google.com/maps?saddr=" + CURRENT_LAT_LNG.latitude + "," + CURRENT_LAT_LNG.longitude + "&daddr=" + marker.getPosition().latitude + "," + marker.getPosition().longitude);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.google.android.apps.maps");
                        try {
                            context.startActivity(intent);
                        } catch (ActivityNotFoundException ex) {
                            try {
                                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, uri);
                                ((Activity) context).startActivity(unrestrictedIntent);
                            } catch (ActivityNotFoundException innerEx) {
                                Toast.makeText(context, "Please install google maps application", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });


                if (IS_PUBLIC_SHARED_ENABEL) {
                    pointer_item_note.setVisibility(View.GONE);
                    pointer_item_list.setVisibility(View.VISIBLE);
                } else {
                    pointer_item_note.setVisibility(View.VISIBLE);
                    pointer_item_list.setVisibility(View.GONE);
                }
                return v;
            }

        }
    }

    public interface IView {
        void hideHomeMarkerInfo();

        void propertyAddToList();
    }
}