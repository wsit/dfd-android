package com.houzes.main.view.fragment;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.shimmer.ShimmerFrameLayout;

public class MyShimmerFrameLayout extends ShimmerFrameLayout {


    public MyShimmerFrameLayout(Context context) {
        super(context);
    }

    public MyShimmerFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyShimmerFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyShimmerFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void startNow() {
        setAlpha(1f);
        setVisibility(View.VISIBLE);
        startShimmer();
    }

    public void stopNow() {
        final ValueAnimator animator = ValueAnimator.ofInt(1, 100);
        animator.setDuration(800);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                int xx = (int) animation.getAnimatedValue();
                setAlpha(1 - ((float) xx / 100));
                if (xx > 90) {
                    setVisibility(View.GONE);
                    stopShimmer();
                }
            }
        });
        animator.start();
    }

    public void stopNow(Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ValueAnimator animator = ValueAnimator.ofInt(1, 100);
                animator.setDuration(800);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int xx = (int) animation.getAnimatedValue();
                        setAlpha(1 - ((float) xx / 100));
                        if (xx > 90) {
                            setVisibility(View.GONE);
                            stopShimmer();
                        }
                    }
                });
                animator.start();
            }
        });
    }
}
