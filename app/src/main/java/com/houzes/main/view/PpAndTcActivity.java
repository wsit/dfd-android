package com.houzes.main.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.houzes.main.R;

public class PpAndTcActivity extends AppBaseBackActivity {

    private WebView webView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_pp_tc);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Pleas wait");
        progressDialog.setCancelable(false);
        webView = findViewById(R.id.web_view);

        ((TextView) findViewById(R.id.header_title_text)).setText(getIntent().getExtras().getString("title", "Terms & Conditions"));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PpAndTcActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.show();
                        webView.loadUrl(getIntent().getExtras().getString("url", "https://houzes.com/assets/page/privacy-policy-emulate.html"));
                    }
                });
            }
        }, 500);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            public void onLoadResource(WebView view, String url) {
            }

            public void onPageFinished(WebView view, String url) {
                progressDialog.cancel();
            }
        });
    }
}
