package com.houzes.main.view;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.squareup.picasso.Picasso;


public abstract class AppBaseActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static View currentView = null;
    private NavigationView navigationView;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        if (this.getClass() == HomeActivity.class) {
            header.findViewById(R.id.nav_home).setBackgroundResource(R.color.ic_nav_bg_color);
        } else {
            if (currentView != null) {
                header.findViewById(currentView.getId()).setBackgroundResource(R.color.ic_nav_bg_color);
            } else {
                header.findViewById(R.id.nav_home).setBackgroundResource(R.color.ic_nav_bg_color);
            }
        }

        header.findViewById(R.id.nav_home).setOnClickListener(this);
        header.findViewById(R.id.nav_history).setOnClickListener(this);
        header.findViewById(R.id.nav_properties).setOnClickListener(this);
        header.findViewById(R.id.nav_load_houzes).setOnClickListener(this);
        header.findViewById(R.id.nav_profile).setOnClickListener(this);
        header.findViewById(R.id.nav_team).setOnClickListener(this);
        header.findViewById(R.id.nav_setting).setOnClickListener(this);


        Toolbar toolbar = ActivityHelper.setToolbar(this, R.id.toolbar, R.string.app_name);
        this.setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.toolbar_hamburger);
    }

    @Override
    public void onClick(View v) {
        currentView = v;
        isClick(v.getId());
    }

    @Override
    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private boolean isClick(int id) {
        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(this);
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_properties) {
            Intent intent = new Intent(this, AddPropertyActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_load_houzes) {
            Intent intent = new Intent(this, LoadHouzesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile || id == R.id.imageView || id == R.id.nav_header_user || id == R.id.nav_header_email) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_team) {
            Intent intent = new Intent(this, TeamActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(this, SettingActivity.class));
        } else if (id == R.id.upgrade_text) {
            startActivity(new Intent(this, AppPlanActivity.class));
        }
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            loadView();
        } catch (Exception ex) {
        }
    }

    protected void loadView() {
        try {
            navigationView = findViewById(R.id.nav_view);
            final View header = navigationView.getHeaderView(0);
            ImageView imageView = header.findViewById(R.id.imageView);
            TextView nav_header_user = header.findViewById(R.id.nav_header_user);
            TextView nav_header_email = header.findViewById(R.id.nav_header_email);
            nav_header_user.setText(SharedPreferenceUtil.getValue(SharedPreferenceUtil.FIRST_NAME, this));
            nav_header_email.setText(SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_EMAIL, this));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentView = header.findViewById(R.id.nav_profile);
                    isClick(v.getId());
                }
            });
            nav_header_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentView = header.findViewById(R.id.nav_profile);
                    isClick(v.getId());
                }
            });
            nav_header_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentView = header.findViewById(R.id.nav_profile);
                    isClick(v.getId());
                }
            });

            try {
                Glide.with(this).load(SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_IMAGE, this)).placeholder(R.drawable.loading_fream_round).error(R.drawable.profile_icon).into(imageView);
            } catch (Exception ex) {
                Picasso.with(this).load(SharedPreferenceUtil.getValue(SharedPreferenceUtil.USER_IMAGE, this)).placeholder(R.drawable.loading_fream_round).error(R.drawable.profile_icon).into(imageView);
            }

            header.findViewById(R.id.plan_type).setBackgroundTintList(ContextCompat.getColorStateList(this, StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getColorCode()));
            if (!StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanType().equals(PlanType.TEAM)) {
                header.findViewById(R.id.upgrade_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isClick(R.id.upgrade_text);
                    }
                });
            } else {
                header.findViewById(R.id.upgrade_text).setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.disableUpgrade));
            }

            ((TextView) header.findViewById(R.id.plan_type)).setText(StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanName());

        } catch (Exception ex) {
        }
    }
}
