package com.houzes.main.view.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.houzes.main.R;

public class CustomSpinner extends RelativeLayout {
    private Context context;
    private GetValue getValue;

    private float cs_height = -1f;
    private Integer cs_background;

    public CustomSpinner(Context context) {
        super(context);
        this.init(context);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
        this.initTypeface(attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
        this.initTypeface(attrs);
    }

    private void initTypeface(AttributeSet attrs) {
        try {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomSpinner);
            cs_background = ta.getInt(R.styleable.CustomSpinner_cs_background, 0);
            cs_height = ta.getDimension(R.styleable.CustomSpinner_cs_height, -1);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cs_height < 0) {
                RelativeLayout mainView = findViewById(R.id.main_view);
                mainView.setLayoutParams(mainView.getLayoutParams());
                mainView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) cs_height));
            }
        }
    }

    public void onChange(GetValue getValue) {
        this.getValue = getValue;
    }

    private void init(Context cnx) {
        this.context = cnx;
        this.inflate(context, R.layout.custiomview_spinner, this);
    }

    public interface GetValue {
        void values(int position, String text, String tag);
    }
}
