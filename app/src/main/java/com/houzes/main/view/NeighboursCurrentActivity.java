package com.houzes.main.view;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.adapter.CurrentNeighboursAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.model.api.NeighborInfoModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.sys.SelectedNewNeighbour;
import com.houzes.main.presenter.NeighboursActivityPresenter;
import com.houzes.main.presenter.PaymentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NeighboursCurrentActivity extends AppBaseBackActivity implements NeighboursActivityPresenter.IView, PaymentPresenter.IView {
    private ListView listView;
    private PaymentPresenter $$;
    private PropertyModel propertyModel;
    private NeighboursActivityPresenter $;
    private NeighborInfoModel[] neighborInfoModelList = new NeighborInfoModel[]{};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_neighbours_current);
        $ = new NeighboursActivityPresenter(this);
        $$ = new PaymentPresenter(this);
        listView = findViewById(R.id.list_view);

        propertyModel = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.PROPERTY_DATA.name()), PropertyModel.class);
        neighborInfoModelList = new Gson().fromJson(getIntent().getExtras().getString(IntentExtraName.SELECT_NEIGHBOR_ONE.name()), NeighborInfoModel[].class);

        if (neighborInfoModelList != null && neighborInfoModelList.length > 0) {
            getCurrentList(neighborInfoModelList);
        } else {
            toastMessage(getString(R.string.nothing_yet_to_show));
            return;
        }


        findViewById(R.id.fetch_bulk_neighbor_pt_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomPopup().fullScreen(NeighboursCurrentActivity.this, R.layout.bottom_popup_nmp_indivisual, new BottomPopup.GetFullDialog() {
                    @Override
                    public void get(final Dialog dialog) {
                        dialog.findViewById(R.id.service_pt_message).setVisibility(View.VISIBLE);
                        ((TextView) dialog.findViewById(R.id.service_name)).setText("Power Trace");
                        ((TextView) dialog.findViewById(R.id.serviceTotal_cost)).setText("$" + VariableHelper.double2D((ActivityHelper.getPowerTraceCoin(NeighboursCurrentActivity.this) * neighborInfoModelList.length)));
                        ((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D((ActivityHelper.getPowerTraceCoin(NeighboursCurrentActivity.this) * neighborInfoModelList.length)));
                        //((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D((ActivityHelper.getPowerTraceCoin(NeighboursCurrentActivity.this))) + " X " + VariableHelper.double2D(neighborInfoModelList.length));

                        dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                                List<Integer> list = new ArrayList<>();
                                for (NeighborInfoModel infoModel : neighborInfoModelList) {
                                    list.add(infoModel.getId());
                                }
                                fetchPowerTraceOwnerInfo(true, false, list);
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public void getCurrentList(NeighborInfoModel[] neighborInfoModels) {
        try {
            int totalCompleteTask = 0;
            if (neighborInfoModels != null) {
                for (NeighborInfoModel ni1 : neighborInfoModels) {
                    for (int tempCount = 0; tempCount < neighborInfoModelList.length; tempCount++) {
                        if (ni1.getId() == neighborInfoModelList[tempCount].getId()) {
                            neighborInfoModelList[tempCount] = ni1;
                            if (ni1.getStatus().toLowerCase().trim().startsWith("complete")) {
                                totalCompleteTask++;
                            }
                            break;
                        }
                    }
                }
            }

            if (totalCompleteTask != neighborInfoModelList.length) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        $.getCurrentList(propertyModel.getId());
                    }
                }, 8000);
            }
        } catch (Exception ex) {
            toastMessage(MessageUtil.getMessage(ex));
        } finally {
            this.listView.setAdapter(new CurrentNeighboursAdapter(this, neighborInfoModelList, new CurrentNeighboursAdapter.IView() {
                @Override
                public void OPM(final int id, final boolean powerTrace, final boolean ownerInfo, final String re) {
                    new BottomPopup().fullScreen(NeighboursCurrentActivity.this, R.layout.bottom_popup_nmp_indivisual, new BottomPopup.GetFullDialog() {
                        @Override
                        public void get(final Dialog dialog) {
                            if (powerTrace) {
                                dialog.findViewById(R.id.service_pt_message).setVisibility(View.VISIBLE);
                            } else {
                                dialog.findViewById(R.id.service_pt_message).setVisibility(View.GONE);
                            }
                            ((TextView) dialog.findViewById(R.id.service_name)).setText(re.concat(powerTrace ? "Power Trace" : "Owner Info"));
                            ((TextView) dialog.findViewById(R.id.serviceTotal_cost)).setText("$" + VariableHelper.double2D(powerTrace ? ActivityHelper.getPowerTraceCoin() : ActivityHelper.getOwnerInfoCoin()));
                            ((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D(powerTrace ? ActivityHelper.getPowerTraceCoin() : ActivityHelper.getOwnerInfoCoin()));

                            dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                    fetchPowerTraceOwnerInfo(powerTrace, ownerInfo, Arrays.asList(new Integer[]{id}));
                                }
                            });
                        }
                    });
                }
            }));
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText($.activity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
    }

    @Override
    public void hideProgressDialog() {
    }

    @Override
    public void getSelectedNewNeighbour(SelectedNewNeighbour _selectedNewNeighbour) {
    }

    @Override
    public void paymentMessage(String message, final boolean isPayment, final boolean isSuccess) {
        new FrontPopup(this).setTitle(getString(R.string.space_fetching_title)).setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
                $$.setAction(isPayment, isSuccess);
            }
        }).show();
        if (isPayment && isSuccess) {
            $.getCurrentList(propertyModel.getId());
        }
    }

    private void fetchPowerTraceOwnerInfo(boolean powerTrace, boolean ownerInfo, List<Integer> selectedNewNeighbours) {
        JsonUtility jsonUtility = new JsonUtility().putInteger("power_trace", powerTrace ? 1 : 0).putInteger("fetch_owner_info", ownerInfo ? 1 : 0).put("neighbor_id", selectedNewNeighbours);
        $$.paymentRequestOrFetch(
                String.format("%s/api/property/%d/get-neighborhood/", AppConfigRemote.BASE_URL, propertyModel.getId()),
                jsonUtility.toString()
        );
    }
}
