package com.houzes.main.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.AuthActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;

import java.util.Arrays;

public class SignInActivity extends AppBaseBackActivity implements AuthActivityPresenter.IView {
    private ProgressDialog progressDialog;
    private static final int RC_GET_AUTH_CODE = 9003;

    private Button signInButton, gSignInButton, fbSignInButton;
    private EditText inputEmail, inputPassword;
    private LinearLayout forgotPasswordView;
    private ImageView showPasswordButtonImage;

    private SignInButton googleSignInButton;
    private LoginButton facebookSignInButton;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private AuthActivityPresenter $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        $ = new AuthActivityPresenter(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        FacebookSdk.sdkInitialize(getApplicationContext());
        init();

        showPasswordButtonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                $.showHidePassword(inputPassword, showPasswordButtonImage);
            }
        });

        forgotPasswordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ($.isInternet()) {
                    ActivityHelper.hideKeyboard(SignInActivity.this);
                    if (inputEmail.getText().toString().length() > 2 && inputPassword.getText().toString().length() > 2) {
                        $.loginByEmail(inputEmail.getText().toString(), inputPassword.getText().toString());
                    } else {
                        $.setMessage("Please input your email and password.", false);
                    }
                }
            }
        });

        facebookSignInButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();
        facebookSignInButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String token = AccessToken.getCurrentAccessToken().getToken();
                $.loginBySocial(token, "facebook");
            }

            @Override
            public void onCancel() {
                $.setMessage(getString(R.string.fb_cancel), true);
            }

            @Override
            public void onError(FacebookException exception) {
                $.setMessage(getString(R.string.fb_get_error), true);
                exception.getStackTrace();
            }
        });

        fbSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                AccessToken.setCurrentAccessToken(null);
                if (AccessToken.getCurrentAccessToken() == null) {
                    facebookSignInButton.performClick();
                    return;
                }

                new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {
                        LoginManager.getInstance().logOut();
                        facebookSignInButton.performClick();
                    }
                }).executeAsync();

            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.server_client_id)).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        googleSignInButton.setSize(SignInButton.SIZE_STANDARD);
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        gSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void init() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        StaticContent.setPermission(this);


        showPasswordButtonImage = findViewById(R.id.view_password);
        signInButton = findViewById(R.id.sign_in_button);
        gSignInButton = findViewById(R.id.g_sign_in_button);
        fbSignInButton = findViewById(R.id.fb_login_button);

        inputEmail = findViewById(R.id.sign_in_email);
        inputPassword = findViewById(R.id.sign_in_password);

        forgotPasswordView = findViewById(R.id.forgot_password);

        facebookSignInButton = findViewById(R.id.login_button);
        googleSignInButton = findViewById(R.id.google_sign_in_button);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_GET_AUTH_CODE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            final GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                progressDialog.show();
                new AsyncTask<String, Void, String>() {
                    @Override
                    protected String doInBackground(String... params) {
                        try {
                            String token = GoogleAuthUtil.getToken(getApplicationContext(), account.getAccount(), "oauth2:profile email");
                            $.loginBySocial(token, "google-oauth2");
                        } catch (Exception e) {
                            $.setMessage(MessageUtil.getMessage(e), true);
                            SignInActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.hide();
                                }
                            });
                        } finally {
                            mGoogleSignInClient.signOut();
                        }
                        return null;
                    }
                }.execute();
            }
        } catch (ApiException e) {
            progressDialog.hide();
            if (e.getStatusCode() == 12500) {
                $.setMessage(getString(R.string.error_12500), false);
            } else if (e.getStatusCode() == 12501) {
                $.setMessage(getString(R.string.go_cancel), false);
            } else {
                $.setMessage(getString(R.string.go_get_error), false);
            }
        }
    }

    @Override
    public void postSuccess(Object message) {
        startActivity(new Intent(this, SplashActivity.class));
    }

    @Override
    public void postFailed(String message) {
        new FrontPopup(this).setTitle("Sign In").setMessage(message).create().setCancel(getString(R.string.cancel), null).show();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }
}
