package com.houzes.main.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.houzes.main.R;

public class FrontPopup {

    private Context context;
    private String title = "";
    private String message = "";
    private AlertDialog alertDialog;
    private Button ok;
    private Button cancel;
    private View root;

    public FrontPopup(Context context) {
        this.context = context;
    }

    public FrontPopup setTitle(String title) {
        this.title = title;
        return this;
    }

    public FrontPopup setMessage(String message) {
        this.message = message+"";
        return this;
    }


    public FrontPopup create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        root = ((Activity) context).getLayoutInflater().inflate(R.layout.app_popup_message_for_all, null);
        builder.setView(root);
        alertDialog = builder.create();
        ok = root.findViewById(R.id.ok);
        cancel = root.findViewById(R.id.cancel);
        ((TextView) root.findViewById(R.id.title)).setText(title);
        ((TextView) root.findViewById(R.id.message)).setText(message);
        return this;
    }
    public FrontPopup create(int layout) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        root = ((Activity) context).getLayoutInflater().inflate(layout, null);
        builder.setView(root);
        alertDialog = builder.create();
        ok = root.findViewById(R.id.ok);
        cancel = root.findViewById(R.id.cancel);
        ((TextView) root.findViewById(R.id.title)).setText(title);
        ((TextView) root.findViewById(R.id.message)).setText(message);
        return this;
    }

    public FrontPopup setOkay(String name, final OkayClick okayClick) {
        ok.setVisibility(View.VISIBLE);
        ok.setText(name);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                okayClick.onClick();
                cancel();
            }
        });
        return this;
    }

    public FrontPopup setOk(String name, final OkClick okClick) {
        final FrontPopup frontPopup = this;
        ok.setVisibility(View.VISIBLE);
        ok.setText(name);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                okClick.onClick(frontPopup);
            }
        });
        return this;
    }

    public FrontPopup setCancel(String name, final CancelClick cancelClick) {
        cancel.setVisibility(View.VISIBLE);
        cancel.setText(name);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
                if (cancelClick != null) {
                    cancelClick.onClick();
                }
            }
        });
        return this;
    }

    public FrontPopup show() {
        alertDialog.show();
        return this;
    }

    public void cancel() {
        alertDialog.cancel();
    }

    public interface OkayClick {
        void onClick();
    }

    public interface OkClick {
        void onClick(FrontPopup frontPopup);
    }

    public interface CancelClick {
        void onClick();
    }
}
