package com.houzes.main.view.fragment.team;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.action.adapter.team.TeamPostAdapter;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.TeamFragmentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;

import java.util.List;

public class TeamFragment extends Fragment implements TeamFragmentPresenter.IView, TeamPostAdapter.IView {
    ImageView addMemberButton;
    SwipeRefreshLayout swipe_refresh_recycler_view;
    RecyclerView team_recycler_view;
    ProgressDialog progressDialog;
    private MyShimmerFrameLayout shimmerFrameLayout;
    private TeamPostAdapter teamPostAdapter;
    private View noDataFound;

    private TeamFragmentPresenter $;
    private Context context;
    private Activity activity;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.activity = (Activity) container.getContext();
        this.root = inflater.inflate(R.layout.fragment_team, container, false);
        this.startView();
        return root;
    }

    private void startView() {
        $ = new TeamFragmentPresenter(this, activity);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing...");
        shimmerFrameLayout = root.findViewById(R.id.shimmer_view);
        noDataFound = root.findViewById(R.id.no_data_found);
        addMemberButton = root.findViewById(R.id.floatingActionButton);
        swipe_refresh_recycler_view = root.findViewById(R.id.teamListSwipeRefresh);
        team_recycler_view = root.findViewById(R.id.team_recycler_view);
        team_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        shimmerFrameLayout.startNow();
        swipe_refresh_recycler_view.setEnabled(false);
        $.loadAllTeamMember();
        if (StaticContent.CURRENT_USER_MODEL.isTeamAdmin()) {
            addMemberButton.setVisibility(View.VISIBLE);
            addMemberButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new BottomPopup().startWithEditText(context, R.layout.bottom_popup_add_member, new BottomPopup.GetDialog() {
                        @Override
                        public void get(final BottomSheetDialog dialog) {
                            dialog.findViewById(R.id.confirm_notes).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            EditText eField = dialog.findViewById(R.id.member_email);
                                            String email = eField.getText().toString();
                                            if (FieldUtil.isEmail(email)) {
                                                $.addMember(((EditText) dialog.findViewById(R.id.member_email)).getText().toString(), false);
                                                dialog.cancel();
                                                progressDialog.show();
                                            } else {
                                                eField.setError("Invalid Email Address");
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            addMemberButton.setVisibility(View.GONE);
        }
    }


    @Override
    public void loadAllTeamMember(List<TeamModel> teamModels) {
        teamPostAdapter = new TeamPostAdapter(teamModels, context, this);
        team_recycler_view.setAdapter(teamPostAdapter);


        if (teamPostAdapter != null && teamPostAdapter.teamModels != null && teamPostAdapter.teamModels.size() > 0) {
            noDataFound.setVisibility(View.GONE);
        } else {
            noDataFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addMemberSuccess() {
        $.loadAllTeamMember();
    }

    @Override
    public void onTeamDelete() {
        $.loadAllTeamMember();
    }

    @Override
    public void showShimmer() {
        shimmerFrameLayout.startNow();
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideShimmer() {
        shimmerFrameLayout.stopNow();
    }

    @Override
    public void hideProgress() {
        progressDialog.cancel();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addMemberRequest(String email) {
        $.addMember(email, true);
    }

    @Override
    public void deleteMemberRequest(final long memberId) {
        new FrontPopup(context)
                .setTitle("Remove Member")
                .setMessage("Are you sure you want to remove this member?")
                .create()
                .setCancel("No", null)
                .setOkay("Yes", new FrontPopup.OkayClick() {
                    @Override
                    public void onClick() {
                        try {
                            $.deleteTeam(memberId, false);
                        } catch (Exception ex) {
                        }
                    }
                }).show();
    }
}
