package com.houzes.main.view.fragment.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.gmap.CarMovement;
import com.houzes.main.action.gmap.MapUtility;
import com.houzes.main.action.gmap.anim.LatLngInterpolator;
import com.houzes.main.action.gmap.anim.MarkerAnimation;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.socket.HouzesSocketHandler;
import com.houzes.main.action.socket.socket.SocketDrivingMarker;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.SocketDriveType;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.list.ListPropertyListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.view.HistoryDetailsActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeMapFragment {
    private Activity activity;
    private Context context;
    private GoogleMap googleMap;
    private ProgressDialog progressDialog;
    private Polyline polyLine = null;
    private int countInterval = 0;
    private Handler loadHandlerAfter12Sec;
    private Runnable runnable;
    private boolean cameraMoveNkToMyLocation = true;

    public HomeMapFragment(Context _context, GoogleMap _googleMap) {
        this.context = _context;
        this.activity = (Activity) _context;
        this.googleMap = _googleMap;
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage("Processing...");
        this.loadHandlerAfter12Sec = new Handler();
        this.myLocationChangeListener();
        this.googleMap.setMapType(SharedPreferenceUtil.getMapStyle(context));
        this.loadMapAllContents();
        this.googleMap.resetMinMaxZoomPreference();
        this.activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.loadingMyCurrentDriveAllProperty();

    }

    private void myLocationChangeListener() {
        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (StaticContetDrive.CURRENT_LAT != location.getLatitude()) {
                    cameraMoveToMyLocationInOnDrive(location);
                }
            }
        });

        runnable = new Runnable() {
            @Override
            public void run() {

            }
        };
        loadHandlerAfter12Sec.postDelayed(runnable, 2000);

    }

    public void userDriveStop(String historyModel) {
        googleMap.clear();
        this.drawCurrentActiveDriverPolylineProperty();
        StaticContetDrive.POLYLINE_MODEL_LIST = new HashMap<>();
        this.activity.startActivity(new Intent(context, HistoryDetailsActivity.class).putExtra("historyModel", historyModel).putExtra(IntentExtraName.TAKE_PHOTO_FROM_MAPVIEW.name(), true));
    }

    private void drawCurrentActiveDriverPolylineProperty() {
        try {
            /**
             * draw drive
             */
            List<Integer> tempList = new ArrayList<>();
            for (Map.Entry<Integer, SocketDrivingMarker> socketReceiveEntry : StaticContetDrive.CONNECTED_SOCKET_USER.entrySet()) {
                long interval = ((new Date().getTime() - socketReceiveEntry.getValue().latTime)) / 1000;
                if (interval > 55) {
                    tempList.add(socketReceiveEntry.getValue().id);
                }
            }
            for (Integer i : tempList) {
                StaticContetDrive.CONNECTED_SOCKET_USER.remove(i);
            }
            for (Map.Entry<Integer, SocketDrivingMarker> socketReceiveEntry : StaticContetDrive.CONNECTED_SOCKET_USER.entrySet()) {
                socketReceiveEntry.getValue().marker = googleMap.addMarker(MapUtility.getMarkerOptions(context, socketReceiveEntry.getValue()));
                socketReceiveEntry.getValue().marker.setTag(socketReceiveEntry.getValue().id);
            }
            /**
             * draw property
             */
            if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
                for (Map.Entry<Integer, PropertyModel> res : StaticContetDrive.RESULT_PROPERTY.entrySet()) {
                    googleMap.addMarker(new MarkerOptions().title(res.getValue().getPropertyAddress()).position(new LatLng(res.getValue().getLatitude(), res.getValue().getLongitude())).icon(MapUtility.getBitmapDescriptorForPropertyMarker(context, res.getValue().getTags()))).setTag(res.getValue());
                }
            }
            /**
             * draw polyline
             */
            for (Map.Entry<Integer, List<LatLng>> entry : StaticContent.POLYLINE_MODEL_LIST.entrySet()) {
                if (entry.getKey() == StaticContent.CURRENT_LAT_LON_INDEX) {
                    polyLine = googleMap.addPolyline(new PolylineOptions().addAll(entry.getValue()).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));

                } else {
                    googleMap.addPolyline(new PolylineOptions().addAll(entry.getValue()).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));
                }
            }
            drawMyDriveTails();
        } catch (Exception ex) {
        }
    }

    public void onSocketLocationReceived(final SocketShareLocation socketShareLocation) {
        try {
            SocketDrivingMarker $socketDrivingMarker = StaticContetDrive.CONNECTED_SOCKET_USER.get(socketShareLocation.getUserId());
            if ($socketDrivingMarker == null) {
                $socketDrivingMarker = new SocketDrivingMarker().sync(socketShareLocation);
                $socketDrivingMarker.marker = googleMap.addMarker(MapUtility.getMarkerOptions(context, socketShareLocation));
                $socketDrivingMarker.marker.setTag(socketShareLocation.getUserId());
                $socketDrivingMarker.latTime = new Date().getTime();
                StaticContetDrive.CONNECTED_SOCKET_USER.put($socketDrivingMarker.id, $socketDrivingMarker);
            } else {
                $socketDrivingMarker = StaticContetDrive.CONNECTED_SOCKET_USER.get(socketShareLocation.getUserId());
                float ang = (float) CarMovement.bearingBetweenLocations($socketDrivingMarker.marker.getPosition(), new LatLng(socketShareLocation.getLatitude(), socketShareLocation.getLongitude()));
                MarkerAnimation.animateMarkerToICS($socketDrivingMarker.marker, new LatLng(socketShareLocation.getLatitude(), socketShareLocation.getLongitude()), new LatLngInterpolator.Linear());
                MapUtility.rotateMarker($socketDrivingMarker.marker, ang, $socketDrivingMarker);
                $socketDrivingMarker.lat = socketShareLocation.getLatitude();
                $socketDrivingMarker.lon = socketShareLocation.getLongitude();
                $socketDrivingMarker.angel = ang;
                $socketDrivingMarker.latTime = new Date().getTime();
                $socketDrivingMarker.marker.setTag(socketShareLocation.getUserId());
                if (!socketShareLocation.getDriveType().equals($socketDrivingMarker.driveType)) {
                    $socketDrivingMarker.driveType = socketShareLocation.getDriveType();
                    $socketDrivingMarker.marker.setIcon(MapUtility.getIconByType(context, socketShareLocation.getDriveType().equals(SocketDriveType.PRIVATE) ? true : false));
                }
            }
        } catch (Exception ex) {
        }
    }

    public void onUserDisconnected(final int driveUserId) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    SocketDrivingMarker socketUserFromMap = StaticContetDrive.CONNECTED_SOCKET_USER.get(driveUserId);
                    if (socketUserFromMap != null) {
                        try {
                            socketUserFromMap.marker.remove();
                        } catch (Exception ex) {
                        }
                        socketUserFromMap.marker = null;
                        StaticContetDrive.CONNECTED_SOCKET_USER.remove(socketUserFromMap.id);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }


    public void stopRunnable() {
        try {
            if (runnable != null) {
                loadHandlerAfter12Sec.removeCallbacks(runnable);
                countInterval = 0;
                Log.d(StaticContent.TAG, "~X");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadMapAllContents() {
        if (googleMap == null) {
            return;
        }
        googleMap.clear();
        polyLine = null;
        if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
            try {
                StaticContetDrive.POLYLINE_MODEL_LIST = PolylineModel.getListOfList(PolylineModel.listAll(PolylineModel.class));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        drawCurrentActiveDriverPolylineProperty();
    }

    private void drawMyDriveTails() {
        if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
            try {
                if (polyLine != null) {
                    polyLine.remove();
                }
                if (StaticContetDrive.POLYLINE_MODEL_LIST != null && StaticContetDrive.POLYLINE_MODEL_LIST.size() > 0) {
                    polyLine = googleMap.addPolyline(new PolylineOptions().addAll(StaticContetDrive.POLYLINE_MODEL_LIST.get(StaticContent.CURRENT_LAT_LON_INDEX)).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void shareAndMoveLocation() {
        if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL && !StaticContetDrive.IS_PUBLIC_SHARED_ENABEL_RESUME) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(StaticContetDrive.CURRENT_LAT_LNG));
            if (StaticContetDrive.CURRENT_LAT != StaticContetDrive.CURRENT_LAT_STATIC) {
                try {
                    HouzesSocketHandler.shareUserLocation(new SocketShareLocation(StaticContent.CURRENT_USER_MODEL.getId(), StaticContetDrive.CURRENT_LAT, StaticContetDrive.CURRENT_LON, StaticContetDrive.CURRENT_ANGEL, StaticContent.getParentUserId(), SharedPreferenceUtil.getDriveType(activity)));
                } catch (Exception ex) {
                }
                try {
                    new PolylineModel(StaticContetDrive.CURRENT_LAT, StaticContetDrive.CURRENT_LON, StaticContetDrive.CURRENT_LAT_LON_INDEX).save();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    List<LatLng> ll = StaticContetDrive.POLYLINE_MODEL_LIST.get(StaticContent.CURRENT_LAT_LON_INDEX);
                    if (ll == null) {
                        if (polyLine != null) {
                            googleMap.addPolyline(new PolylineOptions().addAll(polyLine.getPoints()).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));
                        }
                        ll = new ArrayList<>();
                        ll.add(StaticContetDrive.CURRENT_LAT_LNG);
                        StaticContetDrive.POLYLINE_MODEL_LIST.put(StaticContent.CURRENT_LAT_LON_INDEX, ll);
                    }
                    ll.add(StaticContetDrive.CURRENT_LAT_LNG);
                } catch (Exception ex) {
                }
                drawMyDriveTails();
            }
        }
    }

    private void cameraMoveToMyLocationInOnDrive(Location location) {
        StaticContetDrive.CURRENT_LAT = location.getLatitude();
        StaticContetDrive.CURRENT_LON = location.getLongitude();
        StaticContetDrive.CURRENT_ANGEL = location.getBearing();
        StaticContetDrive.OLD_LAT_LNG = StaticContetDrive.CURRENT_LAT_LNG;
        StaticContetDrive.CURRENT_LAT_LNG = new LatLng(StaticContetDrive.CURRENT_LAT, StaticContetDrive.CURRENT_LON);

        if (cameraMoveNkToMyLocation) {
            cameraMoveNkToMyLocation = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(StaticContetDrive.CURRENT_LAT_LNG, 18);
                    googleMap.animateCamera(cameraUpdate);
                }
            }, 1000);
        } else {
            shareAndMoveLocation();
        }
    }

    private void loadingMyCurrentDriveAllProperty() {
        try {
            if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL && StaticContetDrive.MY_HISTORY_ID > 0) {
                new GlobalApiService<>(context, ListPropertyListModel.class).getList(String.format("%s/api/history/%d/properties/", AppConfigRemote.BASE_URL, StaticContetDrive.MY_HISTORY_ID), new GlobalIService<ListPropertyListModel>() {
                    @Override
                    public void onGetData(final ListPropertyListModel results) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        for (PropertyModel _PropertyModel : results.results) {
                                            try {
                                                StaticContetDrive.RESULT_PROPERTY.put(_PropertyModel.getId(), _PropertyModel);
                                                Marker mm = googleMap.addMarker(new MarkerOptions().title(_PropertyModel.getPropertyAddress()).position(new LatLng(_PropertyModel.getLatitude(), _PropertyModel.getLongitude())).icon(MapUtility.getBitmapDescriptorForPropertyMarker(context, _PropertyModel.getTags())));
                                                mm.setTag(_PropertyModel);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    }
                                }, 1000);
                            }
                        });
                    }

                    @Override
                    public void onError(final String message) {
                        ActivityHelper.Toast(activity, message);
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
