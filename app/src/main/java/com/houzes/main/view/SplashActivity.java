package com.houzes.main.view;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.houzes.main.BuildConfig;
import com.houzes.main.R;
import com.houzes.main.action.helper.InternetHelper;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.model.api.CurrentUserModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.presenter.AuthActivityPresenter;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

public class SplashActivity extends AppCompatActivity implements AuthActivityPresenter.IView {
    private boolean isAlreadyCallNextPage = false;
    private AuthActivityPresenter $;
    private String bearerToken;
    private String firebaseToken;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        $ = new AuthActivityPresenter(this);
        StaticContent.setPermission(this);

        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        bearerToken = SharedPreferenceUtil.getToken(SplashActivity.this);
        Sentry.init(getString(R.string.sentry_url), new AndroidSentryClientFactory(this));
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseMessaging.getInstance().subscribeToTopic("global");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(StaticContent.TAG, task.getException());
                            return;
                        }
                        firebaseToken = task.getResult().getToken();
                        SharedPreferenceUtil.setFirebaceCurrentTokenOKEN(SplashActivity.this, firebaseToken);
                    }
                });

        findViewById(R.id.body).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGOKey();
                start();
            }
        });
        if (bearerToken == null || bearerToken.length() < 6) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    $.postFailed(getString(R.string.sign_in_required));
                }
            }, 500);
        } else {
            start();
        }
    }

    private void start() {
        try {
            StaticContent.CURRENT_LAT_LON_INDEX=SharedPreferenceUtil.getInt("CURRENT_LAT_LON_INDEX", this);
            StaticContent.IS_PUBLIC_SHARED_ENABEL = SharedPreferenceUtil.PUBLIC_SHARE_ENABLE(SplashActivity.this);
            StaticContent.IS_PUBLIC_SHARED_ENABEL_RESUME = SharedPreferenceUtil.PUBLIC_SHARE_ENABLE_PAUSE(SplashActivity.this);
            StaticContent.MY_HISTORY_ID = SharedPreferenceUtil.getInt(SharedPreferenceUtil.DB_HISTORY_ID, SplashActivity.this);
        } catch (Exception ex) {
            $.setMessage(MessageUtil.getMessage(ex), false);
        } finally {
            if (!VariableHelper.isLocationEnabled(this)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        load();
                    }
                }, 10000);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        load();
                    }
                }, 500);
            }
        }
    }

    private void load() {
        if (InternetHelper.isConnection(SplashActivity.this)) {
            List<String> map = new ArrayList<>();
            try {
                map.add("device_type=android");
                map.add("firebase_token=" + firebaseToken);
                map.add("device_id=" + deviceId);
                map.add("version=" + BuildConfig.VERSION_CODE);
            } catch (Exception ex) {
                map.add("version=" + StaticContent.APP_CURRENT_VERSION);
            }
            $.getCurrentUserInfo(String.format("?%s", TextUtils.join("&", map)));
            Log.i("TOKEN", "Bearer " + bearerToken);
        } else {
            $.setMessage(getString(R.string.no_internet_1), true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    start();
                }
            }, 12000);
        }
    }

    public void getGOKey() {
        try {
            final PackageInfo info = getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance("SHA1");
                md.update(signature.toByteArray());
                final byte[] digest = md.digest();
                final StringBuilder toRet = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    if (i != 0) toRet.append(":");
                    int b = digest[i] & 0xff;
                    String hex = Integer.toHexString(b);
                    if (hex.length() == 1) toRet.append("0");
                    toRet.append(hex);
                }
                Log.e(StaticContent.TAG, toRet.toString());
            }
        } catch (Exception e) {
            Log.e(StaticContent.TAG, e.toString());
        } finally {
            try {
                PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    String hashKey = new String(Base64.encode(md.digest(), 0));
                    Log.i(StaticContent.TAG, hashKey);
                }
            } catch (Exception e) {
                Log.e(StaticContent.TAG, e.getMessage());
            }
        }
    }

    @Override
    public void postSuccess(Object data) {
        if (data != null) {
            CurrentUserModel currentUserModel = (CurrentUserModel) data;
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_ID, currentUserModel.getIdAsString(), SplashActivity.this);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_EMAIL, currentUserModel.getEmail(), SplashActivity.this);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.FIRST_NAME, currentUserModel.getFirstName(), SplashActivity.this);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.LAST_NAME, currentUserModel.getLastName(), SplashActivity.this);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_PHONE, currentUserModel.getPhoneNumber(), SplashActivity.this);
            SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_IMAGE, currentUserModel.getPhoto(), SplashActivity.this);

            StaticContent.SIGN_IN_USER_ID = currentUserModel.getId();
            StaticContent.CURRENT_USER_MODEL = currentUserModel;

            SplashActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isAlreadyCallNextPage) {
                        isAlreadyCallNextPage = true;
                        if (SharedPreferenceUtil.getBool("SHOW_TUTORIAL", $.activity)) {
                            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                            ActivityCompat.finishAffinity(SplashActivity.this);
                        } else {
                            SharedPreferenceUtil.setBool("SHOW_TUTORIAL", true, $.activity);
                            startActivity(new Intent(SplashActivity.this, TutorialScreenActivity.class));
                            ActivityCompat.finishAffinity(SplashActivity.this);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void postFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        if (!isAlreadyCallNextPage) {
            isAlreadyCallNextPage = true;
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            ActivityCompat.finishAffinity(SplashActivity.this);
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
    }

    @Override
    public void hideProgressDialog() {
    }
}