package com.houzes.main.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.BuildConfig;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;

public class SettingActivity extends AppBaseActivity {

    private TextView currentplan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_setting);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");

        currentplan = findViewById(R.id.current_plan);


        currentplan.setText(StaticContent.CURRENT_USER_MODEL.getPlanType().name());
        if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.FREE)) {
            currentplan.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.freeType));
        } else if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.SOLO)) {
            currentplan.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.soloType));
            findViewById(R.id.id_upgrade).setVisibility(View.GONE);
        } else if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
            currentplan.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.teamType));
            findViewById(R.id.id_upgrade).setVisibility(View.GONE);
        }

        findViewById(R.id.id_open_with_browser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityHelper.openURI(SettingActivity.this, "https://houzes.com");
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_welcome_tutorial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, TutorialScreenActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_upgrade).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, AppPlanActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_term_and_conditions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, PpAndTcActivity.class).putExtra("title", "Terms & Conditions").putExtra("url", "https://houzes.com/assets/page/privacy-policy-emulate.html#tc"));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, PpAndTcActivity.class).putExtra("title", "Privacy Policy").putExtra("url", "https://houzes.com/assets/page/privacy-policy-emulate.html#pp"));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityHelper.openURI(SettingActivity.this, "https://help.houzes.com");
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_wallet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, AddCardActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.id_coupon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticContent.CURRENT_USER_MODEL.getInvitedBy() == null) {
                    if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getDiscount() == null) {
                        startActivity(new Intent(SettingActivity.this, CouponActivity.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    } else {
                        Toast.makeText(SettingActivity.this, "Coupon already applied", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SettingActivity.this, getString(R.string.contact_admin), Toast.LENGTH_SHORT).show();
                }
            }
        });
        findViewById(R.id.id_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
                    new FrontPopup(SettingActivity.this).setTitle("Alert").setMessage("Please first stop your driving then logout your account").create().setCancel("Close", null).show();
                } else {
                    SharedPreferenceUtil.logOut(SettingActivity.this);
                    Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });
        findViewById(R.id.id_contact_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomPopup().load(SettingActivity.this, R.layout.bottom_popup_setting_contact, new BottomPopup.GetDialog() {
                    @Override
                    public void get(BottomSheetDialog bottomSheetDialog) {
                        bottomSheetDialog.findViewById(R.id.email_label).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/rfc822");
                                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@houzes.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "Support Help");
                                i.putExtra(Intent.EXTRA_TEXT, "\n\n\n\n--------------------------\nBy\n" + StaticContent.CURRENT_USER_MODEL.getFirstName() + " " + StaticContent.CURRENT_USER_MODEL.getLastName());
                                try {
                                    startActivity(Intent.createChooser(i, "Send mail..."));
                                } catch (Exception ex) {
                                    Toast.makeText(SettingActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        bottomSheetDialog.findViewById(R.id.phone_label).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:(855)7325328"));
                                    startActivity(intent);
                                } catch (Exception e) {
                                }
                            }
                        });
                    }
                });
            }
        });

        TextView versionName = findViewById(R.id.version_name);
        try {
            versionName.setText(String.format("Version %s.%d", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
            versionName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.houzes.main")));
                }
            });
        } catch (Exception ex) {
            versionName.setText("Version known");
        }
    }
}
