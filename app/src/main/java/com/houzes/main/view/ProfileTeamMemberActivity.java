package com.houzes.main.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.houzes.main.R;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.enums.IntentExtraName;

public class ProfileTeamMemberActivity extends AppBaseBackActivity {

    private TeamModel teamModel;
    private TextView profileNameValue, profileEmailValue, profilePhoneValue;
    private ImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_team_member_profile);

        profileNameValue = findViewById(R.id.profile_name_value);
        profileEmailValue = findViewById(R.id.profile_email_value);
        profilePhoneValue = findViewById(R.id.profile_phone_value);
        profileImage = findViewById(R.id.profile_image);


        try {
            teamModel = (TeamModel) getIntent().getExtras().getSerializable(IntentExtraName.TEAM_MEMBER.name());
            String name = (teamModel.getFirstName() == null ? "" : teamModel.getFirstName()) + " " + (teamModel.getLastName() == null ? "" : teamModel.getLastName());
            profileNameValue.setText(name.trim().length() > 0 ? name : getString(R.string.unknown));
            profileEmailValue.setText(teamModel.getEmail());
            profilePhoneValue.setText(FieldUtil.formatUsaPhoneNumber(teamModel.getPhoneNumber()));

            if (teamModel.getPhoto() == null) {
                Glide.with(this).load(R.drawable.profile_icon).circleCrop().into(profileImage);
            } else {
                Glide.with(this).load(teamModel.getPhoto()).placeholder(R.drawable.loading_fream_round).error(R.drawable.nav_profile).circleCrop().into(profileImage);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
