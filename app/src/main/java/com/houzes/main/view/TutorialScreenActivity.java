package com.houzes.main.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;

public class TutorialScreenActivity extends AppCompatActivity {

    private ViewFlipper viewFlipper;
    private Context context;
    private TextView skipContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_screen);
        this.context = this;
        this.viewFlipper = findViewById(R.id.viewFlipper);
        this.skipContinue = findViewById(R.id.skip_continue);
        final GestureDetector gestureDetector = new GestureDetector(new MyGestureDetector());
        this.viewFlipper.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    return false;
                } else {
                    return true;
                }
            }
        });

        this.viewFlipper.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int x = viewFlipper.getDisplayedChild() + 1;
                Picasso.with(context).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_" + x + ".png")).placeholder(getId("tutorial_screen_" + x, R.drawable.class)).error(getId("tutorial_screen_" + x, R.drawable.class)).into((ImageView) viewFlipper.getCurrentView());
                for (int xx = 1; xx <= 8; xx++) {
                    ImageView iv = findViewById(getId("img" + xx, R.id.class));
                    if (xx == x) {
                        iv.setColorFilter(Color.parseColor("#03A9F4"));
                        iv.setPadding(2, 2, 2, 2);
                    } else {
                        iv.setColorFilter(Color.WHITE);
                        iv.setPadding(5, 5, 5, 5);
                    }
                }

                if (x == 8) {
                    skipContinue.setText("Finish");
                } else {
                    skipContinue.setText("Skip");
                }
            }
        });
        skipContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, HomeActivity.class));
                ActivityCompat.finishAffinity(TutorialScreenActivity.this);
            }
        });

        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_1.png")).placeholder(R.drawable.tutorial_screen_1).error(R.drawable.tutorial_screen_1).into((ImageView) findViewById(R.id.iv_tutorial_screen_1));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_2.png")).placeholder(R.drawable.tutorial_screen_2).error(R.drawable.tutorial_screen_2).into((ImageView) findViewById(R.id.iv_tutorial_screen_2));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_3.png")).placeholder(R.drawable.tutorial_screen_3).error(R.drawable.tutorial_screen_3).into((ImageView) findViewById(R.id.iv_tutorial_screen_3));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_4.png")).placeholder(R.drawable.tutorial_screen_4).error(R.drawable.tutorial_screen_4).into((ImageView) findViewById(R.id.iv_tutorial_screen_4));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_5.png")).placeholder(R.drawable.tutorial_screen_5).error(R.drawable.tutorial_screen_5).into((ImageView) findViewById(R.id.iv_tutorial_screen_5));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_6.png")).placeholder(R.drawable.tutorial_screen_6).error(R.drawable.tutorial_screen_6).into((ImageView) findViewById(R.id.iv_tutorial_screen_6));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_7.png")).placeholder(R.drawable.tutorial_screen_7).error(R.drawable.tutorial_screen_7).into((ImageView) findViewById(R.id.iv_tutorial_screen_7));
        Picasso.with(this).load(AppConfigRemote.BASE_URL.concat("/static/dist/img/screenshot/tutorial_screen_8.png")).placeholder(R.drawable.tutorial_screen_8).error(R.drawable.tutorial_screen_8).into((ImageView) findViewById(R.id.iv_tutorial_screen_8));
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                viewFlipper.setInAnimation(context, R.anim.slide_in_right);
                viewFlipper.setOutAnimation(context, R.anim.slide_out_left);
                viewFlipper.showNext();
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                viewFlipper.setInAnimation(context, R.anim.slide_in_left);
                viewFlipper.setOutAnimation(context, R.anim.slide_out_right);
                viewFlipper.showPrevious();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
        }
        return 0;
    }
}
