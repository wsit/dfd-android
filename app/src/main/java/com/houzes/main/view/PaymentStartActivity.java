package com.houzes.main.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.OwnerInfoPowerTraceModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.RequestDataFrom;
import com.houzes.main.view.fragment.FrontPopup;

public class PaymentStartActivity extends AppBaseBackActivity {

    private RequestDataFrom requestDataFrom;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_patment_start);
        requestDataFrom = new Gson().fromJson(getIntent().getExtras().getString(IntentExtraName.REQUEST_FORM_DATA.name(), AppConfigRemote.BASE_URL), RequestDataFrom.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);

        ((TextView) findViewById(R.id.plan_type)).setText(StaticContent.CURRENT_USER_MODEL.getPlanType().name());
        findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                dataRequest();
            }
        });

        findViewById(R.id.textView8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.power_trace_total_cost)).setText("$" + VariableHelper.double2D((double) requestDataFrom.data));

        if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.SOLO)) {
            findViewById(R.id.plan_type_background).setBackgroundResource(R.drawable.bg_payment_solo);
        } else if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
            findViewById(R.id.plan_type_background).setBackgroundResource(R.drawable.bg_payment_team);
        } else {
            findViewById(R.id.plan_type_background).setBackgroundResource(R.drawable.bg_payment_free);
        }
    }


    private void dataRequest() {
        new GlobalApiService<OwnerInfoPowerTraceModel>(PaymentStartActivity.this, new TypeToken<ApiResponseModel<OwnerInfoPowerTraceModel>>() {
        }.getType()).post(requestDataFrom.url, requestDataFrom.json, new CallbackIService<ApiResponseModel<OwnerInfoPowerTraceModel>>() {
            @Override
            public void getValue(final ApiResponseModel<OwnerInfoPowerTraceModel> apiResponseModel) {
                PaymentStartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        if (apiResponseModel != null) {
                            String message = apiResponseModel.message == null ? getString(R.string.error_swwta) : (apiResponseModel.message.trim().startsWith("[")) ? TextUtils.join("\n", new Gson().fromJson(apiResponseModel.message, String[].class)) : apiResponseModel.message;
                            new FrontPopup(PaymentStartActivity.this).setTitle("Fetching Info").setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
                                @Override
                                public void onClick() {
                                    try {
                                        if (apiResponseModel.status) {
                                            if (apiResponseModel.data != null && apiResponseModel.data.upgradeInfo != null) {
                                                StaticContent.CURRENT_USER_MODEL.setUpgradeInfo(apiResponseModel.data.upgradeInfo);
                                            }
                                            onBackPressed();
                                        } else if (apiResponseModel.data != null && apiResponseModel.data.payment != null && !apiResponseModel.data.payment) {
                                            if (StaticContent.CURRENT_USER_MODEL.isAdmin()) {
                                                startActivity(new Intent(PaymentStartActivity.this, AddCardActivity.class));
                                            }
                                        }
                                    } catch (Exception ex) {
                                        Toast.makeText(PaymentStartActivity.this, getString(R.string.error_swwta), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).show();
                        } else {
                            Toast.makeText(PaymentStartActivity.this, getString(R.string.error_swwta), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void getError(Exception ex, final String message) {
                PaymentStartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        new FrontPopup(PaymentStartActivity.this)
                                .setTitle("Fetching Info")
                                .setMessage(message + "").create().setCancel("Close", null).show();
                    }
                });
            }
        });
    }
}
