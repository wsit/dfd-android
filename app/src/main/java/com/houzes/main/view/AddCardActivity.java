package com.houzes.main.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.houzes.main.R;
import com.houzes.main.action.adapter.SpinnerAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.model.api.CardInfoModel;
import com.houzes.main.model.enums.CardType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.AddCardActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class AddCardActivity extends AppBaseBackActivity implements AddCardActivityPresenter.IView {

    private ProgressDialog progressDialog;

    private View addNewCardLayout, myAllCardLayout;
    private TextView textViewSelectedCardUserName, textViewSelectedCardFee, textViewSelectedCardExpiry;
    private EditText inputEnterAmount;
    private TextView showOrHideTitle;
    private ImageView showOrHideIcon;
    private LinearLayout listView;
    private Switch switchSave;
    private EditText cardNumberNew, cardNameNew, cardCVVNew;
    private Spinner cardMonthNew, cardYearNew;

    private CardInfoModel cardInfoModel;

    private AddCardActivityPresenter $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_add_card);
        $ = new AddCardActivityPresenter(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));


        addNewCardLayout = findViewById(R.id.new_card_section);
        myAllCardLayout = findViewById(R.id.my_all_card);
        listView = findViewById(R.id.listview);

        inputEnterAmount = findViewById(R.id.enter_amount);

        showOrHideTitle = findViewById(R.id.sow_or_add_title);
        showOrHideIcon = findViewById(R.id.sow_or_add_icon);

        textViewSelectedCardUserName = findViewById(R.id.selected_card_user_name);
        textViewSelectedCardFee = findViewById(R.id.selected_card_fee);
        textViewSelectedCardExpiry = findViewById(R.id.selected_card_exary);

        cardNameNew = findViewById(R.id.card_holder_name);
        cardNumberNew = findViewById(R.id.card_number);
        cardCVVNew = findViewById(R.id.card_cvc2);
        cardMonthNew = findViewById(R.id.card_month);
        cardYearNew = findViewById(R.id.card_year);
        switchSave = findViewById(R.id.switch1);

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        String[] spinner_year = new String[]{String.valueOf(year), String.valueOf(year + 1), String.valueOf(year + 2), String.valueOf(year + 3), String.valueOf(year + 4), String.valueOf(year + 5), String.valueOf(year + 6), String.valueOf(year + 7), String.valueOf(year + 8), String.valueOf(year + 9)};
        cardMonthNew.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.spinner_month)));
        cardYearNew.setAdapter(new SpinnerAdapter(this, spinner_year));
        cardMonthNew.setSelection(month >= 0 && month <= 11 ? month : 0);

        inputEnterAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                $.inputEnterAmount(inputEnterAmount, hasFocus);
            }
        });

        findViewById(R.id.pay_now_new).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValue(cardNumberNew.getText().toString(), cardCVVNew.getText().toString())) {
                    String json = $.applyPaymentForChargeJson(
                            StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getId(),
                            switchSave.isChecked(),
                            inputEnterAmount.getText().toString(),
                            cardNameNew.getText().toString(),
                            cardNumberNew.getText().toString(),
                            cardCVVNew.getText().toString(),
                            cardYearNew.getSelectedView().getTag().toString().concat("-").concat(FieldUtil.nameToNumber(cardMonthNew.getSelectedView().getTag().toString()))
                    );

                    $.applyPayment(AppConfigRemote.PAYMENT_CHARGE, json);
                }
            }
        });

        findViewById(R.id.selected_card_pay_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valid = true;
                try {
                    if (inputEnterAmount.getText().toString().equals("")) {
                        valid = false;
                        inputEnterAmount.setError("Minimum amount $10");
                    } else {
                        if (Integer.parseInt(inputEnterAmount.getText().toString()) < 10) {
                            valid = false;
                            inputEnterAmount.setError("Minimum amount $10");
                        }
                    }
                } catch (Exception ex) {
                    valid = false;
                    inputEnterAmount.setError("Invalid amount");
                }
                if (((TextView) findViewById(R.id.selected_card_cvv2)).getText().toString().length() <= 2) {
                    ((TextView) findViewById(R.id.selected_card_cvv2)).setError(getString(R.string.find_security_code));
                    valid = false;
                }

                if (valid) {
                    String json = $.applyPaymentForChargeJson(
                            StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getId(),
                            true,
                            inputEnterAmount.getText().toString(),
                            cardInfoModel.getCardName(),
                            cardInfoModel.getCardNumber(),
                            ((TextView) findViewById(R.id.selected_card_cvv2)).getText().toString(),
                            cardInfoModel.getExpDate()
                    );
                    $.applyPayment(AppConfigRemote.PAYMENT_CHARGE, json);
                }
            }
        });


        inputEnterAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                $.inputEnterAmountChange(inputEnterAmount, textViewSelectedCardFee);
            }
        });
        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewCardLayout.setVisibility(View.VISIBLE);
                addNewCardLayout.setAnimation(AnimationUtils.loadAnimation(AddCardActivity.this, R.anim.slide_in_right));
            }
        });

        findViewById(R.id.sow_or_add_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                $.showHideAllCard(v, showOrHideTitle, myAllCardLayout, addNewCardLayout);
            }
        });

        $.loadAllSavedCard(getResources().getInteger(R.integer.animation_time));
        $.editInputOnInput(cardNumberNew, cardCVVNew, ((ImageView) findViewById(R.id.cad_image)));
    }

    private boolean checkValue(String cardNumberValue, String cardCVVValue) {
        boolean valid = true;
        try {
            try {
                if (inputEnterAmount.getText().toString().equals("")) {
                    valid = false;
                    inputEnterAmount.setError("Minimum amount $10");
                } else {
                    if (Integer.parseInt(inputEnterAmount.getText().toString()) < 10) {
                        valid = false;
                        inputEnterAmount.setError("Minimum amount $10");
                    }
                }
            } catch (Exception ex) {
                valid = false;
                inputEnterAmount.setError("Invalid amount");
            }
            if (cardNumberValue.length() < 10) {
                cardNumberNew.setError(getString(R.string.find_card_number));
                valid = false;
            }

            if (cardCVVValue.length() < 3) {
                cardCVVNew.setError(getString(R.string.find_security_code));
                valid = false;
            }
        } catch (Exception ex) {
            Toast.makeText(this, getString(R.string.find_invalid_info), Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;

    }

    @Override
    public void loadAllSavedCard(final List<CardInfoModel> cardInfoModels) {
        if (cardInfoModels != null && cardInfoModels.size() > 0) {
            Collections.sort(cardInfoModels);
            Collections.sort(cardInfoModels, Collections.reverseOrder());

            showOrHideIcon.setTag(cardInfoModels.size());
            addNewCardLayout.setVisibility(View.GONE);
            textViewSelectedCardUserName.setText(String.format("%s\n%s", cardInfoModels.get(0).getCardNumberEnc(), cardInfoModels.get(0).getCardName()));
            textViewSelectedCardFee.setText("USD" + inputEnterAmount.getText().toString());
            textViewSelectedCardExpiry.setText(cardInfoModels.get(0).getExpDate());
            cardInfoModel = cardInfoModels.get(0);
            ((ImageView) findViewById(R.id.card_theme)).setImageResource(CardType.forCardNumber(cardInfoModels.get(0).getCardNumber()).getFrontResource());
            for (int ii = 0; ii < cardInfoModels.size(); ii++) {
                final int position = ii;
                final View convertView = getLayoutInflater().inflate(R.layout.rcview_swipe_item_cards, null);
                final CardType cp = CardType.forCardNumber(cardInfoModels.get(position).getCardNumber());

                ((ImageView) convertView.findViewById(R.id.icon)).setImageResource(CardType.forCardNumber(cardInfoModels.get(position).getCardNumber()).getFrontResource());
                ((TextView) convertView.findViewById(R.id.card_number)).setText(cardInfoModels.get(position).getCardNumberEnc());
                ((TextView) convertView.findViewById(R.id.date)).setText(cardInfoModels.get(position).getExpDate());
                ((TextView) convertView.findViewById(R.id.date_ow)).setText(cardInfoModels.get(position).getCardName() + "");
                ((TextView) convertView.findViewById(R.id.card_t)).setText(cp.name());
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myAllCardLayout.setVisibility(View.GONE);
                        addNewCardLayout.setVisibility(View.GONE);
                        showOrHideTitle.setTag(null);
                        showOrHideTitle.setText("Show saved card");
                        showOrHideIcon.setRotation(0);

                        cardInfoModel = cardInfoModels.get(position);
                        textViewSelectedCardUserName.setText(String.format("%s\n%s", cardInfoModels.get(position).getCardNumberEnc(), cardInfoModels.get(position).getCardName()));
                        textViewSelectedCardFee.setText("USD" + inputEnterAmount.getText().toString());
                        textViewSelectedCardExpiry.setText(cardInfoModels.get(position).getExpDate());
                        ((ImageView) findViewById(R.id.card_theme)).setImageResource(cp.getFrontResource());
                    }
                });
                listView.addView(convertView);
            }
        } else {
            addNewCardLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void applyPayment(String message) {
        new FrontPopup(AddCardActivity.this).setTitle(getString(R.string.space_payment)).setMessage(message).create().setOkay(getString(R.string.okay), new FrontPopup.OkayClick() {
            @Override
            public void onClick() {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        }).show();

    }

    @Override
    public void applyPaymentFailed(String message) {
        new FrontPopup(AddCardActivity.this).setTitle(getString(R.string.space_payment)).setMessage(message).create().setCancel(getString(R.string.cancel), null).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }
}
