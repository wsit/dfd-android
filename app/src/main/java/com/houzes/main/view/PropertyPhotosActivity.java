package com.houzes.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.houzes.main.R;
import com.houzes.main.action.adapter.PropertyPhotoAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.OpenCameraHelper;
import com.houzes.main.action.service.api.PropertyImageApiService;
import com.houzes.main.action.service.api.iapi.PropertyPhotoIService;
import com.houzes.main.model.api.PropertyPhotoModel;
import com.houzes.main.model.api.list.PropertyPhotoListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;
import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class PropertyPhotosActivity extends AppBaseBackActivity {
    private String next;
    private int propertyId;
    private String previous;
    private View noDataFound;
    private OpenCameraHelper cam;
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private Dialog popUpAllPhotoShowDialog;
    private MyShimmerFrameLayout shimmerFrameLayout;
    private PropertyPhotoAdapter propertyPhotoAdapter;
    private PropertyImageApiService propertyImageApiService;
    private List<String> listOfImagePathThatIsWantedToUoload = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_property_photos);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Uploading..");

        shimmerFrameLayout = findViewById(R.id.shimmer_view);
        noDataFound = findViewById(R.id.no_data_found);
        recyclerView = findViewById(R.id.show_all_image);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        propertyId = getIntent().getIntExtra(IntentExtraName.PROPERTY_ID.name(), 0);

        cam = new OpenCameraHelper(this);
        CameraManager cameraManager = null;
        cam.selectCamera(OpenCameraHelper.BACK);
        cam.setStopPreviewOnPicture(false);
        propertyPhotoAdapter = new PropertyPhotoAdapter(this, new PropertyPhotoModel[]{});

        new CroperinoConfig("img_" + System.currentTimeMillis() + ".jpg", "/houzes/Pictures", "/sdcard/houzes/Pictures");
        CroperinoFileUtil.verifyStoragePermissions(this);
        CroperinoFileUtil.setupDirectory(this);

        previous = AppConfigRemote.BASE_URL + "/api/photo/property/" + propertyId + "/?limit=30&offset=0";
        propertyImageApiService = new PropertyImageApiService(this, new PropertyPhotoIService() {

            @Override
            public void onFailed(final String message) {
                PropertyPhotosActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        Toast.makeText(PropertyPhotosActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onGetList(final PropertyPhotoListModel propertyPhotoListModel) {
                try {
                    next = propertyPhotoListModel.next;
                    PropertyPhotosActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.cancel();
                            propertyPhotoAdapter.addAll(propertyPhotoListModel.results);

                            if (propertyPhotoAdapter.propertyPhotoModels.size() == 0) {
                                noDataFound.setVisibility(View.VISIBLE);
                            } else {
                                noDataFound.setVisibility(View.GONE);
                            }

                            if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                                shimmerFrameLayout.stopNow(PropertyPhotosActivity.this);
                            }
                        }
                    });
                } catch (Exception ex) {
                }
            }

            @Override
            public void onUpload(final PropertyPhotoModel[] propertyPhotoModels) {
                PropertyPhotosActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        propertyPhotoAdapter.addAll(propertyPhotoModels);

                        if (propertyPhotoAdapter.propertyPhotoModels.size() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                        } else {
                            noDataFound.setVisibility(View.GONE);
                        }

                        if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                            shimmerFrameLayout.stopNow(PropertyPhotosActivity.this);
                        }
                    }
                });
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (next != null) {
                        propertyImageApiService.getList(next);
                    }
                }
            }
        });
        recyclerView.setAdapter(propertyPhotoAdapter);
        propertyImageApiService.getList(previous);


    }

    public void addPhotos(View view) {
        final ArrayList<String> allShownImagesPath = new ArrayList<>();
        ArrayList<String> temp = getMyDeviceAllPhotos();
        if (temp != null) {
            allShownImagesPath.add("camera");
            allShownImagesPath.addAll(temp);
            new BottomPopup().fullScreen(this, R.layout.bottom_popup_add_photo, new BottomPopup.GetFullDialog() {
                @Override
                public void get(final Dialog _bottomSheetDialog) {
                    popUpAllPhotoShowDialog = _bottomSheetDialog;
                    final ImageView uploadSelectItemButton = popUpAllPhotoShowDialog.findViewById(R.id.send_button);
                    final GridView all_galaray_image = popUpAllPhotoShowDialog.findViewById(R.id.all_galaray_image);
                    final TextView title = popUpAllPhotoShowDialog.findViewById(R.id.title);
                    listOfImagePathThatIsWantedToUoload.clear();
                    uploadSelectItemButton.setVisibility(View.GONE);
                    all_galaray_image.setAdapter(new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return allShownImagesPath.size();
                        }

                        @Override
                        public Object getItem(int position) {
                            return null;
                        }

                        @Override
                        public long getItemId(int position) {
                            return 0;
                        }

                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            final View root = getLayoutInflater().inflate(position == 0 ? R.layout.rcview_item_camera : R.layout.rcview_item_photos, null);
                            final ImageView mainImage = root.findViewById(R.id.main_photo);
                            final ImageView selectImage = root.findViewById(R.id.select_icon);
                            if (position > 0) {
                                Glide.with(PropertyPhotosActivity.this).load("file://" + allShownImagesPath.get(position)).into(mainImage);
                                selectImage.setTag(false);
                                mainImage.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        boolean isCheck = (boolean) selectImage.getTag();
                                        if (isCheck) {
                                            selectImage.setTag(false);
                                            selectImage.setImageDrawable(null);
                                            mainImage.setAlpha(1f);
                                            listOfImagePathThatIsWantedToUoload.remove(allShownImagesPath.get(position));
                                        } else {
                                            selectImage.setTag(true);
                                            selectImage.setImageResource(R.drawable.ic_correct_symbol_black);
                                            mainImage.setAlpha(0.5f);
                                            listOfImagePathThatIsWantedToUoload.add(allShownImagesPath.get(position));
                                        }
                                        title.setText(listOfImagePathThatIsWantedToUoload.size() + " Photo Selected");
                                        if (listOfImagePathThatIsWantedToUoload.size() > 0) {
                                            uploadSelectItemButton.setVisibility(View.VISIBLE);
                                        } else {
                                            uploadSelectItemButton.setVisibility(View.GONE);
                                        }
                                    }
                                });
                            } else {
                                final TextureView textureView = root.findViewById(R.id.main_camera);
                                textureView.setOpaque(false);
                                textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                                    @Override
                                    public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, final int i, final int i1) {
                                        if (textureView.getTag() == null) {
                                            textureView.setTag(true);
                                            cam.startPreview(surfaceTexture, i, i1);
                                        }
                                    }

                                    @Override
                                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                                    }

                                    @Override
                                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                                        textureView.setTag(null);
                                        return false;
                                    }

                                    @Override
                                    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                                    }
                                });
                                textureView.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Croperino.prepareCamera(PropertyPhotosActivity.this);
                                    }
                                });
                            }
                            return root;
                        }
                    });

                    popUpAllPhotoShowDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            try {
                                cam.stopPreview();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });


                    uploadSelectItemButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog.show();
                            popUpAllPhotoShowDialog.cancel();
                            progressDialog.setMessage("Photo uploading 0/" + listOfImagePathThatIsWantedToUoload.size());
                            new UploadImageAWait().execute();
                        }
                    });
                }
            });
        }
    }

    public class UploadImageAWait extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            int countUpload = 1;
            try {
                for (String imagePath : listOfImagePathThatIsWantedToUoload) {
                    final Map<String, Object> map = propertyImageApiService.uploadPhoto(propertyId, countUpload, imagePath);
                    if (map != null && ((boolean) map.get("status"))) {
                        countUpload++;
                        PropertyPhotosActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.setMessage("Photo uploading " + map.get("count") + "/" + listOfImagePathThatIsWantedToUoload.size());
                                propertyPhotoAdapter.addAll((PropertyPhotoModel[]) map.get("data"));
                            }
                        });
                    }
                }

            } catch (Exception ex) {
            } finally {
                final int failed = listOfImagePathThatIsWantedToUoload.size() - ((countUpload - 1));

                PropertyPhotosActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        if (failed > 0) {
                            Toast.makeText(PropertyPhotosActivity.this, failed + " photo failed to upload.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(PropertyPhotosActivity.this, "Photo upload successfully.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                listOfImagePathThatIsWantedToUoload = new ArrayList<>();
            }
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CroperinoConfig.REQUEST_TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 0, 0, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, this);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 0, 0, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_CROP_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri i = Uri.fromFile(CroperinoFileUtil.getTempFile());
                    listOfImagePathThatIsWantedToUoload.clear();
                    listOfImagePathThatIsWantedToUoload.add(CroperinoFileUtil.getTempFile().getAbsolutePath());
                    progressDialog.show();
                    popUpAllPhotoShowDialog.cancel();
                    progressDialog.setMessage("Photo uploading 0/" + listOfImagePathThatIsWantedToUoload.size());
                    new UploadImageAWait().execute();
                }
                break;
            default:
                break;

        }
    }

    private ArrayList<String> getMyDeviceAllPhotos() {
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        try {
            Uri uri;
            Cursor cursor;
            int column_index_data;
            String absolutePathOfImage = null;
            uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

            String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

            cursor = getContentResolver().query(uri, projection, null, null, null);

            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data);
                listOfAllImages.add(absolutePathOfImage);
            }
            return listOfAllImages;
        } catch (Exception ex) {
            if (ex.getClass().equals(SecurityException.class)) {
                Toast.makeText(this, "File permission denied! Please add file permission", Toast.LENGTH_SHORT).show();
                StaticContent.setPermission(PropertyPhotosActivity.this);
            } else {
                Toast.makeText(this, MessageUtil.getMessage(ex), Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }
}
