package com.houzes.main.view.fragment.home;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.houzes.main.R;

import java.util.Random;


public class HomeMapTdhFragment extends Fragment {

    private View root;
    private Context context;
    private Activity activity;
    private boolean isCreate=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.activity = (Activity) container.getContext();
        this.root = inflater.inflate(R.layout.fragment_home_drive_short_disc, container, false);

        root.findViewById(R.id.iiiiiiiiiiiiii1).setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_in_left));
        root.findViewById(R.id.iiiiiiiiiiiiii2).setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_in_right));
        root.findViewById(R.id.iiiiiiiiiiiiii3).setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_in_left));
        setView();
        return root;
    }

    public void setView() {
        try {
            isCreate=true;
            TextView total_all_drive_tag = root.findViewById(R.id.total_all_drive_tag);
            TextView total_all_drive_drive = root.findViewById(R.id.total_all_drive_drive);
            TextView total_all_drive_houzes = root.findViewById(R.id.total_all_drive_houzes);

            ProgressBar progressBarTag = root.findViewById(R.id.progressBarTag);
            ProgressBar progressBarDrive = root.findViewById(R.id.progressBarDrive);
            ProgressBar progressBarHozes = root.findViewById(R.id.progressBarHozes);

            int x = new Random().nextInt(100);
            int y = new Random().nextInt(100);
            int z = new Random().nextInt(100);
            startProgressAnimation(progressBarTag, progressBarTag.getTag() == null ? 0 : (int) progressBarTag.getTag(), x, 3000);
            startProgressAnimation(progressBarDrive, progressBarDrive.getTag() == null ? 0 : (int) progressBarDrive.getTag(), y, 3000);
            startProgressAnimation(progressBarHozes, progressBarHozes.getTag() == null ? 0 : (int) progressBarHozes.getTag(), z, 3000);

            startTextAnimation(total_all_drive_tag, total_all_drive_tag.getTag() == null ? 0 : (int) total_all_drive_tag.getTag(), x, 3000);
            startTextAnimation(total_all_drive_drive, total_all_drive_drive.getTag() == null ? 0 : (int) total_all_drive_drive.getTag(), y, 3000);
            startTextAnimation(total_all_drive_houzes, total_all_drive_houzes.getTag() == null ? 0 : (int) total_all_drive_houzes.getTag(), z, 3000);
        } catch (Exception ex) {
        }
    }

    private void startTextAnimation(final TextView textView, int start, int end, int durationMaliSecond) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.setDuration(durationMaliSecond);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setText(animation.getAnimatedValue().toString());
            }
        });
        textView.setTag(end);
        animator.start();
    }

    private void startProgressAnimation(final ProgressBar progressBar, int start, int end, int durationMaliSecond) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.setDuration(durationMaliSecond);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                progressBar.setProgress((int) animation.getAnimatedValue());
            }
        });
        progressBar.setTag(end);
        animator.start();
    }

    public boolean isCreated() {
        return isCreate;
    }
}
