package com.houzes.main.view;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.gmap.MapUtility;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.api.ApiService;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.service.views.iviews.HomeListActivityService;
import com.houzes.main.action.socket.socket.SocketDrivingMarker;
import com.houzes.main.action.socket.socket.SocketShareLocation;
import com.houzes.main.model.api.PreviousDrivesModel;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.SimpleUserInfo;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.model.statics.StaticProperty;
import com.houzes.main.presenter.HomeActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.ListActivity;
import com.houzes.main.view.fragment.home.HomeDriveFragment;
import com.houzes.main.view.fragment.home.HomeMapFragment;

public class HomeActivity extends AppBaseActivity implements OnMapReadyCallback, HomeActivityPresenter.IView, GoogleMap.OnMarkerClickListener {

    private GoogleMap gMap;
    private MapView mapView;

    private RelativeLayout tapPointerLayout;
    private FrameLayout fragmentListOfList;
    private HomeMapFragment fragmentHomeMap;
    private HomeActivityPresenter $;
    private Marker longPressSelectedMarker;
    private ProgressDialog progressDialog;
    private String filterUrl1 = null;
    private String filterUrl2 = null;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_nav_home);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");
        this.$ = new HomeActivityPresenter(this);
        this.initView();
        this.getFragmentManager().beginTransaction().replace(R.id.my_drive_freamlayout, new HomeDriveFragment().load(this)).commit();
        ApiService.loadTags(this, null);
        if (StaticContent.CURRENT_USER_MODEL != null && StaticContent.CURRENT_USER_MODEL.getEmail() != null) {
            if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.FREE) && StaticContent.IS_UPGRADE_MESSAGE_LOAD) {
                StaticContent.IS_UPGRADE_MESSAGE_LOAD = false;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                View popView = getLayoutInflater().inflate(R.layout.app_popup_message_upgrade, null);
                builder.setView(popView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                popView.findViewById(R.id.upgrade_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
                popView.findViewById(R.id.upgrade_button_1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                        startActivity(new Intent(HomeActivity.this, AppPlanActivity.class));
                    }
                });
                popView.findViewById(R.id.upgrade_button_2).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                        startActivity(new Intent(HomeActivity.this, AppPlanActivity.class));
                    }
                });
                VideoView videoView = popView.findViewById(R.id.videoView);
                MediaController mediaController = new MediaController(this);
                mediaController.setAnchorView(videoView);
                Uri uri = Uri.parse("https://www.w3schools.com/html/mov_bbb.mp4");
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
            }
        } else {
            startActivity(new Intent(this, SplashActivity.class));
        }


        findViewById(R.id.driver_info_section).setVisibility(View.GONE);
        findViewById(R.id.driver_info_section).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.driver_info_section).setVisibility(View.GONE);
                findViewById(R.id.driver_info_section).setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_out_right));
            }
        });
        findViewById(R.id.driver_info_section_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.driver_info_section).setVisibility(View.GONE);
                findViewById(R.id.driver_info_section).setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_out_right));
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(StaticContent.MAP_VIEW_BUNDLE_KEY);
        mapView.onSaveInstanceState(mapViewBundle);
    }

    private void initView() {
        StaticContent.setPermission(this);
        tapPointerLayout = findViewById(R.id.pointer_item);
        tapPointerLayout.setVisibility(View.GONE);
        mapView = findViewById(R.id.home_map_view);
        fragmentListOfList = findViewById(R.id.view_list_of_list);
        mapView.onCreate(null);
        mapView.getMapAsync(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing...");

        IntentFilter filter = new IntentFilter();
        filter.addAction(IntentExtraName.HIT_GEOFENCE.name());
        registerReceiver(this.geofenceBroadcastReceiver, filter);
    }

    private BroadcastReceiver backBroadcastReceiver = new BroadcastReceiver() {

        /**
         * detected back from list of list view
         */
        @Override
        public void onReceive(Context _context, Intent _intent) {
            getSupportActionBar().hide();
            final ListActivity listActivity = new ListActivity();
            listActivity.reRender(new HomeListActivityService() {
                @Override
                public void hideListActivityToolbar() {
                    getSupportActionBar().show();
                    fragmentListOfList.setVisibility(View.GONE);
                }

                @Override
                public void addSuccess(PropertyModel _PropertyModel) {
                    getSupportActionBar().show();
                    fragmentListOfList.setVisibility(View.GONE);
                    Intent intent = new Intent($.activity, PropertyDetailsActivity.class);
                    intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(_PropertyModel));
                    startActivity(intent);
                }

                @Override
                public void addFailed(String message) {
                    ActivityHelper.Toast(HomeActivity.this, message);
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        loadlistOfList(listActivity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }, 1000);
        }
    };

    private BroadcastReceiver geofenceBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context _context, Intent _intent) {
            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(_intent);
            if (geofencingEvent.hasError()) {
                return;
            }
            int geofenceTransition = geofencingEvent.getGeofenceTransition();
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                $.createGeofence(StaticContent.CURRENT_LAT_LNG, 2000);
                getFilterUrl(-1, filterUrl1);
                getFilterUrl(-1, filterUrl2);
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(StaticContent.CURRENT_LAT_LNG, 16));
        try {
            $.loadSocket();
            fragmentHomeMap = new HomeMapFragment(this, googleMap);
            gMap.setMyLocationEnabled(true);
            gMap.setBuildingsEnabled(true);
            gMap.getUiSettings().setMyLocationButtonEnabled(false);
            gMap.getUiSettings().setRotateGesturesEnabled(true);
            gMap.getUiSettings().setScrollGesturesEnabled(true);
            gMap.getUiSettings().setTiltGesturesEnabled(true);
            gMap.setOnMarkerClickListener(this);
            gMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    if (StaticContent.IS_PUBLIC_SHARED_ENABEL) {
                        try {
                            if (longPressSelectedMarker == null) {
                                longPressSelectedMarker = gMap.addMarker(new MarkerOptions().position(latLng).title(""));
                            } else {
                                longPressSelectedMarker.setPosition(latLng);
                            }
                            onMarkerClick(longPressSelectedMarker);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            ActivityHelper.mapStyle(this, googleMap);
            findViewById(R.id.current_location_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(StaticContent.CURRENT_LAT_LNG, 17));
                }
            });
            $.createGeofence(StaticContent.CURRENT_LAT_LNG, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadlistOfList(Fragment fragment) {
        fragmentListOfList.setVisibility(View.VISIBLE);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.view_list_of_list, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onLocationUpdateSuccess(boolean isSuccess) {

    }

    @Override
    public void onLocationReceived(SocketShareLocation socketShareLocation) {
        fragmentHomeMap.onSocketLocationReceived(socketShareLocation);
    }

    @Override
    public void onLocationShareSuccess(boolean isSuccess) {

    }

    @Override
    public void onStopDriving(int userId) {
        fragmentHomeMap.onUserDisconnected(userId);
        tapPointerLayout.setVisibility(View.GONE);
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void driveStart() {
        this.filterUrl1 = null;
    }

    @Override
    public void driveStop(String historyModel) {
        this.filterUrl1 = null;
        this.fragmentHomeMap.userDriveStop(historyModel);
    }

    @Override
    public void showProgressDialog(String message) {
        if (message != null) {
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog(boolean isUi) {
        if (isUi) {
            progressDialog.cancel();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.cancel();
                }
            });
        }
    }

    @Override
    public void getSelectedAddress(String address) {
        final TextView pointer_item_text = tapPointerLayout.findViewById(R.id.pointer_item_text);
        pointer_item_text.setText(StaticContent.SELECTED_PROPERTY_DETAILES.getPerfectAddress());
    }

    @Override
    public void getFilterUrl(int type, String url) {
        try {
            if (Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE) != 3) {
                new FrontPopup(this).setTitle(" Permission").setMessage("Enabling Location mode \"High Accuracy\" for better map performance.").create().setCancel(getString(R.string.okay), new FrontPopup.CancelClick() {
                    @Override
                    public void onClick() {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).show();
            }
        } catch (Exception ex) {
            Log.e(StaticContent.TAG, ex.getMessage() + "");
        } finally {
            if (StaticContent.IS_PUBLIC_SHARED_ENABEL) {
                if (url != null) {
                    this.fragmentHomeMap.loadMapAllContents();
                    if (type == 0) {
                        this.filterUrl1 = null;
                        this.filterUrl2 = null;
                    } else if (type == 1) {
                        this.filterUrl1 = url;
                        this.filterUrl2 = null;
                        this.$.fetchAllPropertyByUrl(url + String.format("?latitude=%f&longitude=%f", StaticContent.CURRENT_LAT, StaticContent.CURRENT_LON));
                    } else if (type == 2) {
                        this.filterUrl1 = null;
                        this.filterUrl2 = url;
                        this.$.fetchAllPreviousDriveByUrl(url + String.format("&latitude=%f&longitude=%f", StaticContent.CURRENT_LAT, StaticContent.CURRENT_LON));
                    } else if (type == -1) {
                        if (filterUrl1 != null) {
                            this.$.fetchAllPropertyByUrl(url + String.format("?latitude=%f&longitude=%f", StaticContent.CURRENT_LAT, StaticContent.CURRENT_LON));
                        } else if (filterUrl2 != null) {
                            this.$.fetchAllPreviousDriveByUrl(url + String.format("&latitude=%f&longitude=%f", StaticContent.CURRENT_LAT, StaticContent.CURRENT_LON));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void getPropertyLatLng(PropertyLatLngModel[] propertyLatLngModels) {
        for (PropertyLatLngModel location : propertyLatLngModels) {
            gMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).icon(MapUtility.getLoadListMarker(this, location.getPropertyTags()))).setTag(location);
        }
    }

    @Override
    public void getPreviousDriveLine(PreviousDrivesModel[] previousDrivesModels) {
        for (PreviousDrivesModel previousDrivesModel : previousDrivesModels) {
            try {
                gMap.addPolyline(new PolylineOptions().addAll(previousDrivesModel.getLatLng()).width(6).color(Color.BLUE));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void getGeofencingRequest(final GeofencingRequest geofencingRequest) {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, $.getGeofencePendingIntent()).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    //gMap.addCircle($.getCircleOptions());
                }
            });
        } else {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, $.getGeofencePendingIntent()).setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(@NonNull Status status) {
                                    //gMap.addCircle($.getCircleOptions());
                                }
                            });
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Toast.makeText(HomeActivity.this, "Google Connection Suspend", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            Toast.makeText(HomeActivity.this, "Google Connection Failed", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }
    }

    public void hideHomeMarkerInfo() {
        if (longPressSelectedMarker != null) {
            longPressSelectedMarker.remove();
            longPressSelectedMarker = null;
        }
    }

    public void propertyAddToList() {
        getSupportActionBar().hide();
        ListActivity listActivity = new ListActivity();
        listActivity.reRender(new HomeListActivityService() {
            @Override
            public void hideListActivityToolbar() {
                getSupportActionBar().show();
                fragmentListOfList.setVisibility(View.GONE);
            }

            @Override
            public void addSuccess(PropertyModel _listPropertyModel) {
                getSupportActionBar().show();
                fragmentListOfList.setVisibility(View.GONE);
                Intent intent = new Intent($.activity, PropertyDetailsActivity.class);
                intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(_listPropertyModel));
                startActivity(intent);
            }

            @Override
            public void addFailed(String message) {
                toastMessage(message);
            }
        });
        loadlistOfList(listActivity);
    }

    private void setUserInfo(final SocketDrivingMarker socketDrivingMarker, final TextView tv1, final ImageView iv1) {
        new GlobalApiService<SimpleUserInfo>(this, SimpleUserInfo.class).get(String.format("%s/api/user/%d/", AppConfigRemote.BASE_URL, socketDrivingMarker.id), new GlobalIService<SimpleUserInfo>() {
            @Override
            public void onGetData(SimpleUserInfo simpleUserInfo) {
                if (socketDrivingMarker != null) {
                    socketDrivingMarker.setUserInfo(simpleUserInfo);
                    HomeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv1.setText(socketDrivingMarker.userInfo.getFullName());
                            Glide.with($.activity).load(socketDrivingMarker.userInfo.getPhotoThumb()).circleCrop().placeholder(R.drawable.profile_icon).error(R.drawable.profile_icon).into(iv1);
                        }
                    });
                }
            }

            @Override
            public void onError(String message) {
            }
        });
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (marker.getTag() != null) {
            if (marker.getTag().getClass() == Integer.class) {
                SocketDrivingMarker socketDrivingMarker = StaticContetDrive.CONNECTED_SOCKET_USER.get(marker.getTag());
                if (socketDrivingMarker != null) {
                    findViewById(R.id.driver_info_section).setVisibility(View.VISIBLE);
                    TextView tv1 = findViewById(R.id.driver_user_name);
                    ImageView iv1 = findViewById(R.id.driver_user_image);
                    if (socketDrivingMarker.userInfo != null) {
                        tv1.setText(socketDrivingMarker.userInfo.getFullName());
                        Glide.with(this).load(socketDrivingMarker.userInfo.getPhotoThumb()).circleCrop().placeholder(R.drawable.profile_icon).error(R.drawable.profile_icon).into(iv1);
                    } else {
                        tv1.setText("Loading...");
                        iv1.setImageResource(R.drawable.profile_icon);
                        setUserInfo(socketDrivingMarker, tv1, iv1);
                    }
                } else {
                    Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            } else if (marker.getTag().getClass() == PropertyModel.class) {
                PropertyModel json = (PropertyModel) marker.getTag();
                startActivity(new Intent(this, PropertyDetailsActivity.class).putExtra(IntentExtraName.PROPERTY_DATA.name(), json.getJson()));
            } else if (marker.getTag().getClass() == PropertyLatLngModel.class) {
                PropertyLatLngModel json = (PropertyLatLngModel) marker.getTag();
                startActivity(new Intent(this, PropertyDetailsActivity.class).putExtra(IntentExtraName.PROPERTY_DATA.name(), json.getJson()));
            }
        } else {
            final ImageView pointer_item_cancel = tapPointerLayout.findViewById(R.id.pointer_item_cancel);
            ImageView pointer_item_list = tapPointerLayout.findViewById(R.id.pointer_item_list);
            ImageView pointer_item_love = tapPointerLayout.findViewById(R.id.pointer_item_love);
            ImageView pointer_item_note = tapPointerLayout.findViewById(R.id.pointer_item_note);
            pointer_item_list.setVisibility(View.VISIBLE);
            tapPointerLayout.setVisibility(View.VISIBLE);
            $.getSelectedAddress(marker.getPosition().latitude, marker.getPosition().longitude);

            pointer_item_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tapPointerLayout.setVisibility(View.GONE);
                    hideHomeMarkerInfo();
                }
            });

            pointer_item_love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    propertyAddToList();
                }
            });

            pointer_item_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_ID > 0) {
                        Intent intent = new Intent($.activity, PropertyDetailsActivity.class);
                        intent.putExtra(IntentExtraName.HOME_MAP_MARKER_LIST_ITEM_ADDRESS_INFO.name(), new Gson().toJson(StaticContent.SELECTED_PROPERTY_DETAILES));
                        startActivity(intent);
                    } else {
                        propertyAddToList();
                    }
                }
            });
            if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
                pointer_item_note.setVisibility(View.GONE);
                pointer_item_list.setVisibility(View.VISIBLE);
            } else {
                pointer_item_note.setVisibility(View.VISIBLE);
                pointer_item_list.setVisibility(View.GONE);
            }
        }
        return false;
    }

    @Override
    public void onResume() {
        try {
            if (mapView != null && mapView.getTag() == null) {
                mapView.setTag(true);
                registerReceiver(backBroadcastReceiver, new IntentFilter(StaticProperty.BROADCAST_LOAD_LIST));
            }
        } catch (Exception ex) {
        } finally {
            if (fragmentHomeMap != null) {
                fragmentHomeMap.loadMapAllContents();
            }

            if (filterUrl1 != null) {
                getFilterUrl(1, filterUrl1);
            } else if (filterUrl2 != null) {
                getFilterUrl(2, filterUrl2);
            }
            super.onResume();
            mapView.onResume();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onResume();
    }

    @Override
    public void onDestroy() {
        try {
            if (fragmentHomeMap != null) {
                fragmentHomeMap.stopRunnable();
            }
            unregisterReceiver(backBroadcastReceiver);
            unregisterReceiver(geofenceBroadcastReceiver);
        } catch (Exception ex) {
        } finally {
            mapView.onDestroy();
            super.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}