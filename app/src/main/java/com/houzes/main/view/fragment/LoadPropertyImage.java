package com.houzes.main.view.fragment;

import android.app.Activity;
import android.widget.ImageView;

import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.squareup.picasso.Picasso;

public class LoadPropertyImage {

    public static void originalImage(Activity activity, ImageView imageView, int id) {
        try {
            Picasso.with(activity).load(AppConfigRemote.getBASE_URL() + "/api/photos/property/" + id + "/thumb").placeholder(R.drawable.loading_fream_round).error(R.drawable.main_page_slider_image_3).into(imageView);
        } catch (Exception ex) {
        }
    }
}
