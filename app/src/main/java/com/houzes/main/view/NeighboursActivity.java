package com.houzes.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.model.api.NeighborInfoModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.sys.SelectedNewNeighbour;
import com.houzes.main.presenter.NeighboursActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NeighboursActivity extends AppBaseBackActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLongClickListener, NeighboursActivityPresenter.IView {
    private GoogleMap googleMap;
    private MapView googleMapView;
    private PropertyModel propertyModel;
    private NeighboursActivityPresenter $;
    private Button fetchSelectedPropertyButton;
    private NeighborInfoModel[] neighborInfoModels;
    private Map<Long, Marker> TAP_NEW_PROPERTY = new HashMap<>();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_neighbours);
        this.$ = new NeighboursActivityPresenter(this);
        this.fetchSelectedPropertyButton = findViewById(R.id.fetch_selected_property_button);
        this.fetchSelectedPropertyButton.setVisibility(View.GONE);
        this.googleMapView = findViewById(R.id.mapView);
        this.googleMapView.onCreate(null);
        this.googleMapView.getMapAsync(this);
        this.propertyModel = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.PROPERTY_DATA.name()), PropertyModel.class);

        fetchSelectedPropertyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<SelectedNewNeighbour> selectedNewNeighbours = new ArrayList<>();
                for (Map.Entry<Long, Marker> longMarkerMap : TAP_NEW_PROPERTY.entrySet())
                    if (longMarkerMap.getValue() != null) {
                        selectedNewNeighbours.add((SelectedNewNeighbour) longMarkerMap.getValue().getTag());
                    }

                Intent i = new Intent(NeighboursActivity.this, NeighboursNewActivity.class);
                i.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
                i.putExtra(IntentExtraName.SELECTED_NEW_NEIGHBOUR.name(), new Gson().toJson(selectedNewNeighbours));
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        findViewById(R.id.properties_all_current_neighbours_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (neighborInfoModels != null && neighborInfoModels.length > 0) {
                    Intent i = new Intent($.activity, NeighboursCurrentActivity.class);
                    i.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
                    i.putExtra(IntentExtraName.SELECT_NEIGHBOR_ONE.name(), new Gson().toJson(neighborInfoModels));
                    i.putExtra(IntentExtraName.SELECT_NEIGHBOR_ALL.name(), new Gson().toJson(neighborInfoModels));
                    startActivity(i);
                } else {
                    toastMessage(getString(R.string.nothing_yet_to_show));
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap _googleMap) {
        googleMap = _googleMap;
        ActivityHelper.mapStyle(this, _googleMap);
        if (propertyModel != null) {
            googleMap.addMarker(new MarkerOptions().title("").position(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude()), 19f));
        }
        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        $.getCurrentList(propertyModel.getId());
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (marker.getTag() != null && marker.getTag().getClass() == SelectedNewNeighbour.class) {
            new FrontPopup($.activity).setTitle("Neighbour Address").setMessage(marker.getTitle()).create().setOkay("Remove", new FrontPopup.OkayClick() {
                @Override
                public void onClick() {
                    SelectedNewNeighbour selectedNewNeighbour = (SelectedNewNeighbour) marker.getTag();
                    TAP_NEW_PROPERTY.get(selectedNewNeighbour.id).remove();
                    TAP_NEW_PROPERTY.put(selectedNewNeighbour.id, null);
                    TAP_NEW_PROPERTY.remove(selectedNewNeighbour.id);
                    if (TAP_NEW_PROPERTY.size() > 0) {
                        fetchSelectedPropertyButton.setVisibility(View.VISIBLE);
                    } else {
                        fetchSelectedPropertyButton.setVisibility(View.GONE);
                    }
                }
            }).show();
        } else if (marker.getTag() != null && marker.getTag().getClass() == NeighborInfoModel.class) {
            Intent i = new Intent($.activity, NeighboursCurrentActivity.class);
            i.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
            i.putExtra(IntentExtraName.SELECT_NEIGHBOR_ONE.name(), new Gson().toJson(new NeighborInfoModel[]{(NeighborInfoModel) marker.getTag()}));
            i.putExtra(IntentExtraName.SELECT_NEIGHBOR_ALL.name(), new Gson().toJson(neighborInfoModels));
            startActivity(i);
        } else {

        }
        return true;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (neighborInfoModels == null) {
            Toast.makeText($.activity, getString(R.string.loading_pre_neighbor_info), Toast.LENGTH_SHORT).show();
        } else if (neighborInfoModels.length < 10) {
            if ((TAP_NEW_PROPERTY.size() + neighborInfoModels.length) < 10) {
                SelectedNewNeighbour selectedNewNeighbour = new SelectedNewNeighbour();
                selectedNewNeighbour.id = new Date().getTime();
                selectedNewNeighbour.latitude = latLng.latitude;
                selectedNewNeighbour.longitude = latLng.longitude;

                Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                marker.setTag(selectedNewNeighbour);

                TAP_NEW_PROPERTY.put(selectedNewNeighbour.id, marker);
                $.getAddressFromSelectedNewNeighbour(selectedNewNeighbour);

                if (TAP_NEW_PROPERTY.size() > 0) {
                    fetchSelectedPropertyButton.setVisibility(View.VISIBLE);
                } else {
                    fetchSelectedPropertyButton.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText($.activity, getString(R.string.already_added_10_neighbor_info), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText($.activity, getString(R.string.already_added_10_neighbor_info), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        googleMapView.onResume();
        if (googleMap != null && propertyModel != null) {
            $.getCurrentList(propertyModel.getId());
            toastMessage("Collecting records");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleMapView.onStop();
    }

    @Override
    protected void onPause() {
        googleMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        googleMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        googleMapView.onLowMemory();
    }

    @Override
    public void getCurrentList(NeighborInfoModel[] _neighborInfoModels) {
        googleMap.clear();
        TAP_NEW_PROPERTY = new HashMap<>();
        fetchSelectedPropertyButton.setVisibility(View.GONE);
        neighborInfoModels = _neighborInfoModels;
        googleMap.addMarker(new MarkerOptions().title("").position(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        for (NeighborInfoModel neighborInfoModel : neighborInfoModels) {
            googleMap.addMarker(new MarkerOptions().title("").position(new LatLng(neighborInfoModel.getLatitude(), neighborInfoModel.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))).setTag(neighborInfoModel);
        }
    }

    @Override
    public void getSelectedNewNeighbour(SelectedNewNeighbour selectedNewNeighbour) {
        TAP_NEW_PROPERTY.get(selectedNewNeighbour.id).setTitle(selectedNewNeighbour.address);
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText($.activity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDialog() {

    }
}
