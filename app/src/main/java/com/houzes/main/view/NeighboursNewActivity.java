package com.houzes.main.view;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.sys.SelectedNewNeighbour;
import com.houzes.main.presenter.PaymentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;

public class NeighboursNewActivity extends AppBaseBackActivity implements PaymentPresenter.IView {
    private ListView listView;

    private PaymentPresenter $;
    private PropertyModel propertyModel;
    private SelectedNewNeighbour[] selectedNewNeighbours;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_neighbours_new);
        this.$ = new PaymentPresenter(this);
        this.listView = findViewById(R.id.list_view);
        this.propertyModel = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.PROPERTY_DATA.name()), PropertyModel.class);
        this.selectedNewNeighbours = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.SELECTED_NEW_NEIGHBOUR.name()), SelectedNewNeighbour[].class);

        this.listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return selectedNewNeighbours.length;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                view = getLayoutInflater().inflate(R.layout.rcview_new_get_neighbours, null);
                TextView textView = view.findViewById(R.id.address);
                textView.setText(selectedNewNeighbours[i].getPerfectAddress());

                return view;
            }
        });

        findViewById(R.id.fetch_all_property_info_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BottomPopup().fullScreen($.activity, R.layout.bottom_popup_nmp, new BottomPopup.GetFullDialog() {
                    @Override
                    public void get(Dialog dialog) {
                        ownerInfo(dialog);
                    }
                });
            }
        });
    }

    private void ownerInfo(final Dialog dialog) {
        final TextView totalCost = dialog.findViewById(R.id.total_cost_lebel);
        final CheckBox checkBoxOI = dialog.findViewById(R.id.checkBox_nd);
        final CheckBox checkBoxPT = dialog.findViewById(R.id.checkBox_pt);

        final double ownerInfoAmount = ActivityHelper.getOwnerInfoCoin() * selectedNewNeighbours.length;
        final double powerTraceAmount = ActivityHelper.getPowerTraceCoin() * selectedNewNeighbours.length;

        totalCost.setText("$" + VariableHelper.double2D(ownerInfoAmount));
        totalCost.setTag(ownerInfoAmount);

        //((TextView) dialog.findViewById(R.id.amount_nd)).setText(String.format("$%sx%d", VariableHelper.double2D(ActivityHelper.getOwnerInfoCoin()), selectedNewNeighbours.length));
        //((TextView) dialog.findViewById(R.id.amount_pt)).setText(String.format("$%sx%d", VariableHelper.double2D(ActivityHelper.getPowerTraceCoin()), selectedNewNeighbours.length));
        ((TextView) dialog.findViewById(R.id.amount_nd)).setText(VariableHelper.double2D(ownerInfoAmount));
        ((TextView) dialog.findViewById(R.id.amount_pt)).setText(VariableHelper.double2D(powerTraceAmount));

        CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                double total3itemCost = 0.00d;
                if (checkBoxOI.isChecked()) {
                    total3itemCost += ownerInfoAmount;
                }
                if (checkBoxPT.isChecked()) {
                    total3itemCost += powerTraceAmount;
                }
                totalCost.setText("$" + VariableHelper.double2D(total3itemCost));
                totalCost.setTag(total3itemCost);
            }
        };
        checkBoxOI.setOnCheckedChangeListener(checkedChangeListener);
        checkBoxPT.setOnCheckedChangeListener(checkedChangeListener);

        dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchPowerTraceOwnerInfo(checkBoxPT.isChecked(), checkBoxOI.isChecked());
                dialog.cancel();
            }
        });
    }

    private void fetchPowerTraceOwnerInfo(boolean powerTrace, boolean ownerInfo) {
        JsonUtility jsonUtility = new JsonUtility().putInteger("power_trace", powerTrace ? 1 : 0).putInteger("fetch_owner_info", ownerInfo ? 1 : 0).put("address", selectedNewNeighbours);
        $.paymentRequestOrFetch(
                String.format("%s/api/property/%d/get-neighborhood/", AppConfigRemote.BASE_URL, propertyModel.getId()),
                jsonUtility.toString()
        );
    }

    @Override
    public void paymentMessage(String message, final boolean isPayment, final boolean isSuccess) {
        new FrontPopup(this).setTitle(getString(R.string.space_fetching_title)).setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
                $.setAction(isPayment, isSuccess);
            }
        }).show();
    }
}
