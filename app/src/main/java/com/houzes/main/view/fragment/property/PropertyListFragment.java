package com.houzes.main.view.fragment.property;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.houzes.main.R;
import com.houzes.main.action.adapter.LoadHouzesListAdapter;
import com.houzes.main.action.gmap.MapClusterRenderer;
import com.houzes.main.action.gmap.MarkerItem;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.views.iviews.PropertyFragmentIService;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.view.PropertyDetailsActivity;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;
import com.houzes.main.view.fragment.sys.RecyclerViewEvent;

import java.util.ArrayList;
import java.util.List;

public class PropertyListFragment extends Fragment {

    private View root;
    private Activity activity;
    private MapView mapView;
    private GoogleMap googleMap;
    private ClusterManager<MarkerItem> mClusterManager;
    private LoadHouzesListAdapter propertyAdapter;
    private MyShimmerFrameLayout shimmerFrameLayout;
    private PropertyFragmentIService $;
    private RecyclerView recyclerView;
    private LatLngBounds.Builder builder;
    private TextView emptyText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.activity = (Activity) container.getContext();
        this.root = inflater.inflate(R.layout.fragment_property_list, container, false);
        this.loadViewsAndAction();
        this.shimmerFrameLayout.stopNow();
        this.shimmerFrameLayout.setVisibility(View.GONE);
        this.mapView.onCreate(null);
        this.mapView.getMapAsync(onMapReadyCallback);
        return root;
    }

    private void loadViewsAndAction() {
        this.mapView = root.findViewById(R.id.mapView);
        this.emptyText = root.findViewById(R.id.emptyText);
        this.recyclerView = root.findViewById(R.id.recyclerView);
        this.shimmerFrameLayout = root.findViewById(R.id.shimmer_view);
        this.root.findViewById(R.id.mapViewButton).setOnClickListener(mapViewButton);
        this.propertyAdapter = new LoadHouzesListAdapter(activity, new ArrayList<PropertyModel>());

        this.root.findViewById(R.id.viewPropertyMap).setVisibility(View.GONE);
        this.root.findViewById(R.id.viewPropertyList).setVisibility(View.GONE);
        this.root.findViewById(R.id.viewEmptyList).setVisibility(View.VISIBLE);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        this.recyclerView.setAdapter(this.propertyAdapter);
        this.$.fragmentLoad();

        RecyclerViewEvent.recyclerViewEnd(recyclerView, new RecyclerViewEvent.EndScroll() {
            @Override
            public void end() {
                $.goNext();
            }
        });
    }

    private View.OnClickListener mapViewButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag() == null) {
                view.setTag(1);
                root.findViewById(R.id.viewPropertyMap).setVisibility(View.VISIBLE);
                root.findViewById(R.id.viewPropertyList).setVisibility(View.GONE);
                ((ImageView) view).setImageResource(R.drawable.rightbottom_views_list_switch);
            } else {
                view.setTag(null);
                root.findViewById(R.id.viewPropertyMap).setVisibility(View.GONE);
                root.findViewById(R.id.viewPropertyList).setVisibility(View.VISIBLE);
                ((ImageView) view).setImageResource(R.drawable.rightbottom_views_map_switch);
            }
        }
    };

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap _googleMap) {
            googleMap = _googleMap;
            mClusterManager = new ClusterManager<>(activity, googleMap);
            mClusterManager.setRenderer(new MapClusterRenderer(activity, googleMap, mClusterManager));
            googleMap.setOnCameraIdleListener(mClusterManager);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    try {
                        if (marker.getSnippet() != null && marker.getSnippet().startsWith("{")) {
                            Intent intent = new Intent(activity, PropertyDetailsActivity.class);
                            intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), marker.getSnippet());
                            startActivity(intent);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return true;
                }
            });
            ActivityHelper.mapStyle(activity, googleMap);
        }
    };

    public void loadData(List<PropertyModel> propertyModelList) {
        this.propertyAdapter.addAll(propertyModelList);
        View view = this.root.findViewById(R.id.mapViewButton);
        if (view.getTag() == null) {
            root.findViewById(R.id.viewPropertyMap).setVisibility(View.GONE);
            root.findViewById(R.id.viewPropertyList).setVisibility(View.VISIBLE);
        } else {
            root.findViewById(R.id.viewPropertyMap).setVisibility(View.VISIBLE);
            root.findViewById(R.id.viewPropertyList).setVisibility(View.GONE);
        }

        if (propertyAdapter.propertyModels != null && propertyAdapter.propertyModels.size() > 0) {
            $.loadedProperty(propertyAdapter.propertyModels.size());
            root.findViewById(R.id.viewEmptyList).setVisibility(View.GONE);
        } else {
            $.loadedProperty(0);
            root.findViewById(R.id.viewEmptyList).setVisibility(View.VISIBLE);
            emptyText.setText(getString(R.string.nothing_yet_to_show));
        }

        if (this.shimmerFrameLayout.getVisibility() == View.VISIBLE) {
            this.shimmerFrameLayout.stopNow();
        }
    }

    public void loadMap(PropertyLatLngModel[] propertyLatLngModels) {
        try {
            for (PropertyLatLngModel propertyLatLngModel : propertyLatLngModels) {
                this.builder.include(new LatLng(propertyLatLngModel.getLatitude(), propertyLatLngModel.getLongitude()));
                this.mClusterManager.addItem(new MarkerItem(propertyLatLngModel.getLatitude(), propertyLatLngModel.getLongitude(), propertyLatLngModel));
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) (getResources().getDisplayMetrics().widthPixels * .6), (int) (getResources().getDisplayMetrics().heightPixels * .6), (int) (getResources().getDisplayMetrics().widthPixels * 0.10)));
        } catch (Exception ex) {
        }
    }

    public void newCall() {
        this.mClusterManager.clearItems();
        this.googleMap.clear();
        this.shimmerFrameLayout.startNow();
        this.propertyAdapter.propertyModels = new ArrayList<>();
        this.builder = new LatLngBounds.Builder();
        this.propertyAdapter = new LoadHouzesListAdapter(activity, new ArrayList<PropertyModel>());
        this.recyclerView.setAdapter(this.propertyAdapter);
        this.root.findViewById(R.id.viewPropertyMap).setVisibility(View.GONE);
        this.root.findViewById(R.id.viewPropertyList).setVisibility(View.GONE);
        this.root.findViewById(R.id.viewEmptyList).setVisibility(View.GONE);
    }

    public void emptyText(String message) {
        if (emptyText != null) {
            emptyText.setText(message);
        }
    }

    public void getCalBack(PropertyFragmentIService propertyFragmentIService) {
        this.$ = propertyFragmentIService;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
