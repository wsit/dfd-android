package com.houzes.main.view.partial;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.houzes.main.view.AppBaseBackActivity;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.ListOfTagModel;
import com.houzes.main.model.api.PowerTraceModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticProperty;
import com.houzes.main.action.service.api.ApiService;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;

import java.util.ArrayList;
import java.util.List;

import me.kaede.tagview.TagView;

public class PropertyDetailsActivityPartial extends AppBaseBackActivity {
    protected PropertyModel propertyModel;
    protected PowerTraceModel powerTraceModel;
    private MapView mapView;

    protected Activity activity;
    protected TagView tagView;
    protected LatLng thisPageMapLocation;
    protected String thisPageMapAddress = "Unknown";

    private String tempSelectTag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_property_details);
        activity = this;
        tagView = findViewById(R.id.tagView);
        mapView = findViewById(R.id.mapViewPD);
        propertyModel = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.PROPERTY_DATA.name()), PropertyModel.class);
        try {
            if (propertyModel == null) {
                thisPageMapAddress = StaticContent.SELECTED_PROPERTY_DETAILES.getState();
                thisPageMapLocation = new LatLng(StaticContent.SELECTED_PROPERTY_DETAILES.getLatitude(), StaticContent.SELECTED_PROPERTY_DETAILES.getLongitude());

            } else {
                thisPageMapAddress = propertyModel.getPropertyAddress();
                thisPageMapLocation = new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude());
            }
            mapView.onCreate(null);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.addMarker(new MarkerOptions().position(thisPageMapLocation).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(thisPageMapLocation, 19f));
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            });
        } catch (Exception ex) {
        }
    }

    protected String[] getMyAllSelectedTagFromPopUp(String string) {
        String str = string.trim().replace("><", ",");
        str = str.replace("<", "");
        str = str.replace(">", "");
        return str.split(",");
    }

    protected ListOfTagModel[] getListOfTagModels(String[] strArray) {
        List<ListOfTagModel> l = new ArrayList<>();
        for (String s : strArray) {
            try {
                for (ListOfTagModel listOfTagModel : StaticProperty.LIST_OF_TAGS) {
                    if (s.equals(listOfTagModel.id + "")) {
                        l.add(listOfTagModel);
                        break;
                    }
                }
            } catch (Exception ex) {
            }
        }
        return l.toArray(new ListOfTagModel[l.size()]);
    }

    protected void loadTags() {
        ApiService.loadTags(this, null);
        findViewById(R.id.property_tag_assign).setVisibility(View.VISIBLE);
    }

    protected void assignTagToProperty() {
        String xxxx = "";
        tempSelectTag = "";
        for (ListOfTagModel ltm : propertyModel.getTags() != null ? propertyModel.getTags() : new ListOfTagModel[]{}) {
            final String ss = "<" + ltm.id + ">";
            xxxx += ss;

        }
        tempSelectTag = xxxx;

        new BottomPopup().start(this, R.layout.bottom_popup_tag_assign, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog bottomSheetDialog) {
                bottomSheetDialog.show();
                ListView listView = bottomSheetDialog.findViewById(R.id.list_id);
                final Button conform = bottomSheetDialog.findViewById(R.id.conform);
                conform.setVisibility(View.GONE);
                listView.setAdapter(new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return StaticProperty.LIST_OF_TAGS.length;
                    }

                    @Override
                    public Object getItem(int i) {
                        return null;
                    }

                    @Override
                    public long getItemId(int i) {
                        return 0;
                    }

                    @Override
                    public View getView(int position, View v, ViewGroup viewGroup) {
                        final ListOfTagModel listOfTag = StaticProperty.LIST_OF_TAGS[position];
                        final View view = getLayoutInflater().inflate(R.layout.rcview_item_tag_select, null);
                        GradientDrawable gradientDrawable = new GradientDrawable();
                        gradientDrawable.setColor(Color.parseColor(StaticProperty.LIST_OF_TAGS[position].getColorCode().trim()));
                        gradientDrawable.setCornerRadius(50);
                        view.findViewById(R.id.color_code).setBackground(gradientDrawable);

                        ((TextView) view.findViewById(R.id.title)).setText(StaticProperty.LIST_OF_TAGS[position].name);
                        final ImageView sel = view.findViewById(R.id.select);

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final String ss = "<" + listOfTag.id + ">";
                                if (view.getTag() == null) {
                                    sel.setImageResource(R.drawable.ic_success_png);
                                    view.setTag(listOfTag);
                                    tempSelectTag = tempSelectTag.replaceAll(ss, "");
                                    tempSelectTag += ss;
                                } else {
                                    sel.setImageResource(R.drawable.list_unselected_item);
                                    view.setTag(null);
                                    tempSelectTag = tempSelectTag.replaceAll(ss, "");
                                }
                                conform.setVisibility(View.VISIBLE);
                                conform.setAnimation(AnimationUtils.loadAnimation(PropertyDetailsActivityPartial.this, R.anim.popup_show));

                            }
                        });
                        for (ListOfTagModel ltm : propertyModel.getTags() != null ? propertyModel.getTags() : new ListOfTagModel[]{}) {
                            if (listOfTag.id == ltm.id) {
                                sel.setImageResource(R.drawable.ic_success_png);
                                view.setTag(listOfTag);
                            }
                        }


                        return view;
                    }
                });

                conform.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.cancel();
                        final String[] getTagIDs = getMyAllSelectedTagFromPopUp(tempSelectTag);
                        final ListOfTagModel[] tagModels = getListOfTagModels(getTagIDs);
                        new GlobalApiService<>(activity, ApiResponseModel.class).post(AppConfigRemote.BASE_URL + "/api/property/" + propertyModel.getId() + "/assign-multiple-tags/", "{\"tags\" : \"" + TextUtils.join(",", getTagIDs) + "\"}", new CallbackIService<ApiResponseModel>() {
                            @Override
                            public void getValue(final ApiResponseModel apiResponseModel) {
                                if (apiResponseModel.status) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            propertyModel.setTags(tagModels);
                                            tagView.removeAllTags();
                                            tagView.addTags(ListOfTagModel.getTags(tagModels));
                                            Toast.makeText(activity, apiResponseModel.message, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }

                            @Override
                            public void getError(Exception ex, final String message) {
                                ActivityHelper.Toast(activity, message);
                            }
                        });

                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mapView != null)
            mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mapView != null)
            mapView.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }
}
