package com.houzes.main.view.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.adapter.ListsAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.ListApiService;
import com.houzes.main.action.service.api.iapi.ListIService;
import com.houzes.main.action.service.views.iviews.HomeListActivityService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.model.statics.StaticProperty;
import com.houzes.main.view.HomeActivity;

import java.util.HashMap;
import java.util.Map;

public class ListActivity extends Fragment {
    private View root;
    private String next;
    private Context context;
    private Activity activity;
    private boolean isFirst = true;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListsAdapter listAdapter;
    private ListApiService listApiService;
    private ProgressDialog progressDialog;
    private ListIService listIService;
    private View done_button;
    private HomeListActivityService homeListActivityService;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.root = inflater.inflate(R.layout.activity_next_lists, container, false);
        this.activity = (Activity) context;

        Toolbar toolbar = root.findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.toolbar_back_arrow);
        toolbar.getContentInsetLeft();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                homeListActivityService.hideListActivityToolbar();
            }
        });

        done_button = root.findViewById(R.id.done_button);
        done_button.setVisibility(View.GONE);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing...");
        recyclerView = root.findViewById(R.id.recyclerView);
        swipeRefreshLayout = root.findViewById(R.id.swipe_refresh_recycler_view);
        swipeRefreshLayout.setOnRefreshListener(swipeRefresh);
        recyclerView.addOnScrollListener(onScrollListener);
        root.findViewById(R.id.add_button).setOnClickListener(createNewListPopUp);

        this.getApiServiceResponse();
        listApiService = new ListApiService(context, listIService);
        load(AppConfigRemote.ALL_LIST_URL, false);
        done_button.setOnClickListener(propertyAddToList);
        return root;
    }


    public void load(String requestUrl, boolean refresh) {
        swipeRefreshLayout.setRefreshing(true);
        listApiService.getList(requestUrl, refresh);

    }

    private SwipeRefreshLayout.OnRefreshListener swipeRefresh = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            load(AppConfigRemote.ALL_LIST_URL, true);
        }
    };

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                if (next != null) {
                    load(next, false);
                }
            }
        }
    };

    private View.OnClickListener createNewListPopUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new BottomPopup().startWithEditText(context, R.layout.bottom_popup_list_add, new BottomPopup.GetDialog() {
                @Override
                public void get(final BottomSheetDialog dialog) {
                    final EditText title = dialog.findViewById(R.id.add_title);
                    dialog.findViewById(R.id.add_button).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (title.getText().toString().trim().length() > 0) {
                                progressDialog.show();
                                listApiService.addList(title.getText().toString());
                                dialog.cancel();
                            } else {
                                title.setError("What's your list name.");
                            }
                        }
                    });
                    dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                }
            });
        }
    };

    private View.OnClickListener propertyAddToList = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserListModel userListModel = (UserListModel) done_button.getTag();
            int user_list = userListModel.getId();
            listApiService.saveProperty(user_list, StaticContent.MY_HISTORY_ID);
            progressDialog.show();
            StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_ID = userListModel.getId();
            StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_NAME = userListModel.getName();
        }
    };

    private void getApiServiceResponse() {
        listIService = new ListIService() {
            @Override
            public void onListAdd(final UserListModel userListModel) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        load(AppConfigRemote.ALL_LIST_URL, true);
                        Toast.makeText(activity, "List Add Successfully", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onListAddAssignDone(final Map map) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        final Map<String, Object> calbackMap = new HashMap<>();
                        calbackMap.put(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(map.get("data")));
                        final ApiResponseModel<PropertyModel> apiResponseModel = new Gson().fromJson(new Gson().toJson(map), new TypeToken<ApiResponseModel<PropertyModel>>() {
                        }.getType());
                        try {
                            if (apiResponseModel != null && apiResponseModel.status) {
                                homeListActivityService.addSuccess(apiResponseModel.data);
                                if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL && StaticContetDrive.RESULT_PROPERTY != null) {
                                    StaticContetDrive.RESULT_PROPERTY.put(apiResponseModel.data.getId(), apiResponseModel.data);
                                }
                            } else {
                                homeListActivityService.addFailed(apiResponseModel.message);
                            }
                        } catch (Exception exc) {
                            Toast.makeText(context, MessageUtil.getMessage(exc), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void onListGet(final ListListModel listListModel, final boolean refresh) {
                next = listListModel.next;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isFirst) {
                            listAdapter = new ListsAdapter(context, listIService, listListModel.results);
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            recyclerView.setAdapter(listAdapter);
                            swipeRefreshLayout.setRefreshing(false);
                            isFirst = false;
                        } else {
                            if (refresh) {
                                listAdapter = new ListsAdapter(context, listIService, listListModel.results);
                                recyclerView.setAdapter(listAdapter);
                            } else {
                                listAdapter.addAll(listListModel.results);
                            }
                        }
                        swipeRefreshLayout.setRefreshing(false);

                        if (listListModel != null && listListModel.results.size() > 0) {
                            root.findViewById(R.id.no_items).setVisibility(View.GONE);
                        } else {
                            root.findViewById(R.id.no_items).setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void onListFailed(final String message) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onClickEvent(final UserListModel userListModel, final ImageView privious, final ImageView now) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (privious != null) {
                            privious.setImageResource(R.drawable.list_unselected_item);
                        }
                        now.setImageResource(R.drawable.ic_success_png);
                        now.setBackground(null);
                        done_button.setVisibility(View.VISIBLE);
                        done_button.setTag(userListModel);
                    }
                });
            }
        };
    }

    public void reRender(HomeListActivityService homeListActivityService) {
        this.homeListActivityService = homeListActivityService;
    }
}
