package com.houzes.main.view.fragment.sys;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.service.autofill.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

public class LoadImage {
    private Activity context;
    private String url;
    private Integer placeholder;
    private Integer error;
    private static LoadImage singeTon = new LoadImage();

    public LoadImage() {
        url = null;
        error = null;
        context = null;
        placeholder = null;
    }

    public static LoadImage with(Activity context) {
        singeTon.context = context;
        return singeTon;
    }

    public static LoadImage load(String url) {
        singeTon.url = url;
        return singeTon;
    }

    public static LoadImage placeholder(int placeholder) {
        singeTon.placeholder = placeholder;
        return singeTon;
    }

    public static LoadImage error(int error) {
        singeTon.error = error;
        return singeTon;
    }

    public void into(final ImageView imageView) {
        imageView.measure(0, 0);
        if (placeholder != null) {
            Glide.with(context).asGif().load(placeholder).into(imageView);
        }
        Glide.with(context)
                .asBitmap()
                .load(url)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                        Glide.with(context).load(resource).into(imageView);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        Glide.with(context).load(error).into(imageView);
                    }
                });
    }
}
