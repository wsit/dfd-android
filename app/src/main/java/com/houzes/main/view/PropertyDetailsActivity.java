package com.houzes.main.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.UriHelper;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.ListApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.ListOfTagModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.api.list.ListListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.model.statics.StaticProperty;
import com.houzes.main.presenter.PaymentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.partial.PropertyDetailsActivityPartial;

import java.util.ArrayList;
import java.util.List;

import me.kaede.tagview.Tag;

public class PropertyDetailsActivity extends PropertyDetailsActivityPartial implements PaymentPresenter.IView {

    private PaymentPresenter $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $ = new PaymentPresenter(this);
        try {
            if (propertyModel == null) {
                runIfPropertyIsNotExiest();
            } else {
                runIfPropertyIsExiest();
                loadProperty();
            }
        } catch (Exception ex) {
        } finally {
            loadTags();
            ((TextView) findViewById(R.id.current_address)).setText(thisPageMapAddress);
        }

    }

    private void runIfPropertyIsExiest() {
        ((TextView) findViewById(R.id.note_count)).setText(propertyModel.getNoteCount() + " Notes");
        ((TextView) findViewById(R.id.photo_count)).setText(propertyModel.getPhotoCount() + " Photos");
        if (propertyModel.getNoteCount() <= 0) {
            findViewById(R.id.note_count).setVisibility(View.GONE);
        } else {
            findViewById(R.id.note_count).setVisibility(View.VISIBLE);
        }
        if (propertyModel.getPhotoCount() <= 0) {
            findViewById(R.id.photo_count).setVisibility(View.GONE);
        } else {
            findViewById(R.id.photo_count).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.hide_all_content).setVisibility(View.GONE);
        findViewById(R.id.property_list_change).setVisibility(View.GONE);

        findViewById(R.id.property_notes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PropertyDetailsActivity.this, PropertyNotesActivity.class);
                i.putExtra(IntentExtraName.PROPERTY_ID.name(), propertyModel.getId());
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        findViewById(R.id.property_photos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PropertyDetailsActivity.this, PropertyPhotosActivity.class);
                i.putExtra(IntentExtraName.PROPERTY_ID.name(), propertyModel.getId());
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        findViewById(R.id.property_neighbour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PropertyDetailsActivity.this, NeighboursActivity.class);
                i.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        findViewById(R.id.property_tag_assign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticProperty.LIST_OF_TAGS != null && StaticProperty.LIST_OF_TAGS.length > 0) {
                    assignTagToProperty();
                } else {
                    Toast.makeText(PropertyDetailsActivity.this, "Property Tag Loading. Please wait...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        findViewById(R.id.property_owner_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (propertyModel.getOwnerInfo() != null && propertyModel.getOwnerInfo().size() > 0) {
                    Intent i = new Intent(PropertyDetailsActivity.this, PropertyOwnerInfoActivity.class);
                    i.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    new BottomPopup().fullScreen(PropertyDetailsActivity.this, R.layout.bottom_popup_nmp, new BottomPopup.GetFullDialog() {
                        @Override
                        public void get(Dialog dialog) {
                            ownerInfo(dialog);
                        }
                    });
                }
            }
        });

        findViewById(R.id.property_power_trace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (propertyModel.getOwnerInfo() != null && propertyModel.getOwnerInfo().size() > 0) {
                    if (propertyModel.getPowerTraceRequestId() > 0) {
                        Intent intent = new Intent(PropertyDetailsActivity.this, PowerTraceActivity.class);
                        intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModel));
                        intent.putExtra(IntentExtraName.OWNER_INFO_POWER_TRACE__MAIL_WIZARD_AMOUNT.name(), ActivityHelper.getPowerTraceCoin(PropertyDetailsActivity.this) + "");
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    } else {
                        new BottomPopup().fullScreen(PropertyDetailsActivity.this, R.layout.bottom_popup_nmp_indivisual, new BottomPopup.GetFullDialog() {
                            @Override
                            public void get(final Dialog dialog) {
                                dialog.findViewById(R.id.service_pt_message).setVisibility(View.VISIBLE);
                                ((TextView) dialog.findViewById(R.id.service_name)).setText("Power Trace");
                                ((TextView) dialog.findViewById(R.id.serviceTotal_cost)).setText("$" + VariableHelper.double2D(ActivityHelper.getPowerTraceCoin()));
                                ((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D(ActivityHelper.getPowerTraceCoin()));
                                dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                        $.paymentRequestOrFetch(UriHelper.getOPUrl(propertyModel.getId()), new JsonUtility().put("fetch_owner_info", 0).put("power_trace", 1).toString());
                                    }
                                });
                            }
                        });
                    }
                } else {
                    Toast.makeText(PropertyDetailsActivity.this, getString(R.string.open_feature), Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.property_mail_wizard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (propertyModel.getOwnerInfo() != null && propertyModel.getOwnerInfo().size() > 0) {
                    startActivity(new Intent(PropertyDetailsActivity.this, MailerWizardTempActivity.class).putExtra(IntentExtraName.URL.name(), UriHelper.getMailWizard(true, propertyModel.getId())));
                } else {
                    Toast.makeText(PropertyDetailsActivity.this, getString(R.string.open_feature), Toast.LENGTH_SHORT).show();
                }
            }
        });


        findViewById(R.id.power_trace_success).setVisibility(View.GONE);
        findViewById(R.id.mail_wizard_success).setVisibility(View.GONE);
        findViewById(R.id.owner_details_success).setVisibility(View.GONE);
        if (propertyModel.getOwnerInfo() != null && propertyModel.getOwnerInfo().size() > 0) {
            findViewById(R.id.owner_details_success).setVisibility(View.VISIBLE);

            ((ImageView) findViewById(R.id.power_trace_icon_active)).setImageResource(R.drawable.pd_item_power_trace);
            ((TextView) findViewById(R.id.power_trace_text_active)).setTextColor(Color.parseColor("#CC000000"));

            ((ImageView) findViewById(R.id.mail_wizard_icon_active)).setImageResource(R.drawable.pd_item_mail_wizard);
            ((TextView) findViewById(R.id.ail_wizard_text_active)).setTextColor(Color.parseColor("#CC000000"));

            if (propertyModel.getPowerTraceRequestId() > 0) {
                findViewById(R.id.power_trace_success).setVisibility(View.VISIBLE);
            }
        }
    }

    private void runIfPropertyIsNotExiest() {
        findViewById(R.id.saving_property_loading).setVisibility(View.GONE);
        findViewById(R.id.hide_all_content).setVisibility(View.VISIBLE);
        findViewById(R.id.property_list_change).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.list_name_label)).setText(String.format("Property added to \"%s\"", StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_NAME));

        findViewById(R.id.property_add_to_list_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.list_change_label).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent(StaticProperty.BROADCAST_LOAD_LIST));
                onBackPressed();
            }
        });

        findViewById(R.id.property_list_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent(StaticProperty.BROADCAST_LOAD_LIST));
                onBackPressed();
            }
        });

        findViewById(R.id.property_add_to_list_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.saving_property_loading).setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext()).asGif().load(R.raw.loading).into((ImageView) findViewById(R.id.saving_property_loading));

                ListApiService.saveProperty(PropertyDetailsActivity.this, StaticContetDrive.SELECTED_CURRENT_DRIVE_LIST_ID, StaticContent.MY_HISTORY_ID, new CallbackIService<PropertyModel>() {
                    @Override
                    public void getValue(PropertyModel _PropertyModel) {
                        propertyModel = _PropertyModel;
                        PropertyDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.saving_property_loading).setVisibility(View.GONE);
                                Toast.makeText(PropertyDetailsActivity.this, "Property Add Successfully", Toast.LENGTH_SHORT).show();
                                runIfPropertyIsExiest();
                            }
                        });

                        if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL && StaticContetDrive.RESULT_PROPERTY != null) {
                            StaticContetDrive.RESULT_PROPERTY.put(propertyModel.getId(), propertyModel);
                        }
                    }

                    @Override
                    public void getError(Exception ex, final String message) {
                        PropertyDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.saving_property_loading).setVisibility(View.GONE);
                                Toast.makeText(PropertyDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });

    }

    private void loadProperty() {
        new GlobalApiService<PropertyModel>(this, PropertyModel.class).getList(AppConfigRemote.BASE_URL + "/api/property/" + propertyModel.getId(), new GlobalIService<PropertyModel>() {
            @Override
            public void onGetData(PropertyModel data) {
                if (data != null) {
                    propertyModel = data;
                    thisPageMapAddress = propertyModel.getPropertyAddress();
                    PropertyDetailsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runIfPropertyIsExiest();
                            try {
                                List<Tag> tags = new ArrayList<>();
                                if (propertyModel.getTags() != null && propertyModel.getTags().length > 0) {
                                    ListOfTagModel[] lts = tagView.getTag() != null ? (ListOfTagModel[]) tagView.getTag() : new ListOfTagModel[]{};
                                    for (ListOfTagModel t1 : propertyModel.getTags()) {
                                        tags.add(new Tag(t1.name, t1.getColorCode()));
                                    }
                                    tagView.setTag(propertyModel.getTags());
                                }
                                tagView.removeAllTags();
                                tagView.addTags(tags);
                                ((TextView) findViewById(R.id.current_address)).setText(thisPageMapAddress);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            } finally {
                                loadLists();
                                if (StaticContetDrive.RESULT_PROPERTY != null && StaticContetDrive.RESULT_PROPERTY.get(propertyModel.getId()) != null) {
                                    StaticContetDrive.RESULT_PROPERTY.put(propertyModel.getId(), propertyModel);
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    private void loadLists() {
        final TextView propertyListSelectName = findViewById(R.id.property_list_select_name);
        if (findViewById(R.id.property_list_select).getVisibility() != View.VISIBLE) {
            findViewById(R.id.property_list_select).setVisibility(View.VISIBLE);
            new GlobalApiService<ListListModel>(this, ListListModel.class).getList(AppConfigRemote.ALL_LIST_URL, new GlobalIService<ListListModel>() {
                @Override
                public void onGetData(final ListListModel data) {
                    PropertyDetailsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            propertyListSelectName.setText("");
                            if (data != null && data.results != null) {
                                for (final UserListModel _userListModel : data.results) {
                                    if (_userListModel.getId() == propertyModel.getUserList()) {
                                        propertyListSelectName.setText(_userListModel.getName());
                                        break;
                                    }
                                }
                            }
                        }
                    });

                }

                @Override
                public void onError(String message) {
                }
            });
        }
    }

    private void ownerInfo(final Dialog dialog) {
        final TextView totalCost = dialog.findViewById(R.id.total_cost_lebel);
        final CheckBox checkBoxOI = dialog.findViewById(R.id.checkBox_nd);
        final CheckBox checkBoxPT = dialog.findViewById(R.id.checkBox_pt);

        final double ownerInfoAmount = StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getOwnerInfo();
        final double powerTraceAmount = StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPowerTrace();

        totalCost.setText("$" + ownerInfoAmount);
        totalCost.setTag(ownerInfoAmount);

        ((TextView) dialog.findViewById(R.id.amount_nd)).setText("$" + VariableHelper.double2D(ownerInfoAmount));
        ((TextView) dialog.findViewById(R.id.amount_pt)).setText("$" + VariableHelper.double2D(powerTraceAmount));

        CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                double total3itemCose = 0.00f;
                if (checkBoxOI.isChecked()) {
                    total3itemCose += ownerInfoAmount;
                }
                if (checkBoxPT.isChecked()) {
                    total3itemCose += powerTraceAmount;
                }
                totalCost.setText("$" + VariableHelper.double2D(total3itemCose));
                totalCost.setTag(total3itemCose);
            }
        };

        checkBoxOI.setOnCheckedChangeListener(checkedChangeListener);
        checkBoxPT.setOnCheckedChangeListener(checkedChangeListener);

        dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                $.paymentRequestOrFetch(UriHelper.getOPUrl(propertyModel.getId()), new JsonUtility().putInteger("fetch_owner_info", 1).putInteger("power_trace", checkBoxPT.isChecked()).toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (propertyModel != null) {
            loadProperty();
        }
    }

    @Override
    public void paymentMessage(String message, final boolean isPayment, final boolean isSuccess) {
        new FrontPopup(this).setTitle("Result").setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
                $.setAction(isPayment, isSuccess);
            }
        }).show();
        if (isSuccess) {
            loadProperty();
        }
    }
}
