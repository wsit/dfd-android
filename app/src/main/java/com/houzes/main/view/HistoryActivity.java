package com.houzes.main.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.fragment.history.PreviousDrive;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppBaseActivity {

    private String previous = "";
    private View layoutTeamSort;
    private PreviousDrive previousDrive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_history);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");

        layoutTeamSort = findViewById(R.id.team_filter);
        layoutTeamSort.setVisibility(View.GONE);
        loadMember();

        previous = AppConfigRemote.BASE_URL + "/api/history/user/?limit=10";
        previousDrive = new PreviousDrive();
        loadFragment(previousDrive);
    }

    private void loadFragment(Fragment fragment) {
        findViewById(R.id.frameLayout).setVisibility(View.VISIBLE);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.history, menu);
        menu.findItem(R.id.history).setVisible(false);
        if (StaticContent.CURRENT_USER_MODEL.isTeamAdmin()) {
            menu.findItem(R.id.history).setVisible(true);
        } else {
            menu.findItem(R.id.history).setVisible(false);
        }
        return true;
    }

    boolean isSortClick = false;
    private MenuItem menuItem;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.menuItem = item;
        if (item.getItemId() == R.id.history) {
            if (isSortClick) {
                item.setIcon(R.drawable.toolbar_filter_item);
                layoutTeamSort.setVisibility(View.GONE);
                layoutTeamSort.setAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_left));
                isSortClick = false;
            } else {
                item.setIcon(R.drawable.toolbar_filter_item_selected);
                layoutTeamSort.setVisibility(View.VISIBLE);
                layoutTeamSort.setAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_right));
                isSortClick = true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private List<String> sortList = new ArrayList<>();

    private void loadMember() {
        new GlobalApiService<JsonObject>(this, JsonObject.class).getList(AppConfigRemote.ALL_TEAM_URL, new GlobalIService<JsonObject>() {
            @Override
            public void onGetData(JsonObject data) {
                try {
                    TeamModel[] _teamModels = new Gson().fromJson(data.getAsJsonArray("users"), TeamModel[].class);
                    final TeamModel[] teamModels = TeamModel.removeMe(_teamModels);
                    ((ListView) findViewById(R.id.listview)).setAdapter(new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return teamModels.length;
                        }

                        @Override
                        public Object getItem(int i) {
                            return null;
                        }

                        @Override
                        public long getItemId(int i) {
                            return 0;
                        }

                        @Override
                        public View getView(int i, View view, ViewGroup viewGroup) {
                            final TeamModel teamModel = teamModels[i];
                            View root = getLayoutInflater().inflate(R.layout.rcview_item_team_sort, null);
                            CheckBox checkBox = root.findViewById(R.id.checkBox);
                            checkBox.setText(teamModel.getFirstName() + " " + teamModel.getLastName());
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b) {
                                        try {
                                            sortList.add(teamModel.getId() + "");
                                        } catch (Exception ex) {
                                        }
                                    } else {
                                        try {
                                            sortList.remove(teamModel.getId() + "");
                                        } catch (Exception ex) {
                                        }
                                    }
                                }
                            });
                            return root;
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    CheckBox userCheckBox = findViewById(R.id.checkBox);
                    if (!sortList.contains(String.valueOf(StaticContent.SIGN_IN_USER_ID))) {
                        sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                    }
                    userCheckBox.setChecked(true);
                    userCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                try {
                                    sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                                } catch (Exception ex) {
                                }
                            } else {
                                try {
                                    sortList.remove(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                                } catch (Exception ex) {
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(String message) {
                ActivityHelper.Toast(HistoryActivity.this, message);
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
                previous = AppConfigRemote.BASE_URL + "/api/history/member/?members=" + TextUtils.join(", ", sortList) + "&limit=10&page=1";
                previousDrive = new PreviousDrive();
                loadFragment(previousDrive);
            }
        });
    }

    public String getPrevious() {
        return previous;
    }
}
