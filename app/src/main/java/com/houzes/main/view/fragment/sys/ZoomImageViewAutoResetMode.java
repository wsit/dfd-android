package com.houzes.main.view.fragment.sys;

import androidx.annotation.IntDef;

@IntDef({ZoomImageViewAutoResetMode.NEVER, ZoomImageViewAutoResetMode.UNDER, ZoomImageViewAutoResetMode.OVER, ZoomImageViewAutoResetMode.ALWAYS})
public @interface ZoomImageViewAutoResetMode {

    int UNDER = 0;
    int OVER = 1;
    int ALWAYS = 2;
    int NEVER = 3;

    class Parser {

        @ZoomImageViewAutoResetMode
        public static int fromInt(final int value) {
            switch (value) {
                case OVER:
                    return OVER;
                case ALWAYS:
                    return ALWAYS;
                case NEVER:
                    return NEVER;
                default:
                    return UNDER;
            }
        }
    }

}