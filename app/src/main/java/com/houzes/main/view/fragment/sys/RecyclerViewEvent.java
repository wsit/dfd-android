package com.houzes.main.view.fragment.sys;

import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewEvent {
    public static void recyclerViewEnd(RecyclerView recyclerView, final EndScroll endScroll) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    endScroll.end();
                }
            }
        });
    }

    public interface EndScroll {
        void end();
    }
}
