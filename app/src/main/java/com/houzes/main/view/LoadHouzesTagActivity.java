package com.houzes.main.view;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.houzes.main.R;
import com.houzes.main.action.adapter.SpinnerAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.service.views.iviews.PropertyFragmentIService;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.SpinnerValueModel;
import com.houzes.main.presenter.PropertyPresenter;
import com.houzes.main.view.fragment.property.PropertyListFragment;

import java.util.ArrayList;
import java.util.List;

public class LoadHouzesTagActivity extends AppBaseBackActivity implements PropertyPresenter.IView {
    private TeamModel[] teamModels = new TeamModel[]{};
    private String next;
    private Spinner layoutSpinnerShowMyAllList;
    private PropertyListFragment $propertyListFragment;
    private View layoutSortPropertyByMember;
    private List<String> sortList = new ArrayList<>();
    private PropertyPresenter $;

    private View toolbarRightItem;
    private ImageView toolbarRightItemAssign;
    private ImageView toolbarRightItemFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_houzes_list);
        ((TextView) findViewById(R.id.toolbar_title)).setText("Property By Tag");
        this.$ = new PropertyPresenter(this);
        this.toolbarRightItem = findViewById(R.id.toolbar_right_item);
        this.toolbarRightItemAssign = findViewById(R.id.toolbar_right_item_assign);
        this.toolbarRightItemFilter = findViewById(R.id.toolbar_right_item_filter);
        this.layoutSortPropertyByMember = findViewById(R.id.team_filter);
        this.layoutSortPropertyByMember.setVisibility(View.GONE);
        this.layoutSpinnerShowMyAllList = findViewById(R.id.spinner_list);
        this.layoutSpinnerShowMyAllList.setOnItemSelectedListener(onSpinnerTagSelect);
        this.$.loadAllTeam();
        this.findViewById(R.id.done).setOnClickListener(filterApply);
        this.findViewById(R.id.close).setOnClickListener(filterClose);
        this.$.getTag();
        this.loadPropertyListFragment();

        this.toolbarRightItemAssign.setVisibility(View.GONE);
        if (StaticContent.CURRENT_USER_MODEL.isTeamAdmin()) {
            this.toolbarRightItemFilter.setVisibility(View.VISIBLE);
        } else {
            this.toolbarRightItemFilter.setVisibility(View.GONE);
        }
        this.toolbarRightItemFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutSortPropertyByMember.getTag() != null) {
                    toolbarRightItemFilter.setImageResource(R.drawable.toolbar_filter_item);
                    layoutSortPropertyByMember.setTag(null);
                    layoutSortPropertyByMember.setVisibility(View.GONE);
                    layoutSortPropertyByMember.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_out_left));
                } else {
                    toolbarRightItemFilter.setImageResource(R.drawable.toolbar_filter_item_selected);
                    layoutSortPropertyByMember.setTag(1);
                    layoutSortPropertyByMember.setVisibility(View.VISIBLE);
                    layoutSortPropertyByMember.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_in_right));
                }
            }
        });
    }

    private AdapterView.OnItemSelectedListener onSpinnerTagSelect = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                $propertyListFragment.newCall();
                int val = (layoutSpinnerShowMyAllList.getSelectedView().getTag() != null ? ((UserListModel) layoutSpinnerShowMyAllList.getSelectedView().getTag()).getId() : 0);
                $.getProperty(String.format("%s/api/property/filter/?members=%s&tags=%d&page=1&limit=10", AppConfigRemote.BASE_URL, String.valueOf(StaticContent.CURRENT_USER_MODEL.getId()), val));
                $.getPropertyLatLng(String.format("%s/api/tag/%d/property-cluster/?members=%s", AppConfigRemote.BASE_URL, val, String.valueOf(StaticContent.CURRENT_USER_MODEL.getId())), null);
                $propertyListFragment.emptyText("Select a Tag to \ndiscover number of properties");
            } else {
                loadPropertyListFragment();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private View.OnClickListener filterClose = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            toolbarRightItemFilter.performClick();
        }
    };

    private View.OnClickListener filterApply = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int tagId = (layoutSpinnerShowMyAllList.getSelectedView().getTag() != null ? ((UserListModel) layoutSpinnerShowMyAllList.getSelectedView().getTag()).getId() : 0);
            if (tagId > 0) {
                $propertyListFragment.newCall();
                $.getProperty(String.format("%s/api/property/filter/?members=%s&tags=%d&page=1&limit=10", AppConfigRemote.BASE_URL, TextUtils.join(",", sortList), tagId));
                $.getPropertyLatLng(String.format("%s/api/tag/%d/property-cluster/?members=%s", AppConfigRemote.BASE_URL, tagId, TextUtils.join(",", sortList)), null);
                toolbarRightItemFilter.performClick();
            } else {
                toastMessage("Please choose a tag.");
            }
        }
    };

    private void loadPropertyListFragment() {
        findViewById(R.id.frameLayout).setVisibility(View.VISIBLE);
        $propertyListFragment = new PropertyListFragment();
        $propertyListFragment.getCalBack(new PropertyFragmentIService() {
            @Override
            public void goNext() {
                if (next != null) {
                    $.getPropertyNext(next);
                }
            }

            @Override
            public void fragmentLoad() {
                $propertyListFragment.emptyText("Select a Tag to \ndiscover number of properties");
            }

            @Override
            public void loadedProperty(int count) {

            }
        });
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, $propertyListFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void getPropertyNext(String next) {
        this.next = next;
    }

    @Override
    public void getPropertyLatLng(PropertyLatLngModel[] propertyLatLngModels, String _next) {
        if ($propertyListFragment != null) {
            $propertyListFragment.loadMap(propertyLatLngModels);
        }
    }

    @Override
    public void getPropertyPrevious(String previous) {

    }

    @Override
    public void getPropertyCount(int count) {

    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getProperty(List<PropertyModel> propertyModels) {
        $propertyListFragment.loadData(propertyModels);
    }

    @Override
    public void getList(List<SpinnerValueModel> listModels) {
    }

    @Override
    public void getTag(List<SpinnerValueModel> listModels) {
        layoutSpinnerShowMyAllList.setAdapter(new SpinnerAdapter(this, listModels));
    }

    @Override
    public void getAllTeamMate(TeamModel[] _teamModels) {
        this.teamModels = TeamModel.removeMe(_teamModels);
        try {
            ((ListView) findViewById(R.id.listview)).setAdapter(new BaseAdapter() {
                @Override
                public int getCount() {
                    return teamModels.length;
                }

                @Override
                public Object getItem(int i) {
                    return null;
                }

                @Override
                public long getItemId(int i) {
                    return 0;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    final TeamModel teamModel = teamModels[i];
                    View root = getLayoutInflater().inflate(R.layout.rcview_item_team_sort, null);
                    CheckBox checkBox = root.findViewById(R.id.checkBox);
                    checkBox.setText(teamModel.getFirstName() + " " + teamModel.getLastName());
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                try {
                                    sortList.add(teamModel.getIdAsString());
                                } catch (Exception ex) {
                                }
                            } else {
                                try {
                                    sortList.remove(teamModel.getIdAsString());
                                } catch (Exception ex) {
                                }
                            }
                        }
                    });
                    return root;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            CheckBox userCheckBox = findViewById(R.id.checkBox);
            sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
            userCheckBox.setChecked(true);
            userCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        try {
                            sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                        } catch (Exception ex) {
                        }
                    } else {
                        try {
                            sortList.remove(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                        } catch (Exception ex) {
                        }
                    }
                }
            });
        }
    }

    @Override
    public void loadNewly() {
        $propertyListFragment.newCall();
    }
}
