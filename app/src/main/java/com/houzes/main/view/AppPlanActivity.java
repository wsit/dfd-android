package com.houzes.main.view;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.model.api.SubscriptionPlanModel;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.AddPlanActivityPresenter;

import java.util.List;

public class AppPlanActivity extends AppCompatActivity implements AddPlanActivityPresenter.IView {

    private ViewPager pageViewer;
    private int getOldPageIndex = 0;
    private int geNewtPageIndex = 0;
    private AddPlanActivityPresenter $;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_add_plan);
        pageViewer = findViewById(R.id.pageViewer);
        $ = new AddPlanActivityPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));


        $.getPlanList();
        findViewById(R.id.goBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
            }
        });

        pageViewer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                try {
                    getOldPageIndex = geNewtPageIndex;
                    geNewtPageIndex = position;
                    final View v1 = pageViewer.getChildAt(getOldPageIndex);
                    final View v2 = pageViewer.getChildAt(geNewtPageIndex);
                    v2.setPadding(0, 0, 0, 0);
                    ValueAnimator animator = ValueAnimator.ofInt(30).setDuration(500);
                    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            int xxx = (Integer) valueAnimator.getAnimatedValue();
                            v1.setPadding(0, xxx, 0, 0);
                            if (xxx == 30) {
                                v2.setPadding(0, 0, 0, 0);
                            } else {
                                v2.setPadding(0, 30 - xxx, 0, 0);
                            }
                        }
                    });
                    animator.start();
                } catch (Exception ex) {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void getPlanList(final List<SubscriptionPlanModel> paymentPlanModels) {
        pageViewer.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return paymentPlanModels.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == ((View) object);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                final SubscriptionPlanModel planModel = paymentPlanModels.get(position);
                Double totalPlanCost = 0.0d;
                if (StaticContent.CURRENT_USER_MODEL != null && StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getDiscount() != null) {
                    float t = planModel.getPlanCost() - ((StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getDiscount() / 100) * planModel.getPlanCost());
                    totalPlanCost = Double.valueOf(String.format("%.2f", t));
                } else {
                    totalPlanCost = Double.valueOf(String.valueOf(planModel.getPlanCost()));
                }

                View view = getLayoutInflater().inflate(R.layout.pageviewer_plan_all, null);
                ((ImageView) view.findViewById(R.id.card_image)).setImageResource(planModel.getCardImage());
                ((TextView) view.findViewById(R.id.plan_name)).setText(String.format("%s Plan", planModel.getPlanName()));
                ((TextView) view.findViewById(R.id.subscription_fee)).setText("$" + totalPlanCost + "/month");
                ((TextView) view.findViewById(R.id.user_name)).setText(String.format("xxxx-0000-0000-xxxx\n%s %s", StaticContent.CURRENT_USER_MODEL.getFirstName(), StaticContent.CURRENT_USER_MODEL.getLastName()));
                ((TextView) view.findViewById(R.id.plan_discription)).setText(planModel.getPlanText(AppPlanActivity.this)[1]);
                view.findViewById(R.id.subscribe_now).setBackgroundTintList(ContextCompat.getColorStateList(AppPlanActivity.this, planModel.getColorCode()));

                if (planModel.getPlanType().equals(PlanType.FREE)) {
                    view.findViewById(R.id.subscribe_now).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.FREE)) {
                                startActivity(new Intent(AppPlanActivity.this, HomeActivity.class));
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                ActivityCompat.finishAffinity(AppPlanActivity.this);
                            } else {
                                Toast.makeText(AppPlanActivity.this, getString(R.string.cannot_downgrade), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    view.findViewById(R.id.subscribe_now).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (planModel.getPlanType().equals((StaticContent.CURRENT_USER_MODEL.getPlanType()))) {
                                onBackPressed();
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                startActivity(new Intent(AppPlanActivity.this, AddCardSubscribeActivity.class).putExtra("plan", new Gson().toJson(planModel)));
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                        }
                    });
                }
                if (position == 0) {
                    view.setPadding(0, 0, 0, 0);
                } else {
                    view.setPadding(0, 30, 0, 0);
                }
                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            }
        });
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(AppPlanActivity.this, message + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }
}
