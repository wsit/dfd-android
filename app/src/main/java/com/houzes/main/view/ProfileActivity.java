package com.houzes.main.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.BrowseImageHelper;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.model.api.CurrentUserModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.statics.StaticContetDrive;
import com.houzes.main.presenter.ProfileActivityPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class ProfileActivity extends AppBaseActivity implements ProfileActivityPresenter.IView {
    private ImageView profile_image;
    private ProfileActivityPresenter $;
    private TextView profileNameValue, profileEmailValue, profilePhoneValue, profileCurrentAmount, userCurrentWalletBalance;

    private LinearLayout button_name, button_email, button_phone, button_password;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_profile);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        $ = new ProfileActivityPresenter(this);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");
        new CroperinoConfig("img_" + System.currentTimeMillis() + ".jpg", "/houzes/Pictures", "/sdcard/houzes/Pictures");
        CroperinoFileUtil.verifyStoragePermissions(this);
        CroperinoFileUtil.setupDirectory(this);

        ImageView change_profile_image = findViewById(R.id.change_profile_image);
        profileCurrentAmount = findViewById(R.id.user_current_amount);
        profileNameValue = findViewById(R.id.profile_name_value);
        profileEmailValue = findViewById(R.id.profile_email_value);
        profilePhoneValue = findViewById(R.id.profile_phone_value);
        profile_image = findViewById(R.id.profile_image);
        userCurrentWalletBalance = findViewById(R.id.user_current_wallet_balance);

        button_name = findViewById(R.id.button_name);
        button_email = findViewById(R.id.button_email);
        button_phone = findViewById(R.id.button_phone);
        button_password = findViewById(R.id.button_password);

        button_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameUpdate();
            }
        });


        button_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //userEmailUpdate();
            }
        });

        button_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPhoneUpdate();
            }
        });

        button_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPasswordUpdate();
            }
        });

        change_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Croperino.prepareChooser($.activity, "Choose from camera or gallery", R.color.drive_color);
            }
        });

        findViewById(R.id.button_amount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticContent.CURRENT_USER_MODEL.getInvitedBy() == null) {
                    startActivity(new Intent(ProfileActivity.this, AddCardActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    toastMessage(getString(R.string.contact_admin));
                }
            }
        });

        Glide.with($.activity).asGif().load(R.raw.loading_ring).circleCrop().into(profile_image);
        getUser(StaticContent.CURRENT_USER_MODEL, null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                $.getUser();
            }
        }, 1000);
    }

    private void userNameUpdate() {
        new BottomPopup().startWithEditText(ProfileActivity.this, R.layout.bottom_popup_user_change_name, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                final EditText firstNname = dialog.findViewById(R.id.first_name);
                final EditText lastName = dialog.findViewById(R.id.last_name);
                firstNname.setText(StaticContent.CURRENT_USER_MODEL.getFirstName());
                lastName.setText(StaticContent.CURRENT_USER_MODEL.getLastName());
                dialog.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (firstNname.getText().toString().trim().length() < 2) {
                            firstNname.setError(getString(R.string.find_fname));
                        } else if (lastName.getText().toString().trim().length() < 2) {
                            lastName.setError(getString(R.string.find_lname));
                        } else {
                            progressDialog.show();
                            Map<String, String> map = new HashMap<>();
                            map.put("first_name", firstNname.getText().toString());
                            map.put("last_name", lastName.getText().toString());
                            $.update(map, dialog);
                        }
                    }
                });
            }
        });
    }

    private void userEmailUpdate() {
        new BottomPopup().startWithEditText(ProfileActivity.this, R.layout.bottom_popup_user_chnage_email, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                final EditText emailText = dialog.findViewById(R.id.email);
                emailText.setText(StaticContent.CURRENT_USER_MODEL.getEmail());
                dialog.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (FieldUtil.isEmail(emailText.getText().toString())) {
                            progressDialog.show();
                            Map<String, String> map = new HashMap<>();
                            map.put("email", emailText.getText().toString());
                            $.update(map, dialog);
                        } else {
                            emailText.setError(getString(R.string.find_email));
                        }
                    }
                });
                dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
    }

    private void userPhoneUpdate() {
        new BottomPopup().startWithEditText(ProfileActivity.this, R.layout.bottom_popup_user_change_phone, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                final EditText phoneText = dialog.findViewById(R.id.phone);
                phoneText.setText(StaticContent.CURRENT_USER_MODEL.getPhoneNumber());
                dialog.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String ph = FieldUtil.usaPhoneNumber(phoneText.getText().toString());
                        if (ph != null) {
                            progressDialog.show();
                            Map<String, String> map = new HashMap<>();
                            map.put("phone_number", ph);
                            $.update(map, dialog);
                        } else {
                            phoneText.setError(getString(R.string.find_phone));
                        }
                    }
                });
                dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                phoneText.setSelection(phoneText.getText().length());
            }
        });
    }

    private void userPasswordUpdate() {
        new BottomPopup().startWithEditText(ProfileActivity.this, R.layout.bottom_popup_user_change_password, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                final EditText crPasswordText = dialog.findViewById(R.id.cr_password);
                final EditText newPasswordText = dialog.findViewById(R.id.new_password);

                $.onChangeErrorHide(crPasswordText);
                $.onChangeErrorHide(newPasswordText);
                $.showHidePassword(crPasswordText, (ImageView) dialog.findViewById(R.id.cr_view_password));
                $.showHidePassword(newPasswordText, (ImageView) dialog.findViewById(R.id.new_view_password));

                dialog.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (crPasswordText.getText().toString().trim().length() < 4) {
                            crPasswordText.setError("Please enter your old password");
                        } else if (newPasswordText.getText().toString().trim().length() < 6) {
                            newPasswordText.setError(getString(R.string.find_password));
                        } else {
                            progressDialog.show();
                            Map<String, String> map = new HashMap<>();
                            map.put("old_password", crPasswordText.getText().toString());
                            map.put("password", newPasswordText.getText().toString());
                            $.update(map, dialog);
                        }
                    }
                });
                dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CroperinoConfig.REQUEST_TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, this);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_CROP_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with($.activity).asGif().load(R.raw.loading_ring).circleCrop().into(profile_image);
                        }
                    });
                    $.updatePhoto(new File(CroperinoFileUtil.getTempFile().getAbsolutePath()));
                }
                break;
            default:
                break;

        }

        String path = null;
        if (data != null) {
            if (data.getClipData() != null) {
                ClipData clipData = data.getClipData();
                for (int c = 0; c < clipData.getItemCount(); c++) {
                    path = BrowseImageHelper.getAndroidImagePath(ProfileActivity.this, clipData.getItemAt(c).getUri());
                    break;

                }
            } else if (data.getData() != null) {
                path = BrowseImageHelper.getAndroidImagePath(ProfileActivity.this, data.getData());
            }
        }
        if (path != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Glide.with($.activity).asGif().load(R.raw.loading_ring).circleCrop().into(profile_image);
                }
            });
            $.updatePhoto(new File(path));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            if (StaticContetDrive.IS_PUBLIC_SHARED_ENABEL) {
                new FrontPopup(this).setTitle("Alert").setMessage("Please first stop your driving then logout your account").create().setCancel("Close", null).show();
            } else {
                SharedPreferenceUtil.logOut(this);
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getUser(@NonNull CurrentUserModel currentUserModel, BottomSheetDialog dialog) {
        try {
            if (dialog != null) {
                dialog.cancel();
            }

            if (currentUserModel != null && currentUserModel.getFirstName() != null) {
                StaticContent.CURRENT_USER_MODEL = currentUserModel;
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_ID, currentUserModel.getIdAsString(), ProfileActivity.this);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_EMAIL, currentUserModel.getEmail(), ProfileActivity.this);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.FIRST_NAME, currentUserModel.getFirstName(), ProfileActivity.this);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.LAST_NAME, currentUserModel.getLastName(), ProfileActivity.this);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_IMAGE, currentUserModel.getPhoto(), ProfileActivity.this);
                SharedPreferenceUtil.setValue(SharedPreferenceUtil.USER_PHONE, currentUserModel.getPhoto(), ProfileActivity.this);

                profileNameValue.setText(String.format("%s %s", currentUserModel.getFirstName(), currentUserModel.getLastName()));
                profileEmailValue.setText(currentUserModel.getEmail() + "");
                profilePhoneValue.setText(FieldUtil.formatUsaPhoneNumber(currentUserModel.getPhoneNumber()));
                profileCurrentAmount.setText("$" + currentUserModel.getUpgradeInfo().getAmount());
                userCurrentWalletBalance.setText("$" + currentUserModel.getUpgradeInfo().getAmount());

                ((TextView) findViewById(R.id.plan_type)).setText(StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanName());
                if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanName().toLowerCase().equals("free")) {
                    findViewById(R.id.plan_type).setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.freeType));
                } else if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanName().toLowerCase().equals("solo")) {
                    findViewById(R.id.plan_type).setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.soloType));
                } else if (StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getPlan().getPlanName().toLowerCase().equals("team")) {
                    findViewById(R.id.plan_type).setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.teamType));
                }

                if (currentUserModel.getPhoto() != null) {
                    Glide.with($.activity).asGif().load(R.raw.loading_ring).circleCrop().into(profile_image);
                    Glide.with($.activity)
                            .asBitmap()
                            .load(currentUserModel.getPhoto())
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    Glide.with($.activity).load(resource).circleCrop().into(profile_image);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {
                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    Glide.with($.activity).load(R.drawable.profile_icon).circleCrop().into(profile_image);
                                }
                            });
                } else {
                    Glide.with($.activity).load(R.drawable.profile_icon).circleCrop().into(profile_image);
                }
            }
            loadView();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void getError(@NonNull String message, BottomSheetDialog dialog) {
        if (dialog != null) {
            dialog.cancel();
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastMessage(@NonNull String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        $.getUser();
    }
}
