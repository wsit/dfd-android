package com.houzes.main.view.partial;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerPartial {

    public static void spinnerSelect(Spinner spinner, final SpinnerSelect spinnerSelect) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                TextView tv = (TextView) selectedItemView;
                spinnerSelect.get(position, tv.getText().toString(), tv.getTag());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    public interface SpinnerSelect {
        void get(int position, String text, Object tag);
    }
}
