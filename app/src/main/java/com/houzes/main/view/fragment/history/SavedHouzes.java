package com.houzes.main.view.fragment.history;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.action.adapter.LoadHouzesListAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.list.ListPropertyListModel;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;

import java.util.ArrayList;
import java.util.List;

public class SavedHouzes extends Fragment {
    private Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private LoadHouzesListAdapter historyAdapter;
    private MapView save_houzes_map;
    private GoogleMap googleMap;

    private List<PropertyModel> modelList = new ArrayList<>();
    private String previous;
    private String next;

    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.root = inflater.inflate(R.layout.content_history_saved_houzes, container, false);
        this.previous = new AppConfigRemote().getBASE_URL().concat("/api/history/user/visited-properties/?limit=10&offset=0");
        this.historyAdapter = new LoadHouzesListAdapter(context, modelList);
        this.save_houzes_map = root.findViewById(R.id.save_houzes_map);
        this.swipeRefreshLayout = root.findViewById(R.id.history_swipe_refresh_recycler_view);
        this.recyclerView = root.findViewById(R.id.history_recycler_view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.recyclerView.setAdapter(historyAdapter);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle("MapViewBundleKey");
        }
        this.swipeRefreshLayout.setRefreshing(true);
        this.save_houzes_map.onCreate(mapViewBundle);
        this.save_houzes_map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap _googleMap) {
                googleMap = _googleMap;
                googleMap.setMaxZoomPreference(16);
                googleMap.setMinZoomPreference(16);

            }
        });
        this.startView();
        this.onCLick();
        return root;
    }

    private void startView() {
        loadData(previous);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                historyAdapter = new LoadHouzesListAdapter(context, new ArrayList<PropertyModel>());
                recyclerView.setAdapter(historyAdapter);
                loadData(previous);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (next != null) {
                        loadData(next);
                        googleMap.clear();
                    }
                }
            }
        });
    }

    private void mapViewButton(View view) {
        if (googleMap != null) {
            boolean check = (boolean) view.getTag();
            if (check) {
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                save_houzes_map.setVisibility(View.GONE);
                view.setTag(false);
            } else {
                swipeRefreshLayout.setVisibility(View.GONE);
                save_houzes_map.setVisibility(View.VISIBLE);
                view.setTag(true);
            }
        }
    }

    private void userViewButton(View view) {
        new BottomPopup().start(context, R.layout.bottom_popup_history_user_type, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                dialog.show();
                dialog.findViewById(R.id.optionMy).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        historyAdapter = new LoadHouzesListAdapter(context, new ArrayList<PropertyModel>());
                        recyclerView.setAdapter(historyAdapter);
                        previous = new AppConfigRemote().getBASE_URL() + "/api/history/user/visited-properties/?limit=10&offset=0";
                        loadData(previous);
                        googleMap.clear();
                        googleMap.resetMinMaxZoomPreference();
                        dialog.cancel();
                    }
                });
                dialog.findViewById(R.id.optionTeam).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        historyAdapter = new LoadHouzesListAdapter(context, new ArrayList<PropertyModel>());
                        recyclerView.setAdapter(historyAdapter);
                        previous = new AppConfigRemote().getBASE_URL().concat("/api/history/team/visited-properties/?limit=10&offset=0");
                        loadData(previous);
                        googleMap.clear();
                        googleMap.resetMinMaxZoomPreference();
                        dialog.cancel();
                    }
                });
            }
        });
    }

    private void onCLick() {
        save_houzes_map.setVisibility(View.GONE);
        root.findViewById(R.id.map_view_button).setTag(false);
        root.findViewById(R.id.map_view_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapViewButton(view);
            }
        });
        root.findViewById(R.id.user_view_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userViewButton(view);
            }
        });
    }

    private void loadData(String path) {
        swipeRefreshLayout.setRefreshing(true);
        new GlobalApiService(context, ListPropertyListModel.class).getList(path, new GlobalIService<ListPropertyListModel>() {
            @Override
            public void onGetData(final ListPropertyListModel data) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (data.results != null) {
                            next = data.next;
                            modelList = data.results;
                            int sTaRt = 0;
                            for (PropertyModel propertyModel : modelList) {
                                try {
                                    if (googleMap != null) {
                                        googleMap.addMarker(new MarkerOptions().position(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude())).title(""));
                                        if (sTaRt == 0 && propertyModel.getLatitude() != 0) {
                                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude())));
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                } finally {
                                    sTaRt++;
                                }
                            }
                            historyAdapter.addAll(modelList);
                        } else {

                        }
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            @Override
            public void onError(final String message) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        save_houzes_map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        save_houzes_map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        save_houzes_map.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        save_houzes_map.onLowMemory();
    }
}
