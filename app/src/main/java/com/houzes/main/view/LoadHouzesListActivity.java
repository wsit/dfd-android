package com.houzes.main.view;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.houzes.main.R;
import com.houzes.main.action.adapter.SpinnerAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.action.service.views.iviews.PropertyFragmentIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PropertyLatLngModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.TeamModel;
import com.houzes.main.model.api.UserListModel;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.SpinnerValueModel;
import com.houzes.main.presenter.PropertyPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.property.PropertyListFragment;

import java.util.ArrayList;
import java.util.List;

public class LoadHouzesListActivity extends AppBaseBackActivity implements PropertyPresenter.IView {
    private TeamModel[] teamModels = new TeamModel[]{};
    private String next;
    private Spinner layoutSpinnerShowMyAllList;
    private PropertyListFragment $propertyListFragment;
    private View layoutSortPropertyByMember;
    private List<String> sortList = new ArrayList<>();
    private PropertyPresenter $;

    private View toolbarRightItem;
    private ImageView toolbarRightItemAssign;
    private ImageView toolbarRightItemFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_houzes_list);
        ((TextView) findViewById(R.id.toolbar_title)).setText("Property By List");
        this.$ = new PropertyPresenter(this);
        this.toolbarRightItem = findViewById(R.id.toolbar_right_item);
        this.toolbarRightItemAssign = findViewById(R.id.toolbar_right_item_assign);
        this.toolbarRightItemFilter = findViewById(R.id.toolbar_right_item_filter);
        this.layoutSortPropertyByMember = findViewById(R.id.team_filter);
        this.layoutSortPropertyByMember.setVisibility(View.GONE);
        this.layoutSpinnerShowMyAllList = findViewById(R.id.spinner_list);
        this.layoutSpinnerShowMyAllList.setOnItemSelectedListener(onSpinnerTagSelect);

        this.$.loadAllTeam();
        this.findViewById(R.id.done).setOnClickListener(filterApply);
        this.findViewById(R.id.close).setOnClickListener(filterClose);
        this.$.getList(String.format("%s/api/list/filter/?members=%d", AppConfigRemote.BASE_URL, StaticContent.CURRENT_USER_MODEL.getId()));
        this.loadPropertyListFragment();

        this.toolbarRightItemAssign.setVisibility(View.GONE);
        this.toolbarRightItemFilter.setVisibility(View.GONE);

        if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
            if (StaticContent.CURRENT_USER_MODEL.isTeamAdmin()) {
                this.toolbarRightItemAssign.setVisibility(View.VISIBLE);
                this.toolbarRightItemFilter.setVisibility(View.VISIBLE);
            } else {
                this.toolbarRightItemAssign.setVisibility(View.GONE);
                this.toolbarRightItemFilter.setVisibility(View.GONE);
            }
        } else {
            this.toolbarRightItemAssign.setVisibility(View.VISIBLE);
        }

        this.toolbarRightItemAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StaticContent.CURRENT_USER_MODEL.isTeamAdmin()) {
                    int val = (layoutSpinnerShowMyAllList.getSelectedView().getTag() != null ? ((UserListModel) layoutSpinnerShowMyAllList.getSelectedView().getTag()).getId() : 0);
                    if (val > 0) {
                        openAssignDialog();
                    } else {
                        toastMessage("Please choose a list.");
                    }
                } else {
                    if (!StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
                        new FrontPopup(LoadHouzesListActivity.this).setTitle(getString(R.string.space_unauthorized)).setMessage(getString(R.string.choose_team_plan)).create().setOkay("Okay", new FrontPopup.OkayClick() {
                            @Override
                            public void onClick() {
                                startActivity(new Intent(LoadHouzesListActivity.this, AppPlanActivity.class));
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                        }).show();
                    }
                }
            }
        });
        this.toolbarRightItemFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutSortPropertyByMember.getTag() != null) {
                    toolbarRightItemFilter.setImageResource(R.drawable.toolbar_filter_item);
                    layoutSortPropertyByMember.setTag(null);
                    layoutSortPropertyByMember.setVisibility(View.GONE);
                    layoutSortPropertyByMember.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_out_left));
                } else {
                    toolbarRightItemFilter.setImageResource(R.drawable.toolbar_filter_item_selected);
                    layoutSortPropertyByMember.setTag(1);
                    layoutSortPropertyByMember.setVisibility(View.VISIBLE);
                    layoutSortPropertyByMember.setAnimation(AnimationUtils.loadAnimation($.activity, R.anim.slide_in_right));
                }
            }
        });
    }

    private AdapterView.OnItemSelectedListener onSpinnerTagSelect = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                int val = (layoutSpinnerShowMyAllList.getSelectedView().getTag() != null ? ((UserListModel) layoutSpinnerShowMyAllList.getSelectedView().getTag()).getId() : 0);
                $propertyListFragment.newCall();
                $.getProperty(String.format("%s/api/property/list/%d/", AppConfigRemote.BASE_URL, val));
                $.getPropertyLatLng(String.format("%s/api/list/%d/property-cluster/", AppConfigRemote.BASE_URL, val));
                $propertyListFragment.emptyText("Select a List to \ndiscover number of properties");
            } else {
                loadPropertyListFragment();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private View.OnClickListener filterClose = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            toolbarRightItemFilter.performClick();
        }
    };

    private View.OnClickListener filterApply = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            $.getList(AppConfigRemote.BASE_URL + "/api/list/filter/?members=" + TextUtils.join(",", sortList));
            toolbarRightItemFilter.performClick();
        }
    };

    private void openAssignDialog() {
        new BottomPopup().load(LoadHouzesListActivity.this, R.layout.bottom_popup_assign_member, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                ListView listView = dialog.findViewById(R.id.recycle_assign_teams);
                if (teamModels.length == 0) {
                    dialog.findViewById(R.id.no_member).setVisibility(View.VISIBLE);
                } else {
                    dialog.findViewById(R.id.no_member).setVisibility(View.GONE);
                }

                if (teamModels != null && teamModels.length > 0) {
                    if (teamModels.length > 5) {
                        listView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 450));
                    } else {
                        listView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    }
                    listView.setAdapter(new BaseAdapter() {

                        @Override
                        public int getCount() {
                            return teamModels.length;
                        }

                        @Override
                        public Object getItem(int position) {
                            return null;
                        }

                        @Override
                        public long getItemId(int position) {
                            return 0;
                        }

                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            final View view = getLayoutInflater().inflate(R.layout.rcview_item_member_info, null);
                            TextView teamName = view.findViewById(R.id.team_user_name);
                            teamName.setText(teamModels[position].getFirstName() + " " + teamModels[position].getLastName());
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new FrontPopup(LoadHouzesListActivity.this)
                                            .setTitle("Attention")
                                            .setMessage("Are you sure you want to assign this member?")
                                            .create()
                                            .setCancel("No", null)
                                            .setOkay("Yes", new FrontPopup.OkayClick() {
                                                @Override
                                                public void onClick() {
                                                    int listId = (layoutSpinnerShowMyAllList.getSelectedView().getTag() != null ? ((UserListModel) layoutSpinnerShowMyAllList.getSelectedView().getTag()).getId() : 0);
                                                    JsonObject jsonObject = new JsonObject();
                                                    jsonObject.addProperty("member", teamModels[position].getId());
                                                    new GlobalApiService<ApiResponseModel<String>>(LoadHouzesListActivity.this, ApiResponseModel.class).post((AppConfigRemote.BASE_URL + "/api/list/" + listId + "/push/"), new Gson().toJson(jsonObject), new CallbackIService<ApiResponseModel<String>>() {
                                                        @Override
                                                        public void getValue(final ApiResponseModel<String> apiResponseModel) {
                                                            ActivityHelper.Toast(LoadHouzesListActivity.this, apiResponseModel.message + "");
                                                        }

                                                        @Override
                                                        public void getError(Exception ex, String message) {
                                                            ActivityHelper.Toast(LoadHouzesListActivity.this, message + "");
                                                        }
                                                    });
                                                }
                                            }).show();
                                }
                            });
                            try {
                                ImageView team_user_icon = view.findViewById(R.id.team_user_icon);
                                Glide.with(LoadHouzesListActivity.this).load(teamModels[position].getPhotoTheme()).circleCrop().placeholder(R.drawable.loading_fream_round).error(R.drawable.profile_name).into(team_user_icon);
                            } catch (Exception ex) {
                            }
                            return view;
                        }
                    });
                } else {
                    Toast.makeText(LoadHouzesListActivity.this, "Your haven't any team mate.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void loadPropertyListFragment() {
        findViewById(R.id.frameLayout).setVisibility(View.VISIBLE);
        $propertyListFragment = new PropertyListFragment();
        $propertyListFragment.getCalBack(new PropertyFragmentIService() {
            @Override
            public void goNext() {
                if (next != null) {
                    $.getPropertyNext(next);
                }
            }

            @Override
            public void fragmentLoad() {
                $propertyListFragment.emptyText("Select a List to \ndiscover number of properties");
            }

            @Override
            public void loadedProperty(int count) {

            }
        });
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, $propertyListFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void getPropertyNext(String next) {
        this.next = next;
    }

    @Override
    public void getPropertyLatLng(PropertyLatLngModel[] propertyLatLngModels, String _next) {
        if ($propertyListFragment != null) {
            $propertyListFragment.loadMap(propertyLatLngModels);
        }
    }

    @Override
    public void getPropertyPrevious(String previous) {

    }

    @Override
    public void getPropertyCount(int count) {

    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getProperty(List<PropertyModel> propertyModels) {
        $propertyListFragment.loadData(propertyModels);
    }

    @Override
    public void getList(List<SpinnerValueModel> listModels) {
        layoutSpinnerShowMyAllList.setAdapter(new SpinnerAdapter(this, listModels));
    }

    @Override
    public void getTag(List<SpinnerValueModel> listModels) {

    }

    @Override
    public void getAllTeamMate(TeamModel[] _teamModels) {
        this.teamModels = TeamModel.removeMe(_teamModels);
        try {
            ((ListView) findViewById(R.id.listview)).setAdapter(new BaseAdapter() {
                @Override
                public int getCount() {
                    return teamModels.length;
                }

                @Override
                public Object getItem(int i) {
                    return null;
                }

                @Override
                public long getItemId(int i) {
                    return 0;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    final TeamModel teamModel = teamModels[i];
                    View root = getLayoutInflater().inflate(R.layout.rcview_item_team_sort, null);
                    CheckBox checkBox = root.findViewById(R.id.checkBox);
                    checkBox.setText(teamModel.getFirstName() + " " + teamModel.getLastName());
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                try {
                                    sortList.add(teamModel.getIdAsString());
                                } catch (Exception ex) {
                                }
                            } else {
                                try {
                                    sortList.remove(teamModel.getIdAsString());
                                } catch (Exception ex) {
                                }
                            }
                        }
                    });
                    return root;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            CheckBox userCheckBox = findViewById(R.id.checkBox);
            sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
            userCheckBox.setChecked(true);
            userCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        try {
                            sortList.add(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                        } catch (Exception ex) {
                        }
                    } else {
                        try {
                            sortList.remove(String.valueOf(StaticContent.SIGN_IN_USER_ID));
                        } catch (Exception ex) {
                        }
                    }
                }
            });
        }
    }

    @Override
    public void loadNewly() {
        $propertyListFragment.newCall();
    }
}
