package com.houzes.main.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.LayoutRes;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;


public class BottomPopup {

    public void load(Context context, @LayoutRes int resource, GetDialog dialog) {
        final BottomSheetDialog _dialog = new BottomSheetDialog(context, R.style.SheetDialog);
        Window window = _dialog.getWindow();
        View root = ((Activity) context).getLayoutInflater().inflate(resource, null);
        _dialog.setContentView(root);
        _dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialog.cancel();
            }
        });
        dialog.get(_dialog);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        _dialog.show();
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void start(Context context, @LayoutRes int resource, GetDialog dialog) {
        final BottomSheetDialog _dialog = new BottomSheetDialog(context, R.style.SheetDialog);
        Window window = _dialog.getWindow();
        View root = ((Activity) context).getLayoutInflater().inflate(resource, null);
        _dialog.setContentView(root);
        _dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialog.cancel();
            }
        });
        dialog.get(_dialog);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void startWithEditText(Context context, @LayoutRes int resource, GetDialog getDialog) {
        final BottomSheetDialog _dialog = new BottomSheetDialog(context, R.style.bottomSheetDialogDialogStyle);
        View root = ((Activity) context).getLayoutInflater().inflate(resource, null);
        _dialog.setContentView(root);
        _dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialog.cancel();
            }
        });
        _dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BottomSheetDialog d = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
                        bottomSheet.setBackgroundColor(Color.TRANSPARENT);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }, 0);
            }
        });
        _dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        _dialog.show();
        getDialog.get(_dialog);
    }

    public void fullScreen(Context context, @LayoutRes int resource, GetFullDialog dialog) {
        final Dialog _dialog = new Dialog(context, R.style.SheetDialog);
        Window window = _dialog.getWindow();
        View root = ((Activity) context).getLayoutInflater().inflate(resource, null);
        _dialog.setContentView(root);
        _dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        _dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialog.cancel();
            }
        });
        dialog.get(_dialog);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        _dialog.show();
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void alertYesNo(Context context, String title, String message, int image, final IsClick isClick) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isClick.click(true);
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(image)
                .show();
    }

    public interface GetDialog {
        void get(BottomSheetDialog bottomSheetDialog);
    }

    public interface GetFullDialog {
        void get(Dialog dialog);
    }

    public interface IsClick {
        void click(boolean isClick);
    }
}
