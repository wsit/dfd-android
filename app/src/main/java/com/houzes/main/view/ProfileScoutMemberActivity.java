package com.houzes.main.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.model.api.ScoutModel;
import com.houzes.main.model.enums.IntentExtraName;

public class ProfileScoutMemberActivity extends AppBaseBackActivity {

    private ScoutModel scoutModel;
    private TextView profileNameValue, profileEmailValue, profilePhoneValue, profileLinklValue;
    private ImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_scout_member_profile);

        profileNameValue = findViewById(R.id.profile_name_value);
        profileEmailValue = findViewById(R.id.profile_email_value);
        profileEmailValue = findViewById(R.id.profile_email_value);
        profileLinklValue = findViewById(R.id.profile_email_link);
        profilePhoneValue = findViewById(R.id.profile_phone_value);
        profileImage = findViewById(R.id.profile_image);


        try {
            scoutModel = (ScoutModel) getIntent().getExtras().getSerializable(IntentExtraName.SCOUT_MEMBER.name());
            String name = (scoutModel.getFirst_name() == null ? "" : scoutModel.getFirst_name()) + " " + (scoutModel.getLast_name() == null ? "" : scoutModel.getLast_name());
            profileNameValue.setText(name.trim().length() > 0 ? name : getString(R.string.unknown));
            profileEmailValue.setText(scoutModel.getEmail() != null ? scoutModel.getEmail() : getString(R.string.unknown));
            profilePhoneValue.setText(FieldUtil.formatUsaPhoneNumber(scoutModel.getPhone_number()));
            profileLinklValue.setText((scoutModel.getUrl() + "").startsWith("http") ? (scoutModel.getUrl() + "") : ("https://" + scoutModel.getUrl()));
            profileLinklValue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityHelper.openURI(ProfileScoutMemberActivity.this, profileLinklValue.getText().toString());
                }
            });
            Glide.with(this).load(R.drawable.profile_icon).circleCrop().into(profileImage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
