package com.houzes.main.view.fragment.history;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.houzes.main.view.HistoryActivity;
import com.houzes.main.R;
import com.houzes.main.action.adapter.HistoryAdapter;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;
import com.houzes.main.view.fragment.sys.RecyclerViewEvent;
import com.houzes.main.action.helper.UriHelper;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.HistoryModel;
import com.houzes.main.model.api.list.HistoryListModel;
import com.houzes.main.action.service.api.HistotyApiService;
import com.houzes.main.action.service.api.iapi.HistoryIService;

import java.util.ArrayList;
import java.util.List;

public class PreviousDrive extends Fragment {
    private Context context;
    private View emptyResultView;
    private ImageView loadingTheme;
    private RecyclerView recyclerView;
    private HistoryAdapter historyAdapter;
    private HistotyApiService histotyApiService;

    private String next;
    private String previous;
    private List<HistoryModel> modelList = new ArrayList<>();

    private View root;
    private MyShimmerFrameLayout shimmerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        this.root = inflater.inflate(R.layout.content_history_previous_drive, container, false);
        this.shimmerView = ((Activity) context).findViewById(R.id.shimmer_view);
        this.shimmerView.startNow();
        this.startView();
        return root;
    }

    private void startView() {
        try {
            this.previous = ((HistoryActivity) getActivity()).getPrevious();
            this.loadApiService();
            this.loadingTheme = root.findViewById(R.id.loading_theme);
            this.emptyResultView = getActivity().findViewById(R.id.no_items);
            this.recyclerView = root.findViewById(R.id.history_recycler_view);
            loadingTheme.setVisibility(View.GONE);
            emptyResultView.setVisibility(View.GONE);

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            historyAdapter = new HistoryAdapter(new ArrayList<HistoryModel>(), context);
            recyclerView.setAdapter(historyAdapter);
            histotyApiService.getList(previous);

            RecyclerViewEvent.recyclerViewEnd(recyclerView, new RecyclerViewEvent.EndScroll() {
                @Override
                public void end() {
                    if (next != null) {
                        histotyApiService.getList(next);
                        loadingTheme.setVisibility(View.VISIBLE);
                    }
                }
            });

            ((AnimationDrawable) loadingTheme.getDrawable()).start();
        } catch (Exception ex) {
        }
    }

    private void loadApiService() {
        histotyApiService = new HistotyApiService(context, new HistoryIService<HistoryModel>() {
            @Override
            public void getHistory(ApiResponseModel<HistoryModel> historyModelApiResponseModel) {

            }

            @Override
            public void getHistoryList(final HistoryListModel historyListModel) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        next = UriHelper.next(historyListModel.next);
                        modelList = historyListModel.results;
                        historyAdapter.addAll(modelList);
                        shimmerViewHide();
                    }
                });


            }

            @Override
            public void onHistoryFailed(final String message) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        shimmerViewHide();
                        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public void shimmerViewHide() {

        if (historyAdapter != null && historyAdapter.historyModels != null && historyAdapter.historyModels.size() > 0) {
            emptyResultView.setVisibility(View.GONE);
        } else {
            getActivity().findViewById(R.id.frameLayout).setVisibility(View.GONE);
            emptyResultView.setVisibility(View.VISIBLE);
        }


        if (shimmerView.getVisibility() == View.VISIBLE) {
            shimmerView.stopNow();
        }
        loadingTheme.setVisibility(View.GONE);
    }
}
