package com.houzes.main.view;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.UriHelper;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.PowerTraceModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.PaymentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;

public class PowerTraceActivity extends AppBaseBackActivity implements PaymentPresenter.IView {

    protected LinearLayout layoutFullName;
    protected LinearLayout layoutEmail;
    protected LinearLayout layoutAddress;
    protected LinearLayout layoutTwitter;
    protected LinearLayout layoutLindkin;
    protected LinearLayout layoutGoogle;
    protected LinearLayout layoutFacebook;
    protected LinearLayout layoutPhone;
    protected LinearLayout allPhoneNumber;
    protected View fetchPowerTrace;
    protected View fetchOwner;
    protected View fetchMailWizard;
    private MyShimmerFrameLayout shimmerView;

    private PaymentPresenter $;
    private PropertyModel propertyModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_owner_info);
        $ = new PaymentPresenter(this);
        layoutFullName = findViewById(R.id.layout_fullname);
        layoutEmail = findViewById(R.id.layout_email);
        layoutAddress = findViewById(R.id.layout_address);
        layoutTwitter = findViewById(R.id.layout_twitter);
        layoutLindkin = findViewById(R.id.layout_lindkin);
        layoutGoogle = findViewById(R.id.layout_google);
        layoutFacebook = findViewById(R.id.layout_facebook);
        layoutPhone = findViewById(R.id.layout_phone);
        allPhoneNumber = findViewById(R.id.all_phone_number);
        fetchPowerTrace = findViewById(R.id.fetch_power_trace);
        fetchOwner = findViewById(R.id.fetch_owner_info);
        fetchMailWizard = findViewById(R.id.fetch_mail_wizard);
        shimmerView = findViewById(R.id.shimmer_view);

        ((TextView) findViewById(R.id.toolbar_title)).setText(R.string.title_activity_power_trace);
        layoutFullName.setVisibility(View.GONE);
        layoutAddress.setVisibility(View.GONE);
        layoutPhone.setVisibility(View.GONE);
        shimmerView.startNow();

        propertyModel = new Gson().fromJson(getIntent().getStringExtra(IntentExtraName.PROPERTY_DATA.name()), PropertyModel.class);
        if (propertyModel.getPowerTraceRequestId() > 0) {
            Toast.makeText(this, "Collecting data.", Toast.LENGTH_SHORT).show();
            collectingPT();
        } else {
            loadOwnerInfoModel(new PowerTraceModel());
        }

        fetchPowerTrace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BottomPopup().fullScreen(PowerTraceActivity.this, R.layout.bottom_popup_nmp_indivisual, new BottomPopup.GetFullDialog() {
                    @Override
                    public void get(final Dialog dialog) {
                        dialog.findViewById(R.id.service_pt_message).setVisibility(View.VISIBLE);
                        ((TextView) dialog.findViewById(R.id.service_name)).setText("Re-fetch Power Trace");
                        ((TextView) dialog.findViewById(R.id.serviceTotal_cost)).setText("$" + VariableHelper.double2D(ActivityHelper.getPowerTraceCoin()));
                        ((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D(ActivityHelper.getPowerTraceCoin()));
                        dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                                $.paymentRequestOrFetch(UriHelper.getOPUrl(propertyModel.getId()), new JsonUtility().put("fetch_owner_info", 0).put("power_trace", 1).toString());
                            }
                        });
                    }
                });
            }
        });

        fetchOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BottomPopup().fullScreen(PowerTraceActivity.this, R.layout.bottom_popup_nmp_indivisual, new BottomPopup.GetFullDialog() {
                    @Override
                    public void get(final Dialog dialog) {
                        dialog.findViewById(R.id.service_pt_message).setVisibility(View.GONE);
                        ((TextView) dialog.findViewById(R.id.service_name)).setText("Re-fetch Owner Info");
                        ((TextView) dialog.findViewById(R.id.serviceTotal_cost)).setText("$" + VariableHelper.double2D(ActivityHelper.getOwnerInfoCoin()));
                        ((TextView) dialog.findViewById(R.id.service_amount_mul)).setText("$" + VariableHelper.double2D(ActivityHelper.getOwnerInfoCoin()));
                        dialog.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                                $.paymentRequestOrFetch(UriHelper.getOPUrl(propertyModel.getId()), new JsonUtility().put("fetch_owner_info", 1).put("power_trace", 0).toString());
                            }
                        });
                    }
                });
            }
        });

        fetchMailWizard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PowerTraceActivity.this, MailerWizardTempActivity.class).putExtra(IntentExtraName.URL.name(), UriHelper.getMailWizard(true, propertyModel.getId())));
            }
        });
    }


    private void loadOwnerInfoModel(final PowerTraceModel powerTraceModel) {
        if (powerTraceModel.getEmail() != null) {
            layoutEmail.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.label_email)).setText(TextUtils.join("\n", powerTraceModel.getEmail().toLowerCase().split(",")).replace(" ", ""));
            findViewById(R.id.label_email).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityHelper.sendEmailActivity(
                            PowerTraceActivity.this,
                            StaticContent.CURRENT_USER_MODEL.getFullName(),
                            powerTraceModel.getEmail().toLowerCase().replace(" ", "").trim().split(",")
                    );
                }
            });
        } else {
            layoutEmail.setVisibility(View.GONE);
        }

        if (powerTraceModel.getTwitter() != null) {
            layoutTwitter.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.label_twitter)).setText(powerTraceModel.getTwitter());
            layoutTwitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(PowerTraceActivity.this, powerTraceModel.getTwitter());
                }
            });
        } else {
            layoutTwitter.setVisibility(View.GONE);
        }


        if (powerTraceModel.getFacebook() != null) {
            layoutFacebook.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.label_facebook)).setText(powerTraceModel.getFacebook());
            layoutFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openFacebook(PowerTraceActivity.this, powerTraceModel.getFacebook());
                }
            });
        } else {
            layoutFacebook.setVisibility(View.GONE);
        }

        if (powerTraceModel.getGoogle() != null) {
            layoutGoogle.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.label_google)).setText(powerTraceModel.getGoogle());
            layoutGoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(PowerTraceActivity.this, powerTraceModel.getGoogle());
                }
            });
        } else {
            layoutGoogle.setVisibility(View.GONE);
        }

        if (powerTraceModel.getLinkedIn() != null) {
            layoutLindkin.setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.label_linkedin)).setText(powerTraceModel.getLinkedIn());
            layoutLindkin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UriHelper.openUrl(PowerTraceActivity.this, powerTraceModel.getLinkedIn());
                }
            });
        } else {
            layoutLindkin.setVisibility(View.GONE);
        }

        if (powerTraceModel.getAllPhoneNumber().size() > 0) {
            layoutPhone.setVisibility(View.VISIBLE);
            allPhoneNumber.setVisibility(View.VISIBLE);
            allPhoneNumber.removeAllViews();
            for (final String ph : powerTraceModel.getAllPhoneNumber()) {
                TextView tv = (TextView) getLayoutInflater().inflate(R.layout.rcview_item_phone_numbers, null);
                tv.setText(("" + ph).trim());
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityHelper.sendSmsOrPhone(PowerTraceActivity.this, (ph + ""));
                    }
                });
                allPhoneNumber.addView(tv);
            }
        } else {
            layoutPhone.setVisibility(View.GONE);
        }

        if (findViewById(R.id.shimmer_view_parent).getVisibility() == View.VISIBLE) {
            final ValueAnimator animator = ValueAnimator.ofInt(1, 100);
            animator.setDuration(500);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    int xx = (int) animation.getAnimatedValue();
                    shimmerView.setAlpha(1 - ((float) xx / 100));
                    if (xx > 90) {
                        findViewById(R.id.shimmer_view_parent).setVisibility(View.GONE);
                        shimmerView.stopShimmer();
                    }
                }
            });
            animator.start();
        }
    }

    private void collectingPT() {
        new GlobalApiService<>(this, new TypeToken<ApiResponseModel<PowerTraceModel>>() {
        }.getType()).get(AppConfigRemote.BASE_URL + "/api/property/" + propertyModel.getId() + "/get-existing-power-trace/", new GlobalIService<ApiResponseModel<PowerTraceModel>>() {
            @Override
            public void onGetData(final ApiResponseModel<PowerTraceModel> apiResponseModel) {
                if (apiResponseModel != null && apiResponseModel.data != null) {
                    PowerTraceActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadOwnerInfoModel(apiResponseModel.data);
                        }
                    });
                }
            }

            @Override
            public void onError(String message) {
                ActivityHelper.Toast(PowerTraceActivity.this, message);
            }
        });
    }

    @Override
    public void paymentMessage(String message, final boolean isPayment, final boolean isSuccess) {
        new FrontPopup(this).setTitle("Result").setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
                $.setAction(isPayment, isSuccess);
            }
        }).show();
        if (isPayment) {
            collectingPT();
        }
    }
}
