package com.houzes.main.view;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.adapter.SpinnerAdapter;
import com.houzes.main.action.helper.BrowseImageHelper;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.model.api.MailWizardSubscriptionsModel;
import com.houzes.main.model.api.MailerWizardTempleteModel;
import com.houzes.main.model.api.ReturnAddressModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.SpinnerValueModel;
import com.houzes.main.presenter.MailerWizardTempActivityPresenter;
import com.houzes.main.presenter.PaymentPresenter;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MailerWizardTempActivity extends AppBaseBackActivity implements MailerWizardTempActivityPresenter.IView, PaymentPresenter.IView {
    private GridView gridView;
    private MyShimmerFrameLayout shimmerView;

    private String url;
    private MailerWizardTempActivityPresenter $;
    private PaymentPresenter $$;
    private ReturnAddressModel returnAddressModel;
    private List<SpinnerValueModel> spinnerValueModels = new ArrayList<>();
    private BottomSheetDialog dialogForImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_mailer_wizard_temp);
        shimmerView = findViewById(R.id.shimmer_view);
        gridView = findViewById(R.id.show_all_mwt);
        url = getIntent().getExtras().getString(IntentExtraName.URL.name());
        $$ = new PaymentPresenter(this);
        $ = new MailerWizardTempActivityPresenter(this);
        $.loadMailerWizardTemplate();
        $.loadAllPackage();
        $.loadPreviousReturnAddress();

    }

    private void openMailWizardLayout(final int tempId, final String imageUrl) {
        new BottomPopup().startWithEditText(this, R.layout.bottom_popup_add_mailwizard, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                dialogForImage = dialog;
                final double perMailWizardCost = StaticContent.CURRENT_USER_MODEL.getUpgradeInfo().getMailWizard();
                final View viewFirst = dialog.findViewById(R.id.template_imageview_section);
                final View viewSecond = dialog.findViewById(R.id.template_send_sction);
                final Button cancelOrBack = dialog.findViewById(R.id.cancel_button);
                final Button next = dialog.findViewById(R.id.confirm_button);

                final Spinner subsTypeView = dialog.findViewById(R.id.mw_name);
                final EditText frequencyView = dialog.findViewById(R.id.mw_number);
                final TextView totalCostView = dialog.findViewById(R.id.scout_cost);

                final EditText mwfirstName = dialog.findViewById(R.id.mw_rt_first_name);
                final EditText mwLastName = dialog.findViewById(R.id.mw_rt_last_name);
                final EditText mwEmail = dialog.findViewById(R.id.mw_rt_email);
                final EditText mwPhone = dialog.findViewById(R.id.mw_rt_phone);
                final EditText mwStreet = dialog.findViewById(R.id.mw_rt_address_street);
                final Spinner mwState = dialog.findViewById(R.id.mw_rt_address_state);
                final EditText mwCity = dialog.findViewById(R.id.mw_rt_address_city);
                final EditText mwZipCode = dialog.findViewById(R.id.mw_rt_address_zip);

                final EditText companyName = dialog.findViewById(R.id.mw_rt_company_name);
                final EditText agentWebsite = dialog.findViewById(R.id.mw_rt_website);
                final EditText agentLicense = dialog.findViewById(R.id.mw_rt_agent_address);

                viewFirst.setVisibility(View.VISIBLE);
                viewSecond.setVisibility(View.GONE);
                subsTypeView.setAdapter(new SpinnerAdapter(MailerWizardTempActivity.this, spinnerValueModels));
                Picasso.with(MailerWizardTempActivity.this).load(imageUrl).placeholder(R.drawable.loading_fream_round).error(R.drawable.ic_error).into((ImageView) dialog.findViewById(R.id.pu_image));
                totalCostView.setText("Total Cost: $" + VariableHelper.double2D(8 * perMailWizardCost));
                $.inOrDecreaseMwNumber(frequencyView, totalCostView, perMailWizardCost);
                if (returnAddressModel == null) {
                    returnAddressModel = new Gson().fromJson(SharedPreferenceUtil.getValue(ReturnAddressModel.class.getSimpleName(), MailerWizardTempActivity.this), ReturnAddressModel.class);
                }
                if (returnAddressModel != null && returnAddressModel.getFirstName() != null) {
                    try {
                        mwfirstName.setText(returnAddressModel.getLastName());
                        mwLastName.setText(returnAddressModel.getFirstName());
                        mwEmail.setText(returnAddressModel.getEmail());
                        mwPhone.setText(returnAddressModel.getPhoneNo());
                        mwStreet.setText(returnAddressModel.getAddressStreet());
                        mwCity.setText(returnAddressModel.getAddressCity());
                        mwZipCode.setText(returnAddressModel.getAddressZip());
                        agentWebsite.setText(returnAddressModel.getWebsite());
                        agentLicense.setText(returnAddressModel.getAgentLicense());
                        companyName.setText(returnAddressModel.getCompanyName());
                        String[] aa = getResources().getStringArray(R.array.usa_state);
                        for (int xx = 0; xx < aa.length; xx++) {
                            if (aa[xx].toUpperCase().equals(returnAddressModel.getAddressState())) {
                                mwState.setSelection(xx);
                            }
                        }
                    } catch (Exception ex) {
                    }
                }

                if (returnAddressModel != null && returnAddressModel.getLogo() != null && returnAddressModel.getLogo().trim().length()>3) {
                    Picasso.with($.activity).load(returnAddressModel.getLogo()).into((ImageView) dialogForImage.findViewById(R.id.but_upload_logo));
                }
                if (returnAddressModel != null && returnAddressModel.getCoverPhoto() != null && returnAddressModel.getCoverPhoto().trim().length()>3) {
                    Picasso.with($.activity).load(returnAddressModel.getCoverPhoto()).into((ImageView) dialogForImage.findViewById(R.id.but_cp_logo));
                }

                cancelOrBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cancelOrBack.getTag() == null) {
                            dialog.cancel();
                        } else {
                            next.setTag(null);
                            cancelOrBack.setTag(null);
                            cancelOrBack.setText("Cancel");
                            next.setText("Next");
                            viewFirst.setVisibility(View.VISIBLE);
                            viewSecond.setVisibility(View.GONE);
                            viewFirst.setAnimation(AnimationUtils.loadAnimation(MailerWizardTempActivity.this, R.anim.slide_in_left));
                            viewSecond.setAnimation(AnimationUtils.loadAnimation(MailerWizardTempActivity.this, R.anim.slide_out_right));
                        }
                    }
                });

                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cancelOrBack.setTag(true);
                        cancelOrBack.setText("Back");
                        next.setText("Confirm");
                        if (next.getTag() == null) {
                            next.setTag(true);
                            viewFirst.setVisibility(View.GONE);
                            viewSecond.setVisibility(View.VISIBLE);
                            viewFirst.setAnimation(AnimationUtils.loadAnimation(MailerWizardTempActivity.this, R.anim.slide_out_left));
                            viewSecond.setAnimation(AnimationUtils.loadAnimation(MailerWizardTempActivity.this, R.anim.slide_in_right));
                        } else {
                            boolean isAllValidField = true;
                            if (frequencyView.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                frequencyView.setError(getString(R.string.find_occurrence_number));
                            }
                            if (mwfirstName.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwfirstName.setError(getString(R.string.find_fname));
                            }
                            if (mwLastName.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwLastName.setError(getString(R.string.find_lname));
                            }
                            if (mwEmail.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwEmail.setError(getString(R.string.find_email));
                            }
                            if (mwPhone.getText().toString().trim().length() < 5 || mwPhone.getText().toString().trim().length() > 15) {
                                isAllValidField = false;
                                mwPhone.setError(getString(R.string.find_phone));
                            }
                            if (mwStreet.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwStreet.setError("What's your street number");
                            }
                            if (mwCity.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwCity.setError("What's your city name");
                            }
                            if (mwZipCode.getText().toString().trim().length() == 0) {
                                isAllValidField = false;
                                mwZipCode.setError("What's your zipcode");
                            }

                            if (isAllValidField) {
                                dialog.cancel();
                                $.postMailWizard(
                                        /* url */url,
                                        /* template id */ tempId,
                                        /* subscribe id */ ((int) subsTypeView.getSelectedView().getTag()),
                                        /* frequency num*/ Integer.parseInt(frequencyView.getText().toString()),
                                        /* ur first name*/ mwfirstName.getText().toString(),
                                        /* ur last name */ mwLastName.getText().toString(),
                                        /* ur email adr */ mwEmail.getText().toString(),
                                        /* ur email adr */ mwPhone.getText().toString(),
                                        /* ur street adr*/ mwStreet.getText().toString(),
                                        /* ur city adr  */ mwCity.getText().toString(),
                                        /* ur state adr */ mwState.getSelectedItem().toString(),
                                        /* ur zip code  */ mwZipCode.getText().toString(),
                                        /* ur company name  */ companyName.getText().toString(),
                                        /* ur agent address  */ agentLicense.getText().toString(),
                                        /* ur website  */ agentWebsite.getText().toString(),
                                        /* ur logo image  */ dialog.findViewById(R.id.but_upload_logo).getTag(),
                                        /* ur cover image code  */ dialog.findViewById(R.id.but_cp_logo).getTag()
                                );

                                if (returnAddressModel == null) {
                                    returnAddressModel = new ReturnAddressModel();
                                }
                                returnAddressModel.setFirstName(mwfirstName.getText().toString());
                                returnAddressModel.setLastName(mwLastName.getText().toString());
                                returnAddressModel.setEmail(mwEmail.getText().toString());
                                returnAddressModel.setPhoneNo(mwPhone.getText().toString());
                                returnAddressModel.setAddressStreet(mwStreet.getText().toString());
                                returnAddressModel.setAddressCity(mwCity.getText().toString());
                                returnAddressModel.setAddressZip(mwZipCode.getText().toString());
                                returnAddressModel.setAddressState(mwState.getSelectedItem().toString());
                                SharedPreferenceUtil.setValue(ReturnAddressModel.class.getSimpleName(), new Gson().toJson(returnAddressModel), MailerWizardTempActivity.this);
                            }
                        }
                    }
                });

                dialog.findViewById(R.id.but_upload_logo_main).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent("android.intent.action.PICK");
                        i.setType("image/*");
                        startActivityForResult(i, 1);
                    }
                });
                dialog.findViewById(R.id.but_upload_cp_main).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent("android.intent.action.PICK");
                        i.setType("image/*");
                        startActivityForResult(i, 2);
                    }
                });
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1) {
                String path = BrowseImageHelper.getAndroidImagePath($.activity, data);
                dialogForImage.findViewById(R.id.but_upload_logo).setTag(path);
                Picasso.with($.activity).load(new File(path)).into((ImageView) dialogForImage.findViewById(R.id.but_upload_logo));
            } else if (requestCode == 2) {
                String path = BrowseImageHelper.getAndroidImagePath($.activity, data);
                dialogForImage.findViewById(R.id.but_cp_logo).setTag(path);
                Picasso.with($.activity).load(new File(path)).into((ImageView) dialogForImage.findViewById(R.id.but_cp_logo));
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void loadMailerWizardTemplate(final MailerWizardTempleteModel[] mailerWizardTempleteModels) {
        gridView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mailerWizardTempleteModels.length;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(final int i, View v, ViewGroup viewGroup) {
                View view = getLayoutInflater().inflate(R.layout.rcview_item_mailer_wizard_temp, null);
                try {
                    Drawable d = new ScaleDrawable(getResources().getDrawable(R.drawable.loading_fream_round), Gravity.CENTER, 40, 40).getDrawable();
                    Picasso.with(MailerWizardTempActivity.this).load(mailerWizardTempleteModels[i].getThumbUrl()).placeholder(d).error(R.drawable.ic_error).into((ImageView) view.findViewById(R.id.template_image));
                } catch (Exception e) {
                }
                ((TextView) view.findViewById(R.id.template_name)).setText(MessageUtil.toNaming(mailerWizardTempleteModels[i].getCaption()));
                if (i == 0) {
                    view.findViewById(R.id.primium_icon).setVisibility(View.GONE);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openMailWizardLayout(mailerWizardTempleteModels[i].getId(), mailerWizardTempleteModels[i].getUrl());
                        }
                    });
                } else {

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.FREE)) {
                                new FrontPopup(MailerWizardTempActivity.this).setTitle(getString(R.string.space_unauthorized)).setMessage(getString(R.string.choose_team_plan)).create().setOkay("Okay", new FrontPopup.OkayClick() {
                                    @Override
                                    public void onClick() {
                                        startActivity(new Intent(MailerWizardTempActivity.this, AppPlanActivity.class));
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                }).show();
                            } else {
                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openMailWizardLayout(mailerWizardTempleteModels[i].getId(), mailerWizardTempleteModels[i].getUrl());
                                    }
                                });
                            }
                        }
                    });
                }
                return view;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void loadAllPackage(MailWizardSubscriptionsModel[] _mailWizardSubscriptionsModels) {
        for (MailWizardSubscriptionsModel mailWizardSubscriptionsModel : _mailWizardSubscriptionsModels) {
            spinnerValueModels.add(new SpinnerValueModel(mailWizardSubscriptionsModel.getTypeName(), mailWizardSubscriptionsModel.getId()));
        }
    }

    @Override
    public void shimmerStart() {
        shimmerView.startNow();
    }

    @Override
    public void shimmerStop() {
        shimmerView.stopNow();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getPostMailWizard(String json) {
        //$$.paymentRequestOrFetch(url, json);
    }

    @Override
    public void getPreviousReturnAddress(ReturnAddressModel returnAddressModel) {
        this.returnAddressModel = returnAddressModel;
    }

    @Override
    public void paymentMessage1(String message, final boolean isPayment, final boolean isSuccess) {
        paymentMessage(message, isPayment, isSuccess);
    }

    @Override
    public void paymentMessage(String message, final boolean isPayment, final boolean isSuccess) {
        new FrontPopup(this).setTitle("Result").setMessage(message).create().setCancel("Close", new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
                $$.setAction(isPayment, isSuccess);
            }
        }).show();
    }
}
