package com.houzes.main.view;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.houzes.main.R;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.helper.SharedPreferenceUtil;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.AuthActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements AuthActivityPresenter.IView {
    private static final int RC_GET_AUTH_CODE = 9003;
    private ProgressDialog progressDialog;
    private ViewFlipper viewFlipper;
    private Button fbLoginButton, googleLoginButton, emailSignUpButton;
    private LinearLayout emailSignInButton;
    private AuthActivityPresenter $;
    private CallbackManager facebookCallbackManager;
    private GoogleSignInClient googleCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        $ = new AuthActivityPresenter(this);
        init();
        SharedPreferenceUtil.setToken(this, null);
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                View root = getLayoutInflater().inflate(R.layout.app_popup_agrement, null);
                builder.setView(root);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                root.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        findViewById(R.id.facebook_perfect_click).performClick();
                        alertDialog.cancel();
                    }
                });
                policy(root);
            }
        });

        googleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                View root = getLayoutInflater().inflate(R.layout.app_popup_agrement, null);
                builder.setView(root);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                root.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent signInIntent = googleCallbackManager.getSignInIntent();
                            startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
                            alertDialog.cancel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                policy(root);
            }
        });

        emailSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SignUpActivity.class));
            }
        });
        emailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
            }
        });

        try {
            PolylineModel.deleteAll(PolylineModel.class);
        } catch (Exception ex) {
        } finally {
            if (getRamSize() < 1025) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                View popView = getLayoutInflater().inflate(R.layout.app_popup_requerment, null);
                builder.setView(popView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                popView.findViewById(R.id.cancel_but).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
            }
        }

        try {
            if (Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE) != 3) {
                new FrontPopup(this).setTitle(" Permission").setMessage("Enabling Location mode \"High Accuracy\" for better map performance.").create().setCancel(getString(R.string.okay), new FrontPopup.CancelClick() {
                    @Override
                    public void onClick() {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).show();
            }
        } catch (Exception ex) {
            Log.e(StaticContent.TAG, ex.getMessage() + "");
        }
    }

    private void init() {
        StaticContent.setPermission(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing...");

        viewFlipper = findViewById(R.id.viewFlipper);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);

        fbLoginButton = findViewById(R.id.fb_login_button);
        googleLoginButton = findViewById(R.id.g_sign_in_button);
        emailSignUpButton = findViewById(R.id.email_sign_up_button);
        emailSignInButton = findViewById(R.id.email_sign_in_button);


        facebookCallbackManager = CallbackManager.Factory.create();
        ((com.facebook.login.widget.LoginButton) findViewById(R.id.facebook_perfect_click)).setReadPermissions(Arrays.asList("email", "public_profile"));
        ((com.facebook.login.widget.LoginButton) findViewById(R.id.facebook_perfect_click)).registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String token = AccessToken.getCurrentAccessToken().getToken();
                $.loginBySocial(token, "facebook");
            }

            @Override
            public void onCancel() {
                $.setMessage(getString(R.string.fb_cancel), true);
            }

            @Override
            public void onError(FacebookException exception) {
                $.setMessage(getString(R.string.fb_get_error), true);
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.server_client_id)).requestEmail().build();
        googleCallbackManager = GoogleSignIn.getClient(this, gso);
    }

    private void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return;
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_GET_AUTH_CODE) {
            Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(completedTask);
        } else {
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            final GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                progressDialog.show();
                new AsyncTask<String, Void, String>() {
                    @Override
                    protected String doInBackground(String... params) {
                        try {
                            String token = GoogleAuthUtil.getToken(getApplicationContext(), account.getAccount(), "oauth2:profile email");
                            $.loginBySocial(token, "google-oauth2");
                        } catch (Exception e) {
                            $.setMessage(MessageUtil.getMessage(e), true);
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.hide();
                                }
                            });
                        } finally {
                            googleCallbackManager.signOut();
                        }
                        return null;
                    }
                }.execute();
            }
        } catch (ApiException e) {
            progressDialog.hide();
            if (e.getStatusCode() == 12500) {
                $.setMessage(getString(R.string.error_12500), false);
            } else if (e.getStatusCode() == 12501) {
                $.setMessage(getString(R.string.go_cancel), false);
            } else {
                $.setMessage(getString(R.string.go_get_error), false);
            }
        }
    }

    private void policy(View root) {
        root.findViewById(R.id.policy1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PpAndTcActivity.class).putExtra("title", "Terms & Conditions").putExtra("url", "https://houzes.com/assets/page/privacy-policy-emulate.html#tc"));
            }
        });
        root.findViewById(R.id.policy2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PpAndTcActivity.class).putExtra("title", "Privacy Policy").putExtra("url", "https://houzes.com/assets/page/privacy-policy-emulate.html#pp"));
            }
        });
    }

    @Override
    public void postSuccess(Object message) {
        try {
            disconnectFromFacebook();
        } catch (Exception ex) {
        } finally {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }

    @Override
    public void postFailed(String message) {
        new FrontPopup(this).setTitle(" Sign In").setMessage(message).create().setCancel(getString(R.string.cancel), null).show();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }

    private int getRamSize() {
        ActivityManager actManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        long totalMemory = memInfo.totalMem / (1024 * 1024);
        return (int) totalMemory;
    }
}
