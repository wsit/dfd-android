
package com.houzes.main.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.houzes.main.R;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.view.fragment.team.ScoutFragment;
import com.houzes.main.view.fragment.team.TeamFragment;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.model.enums.PlanType;
import com.houzes.main.model.statics.StaticContent;


public class TeamActivity extends AppBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_team);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");

        final Drawable[] teamBackgrounds = new Drawable[]{getResources().getDrawable(R.drawable.bottom_border_deep_green_deactive), getResources().getDrawable(R.drawable.bottom_border_deep_green)};
        final Drawable[] scoutBackgrounds = new Drawable[]{getResources().getDrawable(R.drawable.bottom_border_deep_green), getResources().getDrawable(R.drawable.bottom_border_deep_green_deactive)};
        final ImageView teamTabitemIcon = findViewById(R.id.team_tabitem_icon);
        final ImageView scoutTabitemIcon = findViewById(R.id.scout_tabitem_icon);
        findViewById(R.id.team_tabitem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
                    loadFragment(new TeamFragment());
                    ((TextView) findViewById(R.id.toolbar_title)).setText("My Team");

                    TransitionDrawable trans1 = new TransitionDrawable(teamBackgrounds);
                    view.setBackground(trans1);
                    trans1.startTransition(1000);

                    TransitionDrawable trans2 = new TransitionDrawable(scoutBackgrounds);
                    findViewById(R.id.scout_tabitem).setBackground(trans2);
                    trans2.startTransition(1000);

                    teamTabitemIcon.setAlpha(.5f);
                    teamTabitemIcon.animate().alpha(1f).setDuration(1000).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            teamTabitemIcon.setBackgroundResource(R.drawable.ts_team_active);
                            findViewById(R.id.team_tabitem).setBackgroundResource(R.drawable.bottom_border_deep_green);
                            findViewById(R.id.scout_tabitem).setBackgroundResource(R.drawable.bottom_border_deep_green_deactive);
                        }
                    });

                    scoutTabitemIcon.animate().alpha(0.5f).setDuration(1000).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            scoutTabitemIcon.setAlpha(1f);
                            scoutTabitemIcon.setBackgroundResource(R.drawable.ts_scout_deactive);
                        }
                    });
                } else {
                    new FrontPopup(TeamActivity.this).setTitle(" Unauthorized").setMessage(getString(R.string.choose_team_plan)).create().setOkay("Okay", new FrontPopup.OkayClick() {
                        @Override
                        public void onClick() {
                            startActivity(new Intent(TeamActivity.this, AppPlanActivity.class));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }).show();
                }

            }
        });
        findViewById(R.id.scout_tabitem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new ScoutFragment());
                ((TextView) findViewById(R.id.toolbar_title)).setText("My Scout");

                TransitionDrawable trans1 = new TransitionDrawable(teamBackgrounds);
                view.setBackground(trans1);
                trans1.startTransition(1000);

                TransitionDrawable trans2 = new TransitionDrawable(scoutBackgrounds);
                findViewById(R.id.team_tabitem).setBackground(trans2);
                trans2.startTransition(1000);

                teamTabitemIcon.animate().alpha(0.5f).setDuration(1000).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        teamTabitemIcon.setAlpha(1f);
                        teamTabitemIcon.setBackgroundResource(R.drawable.ts_team_deactive);
                        findViewById(R.id.scout_tabitem).setBackgroundResource(R.drawable.bottom_border_deep_green);
                        findViewById(R.id.team_tabitem).setBackgroundResource(R.drawable.bottom_border_deep_green_deactive);
                    }
                });

                scoutTabitemIcon.setAlpha(.5f);
                scoutTabitemIcon.animate().alpha(1f).setDuration(1000).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        scoutTabitemIcon.setBackgroundResource(R.drawable.ts_scout_active);
                    }
                });
            }
        });

        if (StaticContent.CURRENT_USER_MODEL.getPlanType().equals(PlanType.TEAM)) {
            findViewById(R.id.team_tabitem).performClick();
        } else {
            findViewById(R.id.scout_tabitem).performClick();
        }

    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }
}
