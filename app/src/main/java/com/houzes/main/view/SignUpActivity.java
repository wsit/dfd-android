package com.houzes.main.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.houzes.main.R;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.presenter.AuthActivityPresenter;
import com.houzes.main.view.fragment.FrontPopup;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppBaseBackActivity implements AuthActivityPresenter.IView {

    EditText sign_up_first_name;
    EditText sign_up_last_name;
    EditText sign_up_email;
    EditText sign_up_password;
    EditText sign_up_phone;
    EditText sign_up_coupon;
    ProgressDialog progressDialog;
    private AuthActivityPresenter $;

    private ImageView view_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        $ = new AuthActivityPresenter(this);
        StaticContent.setPermission(this);
        init();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");

        view_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                $.showHidePassword(sign_up_password, view_password);
            }
        });

        findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(view.getContext(), ForgotPasswordActivity.class);
                view.getContext().startActivity(intent);*/
            }
        });

        findViewById(R.id.create_account_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ($.onFormValidation(sign_up_first_name, sign_up_last_name, sign_up_email, sign_up_phone, sign_up_password)) {
                    $.postReg(requestBody());
                }
            }
        });
    }

    private void init() {
        sign_up_first_name = findViewById(R.id.sign_up_first_name);
        sign_up_last_name = findViewById(R.id.sign_up_last_name);
        sign_up_email = findViewById(R.id.sign_up_email);
        sign_up_phone = findViewById(R.id.sign_up_phone);
        sign_up_password = findViewById(R.id.sign_up_password);
        sign_up_coupon = findViewById(R.id.sign_up_coupon);
        view_password = findViewById(R.id.view_password);

        $.onInputChange(sign_up_first_name, 1);
        $.onInputChange(sign_up_last_name, 1);
        $.onInputChange(sign_up_email, 7);
        $.onInputChange(sign_up_phone, 9);
        $.onInputChange(sign_up_password, 5);
    }


    private RequestBody requestBody() {
        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("first_name", sign_up_first_name.getText().toString())
                .add("last_name", sign_up_last_name.getText().toString())
                .add("email", sign_up_email.getText().toString().trim().toLowerCase())
                .add("phone_number", sign_up_phone.getText().toString())
                .add("password", sign_up_password.getText().toString())
                .add("coupon", sign_up_coupon.getText().toString())
                .add("is_admin", "true")
                .build();
        return formBody;
    }

    @Override
    public void postSuccess(Object message) {
        new FrontPopup(this).setTitle(" Sign Up").setMessage(message + "").create().setCancel(getString(R.string.okay), new FrontPopup.CancelClick() {
            @Override
            public void onClick() {
            }
        }).show();
    }

    @Override
    public void postFailed(String message) {
        new FrontPopup(this).setTitle(" Sign Up").setMessage(message).create().setCancel(getString(R.string.cancel), null).show();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }
}
