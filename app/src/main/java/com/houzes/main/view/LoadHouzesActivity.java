package com.houzes.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;


public class LoadHouzesActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_load_houzes);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");

        findViewById(R.id.by_load_houzes_property_tag).setAnimation(AnimationUtils.loadAnimation(this,R.anim.slide_in_left));
        findViewById(R.id.by_load_houzes_property_list).setAnimation(AnimationUtils.loadAnimation(this,R.anim.slide_in_right));
    }

    public void goTagPage(View view) {
        startActivity(new Intent(this, LoadHouzesTagActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goListPage(View view) {
        startActivity(new Intent(this, LoadHouzesListActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
