package com.houzes.main.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.reflect.TypeToken;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.helper.JsonUtility;
import com.houzes.main.action.helper.MessageUtil;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.iapi.CallbackIService;
import com.houzes.main.model.api.ApiResponseModel;
import com.houzes.main.model.api.CurrentUserModel;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.fragment.FrontPopup;

public class CouponActivity extends AppBaseBackActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_coupon);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);

        findViewById(R.id.couponCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        findViewById(R.id.couponApply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String couponValue = ((EditText) findViewById(R.id.editTextCoupon)).getText().toString().trim();
                if (couponValue.length() > 1) {
                    postCoupon(couponValue);
                } else {
                    Toast.makeText(CouponActivity.this, "Invalid coupon value", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void postCoupon(String coupon) {
        progressDialog.show();
        new GlobalApiService<ApiResponseModel<CurrentUserModel>>(this, new TypeToken<ApiResponseModel<CurrentUserModel>>() {
        }.getType()).post(AppConfigRemote.BASE_URL.concat("/api/upgrade-profile/add-coupon/"), new JsonUtility().put("coupon", coupon).toString(), new CallbackIService<ApiResponseModel<CurrentUserModel>>() {
            @Override
            public void getValue(final ApiResponseModel<CurrentUserModel> apiResponseModel) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        if (apiResponseModel != null) {
                            if (apiResponseModel.status) {
                                StaticContent.CURRENT_USER_MODEL.setUpgradeInfo(apiResponseModel.data.getUpgradeInfo());
                                new FrontPopup(CouponActivity.this)
                                        .setTitle("Coupon")
                                        .setMessage(apiResponseModel.message)
                                        .create()
                                        .setOkay("Okay", new FrontPopup.OkayClick() {
                                            @Override
                                            public void onClick() {
                                                startActivity(new Intent(CouponActivity.this, HomeActivity.class));
                                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                            }
                                        }).show();
                            } else {
                                Toast.makeText(CouponActivity.this, apiResponseModel.message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(CouponActivity.this, MessageUtil.getMessage("null"), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void getError(Exception ex, final String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        Toast.makeText(CouponActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
