package com.houzes.main.view;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.service.views.iviews.HomeListActivityService;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.model.sys.GoogleAddressModel;
import com.houzes.main.model.sys.PlaceModel;
import com.houzes.main.presenter.AddPropertyActivityPresenter;
import com.houzes.main.view.fragment.ListActivity;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.houzes.main.model.statics.StaticContent.CURRENT_LAT;
import static com.houzes.main.model.statics.StaticContent.CURRENT_LON;

public class AddPropertyActivity extends AppBaseActivity implements AddPropertyActivityPresenter.IView {

    private MapView googleMapView;
    private GoogleMap googleMap;
    private PlaceModel selectPlace;
    private FrameLayout view_list_of_list;
    private ListActivity listActivity;
    private View showMarkerItem;

    private AddPropertyActivityPresenter $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_add_property);
        ActivityHelper.setToolbar(this, R.id.toolbar, "");
        $ = new AddPropertyActivityPresenter(this);

        googleMapView = findViewById(R.id.googleMapView);
        showMarkerItem = findViewById(R.id.showMarkerItem);
        showMarkerItem.setVisibility(View.GONE);

        view_list_of_list = findViewById(R.id.view_list_of_list);
        googleMapView.onCreate(null);
        googleMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap _googleMap) {
                googleMap = _googleMap;
                googleMap.setMyLocationEnabled(true);
                googleMap.setBuildingsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
                googleMap.getUiSettings().setTiltGesturesEnabled(true);
                cameraPassion(CURRENT_LAT, CURRENT_LON);
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        showLayout();
                        return true;
                    }
                });

                googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng tempLatLon) {
                        googleMap.clear();
                        selectPlace = new PlaceModel("", "", tempLatLon);
                        try {
                            List<Address> address = new Geocoder(AddPropertyActivity.this, Locale.getDefault()).getFromLocation(tempLatLon.latitude, tempLatLon.longitude, 1);
                            if (address != null && address.size() > 0) {
                                selectPlace.latLng = tempLatLon;
                                selectPlace.address = new GoogleAddressModel(address.get(0)).getPerfectAddress();
                                ((TextView) findViewById(R.id.property_address_lebal)).setText(selectPlace.address);
                                googleMap.addMarker(new MarkerOptions().title(selectPlace.address).position(tempLatLon).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                setDataByAddress(address.get(0));
                                cameraPassion(tempLatLon.latitude, tempLatLon.longitude);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                findViewById(R.id.current_location_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cameraPassion(CURRENT_LAT, CURRENT_LON);
                    }
                });
                ActivityHelper.mapStyle(AddPropertyActivity.this, googleMap);
            }
        });

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_autocomplete_address_key));
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS_COMPONENTS, Place.Field.ADDRESS, Place.Field.LAT_LNG));
        autocompleteFragment.getView().setBackgroundColor(Color.WHITE);
        autocompleteFragment.setCountry("US");
        autocompleteFragment.a.setTextSize(16f);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                try {
                    showLayout();
                    googleMap.clear();
                    selectPlace = new PlaceModel(place.getId(), place.getAddress(), place.getLatLng());
                    LatLng tempLatLon = place.getLatLng();
                    List<Address> address = null;
                    if (tempLatLon != null) {
                        address = new Geocoder(AddPropertyActivity.this, Locale.getDefault()).getFromLocation(tempLatLon.latitude, tempLatLon.longitude, 1);
                    } else {
                        address = new Geocoder(AddPropertyActivity.this).getFromLocationName(place.getAddress(), 1);
                    }

                    if (address != null && address.size() > 0) {
                        ((TextView) findViewById(R.id.property_address_lebal)).setText(place.getAddress());
                        googleMap.addMarker(new MarkerOptions().title(place.getName()).position(tempLatLon).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(tempLatLon));
                        selectPlace.latLng = tempLatLon;
                        selectPlace.address = address.get(0).getSubAdminArea();
                        setDataByAddress(address.get(0));
                    }

                } catch (IOException e) {
                    selectPlace = null;
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Status status) {

            }
        });


        /**
         *
         * Fragment Load
         */
        listActivity = new ListActivity();
        listActivity.reRender(new HomeListActivityService() {
            @Override
            public void hideListActivityToolbar() {
                getSupportActionBar().show();
                view_list_of_list.setVisibility(View.GONE);
            }

            @Override
            public void addSuccess(PropertyModel _PropertyModel) {
                getSupportActionBar().show();
                view_list_of_list.setVisibility(View.GONE);
                Intent intent = new Intent(AddPropertyActivity.this, PropertyDetailsActivity.class);
                intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(_PropertyModel));
                startActivity(intent);
            }

            @Override
            public void addFailed(String message) {
                ActivityHelper.Toast(AddPropertyActivity.this, message);
            }
        });

        findViewById(R.id.add_to_list_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectPlace != null) {
                    getSupportActionBar().hide();
                    view_list_of_list.setVisibility(View.VISIBLE);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
                    fragmentTransaction.replace(R.id.view_list_of_list, listActivity);
                    fragmentTransaction.commit();

                } else {
                    Toast.makeText(AddPropertyActivity.this, "Correct Location Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.openDefaultNav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                $.setDestinationUrl(selectPlace.latLng.latitude, selectPlace.latLng.longitude);
            }
        });

        findViewById(R.id.favourtButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectPlace != null) {
                    getSupportActionBar().hide();
                    view_list_of_list.setVisibility(View.VISIBLE);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
                    fragmentTransaction.replace(R.id.view_list_of_list, listActivity);
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(AddPropertyActivity.this, "Correct Location Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.cancel1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLayout();
            }
        });

    }

    private void setDataByAddress(Address addresses) {
        StaticContent.SELECTED_PROPERTY_DETAILES = new GoogleAddressModel(addresses);
    }

    @Override
    public void onResume() {
        super.onResume();
        googleMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleMapView.onStop();
    }

    @Override
    protected void onPause() {
        googleMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        googleMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        googleMapView.onLowMemory();
    }

    @Override
    public void getAddress(GoogleAddressModel addressModel) {

    }

    @Override
    public void getDestinationUrl(Uri url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, url);
            intent.setPackage("com.google.android.apps.maps");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                try {
                    Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, url);
                    startActivity(unrestrictedIntent);
                } catch (ActivityNotFoundException innerEx) {
                    Toast.makeText(AddPropertyActivity.this, "Please install a maps application", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception ex) {
        } finally {
            hideLayout();
        }
    }

    @Override
    public void cameraPassion(double lat, double lng) {
        try {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
            CameraPosition newCamPos = new CameraPosition(new LatLng(lat, lng), 17f, googleMap.getCameraPosition().tilt, googleMap.getCameraPosition().bearing);
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 3000, null);
        } catch (Exception ex) {
        }
    }

    @Override
    public void hideLayout() {
        showMarkerItem.setVisibility(View.GONE);
        showMarkerItem.setAnimation(AnimationUtils.loadAnimation(AddPropertyActivity.this, R.anim.slide_out_right));
    }

    @Override
    public void showLayout() {
        showMarkerItem.setVisibility(View.VISIBLE);
        showMarkerItem.setAnimation(AnimationUtils.loadAnimation(AddPropertyActivity.this, R.anim.slide_in_left));
    }
}
