package com.houzes.main.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.houzes.main.R;
import com.houzes.main.action.adapter.NotesAdapter;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.service.api.NoteApiService;
import com.houzes.main.action.service.api.iapi.NoteIService;
import com.houzes.main.action.service.views.iviews.NoteEditClickIService;
import com.houzes.main.action.service.views.iviews.SingleDataIService;
import com.houzes.main.model.api.NotesModel;
import com.houzes.main.model.api.list.NotesListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.view.fragment.BottomPopup;
import com.houzes.main.view.fragment.MyShimmerFrameLayout;

public class PropertyNotesActivity extends AppBaseBackActivity implements NoteEditClickIService {

    private String next, previous;
    private Activity activity;
    private boolean isFirst = true;

    private RecyclerView recyclerView;
    private MyShimmerFrameLayout shimmerFrameLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NotesAdapter notesAdapter;
    private NoteApiService noteApiService;
    private ProgressDialog progressDialog;
    private int propertyId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_notes);
        activity = this;
        propertyId = getIntent().getIntExtra(IntentExtraName.PROPERTY_ID.name(), 0) > 0 ? getIntent().getIntExtra(IntentExtraName.PROPERTY_ID.name(), 0) : 0;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        recyclerView = findViewById(R.id.recyclerView);
        shimmerFrameLayout = findViewById(R.id.shimmer_view);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_recycler_view);
        findViewById(R.id.no_data_found).setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load(new AppConfigRemote().getBASE_URL() + "/api/note/property/" + propertyId + "/?limit=10&offset=0", true);
                shimmerFrameLayout.startNow();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (next != null) {
                        load(next, false);
                    }
                }
            }
        });

        findViewById(R.id.add_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomPopup().startWithEditText(PropertyNotesActivity.this, R.layout.bottom_popup_note_add, new BottomPopup.GetDialog() {
                    @Override
                    public void get(final BottomSheetDialog dialog) {
                        final EditText title = dialog.findViewById(R.id.add_title);
                        final EditText info = dialog.findViewById(R.id.add_note);
                        dialog.findViewById(R.id.confirm_notes).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (title.getText().toString().trim().length() > 1 && info.getText().toString().trim().length() > 1) {
                                    progressDialog.show();
                                    noteApiService.addNote(title.getText().toString(), info.getText().toString(), propertyId);
                                    dialog.cancel();
                                } else {
                                    if (title.getText().toString().trim().length() == 0) {
                                        title.setError(getString(R.string.find_node_title));
                                    }
                                    if (info.getText().toString().trim().length() == 0) {
                                        info.setError(getString(R.string.find_node_note));
                                    }
                                }
                            }
                        });
                    }
                });

            }
        });

        noteApiService = new NoteApiService(this, new NoteIService() {
            @Override
            public void onNoteAdd() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        Toast.makeText(activity, "New note added", Toast.LENGTH_SHORT).show();
                        load(new AppConfigRemote().getBASE_URL() + "/api/note/property/" + propertyId + "/?imit=10&offset=0", true);
                    }
                });
            }

            @Override
            public void onNoteList(final NotesListModel notesListModel, final boolean refresh) {
                next = notesListModel.next;
                previous = notesListModel.previous;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (notesListModel.results != null && notesListModel.results.size() > 0) {
                                if (isFirst) {
                                    notesAdapter = new NotesAdapter(notesListModel.results, PropertyNotesActivity.this, noteApiService);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(PropertyNotesActivity.this));
                                    recyclerView.setAdapter(notesAdapter);
                                    swipeRefreshLayout.setRefreshing(false);
                                    isFirst = false;
                                } else {
                                    if (refresh) {
                                        notesAdapter = new NotesAdapter(notesListModel.results, PropertyNotesActivity.this, noteApiService);
                                        recyclerView.setAdapter(notesAdapter);
                                    } else {
                                        notesAdapter.addAll(notesListModel.results);
                                    }
                                }
                            } else {
                                Toast.makeText(activity, getString(R.string.nothing_yet_to_show), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                        } finally {

                            if (notesAdapter != null && notesAdapter.notesModels != null && notesAdapter.notesModels.size() > 0) {
                                findViewById(R.id.no_data_found).setVisibility(View.GONE);
                            } else {
                                findViewById(R.id.no_data_found).setVisibility(View.VISIBLE);
                            }
                        }
                        swipeRefreshLayout.setRefreshing(false);
                        if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                            shimmerFrameLayout.stopNow();
                        }
                    }
                });
            }

            @Override
            public void onNoteUpdate() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "Note update successfully", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onNoteDelete() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "Note delete successfully", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onNoteAddFailed(final String message) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        swipeRefreshLayout.setRefreshing(false);
                        if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                            shimmerFrameLayout.stopNow();
                        }
                        Toast.makeText(activity, "An error occurred while adding a note.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onNoteFailed(final String message) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();
                        swipeRefreshLayout.setRefreshing(false);
                        if (shimmerFrameLayout.getVisibility() == View.VISIBLE) {
                            shimmerFrameLayout.stopNow();
                        }
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        load(new AppConfigRemote().getBASE_URL() + "/api/note/property/" + propertyId + "/?limit=10&offset=0", false);
        swipeRefreshLayout.setEnabled(false);
    }


    public void load(String requestUrl, boolean refresh) {
        noteApiService.getNote(requestUrl, refresh);

    }

    @Override
    public void getValue(final NotesModel notesModel, final TextView _title, final TextView _info) {
        int layoutId = _title != null ? R.layout.bottom_popup_note_add : R.layout.bottom_popup_note_show;
        new BottomPopup().startWithEditText(PropertyNotesActivity.this, layoutId, new BottomPopup.GetDialog() {
            @Override
            public void get(final BottomSheetDialog dialog) {
                if (_title != null) {
                    final EditText title = dialog.findViewById(R.id.add_title);
                    final EditText info = dialog.findViewById(R.id.add_note);
                    final Button confirm_notes = dialog.findViewById(R.id.confirm_notes);
                    title.setText(notesModel.title);
                    info.setText(notesModel.notes);
                    confirm_notes.setText("Update");
                    confirm_notes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (title.getText().toString().trim().length() > 1 && info.getText().toString().trim().length() > 1) {
                                progressDialog.show();
                                noteApiService.updateNote(title.getText().toString(), info.getText().toString(), notesModel.id, new SingleDataIService() {
                                    @Override
                                    public void getValue(Object obj) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressDialog.cancel();
                                                notesModel.title = title.getText().toString();
                                                notesModel.notes = info.getText().toString();
                                                _title.setText(title.getText());
                                                _info.setText(info.getText());
                                                dialog.cancel();
                                            }
                                        });
                                    }
                                });
                            } else {
                                if (title.getText().toString().trim().length() == 0) {
                                    title.setError(getString(R.string.find_node_title));
                                }
                                if (info.getText().toString().trim().length() == 0) {
                                    info.setError(getString(R.string.find_node_note));
                                }
                            }
                        }
                    });
                } else {
                    final TextView title = dialog.findViewById(R.id.add_title);
                    final TextView info = dialog.findViewById(R.id.add_note);
                    title.setText(notesModel.title);
                    info.setText(notesModel.notes);
                    ((TextView) dialog.findViewById(R.id.head_title)).setText("Node Info");
                }
            }
        });
    }
}
