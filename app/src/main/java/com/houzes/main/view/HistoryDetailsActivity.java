package com.houzes.main.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.houzes.main.R;
import com.houzes.main.action.config.AppConfigRemote;
import com.houzes.main.action.gmap.CaptureMapScreen;
import com.houzes.main.action.gmap.MapUtility;
import com.houzes.main.action.helper.ActivityHelper;
import com.houzes.main.action.helper.VariableHelper;
import com.houzes.main.action.service.api.GlobalApiService;
import com.houzes.main.action.service.api.HistotyApiService;
import com.houzes.main.action.service.api.iapi.GlobalIService;
import com.houzes.main.action.service.views.iviews.CallbackObject;
import com.houzes.main.action.sugarentity.PolylineModel;
import com.houzes.main.model.api.HistoryModel;
import com.houzes.main.model.api.PropertyModel;
import com.houzes.main.model.api.list.ListPropertyListModel;
import com.houzes.main.model.enums.IntentExtraName;
import com.houzes.main.model.statics.StaticContent;
import com.houzes.main.view.fragment.BottomPopup;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryDetailsActivity extends AppBaseBackActivity {

    private MapView mapView;
    private GoogleMap googleMap;
    private HistoryModel historyModel;
    private List<PropertyModel> propertyModels;

    private View loadingContent;
    private ImageView loadingContentImageView;

    private LatLngBounds.Builder globalLatLngBounds = new LatLngBounds.Builder();
    private Map<Integer, List<LatLng>> globalPolyLine = new HashMap<>();
    private LatLng startAt = new LatLng(0, 0);
    private LatLng endAt = new LatLng(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_history_details);
        String data = getIntent().getStringExtra("historyModel");

        loadingContent = findViewById(R.id.loading_content);
        loadingContentImageView = findViewById(R.id.loading_content_imageview);
        Glide.with(this).asGif().load(R.raw.loading_ring).into(loadingContentImageView);
        loadingContent.setVisibility(View.VISIBLE);

        if (data != null) {
            historyModel = new Gson().fromJson(data, HistoryModel.class);
            loadPoly();
            mapView = findViewById(R.id.mapView);
            if (savedInstanceState != null) {
                Bundle mapViewBundle = savedInstanceState.getBundle("MapViewBundleKey");
                mapView.onCreate(mapViewBundle);
            } else {
                mapView.onCreate(null);
            }
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap _googleMap) {
                    googleMap = _googleMap;
                    googleMap.setMyLocationEnabled(false);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            try {
                                PropertyModel json = (PropertyModel) marker.getTag();
                                if (json != null) {
                                    Intent intent = new Intent(HistoryDetailsActivity.this, PropertyDetailsActivity.class);
                                    intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(json));
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                }
                            } catch (Exception ex) {
                            }
                            return false;
                        }
                    });
                    if (getIntent().getBooleanExtra(IntentExtraName.TAKE_PHOTO_FROM_MAPVIEW.name(), false) || historyModel.getImage() == null) {
                        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                takeScreenShot();
                            }
                        });
                    } else {
                        loadMap();
                    }
                }
            });

        } else {
            Toast.makeText(this, "Invalid History.", Toast.LENGTH_SHORT).show();
        }

    }

    public void viewDetails(View view) {
        if (historyModel == null) {
            Toast.makeText(this, "History data not found.", Toast.LENGTH_SHORT).show();
        } else {

            if (propertyModels != null) {
                new BottomPopup().start(this, R.layout.layout_history_detailes_info, new BottomPopup.GetDialog() {
                    @Override
                    public void get(BottomSheetDialog bottomSheetDialog) {
                        bottomSheetDialog.show();
                        ((TextView) bottomSheetDialog.findViewById(R.id.hd_date)).setText(historyModel.getEndTimeFormat());
                        ((TextView) bottomSheetDialog.findViewById(R.id.hd_length)).setText(historyModel.getLength2DML() + " Miles");
                        ListView listView = bottomSheetDialog.findViewById(R.id.all_property_list);
                        listView.setAdapter(new BaseAdapter() {
                            @Override
                            public int getCount() {
                                return propertyModels.size();
                            }

                            @Override
                            public Object getItem(int i) {
                                return null;
                            }

                            @Override
                            public long getItemId(int i) {
                                return 0;
                            }

                            @Override
                            public View getView(final int i, View view, ViewGroup viewGroup) {
                                TextView textView = new TextView(HistoryDetailsActivity.this);
                                textView.setPadding(25, 15, 10, 15);
                                textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
                                textView.setTextSize(15);
                                textView.setTextColor(Color.BLACK);
                                textView.setText(propertyModels.get(i).getPropertyAddress());

                                textView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(HistoryDetailsActivity.this, PropertyDetailsActivity.class);
                                        intent.putExtra(IntentExtraName.PROPERTY_DATA.name(), new Gson().toJson(propertyModels.get(i)));
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                });
                                return textView;
                            }
                        });
                    }
                });
            } else {
                Toast.makeText(this, "Loading all property information", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadMap() {
        try {
            googleMap.clear();
            loadingContent.setVisibility(View.GONE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    BitmapDescriptor strtPoint = BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(HistoryDetailsActivity.this, R.drawable.marker_car_icon_my));
                    googleMap.addMarker(new MarkerOptions().position(startAt).icon(strtPoint).rotation(MapUtility.bearingBetweenLocations(startAt, endAt)).anchor(.5f, .5f));
                    //MapAnimator.getInstance().setPrimaryLineColor(R.color.polylineColor);
                    //MapAnimator.getInstance().setSecondaryLineColor(R.color.polylineColorTransparent);
                    for (Map.Entry<Integer, List<LatLng>> m : globalPolyLine.entrySet()) {
                        //MapAnimator.getInstance().animateRoute(googleMap, m.getValue(), StaticContent.POLYLINE_WIDTH);
                        googleMap.addPolyline(new PolylineOptions().addAll(m.getValue()).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));
                    }

                    BitmapDescriptor endPoint = BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(HistoryDetailsActivity.this, R.drawable.marker_drive_end_point));
                    googleMap.addMarker(new MarkerOptions().position(endAt).icon(endPoint));
                }
            }, 2500);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(globalLatLngBounds.build(), (int) (getResources().getDisplayMetrics().widthPixels * .8), (int) (getResources().getDisplayMetrics().heightPixels * .8), (int) (getResources().getDisplayMetrics().widthPixels * 0.10)));
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            loadHistoryProperty();
            ActivityHelper.mapStyle(this, googleMap);
        }

    }

    private void takeScreenShot() {
        float mapHW = .45f;
        if (historyModel.getLength() < 5) {
            mapHW = .35f;
        } else if (historyModel.getLength() < 10) {
            mapHW = .4f;
        } else if (historyModel.getLength() < 50) {
            mapHW = .42f;
        } else if (historyModel.getLength() < 100) {
            mapHW = .43f;
        } else if (historyModel.getLength() < 150) {
            mapHW = .44f;
        } else {
            mapHW = .45f;
        }

        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(startAt).anchor(.5f, .5f).icon(BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(HistoryDetailsActivity.this, R.drawable.marker_start_drive_di))));
        googleMap.addMarker(new MarkerOptions().position(endAt).anchor(.5f, .8f).icon(BitmapDescriptorFactory.fromBitmap(VariableHelper.getBitmapFromDrawable(HistoryDetailsActivity.this, R.drawable.marker_room_red))));

        for (Map.Entry<Integer, List<LatLng>> entry : globalPolyLine.entrySet()) {
            try {
                googleMap.addPolyline(new PolylineOptions().addAll(entry.getValue()).width(StaticContent.POLYLINE_WIDTH).color(StaticContent.POLYLINE_COLOR));
            } catch (Exception ex) {
            }
        }
        try {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(globalLatLngBounds.build(), (int) (getResources().getDisplayMetrics().widthPixels * mapHW), (int) (getResources().getDisplayMetrics().heightPixels * mapHW), (int) (getResources().getDisplayMetrics().widthPixels * 0.10)));
        } catch (Exception ex) {
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CaptureMapScreen.take(googleMap, new CaptureMapScreen.Image() {
                    @Override
                    public void get(Bitmap bitmap, String path, String message) {
                        if (path != null) {
                            HistotyApiService.addImage(HistoryDetailsActivity.this, historyModel.getId(), new File(path), new CallbackObject() {
                                @Override
                                public void get(final Object obj, final Exception ex) {
                                }
                            });
                        }
                        HistoryDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadMap();
                            }
                        });
                    }
                });
            }
        }, 2000);
    }

    private void loadPoly() {
        try {
            globalPolyLine = PolylineModel.getListOfList(new Gson().fromJson(historyModel.getPolylines(), PolylineModel[].class));
            if (globalPolyLine == null) {
                globalPolyLine = new HashMap<>();
            }
        } catch (Exception ex) {
            globalPolyLine = new HashMap<>();
        }

        int endX = 0;
        int startX = 0;
        for (Map.Entry<Integer, List<LatLng>> entry : globalPolyLine.entrySet()) {
            endX = endX + (entry.getValue().size() - 1);
            for (LatLng ll : entry.getValue()) {
                globalLatLngBounds.include(ll);
                if (startX == 0) {
                    startAt = ll;
                } else if (startX == endX) {
                    endAt = ll;
                }
                startX++;
            }
        }
    }

    private void loadHistoryProperty() {
        new GlobalApiService<>(this, ListPropertyListModel.class).getList(new AppConfigRemote().getBASE_URL() + "/api/history/" + historyModel.getId() + "/properties/", new GlobalIService<ListPropertyListModel>() {

            @Override
            public void onGetData(ListPropertyListModel results) {
                propertyModels = results.results;
                HistoryDetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                for (PropertyModel propertyModel : propertyModels) {
                                    try {
                                        googleMap.addMarker(new MarkerOptions().icon(MapUtility.getBitmapDescriptorForPropertyMarker(HistoryDetailsActivity.this, propertyModel.getTags())).title(propertyModel.getPropertyAddress()).position(new LatLng(propertyModel.getLatitude(), propertyModel.getLongitude()))).setTag(propertyModel);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    } finally {
                                    }
                                }
                            }
                        }, 2000);

                    }
                });
            }

            @Override
            public void onError(final String message) {
                ActivityHelper.Toast(HistoryDetailsActivity.this, message);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
