package com.houzes.main.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.houzes.main.R;
import com.houzes.main.view.fragment.FrontPopup;
import com.houzes.main.action.helper.FieldUtil;
import com.houzes.main.presenter.AuthActivityPresenter;

public class ForgotPasswordActivity extends AppBaseBackActivity implements AuthActivityPresenter.IView {

    private EditText fpEmail;
    private ProgressDialog progressDialog;
    private AuthActivityPresenter $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        $ = new AuthActivityPresenter(this);
        fpEmail = findViewById(R.id.fp_email);

        findViewById(R.id.fp_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = fpEmail.getText().toString();
                if (FieldUtil.isEmail(email)) {
                    $.postEmailForResetPassword(email);

                } else {
                    fpEmail.setError(getString(R.string.find_email));
                }
            }
        });
    }

    @Override
    public void postSuccess(Object message) {
        new FrontPopup(this).setTitle("Reset Password").setMessage(message + "").create().setCancel(getString(R.string.okay), null).show();
    }

    @Override
    public void postFailed(String message) {
        new FrontPopup(this).setTitle("Reset Password").setMessage(message).create().setCancel(getString(R.string.cancel), null).show();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.cancel();
    }
}
